$(function() {
	var d;

	// Smooth Scroll at Homepage
	$('.home-bottom-start-content a').click(function(){
		var hash = this.hash;
		$('html, body').animate({
	        scrollTop: $(hash).offset().top
	    }, 600, function(){
	        window.location.hash = hash;
	    });
	});

	// For Destination Selector at Homepage
	$('.selectdestination-go button').click(function(){
		var destination = $('#selectdestination').val();
		$('.schedulehome-selectdestination').css('display','none');
		$('.schedulehome-contentform').css('display','block');
		$('.schedule-home-content-bg').css('background-color','rgba(255, 255, 255, 0.9)');
		$('.schedule-home-content-bg').css('border','solid 1px #2a2a2a');
		$('.schedule-home-content-bg').css('outline','solid 1px #2a2a2a');
		$('.schedule-home-content-bg').css('outline-offset','8px');

		$.ajax({
			headers: {
			    'X-CSRF-TOKEN': $('.token').val()
			},
			type: "POST",
			url: "arrivalreturn_controller",
			data: {country: destination},
			dataType: "json",
			success: function(data){
				var j=0;
				var tesarr = [];
				for (var i = 0; i < data[0].length; i++) {
					if (data[0][i].airport_name.length>1) {
						tesarr[j] = data[0][i].airport_name+" ("+data[0][i].iata_code+")";
						// tesarr[j] = {icao_code: data[0][i].icao_code, iata_code: data[0][i].iata_code, airport_name: data[0][i].airport_name}
						j++;
					}
				};

				$('.homeheader').fadeTo('slow', 0.3, function()
				{
					$(this).css('background-image', 'url("' +data[1]+ '")');
				}).fadeTo('fast', 1);
				
				$('#autocomplete-airport').autocomplete({
				    lookup: tesarr,
				    showNoSuggestionNotice: true,
				    width: '300px'
				});

				$('#autocomplete-airportreturn').autocomplete({
				    lookup: tesarr,
				    showNoSuggestionNotice: true,
				    width: '300px',
				    onHide: function(){
				    	var val2 = $(this).val();
				    	
				    }
				});

				// For home schedule validation
				$('#homepackagescontinuebutton').click(function(){
					$('.schedule-home-arrival input').removeClass('invalid-reg');
					$('.schedule-home-return input').removeClass('invalid-reg');
					var arr_airport = $('.home-airport').val();
					var arr_date = $('.home-date').val();
					var arr_time = $('.home-time').val();
					var ret_airport = $('.return-airport').val();
					var ret_date = $('.return-date').val();
					var ret_time = $('.return-time').val();

					if (arr_airport == '') {
						$('.home-airport').addClass('invalid-reg');
					}
					else{
						$('.home-airport').removeClass('invalid-reg');
					}

					if (arr_date == '') {
						$('.home-date').addClass('invalid-reg');
					}
					else{
						$('.home-date').removeClass('invalid-reg');
					}

					if (arr_time == '') {
						$('.home-time').addClass('invalid-reg');
					}
					else{
						$('.home-time').removeClass('invalid-reg');
					}

					if (ret_airport == '') {
						$('.return-airport').addClass('invalid-reg');
					}
					else{
						$('.return-airport').removeClass('invalid-reg');
					}

					if (ret_date == '') {
						$('.return-date').addClass('invalid-reg');
					}
					else{
						$('.return-date').removeClass('invalid-reg');
					}

					if (ret_time == '') {
						$('.return-time').addClass('invalid-reg');
					}
					else{
						$('.return-time').removeClass('invalid-reg');
					}

					if (!tesarr.includes(arr_airport)) {
						$('.home-airport').addClass('invalid-reg');
					}
					else{
						$('.home-airport').removeClass('invalid-reg');
					}

					if (!tesarr.includes(ret_airport)) {
						$('.return-airport').addClass('invalid-reg');
					}
					else{
						$('.return-airport').removeClass('invalid-reg');
					}

					if ((!tesarr.includes(arr_airport) && arr_airport!='') || (!tesarr.includes(ret_airport) && ret_airport!='')) {
						$('.reg-warning').html("Invalid airport name.<br>");
						$('.reg-warning').css('display','block');
						return false;
					}
					else if (arr_airport!='' && arr_date!='' && arr_time!='' && ret_airport!='' && ret_date!='' && ret_time!= '') {
						document.arrivalreturnform.submit();
					}
					else{
						$('.reg-warning').html("Please fill all the fields.<br>");
						$('.reg-warning').css('display','block');
						return false;
					}
				});

				$('.arrivalreturnform').append('<input type="hidden" name="destination_country" value="'+destination+'">');
			},
			error: function(){
				// alert("Error!");
			}
		});
		return false;
	});

	// DIUBAH vvv From:http://codepen.io/chriscoyier/pen/imdrE
	function modifyOffset() {
		var el, newPoint, newPlace, offset, siblings, k;
		width    = this.offsetWidth;
		newPoint = (this.value - this.getAttribute("min")) / (this.getAttribute("max") - this.getAttribute("min"));
		offset   = -1;
		if (newPoint < 0) { newPlace = 0;  }
		else if (newPoint > 1) { newPlace = width; }
		else { newPlace = width * newPoint + offset; offset -= newPoint;}
		siblings = this.parentNode.childNodes;
		for (var i = 0; i < siblings.length; i++) {
			sibling = siblings[i];
			if (sibling.id == this.id) { k = true; }
			if ((k == true) && (sibling.nodeName == "OUTPUT")) {
				outputTag = sibling;
			}
		}
		outputTag.style.left       = (newPlace-(newPoint * 15)) + "px";
		outputTag.style.marginLeft = offset + "%";
		outputTag.innerHTML        = this.value;
	}

	function modifyInputs() {

		var inputs = document.getElementsByTagName("input");
		var header = $("header").attr('class');
		if (header == 'homeheader') {
			for (var i = 0; i < inputs.length; i++) {
				if (inputs[i].getAttribute("type") == "range") {
					if ($(inputs[i]).attr('id') == 'rangeinput') {
						inputs[i].onchange = modifyOffset;

						// the following taken from http://stackoverflow.com/questions/2856513/trigger-onchange-event-manually
						if ("fireEvent" in inputs[i]) {
							inputs[i].fireEvent("onchange");
						} else {
							var evt = document.createEvent("HTMLEvents");
							evt.initEvent("change", false, true);
							inputs[i].dispatchEvent(evt);
						}

						break;
					}
				}
			}
		}
	}
	modifyInputs();
	// ^^^
	

	// For "Why Itineration?" Section At Homepage
	$('li').mouseenter(function(e) {
		var e_class = $(this).attr('class');
		if ($(this).hasClass('liwhy1')) {
			$('.triangle1, .why-desc1').addClass('active');
			$('.why-desc2, .why-desc3, .why-desc4').removeClass('active');
			$('.triangle2, .triangle3, .triangle4').removeClass('active');
		};
		if ($(this).hasClass('liwhy2')) {
			$('.triangle2, .why-desc2').addClass('active');
			$('.why-desc1, .why-desc3, .why-desc4').removeClass('active');
			$('.triangle1, .triangle3, .triangle4').removeClass('active');
		};
		if ($(this).hasClass('liwhy3')) {
			$('.triangle3, .why-desc3').addClass('active');
			$('.why-desc1, .why-desc2, .why-desc4').removeClass('active');
			$('.triangle1, .triangle2, .triangle4').removeClass('active');
		};
		if ($(this).hasClass('liwhy4')) {
			$('.triangle4, .why-desc4').addClass('active');
			$('.why-desc1, .why-desc2, .why-desc3').removeClass('active');
			$('.triangle1, .triangle2, .triangle3').removeClass('active');
		};
	});


	// $('.input-style-button-simple').click(function() {
	// 	$('.input-style-button-simple').addClass("active");
	// 	$('.input-style-button-detail').removeClass("active");
	// 	$('.checkmark1').addClass("active");
	// 	$('.checkmark2').removeClass("active");
	// });
	// $('.input-style-button-detail').click(function() {
	// 	$('.input-style-button-detail').addClass("active");
	// 	$('.input-style-button-simple').removeClass("active");
	// 	$('.checkmark2').addClass("active");
	// 	$('.checkmark1').removeClass("active");
	// });
	
	// Show red circle on calendar if clicked
	$('.inputstylecalendar').change(function () {
		if ($(this).is(':checked')) {
			$(this).parent('label').parent('td').addClass('selected');
		}
		else{
			$(this).parent('label').parent('td').removeClass('selected');
		}
	});

	$(".nav-tabs").on("click", "a", function (e) {
		e.preventDefault();
		if (!$(this).hasClass('add-new')) {
			$(this).tab('show');
		}
	})
	.on("click", "span", function () {
		var anchor = $(this).siblings('a');
		$(anchor.attr('href')).remove();
		$(this).parent().remove();
		$(".nav-tabs li").children('a').first().click();
	});

	$('.add-new').click(function (e) {
		e.preventDefault();
		var getnavtabsclass = $(this).parent('li').parent('ul').attr('class');
		var getnavtabsclass1 = getnavtabsclass.split(' ');
		var getnavtabsclass2 = getnavtabsclass1[1]; 
	    // var id = $('.nav-tabs').children().length;
	    var id = $('.'+getnavtabsclass2).children().length;
	    
	    var gettabparentid = $(this).parent('li').parent('ul').parent('div').parent('div.tab-pane').attr('id');
	    var gettabparentid1 = gettabparentid.split('-');
	    var gettabparentid2 = gettabparentid1[1];

		if(id===4){

		}
		else{
			d = $(this).parent('li').parent('ul').parent('div').parent('.tab-pane').attr('id');
			var d1=d.split('-');
			var d2=d1[1];
			id=id+((d2*3)-3);
			var tabId = 'tab' + id;
			// $(this).closest('li').before('<li class="navli '+tabId+'"><a href="#tab' + id + '">Tab ' + id + '</a> <span> x </span></li>');
			$(this).closest('li').before('<li class="navli '+tabId+'"><a href="#tab' + id + '"><output id="childtabname'+id+'">Pick a city</output></a><span> x </span></li>');
			$('.tab-content-child'+gettabparentid2).append('<div class="tab-pane" id="' + tabId + '"></div>');
			$('.'+getnavtabsclass2+' li:nth-child(' + id + ') a').click();
			insertContent(id);
		}
		
	});

	$('.featureddestinations li').hover(
		function() {
			$(this).children('a').children('.featured_image').children('.blackmask').fadeOut();
			$(this).children('a').children('.featureddestinations_caption').children('p').css("color","black");
			$(this).children('a').children('.featureddestinations_caption').children('p').css("font-size","38px");	
		}, function() {
			$(this).children('a').children('.featured_image').children('.blackmask').fadeIn();
			$(this).children('a').children('.featureddestinations_caption').children('p').css("color","white");	
			$(this).children('a').children('.featureddestinations_caption').children('p').css("font-size","33px");	
		}
	);
	
	$('.featureditinerators li').hover(
		function() {
			$(this).children('a').children('.itineratorsrating').fadeOut();
			$(this).children('a').children('.featureditinerators_caption').children('p').css("font-size","26px");	
		}, function() {
			$(this).children('a').children('.itineratorsrating').fadeIn();
			$(this).children('a').children('.featureditinerators_caption').children('p').css("font-size","22px");	
		}
	);
	

	$(document).on('change','.citypick',function(){
		var idofpickedcity = $(this).val();
		var dummydate = $(this).siblings('input.pd').val();
		var locdiv = $(this).parent('.pref-city-select').children('.pref-location-select').children('.locselect');
		$.ajax({
			headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			type: "post",
			url: "preferencesdata",
			data: {cityid: idofpickedcity, dummydate: dummydate},
			// data: {cityid: idofpickedcity},
			dataType: "json",
			success: function(data){
				$(locdiv).empty();
				for (var i = 0; i < data.length; i++) {
					$(locdiv).append('<input type="checkbox" class="select-location" name="location[]" value="' + data[i]["loc"]["place_id"] +'">' + data[i]["loc"]["title"]+ '</input>');
					// $(locdiv).append('<br><input type="hidden" name="loc-city[]" value="' + data[i]["loc"]["city_id"] +'">');					
					$(locdiv).append('<img src="' + data[i]["loc"]["image_source"] + '">');
					// $(locdiv).append('<br><input type="hidden" name="dummy-date[]" value="' + data[i]["dummy_date"] +'">');
				};

				$('.select-location').click(function(){
					if ($(this).is(':checked')) {
						$(locdiv).append('<input type="hidden" class="loc-city" name="loc-city[]" value="' + data[0]["loc"]["city_id"] +'">');
						$(locdiv).append('<input type="hidden" class="dummy-date" name="dummy-date[]" value="' + data[0]["dummy_date"] +'">');
					}
					else{
						var notchecked = $('.select-location:not(:checked)').length;
						
						var remove_loccity = $(this).parent('.locselect').children('.loc-city');
						var remove_dummydate = $(this).parent('.locselect').children('.dummy-date');
						for (var i = 0; i < $('.select-location:not(:checked)').length; i++) {
							remove_loccity[i].remove();
							remove_dummydate[i].remove();
						}
					}
				});
				// alert(data);
			},
			error: function(){
				alert("Error!");
			}
		});
		return false;
	});

	// $('.profile-menu li').click(function(){
	// 	$('.profile-content').children('div').removeClass('active-profilecontent');
	// 	if ($(this).hasClass('menu-myprofile')) {
	// 		$('.myprofile').addClass('active-profilecontent');
	// 	}
	// 	if ($(this).hasClass('menu-myitinerary')) {
	// 		$('.userprofile-itinerarylist').addClass('active-profilecontent');
	// 	}
	// });

	// $('.itineratorsitinerary').click(function(){
	// 	var button = $(this).attr('class');
	// 	button = button.split(' ');
	// 	var button1 = button[3];
	// 	button1 = button1.substr(19);
	// 	$('.myprofile-showitinerators.list-'+button1).addClass('show-popup');
	// });

	// $('.close-showitinerators').click(function(){
	// 	var list = $(this).parent('div').parent('div');
	// 	list.removeClass('show-popup');
	// });
	
	// To redirect when clicking "Edit" button in myitinerary page
	$('.edititinerary').click(function(){
		var button = $(this).attr('class');
		button = button.split(' ');
		var itid = button[1];
		itid = itid.substr(2);
		var url = _siteurl+'/preferencesdata/'+itid;
		window.open(url,'_blank');
	});

	// Redirect to latest created itinerary
	$('#lastcreateditinerary').click(function(){
		var itid = $(this).val();
		var url = _siteurl+'/preferencesdata/'+itid;
		window.location.href = url;
	});

	$('.needpaymenttrigger').click(function(){
		$('.bankmethod').css('display','none');
		$('.cashmethod').css('display','none');
		$('.paymentconfirmationform .form-control').val('');
	});

	$('button.paymentmethod').click(function(){
		var button = $(this).val();
		if (button == 'pay') {
			$('.bankmethod').css('display','none');
			$('.cashmethod').slideToggle();
		}
		else{
			$('.cashmethod').css('display','none');
			$('.bankmethod').slideToggle();
		}
	});

});