/* 
 * itinerators.js
 */

jQuery(document).ready(function($) {
    loadItinerators();
    
    $('#form-itinerators').validator();
    
    $('#modal-itinerators').on('show.bs.modal', function() {
        $('#modal-itinerators-title-action').html($('#modal-itinerators-action').val());
    });
    
    $(document.body).on('click', '.itinerators-edit', function() {
        $('#modal-itinerators-action').val('Edit');
        var itineratorsId = $(this).data('id'),
            itinerators = $('#itinerators-table').bootstrapTable('getRowByUniqueId', itineratorsId);
        var photo = itinerators.photo;
        $('.cropme').css('background-image','url("'+photo+'")');
        $('#field-itinerators_id').val(itineratorsId);
        $('#field-name').val(itinerators.name);
        $('#field-kanji').val(itinerators.kanji);
        $('#field-rating').val(itinerators.rating).trigger('change');
        $('#field-mustsee').val(itinerators.skill_mustsee).trigger('change');
        $('#field-cuisine').val(itinerators.skill_cuisine).trigger('change');
        $('#field-history').val(itinerators.skill_history).trigger('change');
        $('#field-entertainment').val(itinerators.skill_entertainment).trigger('change');
        $('#field-adventure').val(itinerators.skill_adventure).trigger('change');
        $('#field-culture').val(itinerators.skill_culture).trigger('change');
        $('#field-nature').val(itinerators.skill_nature).trigger('change');
        $('#field-shopping').val(itinerators.skill_shopping).trigger('change');
        $('#field-sports').val(itinerators.skill_sports).trigger('change');
        
        $('#modal-itinerators').modal('show');
    });
    
    $(document.body).on('click', '.itinerators-delete', function() {
        if (confirm('Do you want to delete this record?')) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: _siteurl+'/uploaditinerators/' + $(this).data('id'),
                type: 'delete',
                data: {},
                dataType: "json",
                success: function(data) {
                    if (data.success) {
                        loadItinerators();
                    } else {
                        alert('Unknown error occured');
                    }
                }
            });
        }
    });
    
    $('.field-range-type').change(function() {
        $('#output_' + $(this).attr('name')).html($(this).val());
    });
    
    $('#btn-add').click(function() {
        $('#modal-itinerators-action').val('Add');
        
        $('#field-itinerators_id').val('');
        $('#field-name').val('');
        $('.field-range-type').val(3).trigger('change');
        
        $('#modal-itinerators').modal('show');
    });
    
    $('#btn-save').click(function() {
        //CHECK BROWSER
        // Opera 8.0+
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';
        // At least Safari 3+: "[object HTMLElementConstructor]"
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;
        // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;
        // Chrome 1+
        var isChrome = !!window.chrome && !!window.chrome.webstore;
        // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!window.CSS;


        var action = $('#modal-itinerators-action').val();

        var cekimg = $(this).parent('div').siblings('div.modal-body').children('div.imgpreview').children().length; //Jika lebih dari 1 brarti ada imgdata
        if (cekimg>1) {
            var tempimgpath = $(this).parent('div').siblings('div').children('.imgpreview').children('.hiddenimgpath').val();
            tempimgpath = tempimgpath.split('\\');

            if (isFirefox){
                var tempimgname = tempimgpath[0];                
            }
            else{
                var tempimgname = tempimgpath[2];
            }
            tempimgname = tempimgname.split('.');
            var tempimgtype = tempimgname[1];
            var imgdata = $('.hiddenimgval').val();
        }
        else{
            var tempimgtype = '';
            var imgdata = '';
        }

        $('#form-itinerator').validator('validate');
        
        if ($('#field-name').val() == "") return;
        
        if (action == "Add") {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: _siteurl+'/uploaditinerators',
                type: 'put',
                data: {
                    'itinerator' : {
                        'name' : $('#field-name').val(),
                        'kanji' : $('#field-kanji').val(),
                        'rating' : $('#field-rating').val(),
                        'skill_mustsee' : $('#field-mustsee').val(),
                        'skill_cuisine' : $('#field-cuisine').val(),
                        'skill_history' : $('#field-history').val(),
                        'skill_entertainment' : $('#field-entertainment').val(),
                        'skill_adventure' : $('#field-adventure').val(),
                        'skill_culture' : $('#field-culture').val(),
                        'skill_nature' : $('#field-nature').val(),
                        'skill_shopping' : $('#field-shopping').val(),
                        'skill_sports' : $('#field-sports').val(),
                        'filetype' : tempimgtype,
                        'imgdata' : imgdata
                    }
                },
                dataType: "json",
                success: function(data) {
                    if (data.success) {
                        $('#modal-itinerators').modal('hide');
                        loadItinerators();
                    } else {
                        alert('Error saving the data');
                    }
                }
            });
        } else if (action == "Edit") {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: _siteurl+'/uploaditinerators',
                type: 'post',
                data: {
                    'itinerator' : {
                        'itinerators_id' : $('#field-itinerators_id').val(),
                        'name' : $('#field-name').val(),
                        'kanji' : $('#field-kanji').val(),
                        'rating' : $('#field-rating').val(),
                        'skill_mustsee' : $('#field-mustsee').val(),
                        'skill_cuisine' : $('#field-cuisine').val(),
                        'skill_history' : $('#field-history').val(),
                        'skill_entertainment' : $('#field-entertainment').val(),
                        'skill_adventure' : $('#field-adventure').val(),
                        'skill_culture' : $('#field-culture').val(),
                        'skill_nature' : $('#field-nature').val(),
                        'skill_shopping' : $('#field-shopping').val(),
                        'skill_sports' : $('#field-sports').val(),
                        'filetype' : tempimgtype,
                        'imgdata' : imgdata
                    }
                },
                dataType: "json",
                success: function(data) {
                    if (data.success) {
                        $('#modal-itinerators').modal('hide');
                        loadItinerators();
                    } else {
                        alert('Error updating the data');
                    }
                }
            });
        }
    });
    
    function loadItinerators(self) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: _siteurl+'/uploaditinerators/all',
            type: 'post',
            data: {},
            dataType: "json",
            success: function(data) {
                var columns = [{
                            field: 'name',
                            title: 'Name',
                            sortable: true,
                            halign: 'center',
                            align: 'left'
                        },{
                            field: 'kanji',
                            title: 'Kanji',
                            sortable: false,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'rating',
                            title: 'Rating',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'photo',
                            title: 'Photo',
                            sortable: false,
                            halign: 'center',
                            align: 'center',
                            formatter: function(value, row) {
                                var tempphotoname = value;
                                var splitphotoname = tempphotoname.split('/');
                                var arrangephotoname = splitphotoname[0]+'/public/'+splitphotoname[1]+'/'+splitphotoname[2]+'/'+splitphotoname[3];
                                return '<img src="' + tempphotoname + '" alt="' + row.name + '" class="photo-thumbnail"/>'
                            }
                        },{
                            field: 'skill_mustsee',
                            title: 'Must See',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_cuisine',
                            title: 'Cuisine',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_history',
                            title: 'History',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_entertainment',
                            title: 'Entertainment',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_adventure',
                            title: 'Adventure',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_culture',
                            title: 'Culture & Art',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_nature',
                            title: 'Nature',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_shopping',
                            title: 'Shopping',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'skill_sports',
                            title: 'Sports',
                            sortable: true,
                            halign: 'center',
                            align: 'right'
                        },{
                            field: 'itinerators_id',
                            title: 'Action',
                            sortable: false,
                            halign: 'center',
                            align: 'center',
                            formatter: function(value, row) {
                                return '<a href="javascript:void(0)" class="itinerators-edit" data-id="' + value + '"><i class="glyphicon glyphicon-edit"></i></a>' +
                                       '<a href="javascript:void(0)" class="itinerators-delete" data-id="' + value + '"><i class="glyphicon glyphicon-remove"></i></a>'
                            }
                        }
                    ];
                $('#itinerators-table').bootstrapTable('destroy');
                $('#itinerators-table').bootstrapTable({
                    pagination: true,
                    smartDisplay: true,
                    striped: true,
                    sortable: true,
                    columns: columns,
                    data: data,
                    uniqueId: 'itinerators_id'
                });
            }
        });
    }
});