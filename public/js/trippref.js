/* 
 * trippref.js
 */

jQuery(document).ready(function($) {
    var _places;
    initialPreferencesTotal(10);
    
    window.onload = function() {
        // Untuk load interest rate
        var numbar = $('.bar').length;
        var bar = $('.bar');
        for (var i = 0; i < numbar; i++) {
            var val = $(bar[i]).val();
            var rate = $(bar[i]).parent('#slider').parent('div').parent('li').parent('ul').siblings('.interestrate');
            prefInterestRateStatus(val, rate);
        };

        // Untuk load apakah preferences city = manual / all
        if (_itineraryDayItem[0] == undefined) {
            var x = 'manual';
        }
        else{
            var x = _itineraryDayItem[0].type; 
        }

        if (x=='all') {
            $('#prefdata-applyall').prop('checked',true);
            $('#prefdata-applyall').trigger('change');
        }

        // Untuk load apakah preferences pada day tertentu 'same as previous'
        var tempitdayid = _itineraryDay[0].itday_id;
        var counter=1;
        for (var i = 0; i < _itineraryDay.length; i++) {
            if (tempitdayid == _itineraryDay[i].itday_id) {
                continue;
            }
            else{
                tempitdayid = _itineraryDay[i].itday_id;
                counter++;
                if(_itineraryDay[i].same_as_previous == 'no'){
                    $('#prefdata-sameasprev'+counter).prop('checked','');
                    $('#prefdata-sameasprev'+counter).trigger('change');
                }
            }
        }

        // Untuk load apakah hotel pada day tertentu 'same as previous'
        var tempitdayid = _itineraryDay[0].itday_id;
        var counter=1;
        for (var i = 0; i < _itineraryDay.length; i++) {
            if (tempitdayid == _itineraryDay[i].itday_id) {
                continue;
            }
            else{
                tempitdayid = _itineraryDay[i].itday_id;
                counter++;
                if(_itineraryDay[i].same_hotel == 'no'){
                    $('#prefdata-samehotel'+counter).prop('checked','');
                    $('#prefdata-samehotel'+counter).trigger('change');
                    
                }
            }
        }

        $('#budget').trigger('change');
    };

    loadSavedDayItem();
    
    $('#modal-city').on('show.bs.modal', function() {
        var dayNo = $(this).find('input[name="day"]').val(),
            dayItemDetail = $('#day-item-detail-' + dayNo),
            cityIdList = Array();
        
        $(this).find('input[name="city"]').prop('checked', false);
        $(this).find('input[name="city"]').prop('disabled', false);
        
        dayItemDetail.find('.panel-day-item').each(function() {
            cityIdList.push($(this).data('id'));
        });
        
        for (var i = 0; i < cityIdList.length; i++) {
            $(this).find('input[name="city"][value="' + cityIdList[i] + '"]').prop('disabled', true);
        }
    });
    
    $('#modal-place').on('show.bs.modal', function() {
        var dayNo = $(this).find('input[name="day"]').val(),
            cityNo = $(this).find('input[name="city"]').val(),
            placeContainer = $('#city-place-' + dayNo + '-' + cityNo),
            placeIdList = Array();
            
        $(this).find('input[name="place"]').prop('checked', false);
        $(this).find('input[name="place"]').prop('disabled', false);
        
        placeContainer.find('.city-place-item').each(function() {
            placeIdList.push($(this).data('id'));
        });
        
        for (var i = 0; i < placeIdList.length; i++) {
            $(this).find('input[name="place"][value="' + placeIdList[i] + '"]').prop('checked', true);
        }
    });
    
    $(document.body).on('click', '.btn-edit-place', function() {
        var dayNo = $(this).data('day'),
            cityNo = $(this).data('city'),
            city_id = $(this).data('id');

        $('.placeinformation').empty();
            
        if (city_id == $('#modal-place').find('input[name="id"]').val()) {
            $('#modal-place').modal('show');
            return;
        }
        
        $('#modal-place').find('input[name="day"]').val(dayNo);
        $('#modal-place').find('input[name="city"]').val(cityNo);
        $('#modal-place').find('input[name="id"]').val(city_id);
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '../city/places/' + city_id,
            contentType: 'application/json; charset=UTF-8',
            type: 'GET',
            data: { },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    var places = _places = data.places,
                        placeContainer = $('#city-place-container'),
                        markup = '';
                    for (var i = 0; i < places.length; i++) {
                        var place = places[i];
                        if (place.del==1) {
                            continue;
                        }
                        markup +=
                            '<div class="col-xs-6 col-md-3">' +
                                '<div class="thumbnail place-option" data-toggle="popover" data-trigger="focus" title="About '+place.title+' : " data-content="'+place.description+'" data-placement="bottom">' +
                                    '<img src="' + place.image_source + '" alt="' + place.title + '">' +
                                    '<div class="caption">' +
                                        '<input id="placeopt-'+(i+1)+'" type="checkbox" name="place" value="' + place.place_id + '">' + place.title +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                    }                    
                    placeContainer.html(markup);
                    $('#modal-place').modal('show');
                }
            }
        });
    });

    $(document.body).on('click', '.thumbnail.place-option', function() {
        var checkbox = $(this).children('div.caption').children('input[type="checkbox"]').attr('id');
        if (document.getElementById(checkbox).checked) {
            // $('[data-toggle="popover"]').popover('hide');
        }
        else{
            // $('[data-toggle="popover"]').popover('show');
        }
    });
    
    $(document.body).on('click', '.place-option', function() {
        var cbPlace = $(this).find('input[name="place"]');
        if (cbPlace.prop('disabled') == false) {
            cbPlace.prop('checked', !cbPlace.prop('checked'));
        }
    });
    
    $('.btn-add-city').click(function() {
        var dayNo = $(this).data('day');

        var dayitemdetail = $('#day-item-detail-'+dayNo);
        var numchild_dayitemdetail = $(dayitemdetail).children().length;
        
        if (numchild_dayitemdetail == 3) {
            $('.reg-warning.warn-prefdatapage').html('');
            $('.reg-warning.warn-prefdatapage').css('display','block');
            $('.reg-warning.warn-prefdatapage').html('You can only pick maximum 3 regions in a day.');

            $('html,body').animate({
                scrollTop: $('.reg-warning.warn-prefdatapage').offset().top},
                'slow');
            return false;
        }

        $('#modal-city').find('input[name="day"]').val(dayNo);
        $('#modal-city').modal('show');
        $('#modal-city').find('.cityactiontype').remove();
        $('#modal-city').append('<input type="hidden" class="cityactiontype" value="addcity">');
    });
    
    $('.city-option').click(function() {
        if ($(this).find('input[name="city"]').prop('disabled') == false) {
            $(this).find('input[name="city"]').prop('checked', true);
        }
    });
    
    $('#btn-city-select').click(function() {
        var dayNo = $('#modal-city').find('input[name="day"]').val(),
            city_id = $('#modal-city').find('input[name="city"]:checked').val(),
            cityactiontype = $('#modal-city').find('.cityactiontype').val();
            
        if (city_id != undefined) {
            addCity(dayNo, city_id, '', cityactiontype);
        }

        $('#modal-city').modal('hide');
    });
    
    $('#btn-place-select').click(function() {
        var dayNo = $('#modal-place').find('input[name="day"]').val(),
            cityNo = $('#modal-place').find('input[name="city"]').val(),
            selectedPlace = $('#modal-place').find('input[name="place"]:checked'),
            places = Array();
        
        for (var i = 0; i < selectedPlace.length; i++) {
            var place_id = selectedPlace[i].value;
            places.push($.grep(_places, function(e){ return e.place_id == place_id; })[0]);
        }
        updatePlace(dayNo, cityNo, places);
        $('#modal-place').modal('hide');
    });
    
    $('#btn-next').click(function() {
        if (document.getElementById('prefdata-applyall').checked) {
            var type="all";
        }
        else{
            var type="manual";
        }

        var form = $('#form-preferencesdata'),
            dayCount = $('#tab-day-item').children().length,
            itemDetails = Array(),
            isSame = Array(),
            isSameHotel = Array();

        // Check if each day empty or not
        // var countcheck = 0;
        // for (var i = 0; i < dayCount; i++) {
        //     if (document.getElementById('prefdata-applyall').checked) {
        //         var check = $('#day-item-detail-0').children().length;
        //         if (check == 0) {
        //             countcheck=1;
        //         }
        //         break;
        //     }
        //     else{
        //         var check = $('#day-item-detail-' + (i + 1)).children().length;
        //         if (check == 0) {
        //             countcheck++;
        //         }
        //     }
        // }

        // if (countcheck > 0) {
        //     $('.reg-warning.warn-prefdatapage').html('');
        //     $('.reg-warning.warn-prefdatapage').css('display','block');
        //     if (type=="manual") {
        //         $('.reg-warning.warn-prefdatapage').html('You have to pick at least one city on each days.');
        //     }
        //     else{
        //         $('.reg-warning.warn-prefdatapage').html('You have to pick at least one city.');
        //     }
            
        //     $('html,body').animate({
        //         scrollTop: $('.reg-warning.warn-prefdatapage').offset().top},
        //         'slow');
        //     return false;
        // }
        
        if (document.getElementById('prefdata-applyall').checked) {
            if ($('#day-item-detail-0').children().length == 0) {
                $('.reg-warning.warn-prefdatapage').html('');
                $('.reg-warning.warn-prefdatapage').css('display','block');
                $('.reg-warning.warn-prefdatapage').html('You have to pick at least one city.');
                
                $('html,body').animate({
                    scrollTop: $('.reg-warning.warn-prefdatapage').offset().top},
                    'slow');
                return false;
            }
        }
        else{
            if ($('#day-item-detail-1').children().length == 0) {
                $('.reg-warning.warn-prefdatapage').html('');
                $('.reg-warning.warn-prefdatapage').css('display','block');
                $('.reg-warning.warn-prefdatapage').html('You have to pick at least one city.');
                
                $('html,body').animate({
                    scrollTop: $('.reg-warning.warn-prefdatapage').offset().top},
                    'slow');
                return false;
            }
        }
        
        
        if (type == 'manual') {
            for (var i = 0; i < dayCount; i++) {
                var itemContainer = $('#day-item-detail-' + (i + 1)),
                    cityNo = 1,
                    pos = 1,
                    issameasprevious = $('#issameasprevious'+(i+1)).val(),
                    issamehotelasprevious = $('#issamehotel'+(i+1)).val(),
                    hotelname = $('#prefhotel'+(i+1)).children('.hotelinput1').children('.hotel-name').val(),
                    hoteladdress = $('#prefhotel'+(i+1)).children('.hotelinput2').children('.hotel-address').val(),
                    hotelphone = $('#prefhotel'+(i+1)).children('.hotelinput3').children('.hotel-phone').val(),
                    hotel = hotelname+'#|#'+hoteladdress+'#|#'+hotelphone,
                    itemDetail = {
                        'it_day_id' : itemContainer.data('it_day_id'),
                        'date' : itemContainer.data('date'),
                        'hotel' : hotel,
                        'cities' : Array()
                    };

                isSame.push(issameasprevious);
                isSameHotel.push(issamehotelasprevious);
                
                itemContainer.find('.panel-day-item').each(function() {
                    // if ($('#city-detail-' + (i + 1) + '-' + cityNo).length==0) {
                    //         cityNo=cityNo+1;
                    //     }
                    cityNo = $(this).data('no');
                    var city = {
                            'city_id' : $(this).data('id'),
                            'places' : Array(),
                            'note' : $(this).find('.pref-comment').val()
                        },
                        placeContainer = $('#city-detail-' + (i + 1) + '-' + cityNo);
                    
                    placeContainer.find('.city-place-item').each(function() {
                        city.places.push({
                            'place_id' : $(this).data('id')
                        });
                    });
                    
                    itemDetail.cities.push(city);
                    // cityNo++;
                });
                
                itemDetails.push(itemDetail);
            }
        }
        else{
            var itemContainer = $('#day-item-detail-0'),
            hotel = $('#prefhotel0').children('textarea').val(),
            cityNo = 1,
            pos = 1,
            itemDetail = {
                'it_day_id' : itemContainer.data('it_day_id'),
                'date' : itemContainer.data('date'),
                'hotel' : hotel,
                'cities' : Array()
            };

            itemContainer.find('.panel-day-item').each(function() {
                cityNo = $(this).data('no');
                var city = {
                    'city_id' : $(this).data('id'),
                    'places' : Array(),
                    'note' : $(this).find('.pref-comment').val()
                },
                placeContainer = $('#city-detail-0-' + cityNo);

                placeContainer.find('.city-place-item').each(function() {
                    city.places.push({
                        'place_id' : $(this).data('id')
                    });
                });

                itemDetail.cities.push(city);
            });

            itemDetails.push(itemDetail);
        }


        if (document.getElementById('pp-family').checked) {
            family = "1";
        }
        else{
            family = "0";
        }

        if (document.getElementById('pp-backpack').checked) {
            backpack = "1";
        }
        else{
            backpack = "0";
        }

        if (document.getElementById('pp-vegetarian').checked) {
            vegetarian = "1";
        }
        else{
            vegetarian = "0";
        }

        if (document.getElementById('pp-romantic').checked) {
            romantic = "1";
        }
        else{
            romantic = "0";
        }

        if (document.getElementById('pp-culinaire').checked) {
            culinaire = "1";
        }
        else{
            culinaire = "0";
        }

        if (document.getElementById('pp-cheapeat').checked) {
            cheapeat = "1";
        }
        else{
            cheapeat = "0";
        }

        if (document.getElementById('pp-barclub').checked) {
            barclub = "1";
        }
        else{
            barclub = "0";
        }

        if (document.getElementById('pp-nightseeing').checked) {
            nightseeing = "1";
        }
        else{
            nightseeing = "0";
        }
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '../preferencesdata',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            type: 'POST',
            data: { 
                'type' : type,
                'budget' : form.find('input[name="budget"]').val(),
                'pace' : form.find('input[name="pace"]').val(),
                'mustsee' : form.find('input[name="mustsee"]').val(),
                'cuisine' : form.find('input[name="cuisine"]').val(),
                'history' : form.find('input[name="history"]').val(),
                'entertainment' : form.find('input[name="entertainment"]').val(),
                'adventure' : form.find('input[name="adventure"]').val(),
                'culture' : form.find('input[name="culture"]').val(),
                'nature' : form.find('input[name="nature"]').val(),
                'shopping' : form.find('input[name="shopping"]').val(),
                'sports' : form.find('input[name="sports"]').val(),
                'family' : family,
                'backpack' : backpack,
                'vegetarian' : vegetarian,
                'romantic' : romantic,
                'culinaire' : culinaire,
                'cheapeat' : cheapeat,
                'barclub' : barclub,
                'nightseeing' : nightseeing,
                'item_details': JSON.stringify(itemDetails),
                'is_same': JSON.stringify(isSame),
                'is_same_hotel': JSON.stringify(isSameHotel)
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    window.location.replace('../pickitinerators/' + _itineraryId);
                } else {
                    
                }
            }
        });
    });

    $(document.body).on('click', '.edit-prefcity', function() {
        var dayNo = $(this).parent('div.prefcityactions').parent('div.row').parent('div.panel-heading').parent('div.panel-day-item').parent('div.panel-group').data('day');
        var citytobeedited = $(this).parent('div.prefcityactions').parent('div.row').parent('div.panel-heading').parent('div.panel-day-item').data('no');
        var citytobeedited_pos = $(this).parent('div.prefcityactions').parent('div.row').parent('div.panel-heading').parent('div.panel-day-item').data('pos');
        $('#modal-city').find('input[name="day"]').val(dayNo);
        $('#modal-city').modal('show');
        $('#modal-city').find('.cityactiontype').remove();
        $('#modal-city').append('<input type="hidden" class="cityactiontype" value="editcity" data-dayno="'+dayNo+'" data-no="'+citytobeedited+'" data-pos="'+citytobeedited_pos+'">');
        return false;
    });
    
    $(document.body).on('click', '.del-prefcity', function() {
        $(this).parent('div.prefcityactions').parent('div.row').parent('div.panel-heading').parent('div.panel-day-item').fadeOut(300, function() { $(this).remove(); });
        // $(this).parent('div').parent('div.panel-day-item').remove();
        return false;
    });

    $(document.body).on('click', '.del-prefplace', function() {
        $(this).parent('div.city-place-item').parent('div.prefcityimgbox').fadeOut(300, function() { $(this).remove(); });
        return false;
    });

    $(document.body).on('change', '.bar', function() {
        var value = $(this).val();
        var rate = $(this).parent('#slider').parent('div').parent('li').parent('ul').siblings('.interestrate');
        prefInterestRateStatus(value,rate);
    });

    function prefInterestRateStatus (prefrateval, spanrate) {
        var msg = '';
        if(prefrateval == 1){
            msg = 'Less Interested';
        }
        else if(prefrateval == 2){
            msg = 'Interested';
        }
        else{
            msg = 'Very Interested';
        }
        $(spanrate).html(msg);
    }

    $('.bar').change(function(){
        var max = 10;
        // var total = initialPreferencesTotal(max);
        var getslider = $(this).attr("id");
        var mustsee = parseInt($('#mustsee').val());
        var entertainment = parseInt($('#entertainment').val());
        var nature = parseInt($('#nature').val());
        var cuisine = parseInt($('#cuisine').val());
        var shopping = parseInt($('#shopping').val());
        var history = parseInt($('#history').val());
        var culture = parseInt($('#culture').val());
        var sports = parseInt($('#sports').val());
        var total = max-(mustsee+entertainment+nature+cuisine+shopping+history+culture+sports-8);
        if (total < 0) {
            if (getslider == "mustsee") {
                altval = max-entertainment-nature-cuisine-shopping-history-culture-sports+8;
                $("#mustsee").val(altval);
                $("#mustseevalue").val(altval);
            }
            if (getslider == "entertainment") {
                altval = max-mustsee-nature-cuisine-shopping-history-culture-sports+8;
                $("#entertainment").val(altval);
                $("#entertainmentvalue").val(altval);
            }
            if (getslider == "nature") {
                altval = max-mustsee-entertainment-cuisine-shopping-history-culture-sports+8;
                $("#nature").val(altval);
                $("#naturevalue").val(altval);
            }
            if (getslider == "cuisine") {
                altval = max-mustsee-entertainment-nature-shopping-history-culture-sports+8;
                $("#cuisine").val(altval);
                $("#cuisinevalue").val(altval);
            }
            if (getslider == "shopping") {
                altval = max-mustsee-entertainment-nature-cuisine-history-culture-sports+8;
                $("#shopping").val(altval);
                $("#shoppingvalue").val(altval);
            }
            if (getslider == "history") {
                altval = max-mustsee-entertainment-nature-cuisine-shopping-culture-sports+8;
                $("#history").val(altval);
                $("#historyvalue").val(altval);
            }
            if (getslider == "culture") {
                altval = max-mustsee-entertainment-nature-cuisine-shopping-history-sports+8;
                $("#culture").val(altval);
                $("#culturevalue").val(altval);
            }
            if (getslider == "sports") {
                altval = max-mustsee-entertainment-nature-cuisine-shopping-history-culture+8;
                // $("#sports").val(altval).slider("refresh");
                $("#sports").val(altval);
                $("#sportsvalue").val(altval);
            }
        }
        total = initialPreferencesTotal(max);
        $('.pointallocation').html(total);
    });

    function initialPreferencesTotal (max) {
        var mustsee = parseInt($('#mustsee').val());
        var entertainment = parseInt($('#entertainment').val());
        var nature = parseInt($('#nature').val());
        var cuisine = parseInt($('#cuisine').val());
        var shopping = parseInt($('#shopping').val());
        var history = parseInt($('#history').val());
        var culture = parseInt($('#culture').val());
        var sports = parseInt($('#sports').val());
        var total = max-(mustsee+entertainment+nature+cuisine+shopping+history+culture+sports-8);
        $('.pointallocation').html(total);
        return total;
    }


    // BUDGET CONVERSION
    $('#budget').change(function(){
        var budget = $(this).val();
        var currency_from = $('#currency').val();
        var currency_to = $('#currencyconversion').val();
        $.getJSON(
            // NB: using Open Exchange Rates here, but you can use any source!
            'https://openexchangerates.org/api/latest.json?app_id=7020316637c5401ea8503b42d1e9ac23',
            function(data) {
                // Check money.js has finished loading:
                if ( typeof fx !== "undefined" && fx.rates ) {
                    fx.rates = data.rates;
                    fx.base = currency_from;
                } else {
                    // If not, apply to fxSetup global:
                    var fxSetup = {
                        rates : data.rates,
                        base : currency_from
                    }
                }
                var convert = fx(budget).from(currency_from).to(currency_to);
                var convert1 = accounting.formatNumber(convert);
                $('.currencyconversionresult').empty();
                $('.currencyconversionresult').html(convert1);
            }
        )
    });


    // MOVE PREF CITY UP/DOWN
    $(document.body).on('click', '.prefcity-up', function(){
        var current = $(this).parent('div.prefcity-move').parent('div.prefcityactions').parent('div.row').parent('div.panel-heading').parent('div.panel-day-item');
        var current_pos = $(current).data('pos');
        var previous = $(current).prev('div.panel-day-item');
        var previous_pos = $(previous).data('pos');
        if(previous.length !== 0){
            $(current).data('pos', previous_pos);
            $(previous).data('pos', current_pos);
            $(current).attr('data-pos', previous_pos);
            $(previous).attr('data-pos', current_pos);
            
            current.insertBefore(previous);
        }
    });

    $(document.body).on('click', '.prefcity-down', function(){
        var current = $(this).parent('div.prefcity-move').parent('div.prefcityactions').parent('div.row').parent('div.panel-heading').parent('div.panel-day-item');
        var current_pos = $(current).data('pos');
        var next = $(current).next('div.panel-day-item');
        var next_pos = $(next).data('pos');
        if(next.length !== 0){
            $(current).data('pos', next_pos);
            $(next).data('pos', current_pos);
            $(current).attr('data-pos', next_pos);
            $(next).attr('data-pos', current_pos);
            
            current.insertAfter(next);
        }
    });

    // Apply All
    $(document.body).on('change', '#prefdata-applyall', function(){
        $('.reg-warning.warn-prefdatapage').html('');
        $('.reg-warning.warn-prefdatapage').css('display','none');
        if (document.getElementById('prefdata-applyall').checked) {
            $('.datelist-manual').css('display','none');
            $('.datelist-all').css('display','block');
            $('.pref-manual').css('display','none');
            $('.pref-all').css('display','block');
        }
        else{
            $('.datelist-manual').css('display','block');
            $('.datelist-all').css('display','none');
            $('.pref-manual').css('display','block');
            $('.pref-all').css('display','none');
        }
    });

    // Same as previous
    $('.tab-link a').click(function(){
        var e_href = this.getAttribute("href");
        var tabid = e_href.substring(1);
        var tabactive = $('#'+tabid);
        var dataday = $(tabactive).children('div').children('.sameasprev').data('day');
        var checkbox = tabactive.find('#prefdata-sameasprev'+dataday);
        var checkboxid = $(checkbox).attr('id');
        if (checkboxid != null) {
            if(document.getElementById(checkboxid).checked){
                $(tabactive).children('div.preferences-option').children('div.row-day-item').css('display','none');
                $(tabactive).children('div.preferences-option').children('div.addnewprefcity').css('display','none');
            }
        }

        var checkboxhotel = tabactive.find('#prefdata-samehotel'+dataday);
        var checkboxhotelid = $(checkboxhotel).attr('id');
        if (checkboxhotelid != null) {
            if(document.getElementById(checkboxhotelid).checked){
                $(tabactive).children('div.preferences-option').children('div.prefhotel').css('display','none');
            }
        }
    });

    // Same as previous city-place toggle
    $('.prefdata-sameasprev').change(function(){
        var checkboxid = $(this).attr('id');
        if(!document.getElementById(checkboxid).checked){
            $(this).parent('div').parent('div').siblings('div.row-day-item').css('display','block');
            $(this).parent('div').parent('div').siblings('div.addnewprefcity').css('display','block');
            $(this).siblings('.issameasprevious').val('no');
        }
        else{
            $(this).parent('div').parent('div').siblings('div.row-day-item').css('display','none');
            $(this).parent('div').parent('div').siblings('div.addnewprefcity').css('display','none');
            $(this).siblings('.issameasprevious').val('yes');
        }
    });

    // Same as previous hotel toggle
    $('.prefdata-samehotel').change(function(){
        var checkboxid = $(this).attr('id');
        if(!document.getElementById(checkboxid).checked){
            $(this).parent('div').parent('div').siblings('div.prefhotel').css('display','block');
            $(this).siblings('.issamehotel').val('no');
        }
        else{
            $(this).parent('div').parent('div').siblings('div.prefhotel').css('display','none');
            $(this).siblings('.issamehotel').val('yes');
        }
    });
});
