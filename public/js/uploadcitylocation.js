$(function() {
	$('.showcityorloc').change(function(){
		var show = $(this).val();
		if (show == 'city') {
			$('.citylist').css('display','block');
			$('.locationlist').css('display','none');
		}
		else{
			$('.citylist').css('display','none');
			$('.locationlist').css('display','block');
		}
	});

	$('.submitcityloc').click(function(){
		var img = '';
		var thisclass = $(this).attr('class');
		var title = $(this).siblings('div').children('.title').val();
		var desc = $(this).siblings('div').children('.description').val();
		var p_error = $(this).parent('form').siblings('p.errorinput');
		var form = $(this).parent('form').attr('class');
		var form = form.split(' ');
		var cekimg = $(this).siblings('div.imgpreview');

		if (form[1]==null) {
			var form_class = form[0];
		}
		else{
			var form_class = form[1];
		}

		p_error.empty();
		$(p_error).css('display','none');

		if ($(this).hasClass('submitcity')) {
			var initial = $(this).siblings('div').children('.initial').val();
			if ($(this).hasClass('newcity')) {
				if (cekimg.children().length>1) {
					img = $(this).siblings('div.imgpreview').children('div').siblings('input.hiddenimgpath').val();
				}
			}
			else{
				img = '-1';
			}
		}
		else{
			var initial = '-1';
			if ($(this).hasClass('newloc')) {
				if (cekimg.children().length>1) {
					img = $(this).siblings('div.imgpreview').children('div').siblings('input.hiddenimgpath').val();
				}
			}
			else{
				img = '-1';
			}
		}

		if (title=='' || desc=='' || initial=='' || img=='') {
			p_error.html('Please fill all of the fields.');
			$(p_error).css('display','block');
			$('.modal').animate({
				scrollTop: p_error.offset().top},
				'slow');
			return false;
		}
		else{
			document.getElementsByClassName(form_class).submit();
		}
	});

	$('.deletecity').click(function(){
		if (confirm('Do you want to delete this record?')) {
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: _siteurl+'/uploadcitylocation/cityid/' + $(this).data('id'),
				type: 'delete',
				data: {},
				dataType: "json",
				success: function(data) {
					if (data.success) {
						location.reload();
					} else {
						alert('Unknown error occured');
					}
				}
			});
		}
	});

	$('.deleteloc').click(function(){
		if (confirm('Do you want to delete this record?')) {
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: _siteurl+'/uploadcitylocation/placeid/' + $(this).data('id'),
				type: 'delete',
				data: {},
				dataType: "json",
				success: function(data) {
					if (data.success) {
						location.reload();
					} else {
						alert('Unknown error occured');
					}
				}
			});
		}
	});

	// $('.place-rates input').click(function(){
	// 	var checkboxid = $(this).attr('id');
	// 	if(!document.getElementById(checkboxid).checked){
	// 		$(this).val('no');
	// 	}
	// 	else{
	// 		$(this).val('yes');
	// 	}
	// });
});