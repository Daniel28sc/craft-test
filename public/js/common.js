/* 
 * common.js
 */

function addCity(dayNo, city_id, note, actiontype) {
    if (actiontype == 'addcity') {
        var dayItemDetail = $('#day-item-detail-' + dayNo),
            numOfCity = dayItemDetail.children().length,
            // cityNo = (numOfCity+1),
            cityPos = (numOfCity == 0) ? 1 : dayItemDetail.find('.panel-day-item').last().data('pos') + 1,
            cityNo = (numOfCity == 0) ? 1 : dayItemDetail.find('.panel-day-item').last().data('pos') + 1,
            // cityNo = (numOfCity == 0) ? 1 : dayItemDetail.find('.panel-day-item').last().data('no') + 1,
            city = $.grep(_cityList, function(e){ return e.city_id == city_id; })[0];
            if (city.del == 1) {
                // $('#day-item-detail-' + dayNo).append('<p>City '+city.title+' has been deleted.');
                return false;
            };
    }
    else{
        var dayItemDetail = $('#day-item-detail-' + dayNo),
            cityNo = $('.cityactiontype').data('no'),
            cityPos = $('.cityactiontype').data('pos'),
            numOfCity = dayItemDetail.children().length,
            city = $.grep(_cityList, function(e){ return e.city_id == city_id; })[0];
        for (var i = 0; i < numOfCity; i++) {
            if ($(dayItemDetail.children()[i]).data('no') == cityNo) {
                dayItemDetail.children()[i].remove();
                break;
            };
        };
    }
    
    var markup = 
            '<div class="panel panel-default panel-day-item" data-no="' + cityNo + '" data-id="' + city_id + '" data-pos="' + cityPos + '">' +
                '<div class="panel-heading" id="city-' + dayNo + '-' + cityNo + '" role="tab" >' +
                    '<div class="row">' +
                        '<div class="col-md-11 prefcityhead">' +
                            '<a class="thumbnail day-item-city-pic' + ((cityNo > 1) ? ' collapsed' : '' ) + '" role="button"' +
                                'href="#city-detail-' + dayNo + '-' + cityNo + '"' +
                                'data-toggle="collapse" data-parent="#day-item-detail-' + dayNo + '" ' + 
                                'aria-expanded="true" aria-controls="city-detail-' + dayNo + '-' + cityNo + '">' +
                                    '<img src="' + city.image_source + '" alt="' + city.title + '">' +
                                    '<div class="patternover prefcitypattern"></div>' +
                                    '<div class="prefcitythumbnail-boxborder"></div>' +
                                    '<div class="prefcitycaption">' +
                                        '<div class="prefcitycaption-blackmask"></div>' +
                                        '<span class="day-item-city-title">' + city.kanji + '<br>' + city.title + '</span>' +
                                    '</div>' +
                            '</a>' +
                        '</div>' +
                        //DIUBAH vvv . Tambah tombol X untuk delete.
                        '<div class="col-md-1 prefcityactions">' +
                            '<button type="button" class="del-prefcity btn btn-primary btn-xs"><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>' +
                            '<button type="button" class="edit-prefcity btn btn-primary btn-xs"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i></button>' +
                            '<div class="prefcity-move">' +
                                '<i class="fa fa-caret-up fa-2x prefcity-up" aria-hidden="true"></i>' +
                                '<i class="fa fa-caret-down fa-2x prefcity-down" aria-hidden="true"></i>' +
                            '</div>' +
                        '</div>' +
                        // ^^^
                    '</div>' +
                '</div>' +
                '<div class="panel-collapse pref-places collapse' + ((cityNo == 1) ? ' in' : '') + '" id="city-detail-' + dayNo + '-' + cityNo + '"' +
                    'role="tabpanel" aria-labelledby="city-' + dayNo + '-' + cityNo + '">' +
                    '<div class="panel-body">' + 
                        // '<div class="row">' +
                        //     '<button type="button" class="btn btn-success btn-edit-place pull-right" ' +
                        //         'data-id="' + city_id + '" data-day="' + dayNo + '" data-city="' + cityNo + '">' +
                        //         '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> ' +
                        //         ' Place' +
                        //     '</button>' +
                        // '</div>' +
                        '<div class="row" id="city-place-' + dayNo + '-' + cityNo + '">' +
                            '<div class="col-md-6 refplaceslist">' +
                                '<h1>Places I must visit</h1>' +
                                '<div class="row selectedplaceslist">' +
                                '</div>' +
                                '<div class="row addplacebuttonrow">' +
                                    '<div class="col-md-12">' +
                                        '<button type="button" class="btn btn-success btn-edit-place" ' +
                                        'data-id="' + city_id + '" data-day="' + dayNo + '" data-city="' + cityNo + '">' +
                                            'Add Place' +
                                        '</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-6 refplacesnote">' +
                                '<h1>What I want to do in the city</h1>' +
                                '<div class="form-group">' +
                                  '<textarea class="form-control pref-comment" rows="5" value="" name="prefcomment">' + note + '</textarea>' +
                                '</div>' +
                                '<p>tell us and describe all things you want to do in this city</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';  

    if (actiontype == 'addcity') {
        $('#day-item-detail-' + dayNo).append(markup);
    }
    else{
        for (var i = 0; i < numOfCity; i++) {
            paneldayitemno = $(dayItemDetail.children()[i]).data('pos');
            // paneldayitemno = $(dayItemDetail.children()[i]).data('no');
            if (i == numOfCity-1) {
                $('#day-item-detail-' + dayNo).append(markup);
                break;
            }

            if (cityPos < paneldayitemno) {
                $(dayItemDetail.children()[i]).before(markup);
                // dayItemDetail.insertBefore(markup, dayItemDetail.children()[i]);
                break;
            }
        }
    }   
}

function updatePlace(dayNo, cityNo, places) {
    var placeContainer = $('#city-place-' + dayNo + '-' + cityNo).children('.refplaceslist').children('.selectedplaceslist'),
        markup = '';

    for (var i = 0; i < places.length; i++) {
        var place = places[i];
        if (place.del == 1) {
            continue;
        }
        markup +=
            '<div class="col-xs-6 col-md-3 prefcityimgbox">' +
                '<div class="thumbnail city-place-item" data-id="' + place.place_id + '">' +
                    '<img src="' + place.image_source + '" alt="' + place.title + '">' +
                    '<div class="caption">' + place.title + '</div>' +
                    '<div class="del-prefplace"><i class="fa fa-times fa-2x" aria-hidden="true"></i></div>' +
                    '<div class="prefplace-whitebox"></div>' +
                '</div>' +
            '</div>';
    }
    placeContainer.html(markup);
}

function loadSavedDayItem() {   
    if (_itineraryDayItem[0] == undefined) {
        var type = 'manual';
    }
    else{
        var type = _itineraryDayItem[0].type; 
    }
     

    // Load items for the manual type
    for(var i = 0; i < _itineraryDayItem.length; i++) {
        var item = _itineraryDayItem[i],
            dayContainer = $('div[data-it_day_id="' + item.itday_id + '"]'),
            dayNo = dayContainer.data('day'),
            // DIUBAH vvv
            placeIds = $.grep(_itineraryDayItem, function(e){ return ((e.itday_id == item.itday_id) && (e.city_id == item.city_id)); }),
            // placeIds = $.grep(_itineraryDayItem, function(e){ return e.itday_id == item.itday_id; }),
            // ^^^
            places = Array();

        for (var j = 0; j < placeIds.length; j++) {
            // DIUBAH vvv.
            if (placeIds[j].place_id == null) {
                continue;
            }
            // ^^^
            var place_id = placeIds[j].place_id,
                place = $.grep(_placeList, function(e) { return e.place_id == place_id; })[0];
            places.push(place);
        }

        if (dayContainer.length == 0) {
            addCity(dayNo, item.city_id, item.note, 'addcity');
            updatePlace(dayNo, 1, places);
        } else {
            var cityExist = false,
                cityNo = dayContainer.find('.panel-day-item').length + 1;
            dayContainer.find('.panel-day-item').each(function() {
                if ($(this).data('id') == item.city_id) cityExist = true;
            })
            if (!cityExist) {
                addCity(dayNo, item.city_id, item.note, 'addcity');
                updatePlace(dayNo, cityNo, places);
            }
        }
    }


    // Load items for the all type
    if (type == 'all') {
        for(var i = 0; i < _itineraryDayItem.length; i++) {
            var item = _itineraryDayItem[i],
                dayContainer = $('div#day-item-detail-0'),
                // dayContainer = $('div[data-it_day_id="' + item.itday_id + '"]'),
                dayNo = 0,
                placeIds = $.grep(_itineraryDayItem, function(e){ return ((e.itday_id == item.itday_id) && (e.city_id == item.city_id)); }),
                places = Array();

            for (var j = 0; j < placeIds.length; j++) {
                if (placeIds[j].place_id == null) {
                    continue;
                }
                var place_id = placeIds[j].place_id,
                    place = $.grep(_placeList, function(e) { return e.place_id == place_id; })[0];
                places.push(place);
            }

            if (dayContainer.length == 0) {
                addCity(dayNo, item.city_id, item.note, 'addcity');
                updatePlace(dayNo, 1, places);
            } else {
                var cityExist = false,
                    cityNo = dayContainer.find('.panel-day-item').length + 1;
                dayContainer.find('.panel-day-item').each(function() {
                    if ($(this).data('id') == item.city_id) cityExist = true;
                })
                if (!cityExist) {
                    addCity(dayNo, item.city_id, item.note, 'addcity');
                    updatePlace(dayNo, cityNo, places);
                }
            }

            if (_itineraryDayItem[i].itday_id != _itineraryDayItem[i+1].itday_id) {
                break;
            }

        }
    }
}