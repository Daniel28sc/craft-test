/* 
 * summary.js
 */

jQuery(document).ready(function($) {
    var _isLogin = (_userId != "");
    
    loadSavedDayItem();
    
    $('#btn-back').click(function () {
        window.location.replace('/pickitinerators/' + _itineraryId);
    });
    
    $('#btn-next').click(function () {
        if (_isLogin) {
            window.location.replace('/profile');
        } else {
            $('#modal-signin').modal('show');
        }
    });
    
    $('#btn-register').click(function() {
        //window.location.replace('/registration/' + _itineraryId);
        var email = $('#reg-email').val(),
            pass = $('#reg-pass').val(),
            pass_conf = $('#reg-pass-conf').val();
        
        if (email == "" || pass == "" || pass_conf == "") {
            alert('Please fill all field');
            return;
        }
        
        if (pass == pass_conf) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/register',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                type: 'POST',
                data: { 
                    'email' : email,
                    'password' : pass,
                    'it_id' : _itineraryId
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $('#modal-signin').modal('hide');
                        alert('Thank you for registering. ' + 
                              'Please check your email for account verification.');
                        window.location.replace('/');
                    } else {
                        var errorMessage = (data.message == null) ?
                                "We are sorry, there was an unexpected error " + 
                                    "in registration process. " +
                                    "Please try again later. " :
                                data.message;
                        alert(errorMessage);
                    }
                }
            });
        } else {
            alert('Password confirmation not match');
        }
    });
    
    $('#btn-signin').click(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/login',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            type: 'POST',
            data: { 
                'username' : $('#username').val(),
                'password' : $('#password').val(),
                'it_id' : _itineraryId
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal-signin').modal('hide');
                    window.location.replace('/profile');
                } else {
                    alert('Login information not match');
                }
            }
        });
    });

    $('#inputstylecontinue').click(function(){
        var buttonval = $(this).val();

        if (buttonval == 'SAVE') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'summary_controller',
                contentType: 'application/json; charset=UTF-8',
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data == 'false') {
                        $('.warn-summarypage').css('display','block');
                        $('html,body').animate({
                            scrollTop: $('.reg-warning.warn-summarypage').offset().top},
                            'slow');
                    }
                    else{
                        window.location.replace("../myitinerary");
                    }
                }
            });
        }
        return false;
    });
});
