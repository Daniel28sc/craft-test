$(function() {
	$('.inputstylepagenext').click(function(){
		var calendar = document.querySelectorAll('input[type="checkbox"].inputstylecalendar:checked').length;
		var daystrip = $('.itinerarynod').val();
		var itineraryname = $('#itineraryname').val();

		if (calendar != daystrip || itineraryname == '') {
			// $(".notification-limitpick").removeClass('show-note');
			$(".notification-limitpick").addClass('show-note');
			$(".notification-limitpick p").empty();
			// $(".notification-limitpick p").html('You must pick '+daystrip+' days.');
			if (calendar != daystrip && itineraryname == '') {
				$(".notification-limitpick p").html('Please write a name for this itinerary.');
				$(".notification-limitpick p").append('<br>You must pick '+daystrip+' days.');
			}
			else if (itineraryname == '') {
				$(".notification-limitpick p").html('You must write a name for this itinerary.');
			}
			else{
				$(".notification-limitpick p").html('You must pick '+daystrip+' days.');
			}
			$('html,body').animate({
				scrollTop: $('.notification-limitpick').offset().top},
				'slow');
			return false;
		}
	});
	
	$('.inputstylecalendar').click(function(){
		var daystrip = $('.itinerarynod').val();
		var calendar = document.forms.inputstyle_form["inputstylecalendar[]"];
		var checkgroup=calendar;
		var itinerarynod = $(".itinerarynod").val();
		itinerarynod = Number(itinerarynod);
		var limit=itinerarynod;
		for (var i=0; i<checkgroup.length; i++){
			var checkedcount=0;
			for (var i=0; i<checkgroup.length; i++){
				checkedcount+=(checkgroup[i].checked)? 1 : 0;
				// $(".notification-limitpick").removeClass('show-note');
				// $(".notification-limitpick p").empty();
				if (checkedcount>limit){
					$(".notification-limitpick").addClass('show-note');
					$(".notification-limitpick p").html('You have already selected '+daystrip+' days');
					this.checked=false;
					$('html,body').animate({
						scrollTop: $('.notification-limitpick').offset().top},
						'slow');
					return false;
				}
			}
		}
	});

	$('.pickitineratorspagenext').click(function(){
		var pickitinerators = document.querySelectorAll('input[type="checkbox"].itinerators-checkbox:checked').length;
		if (pickitinerators == 0) {
			$('.notification-limitpick p').empty();
        	$(".notification-limitpick").removeClass('show-note');
        	$('.notification-limitpick p').html('You have to pick at least 1 itinerator.');
          	$(".notification-limitpick").addClass('show-note');
          	$('html,body').animate({
				scrollTop: $('.notification-limitpick').offset().top},
				'slow');
			return false;
		}
	});

	$('.picktypeoption').click(function(){
		var picktype = $(this).val();
		if (picktype == "random") {
			$('.itinerators-checkbox').prop('checked',true);
			$('.notification-limitpick').removeClass('show-note');
		}
		else{
			$('.itinerators-checkbox').prop('checked',false);
		}
	});
	
	$('.itinerators-checkbox').click(function(){
		var numofcheckboxes = $('.itinerators-checkbox').length;
		var numofcheckboxes_check = $('.itinerators-checkbox:checked').length;
		if (numofcheckboxes!=numofcheckboxes_check) {
			$('.pickmanual').prop('checked',true);
		}

		var itinerators = document.forms.pickitinerators_form["itinerators[]"];
		var checkgroup=itinerators;
		var limit=3;
		for (var i=0; i<checkgroup.length; i++){
			var checkedcount=0;
			for (var i=0; i<checkgroup.length; i++){
				checkedcount+=(checkgroup[i].checked)? 1 : 0;
				$('.notification-limitpick p').empty();
				$(".notification-limitpick").removeClass('show-note');
				if (checkedcount>limit){
					$('.notification-limitpick p').html('You have already selected 3 itinerators.');
					$(".notification-limitpick").addClass('show-note');
					$('html,body').animate({
						scrollTop: $('.notification-limitpick').offset().top},
						'slow');
					this.checked=false;
				}
			}
		}
	});

	$('.submititineraryfile').click(function(){
		var file = $(this).siblings('.uploaditinerarypdf').val();
		if (file == '') {
			return false;
		}
	});
});