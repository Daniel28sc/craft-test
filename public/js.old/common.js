/* 
 * common.js
 */

function addCity(dayNo, city_id) {
    var dayItemDetail = $('#day-item-detail-' + dayNo),
        numOfCity = dayItemDetail.children().length,
        cityNo = (numOfCity == 0) ? 1 : dayItemDetail.find('.panel-day-item').last().data('no') + 1,
        city = $.grep(_cityList, function(e){ return e.city_id == city_id; })[0],
        markup = 
            '<div class="panel panel-default panel-day-item" data-no="' + cityNo + '" data-id="' + city_id + '">' +
                '<div class="panel-heading" id="city-' + dayNo + '-' + cityNo + '" role="tab" >' +
                    '<a class="thumbnail day-item-city-pic' + ((cityNo > 1) ? ' collapsed' : '' ) + '" role="button"' +
                        'href="#city-detail-' + dayNo + '-' + cityNo + '"' +
                        'data-toggle="collapse" data-parent="#day-item-detail-' + dayNo + '" ' + 
                        'aria-expanded="true" aria-controls="city-detail-' + dayNo + '-' + cityNo + '">' +
                            '<img src="' + city.image_source + '" alt="' + city.title + '">' +
                            '<span class="day-item-city-title">' + city.title + '</span>' +
                    '</a>' +
                '</div>' +
                '<div class="panel-collapse collapse' + ((cityNo == 1) ? ' in' : '') + '" id="city-detail-' + dayNo + '-' + cityNo + '"' +
                    'role="tabpanel" aria-labelledby="city-' + dayNo + '-' + cityNo + '">' +
                    '<div class="panel-body">' + 
                        '<div class="row">' +
                            '<button type="button" class="btn btn-success btn-edit-place pull-right" ' +
                                'data-id="' + city_id + '" data-day="' + dayNo + '" data-city="' + cityNo + '">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> ' +
                                ' Place' +
                            '</button>' +
                        '</div>' +
                        '<div class="row" id="city-place-' + dayNo + '-' + cityNo + '"></div>'
                    '</div>' +
                '</div>' +
            '</div>';        
    $('#day-item-detail-' + dayNo).append(markup);
}

function updatePlace(dayNo, cityNo, places) {
    var placeContainer = $('#city-place-' + dayNo + '-' + cityNo),
        markup = '';

    for (var i = 0; i < places.length; i++) {
        var place = places[i];
        markup +=
            '<div class="col-xs-6 col-md-3">' +
                '<div class="thumbnail city-place-item" data-id="' + place.place_id + '">' +
                    '<img src="' + place.image_source + '" alt="' + place.title + '">' +
                    '<div class="caption">' + place.title + '</div>' +
                '</div>' +
            '</div>';
    }
    placeContainer.html(markup);
}

function loadSavedDayItem() {        
    for(var i = 0; i < _itineraryDayItem.length; i++) {
        var item = _itineraryDayItem[i],
            dayContainer = $('div[data-it_day_id="' + item.itday_id + '"]'),
            dayNo = dayContainer.data('day'),
            placeIds = $.grep(_itineraryDayItem, function(e){ return e.itday_id == item.itday_id; }),
            places = Array();

        for (var j = 0; j < placeIds.length; j++) {
           var place_id = placeIds[j].place_id,
               place = $.grep(_placeList, function(e) { return e.place_id == place_id; })[0];
           places.push(place);
        }

        if (dayContainer.length == 0) {
            addCity(dayNo, item.city_id);
            updatePlace(dayNo, 1, places);
        } else {
            var cityExist = false,
                cityNo = dayContainer.find('.panel-day-item').length + 1;
            dayContainer.find('.panel-day-item').each(function() {
                if ($(this).data('id') == item.city_id) cityExist = true;
            })
            if (!cityExist) {
                addCity(dayNo, item.city_id);
                updatePlace(dayNo, cityNo, places);
            }
        }
    }
}