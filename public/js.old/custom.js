$(function() {
	var d;
	initialPreferencesTotal();

	$('.cropme').simpleCropper();

	// $('#image').cropper({
	//   aspectRatio: 16 / 9,
	//   crop: function(e) {
	//     // Output the result data for cropping image.
	//     console.log(e.x);
	//     console.log(e.y);
	//     console.log(e.width);
	//     console.log(e.height);
	//     console.log(e.rotate);
	//     console.log(e.scaleX);
	//     console.log(e.scaleY);
	//   }
	// });
	// $('#image').imgAreaSelect({ maxWidth: 1000, maxHeight: 200, handles: true });
	// var cropperHeader = new Croppic('cityloc-imgpreview');
	
	
	$('li').mouseenter(function(e) {
		var e_class = $(this).attr('class');
		if ($(this).hasClass('liwhy1')) {
			$('.triangle1, .why-desc1').addClass('active');
			$('.why-desc2, .why-desc3, .why-desc4').removeClass('active');
			$('.triangle2, .triangle3, .triangle4').removeClass('active');
		};
		if ($(this).hasClass('liwhy2')) {
			$('.triangle2, .why-desc2').addClass('active');
			$('.why-desc1, .why-desc3, .why-desc4').removeClass('active');
			$('.triangle1, .triangle3, .triangle4').removeClass('active');
		};
		if ($(this).hasClass('liwhy3')) {
			$('.triangle3, .why-desc3').addClass('active');
			$('.why-desc1, .why-desc2, .why-desc4').removeClass('active');
			$('.triangle1, .triangle2, .triangle4').removeClass('active');
		};
		if ($(this).hasClass('liwhy4')) {
			$('.triangle4, .why-desc4').addClass('active');
			$('.why-desc1, .why-desc2, .why-desc3').removeClass('active');
			$('.triangle1, .triangle2, .triangle3').removeClass('active');
		};
	});


	$('.input-style-button-simple').click(function() {
		$('.input-style-button-simple').addClass("active");
		$('.input-style-button-detail').removeClass("active");
		$('.checkmark1').addClass("active");
		$('.checkmark2').removeClass("active");
	});
	$('.input-style-button-detail').click(function() {
		$('.input-style-button-detail').addClass("active");
		$('.input-style-button-simple').removeClass("active");
		$('.checkmark2').addClass("active");
		$('.checkmark1').removeClass("active");
	});

	$('.inputstylecalendar').change(function () {
		if ($(this).is(':checked')) {
			$(this).parent('label').parent('td').addClass('selected');
		}
		else{
			$(this).parent('label').parent('td').removeClass('selected');
		}
	});

	$(".nav-tabs").on("click", "a", function (e) {
		e.preventDefault();
		if (!$(this).hasClass('add-new')) {
			$(this).tab('show');
		}
	})
	.on("click", "span", function () {
		var anchor = $(this).siblings('a');
		$(anchor.attr('href')).remove();
		$(this).parent().remove();
		$(".nav-tabs li").children('a').first().click();
	});

	$('.add-new').click(function (e) {
		e.preventDefault();
		var getnavtabsclass = $(this).parent('li').parent('ul').attr('class');
		var getnavtabsclass1 = getnavtabsclass.split(' ');
		var getnavtabsclass2 = getnavtabsclass1[1]; 
	    // var id = $('.nav-tabs').children().length;
	    var id = $('.'+getnavtabsclass2).children().length;
	    
	    var gettabparentid = $(this).parent('li').parent('ul').parent('div').parent('div.tab-pane').attr('id');
	    var gettabparentid1 = gettabparentid.split('-');
	    var gettabparentid2 = gettabparentid1[1];

		if(id===4){

		}
		else{
			d = $(this).parent('li').parent('ul').parent('div').parent('.tab-pane').attr('id');
			var d1=d.split('-');
			var d2=d1[1];
			id=id+((d2*3)-3);
			var tabId = 'tab' + id;
			// $(this).closest('li').before('<li class="navli '+tabId+'"><a href="#tab' + id + '">Tab ' + id + '</a> <span> x </span></li>');
			$(this).closest('li').before('<li class="navli '+tabId+'"><a href="#tab' + id + '"><output id="childtabname'+id+'">Pick a city</output></a><span> x </span></li>');
			$('.tab-content-child'+gettabparentid2).append('<div class="tab-pane" id="' + tabId + '"></div>');
			$('.'+getnavtabsclass2+' li:nth-child(' + id + ') a').click();
			insertContent(id);
		}
		
	});

	
	$('.prefdataslider').change(function(){
		var max = 20;
		var getslider = $(this).children('div').children('input').attr("id");
		var pace = parseInt($('#pace').val());
		var mustsee = parseInt($('#mustsee').val());
		var entertainment = parseInt($('#entertainment').val());
		var nature = parseInt($('#nature').val());
		var cuisine = parseInt($('#cuisine').val());
		var adventure = parseInt($('#adventure').val());
		var shopping = parseInt($('#shopping').val());
		var art = parseInt($('#art').val());
		var culture = parseInt($('#culture').val());
		var sports = parseInt($('#sports').val());
		var total = pace+mustsee+entertainment+nature+cuisine+adventure+shopping+art+culture+sports;
		if (total > max) {
			if (getslider == "pace") {
				altval = max-mustsee-entertainment-nature-cuisine-adventure-shopping-art-culture-sports;
				$("#pace").val(altval);
				$("#pacevalue").val(altval);				
			}
			if (getslider == "mustsee") {
				altval = max-pace-entertainment-nature-cuisine-adventure-shopping-art-culture-sports;
				$("#mustsee").val(altval);
				$("#mustseevalue").val(altval);
			}
			if (getslider == "entertainment") {
				altval = max-pace-mustsee-nature-cuisine-adventure-shopping-art-culture-sports;
				$("#entertainment").val(altval);
				$("#entertainmentvalue").val(altval);
			}
			if (getslider == "nature") {
				altval = max-pace-mustsee-entertainment-cuisine-adventure-shopping-art-culture-sports;
				$("#nature").val(altval);
				$("#naturevalue").val(altval);
			}
			if (getslider == "cuisine") {
				altval = max-pace-mustsee-entertainment-nature-adventure-shopping-art-culture-sports;
				$("#cuisine").val(altval);
				$("#cuisinevalue").val(altval);
			}
			if (getslider == "adventure") {
				altval = max-pace-mustsee-entertainment-nature-cuisine-shopping-art-culture-sports;
				$("#adventure").val(altval);
				$("#adventurevalue").val(altval);
			}
			if (getslider == "shopping") {
				altval = max-pace-mustsee-entertainment-nature-cuisine-adventure-art-culture-sports;
				$("#shopping").val(altval);
				$("#shoppingvalue").val(altval);
			}
			if (getslider == "art") {
				altval = max-pace-mustsee-entertainment-nature-cuisine-adventure-shopping-culture-sports;
				$("#art").val(altval);
				$("#artvalue").val(altval);
			}
			if (getslider == "culture") {
				altval = max-pace-mustsee-entertainment-nature-cuisine-adventure-shopping-art-sports;
				$("#culture").val(altval);
				$("#culturevalue").val(altval);
			}
			if (getslider == "sports") {
				altval = max-pace-mustsee-entertainment-nature-cuisine-adventure-shopping-art-culture;
				// $("#sports").val(altval).slider("refresh");;
				$("#sports").val(altval);
				$("#sportsvalue").val(altval);
			}
		}
		var pace = parseInt($('#pace').val());
		var mustsee = parseInt($('#mustsee').val());
		var entertainment = parseInt($('#entertainment').val());
		var nature = parseInt($('#nature').val());
		var cuisine = parseInt($('#cuisine').val());
		var adventure = parseInt($('#adventure').val());
		var shopping = parseInt($('#shopping').val());
		var art = parseInt($('#art').val());
		var culture = parseInt($('#culture').val());
		var sports = parseInt($('#sports').val());
		var total = pace+mustsee+entertainment+nature+cuisine+adventure+shopping+art+culture+sports;
		$('.pointallocation').html(total);
	});

	function initialPreferencesTotal () {
		var pace = parseInt($('#pace').val());
		var mustsee = parseInt($('#mustsee').val());
		var entertainment = parseInt($('#entertainment').val());
		var nature = parseInt($('#nature').val());
		var cuisine = parseInt($('#cuisine').val());
		var adventure = parseInt($('#adventure').val());
		var shopping = parseInt($('#shopping').val());
		var art = parseInt($('#art').val());
		var culture = parseInt($('#culture').val());
		var sports = parseInt($('#sports').val());
		var total = pace+mustsee+entertainment+nature+cuisine+adventure+shopping+art+culture+sports;
		$('.pointallocation').html(total);
	}
	

	$(document).on('change','.citypick',function(){
		var idofpickedcity = $(this).val();
		var dummydate = $(this).siblings('input.pd').val();
		var locdiv = $(this).parent('.pref-city-select').children('.pref-location-select').children('.locselect');
		$.ajax({
			headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			type: "post",
			url: "preferencesdata",
			data: {cityid: idofpickedcity, dummydate: dummydate},
			// data: {cityid: idofpickedcity},
			dataType: "json",
			success: function(data){
				$(locdiv).empty();
				for (var i = 0; i < data.length; i++) {
					$(locdiv).append('<input type="checkbox" class="select-location" name="location[]" value="' + data[i]["loc"]["place_id"] +'">' + data[i]["loc"]["title"]+ '</input>');
					// $(locdiv).append('<br><input type="hidden" name="loc-city[]" value="' + data[i]["loc"]["city_id"] +'">');					
					$(locdiv).append('<img src="' + data[i]["loc"]["image_source"] + '">');
					// $(locdiv).append('<br><input type="hidden" name="dummy-date[]" value="' + data[i]["dummy_date"] +'">');
				};
				

				$('.select-location').click(function(){
					if ($(this).is(':checked')) {
						$(locdiv).append('<input type="hidden" class="loc-city" name="loc-city[]" value="' + data[0]["loc"]["city_id"] +'">');
						$(locdiv).append('<input type="hidden" class="dummy-date" name="dummy-date[]" value="' + data[0]["dummy_date"] +'">');
					}
					else{
						var notchecked = $('.select-location:not(:checked)').length;
						
						var remove_loccity = $(this).parent('.locselect').children('.loc-city');
						var remove_dummydate = $(this).parent('.locselect').children('.dummy-date');
						for (var i = 0; i < $('.select-location:not(:checked)').length; i++) {
							remove_loccity[i].remove();
							remove_dummydate[i].remove();
						}
					}
				});
				// alert(data);
			},
			error: function(){
				alert("Error!");
			}
		});
		return false;
	});
	

	$('.picktypeoption').click(function(){
		var picktype = $(this).val();
		if (picktype == "random") {
			$('.itinerators-checkbox').prop('checked',true);
			$('.notification-limitpick').removeClass('show-note');
			// $('.itinerators-checkbox').prop('disabled',true);
		}
		else{
			$('.itinerators-checkbox').prop('checked',false);
			// $('.itinerators-checkbox').prop('disabled',false);
		}
	});

	$('.itinerators-checkbox').click(function(){
		var numofcheckboxes = $('.itinerators-checkbox').length;
		var numofcheckboxes_check = $('.itinerators-checkbox:checked').length;
		if (numofcheckboxes!=numofcheckboxes_check) {
			$('.pickmanual').prop('checked',true);
		} else{
			// $('.pickrandom').prop('checked',true);
		}
	});

	// $('.profile-menu li').click(function(){
	// 	$('.profile-content').children('div').removeClass('active-profilecontent');
	// 	if ($(this).hasClass('menu-myprofile')) {
	// 		$('.myprofile').addClass('active-profilecontent');
	// 	}
	// 	if ($(this).hasClass('menu-myitinerary')) {
	// 		$('.userprofile-itinerarylist').addClass('active-profilecontent');
	// 	}
	// });

	// $('.itineratorsitinerary').click(function(){
	// 	var button = $(this).attr('class');
	// 	button = button.split(' ');
	// 	var button1 = button[3];
	// 	button1 = button1.substr(19);
	// 	$('.myprofile-showitinerators.list-'+button1).addClass('show-popup');
	// });

	// $('.close-showitinerators').click(function(){
	// 	var list = $(this).parent('div').parent('div');
	// 	list.removeClass('show-popup');
	// });
	

	$('.edititinerary').click(function(){
		var button = $(this).attr('class');
		button = button.split(' ');
		var itid = button[1];
		itid = itid.substr(2);
		var url = 'http://itinerarium.rectmedia.com.au/preferencesdata/'+itid;
		window.open(url,'_blank');
	});

});