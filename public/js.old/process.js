$(function() {
  var d = new Date();
  $('.home-date').datepicker({
    format: "yyyy/mm/dd",
    startDate: d,
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });

  $('.reg-dob').datepicker({
    format: "yyyy/mm/dd",
    todayBtn: "linked",
    autoclose: true
  });

  $("#rangevalue").html($("#rangeinput").val());
  arrivalreturn();
  // addNewForm();

  function arrivalreturn () {
    $("#arrivaldate").change(function(){
      var ad = document.getElementById("arrivaldate").value;
      var rd = document.getElementById("returndate").value;
      var ad1 = new Date(ad);
      var dx = new Date(ad1.setDate(ad1.getDate()+1));    

      if(rd.length!==0){
        if(ad>rd){
          $('.home-date1').val('');
          rd = '';
        }
        $('.home-date1').datepicker('remove');
      }
      $('.home-date1').datepicker({
        format: "yyyy/mm/dd",
        startDate: dx,
        autoclose: true,
        todayHighlight: true
      });

      if(rd.length === 0){
        $("#rangeinput").attr({
          "max" : 1,
          "value" : 1
        });
        $("#rangevalue").html($("#rangeinput").val());
      }
      else{
        var arrivaldate = document.getElementById("arrivaldate").value;
        var returndate = document.getElementById("returndate").value;
        if (arrivaldate!==null && returndate!==null) {
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var d1 = new Date(arrivaldate);
          var d1 = d1.getTime();
          var d2 = new Date(returndate);
          var d2 = d2.getTime();
          var dif = d2 - d1;
          var diffDays = Math.round(dif/oneDay); 
          if(diffDays<1){
            $("#rangeinput").attr({
              "max" : 1,
              "value" : 1
            });
          }
          else{
            $("#rangeinput").attr({
              "max" : diffDays,
              "value" : diffDays
            });
          }

          $("#rangevalue").html($("#rangeinput").val());
        }
        else{
          arrivalreturn();
        }
      }
    });

    $("#returndate").change(function(){
      var arrivaldate = document.getElementById("arrivaldate").value;
      var returndate = document.getElementById("returndate").value;
      if (arrivaldate!==null && returndate!==null) {
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var d1 = new Date(arrivaldate);
        var d1 = d1.getTime();
        var d2 = new Date(returndate);
        var d2 = d2.getTime();
        var dif = d2 - d1;
        var diffDays = Math.round(dif/oneDay); 
        if(diffDays<1){
          $("#rangeinput").attr({
            "max" : 1,
            "value" : 1
          });
        }
        else{
          $("#rangeinput").attr({
            "max" : diffDays,
            "value" : diffDays
          });
        }

        $("#rangevalue").html($("#rangeinput").val());
      }
      else{
        arrivalreturn();
      }
    });
  }

  // function checkboxlimit(checkgroup){
  //   var checkgroup=checkgroup;
  //   var itinerarynod = $("#itinerarynod_val").attr('class');
  //   itinerarynod = Number(itinerarynod);
  //   var limit=itinerarynod;
  //   for (var i=0; i<checkgroup.length; i++){
  //     checkgroup[i].onclick=function(){
  //       var checkedcount=0;
  //       for (var i=0; i<checkgroup.length; i++)
  //         checkedcount+=(checkgroup[i].checked)? 1 : 0;
  //       $(".notification-limitpick").removeClass('show-note');
  //       if (checkedcount>limit){
  //         // alert("You can only select a maximum of "+limit+" checkboxes");
  //         $(".notification-limitpick").addClass('show-note');
  //         this.checked=false;
  //       }
  //     }
  //   }
  // }
  // checkboxlimit(document.forms.inputstyle_form["inputstylecalendar[]"]);

  $('.inputstylecalendar').click(function(){
    var calendar = document.forms.inputstyle_form["inputstylecalendar[]"];
    var checkgroup=calendar;
    var itinerarynod = $("#itinerarynod_val").attr('class');
    itinerarynod = Number(itinerarynod);
    var limit=itinerarynod;
    for (var i=0; i<checkgroup.length; i++){
      var checkedcount=0;
      for (var i=0; i<checkgroup.length; i++){
        checkedcount+=(checkgroup[i].checked)? 1 : 0;
        $(".notification-limitpick").removeClass('show-note');
        if (checkedcount>limit){
          $(".notification-limitpick").addClass('show-note');
          this.checked=false;
        }
      }
    }
  });

  $('.itinerators-checkbox').click(function(){
    var itinerators = document.forms.pickitinerators_form["itinerators[]"];
    var checkgroup=itinerators;
    var limit=3;
    for (var i=0; i<checkgroup.length; i++){
      var checkedcount=0;
      for (var i=0; i<checkgroup.length; i++){
        checkedcount+=(checkgroup[i].checked)? 1 : 0;
        $(".notification-limitpick").removeClass('show-note');
        if (checkedcount>limit){
          $(".notification-limitpick").addClass('show-note');
          this.checked=false;
        }
      }
    }
  });

  $('#savesummary').click(function(){
    $('.modal-loginreg').addClass('active-regpage');
    $('.modal-regverificationdone').removeClass('active-regpage');
    document.forms['registration_form'].reset();
  });

  $('.summaryback').click(function(){
    var itid = $('.dummyitid').val();
    window.location.href = "http://localhost/development/laravel/itineration/public/preferencesdata/"+itid;
  });


  $('.reg-signup').click(function(){
    var email = $('.reg-email').val();
    var password = $('.reg-password').val();
    var passwordconfirm = $('.reg-passwordconfirm').val();
    var name = $('.reg-name').val();
    var dob = $('.reg-dob').val();
    var country = $('.reg-country').val();
    var address = $('.reg-address').val();
    var registertype = $('.reg-type').val();
    $('.load').append('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');

    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      type: "post",
      url: "loginregistration_ajax_controller",
      data: {email: email, password: password, passwordconfirm: passwordconfirm, name:name, dob:dob, country:country, address:address, registertype: registertype},
      dataType: "json",
      success: function(data){
        $('span.load').html('');
        if (data[0]['msg']!='') {
          var msg = data[0]['msg'];
          clearRegistrationInvalid();
          $('.reg-warning').append('<p>'+data[0]['msg']+'</p>');
          $('.reg-warning').addClass('show-warning');

          if (msg == 'All fields must be filled.') {
            if(email==''){
              $('.reg-email').addClass('invalid-reg');
            }
            if(password==''){
              $('.reg-password').addClass('invalid-reg');
            }
            if(passwordconfirm==''){
              $('.reg-passwordconfirm').addClass('invalid-reg');
            }
            if(name==''){
              $('.reg-name').addClass('invalid-reg');
            }
            if(dob==''){
              $('.reg-dob').addClass('invalid-reg');
            }
            if(country==''){
              $('.reg-country').addClass('invalid-reg');
            }
            if(address==''){
              $('.reg-address').addClass('invalid-reg');
            }
          }
          else{
            $('.reg-password').addClass('invalid-reg');
            $('.reg-passwordconfirm').addClass('invalid-reg');
          }
        }
        else{
          clearRegistrationInvalid();

          if (registertype=='frompage') {
            $('.registrationform').removeClass('active-regpage');
            $('.registrationapproval').addClass('active-regpage');
            // $('.registrationapproval p').append('<p>'+data[0]['email']+'</p>');
          }
          else{
            $('.modal-loginreg').removeClass('active-regpage');
            $('.modal-regverificationdone').addClass('active-regpage');
            // $('.modal-regverificationdone p').append('<p class="">'+data[0]['email']+'</p>');
          }
          $('.showregemail').html(email);
        }
      },
      error: function(){
        alert("Error!");
      }
    });

    return false;
  });

  function clearRegistrationInvalid () {
    $('.reg-email').removeClass('invalid-reg');
    $('.reg-warning').removeClass('show-warning');
    $('.reg-password').removeClass('invalid-reg');
    $('.reg-passwordconfirm').removeClass('invalid-reg');
    $('.reg-name').removeClass('invalid-reg');
    $('.reg-dob').removeClass('invalid-reg');
    $('.reg-country').removeClass('invalid-reg');
    $('.reg-address').removeClass('invalid-reg');
    $('.reg-warning').html('');
  }


  // $('.uploadimg').click(function(){
  //   var originimg = $('#cityloc-imgupload').val();
  //   originimg = originimg.split('\\');
  //   var imgname = originimg[2];
  //   // var imgpath = './public/images.'+imgname;
    
  //   $.ajax({
  //     headers: {
  //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //       },
  //     type: "post",
  //     url: "imageuploader_controller",
  //     data: {img:imgname},
  //     dataType: "json",
  //     success: function(data){
  //       $("#imagepreview").attr("src", data);
  //     },
  //     error: function(){
  //       alert("Error!");
  //     }
  //   });
  //   return false;
  // });

});