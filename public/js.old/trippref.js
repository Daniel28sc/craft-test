/* 
 * trippref.js
 */

jQuery(document).ready(function($) {
    var _places;
    
    loadSavedDayItem();
    
    $('#modal-city').on('show.bs.modal', function() {
        var dayNo = $(this).find('input[name="day"]').val(),
            dayItemDetail = $('#day-item-detail-' + dayNo),
            cityIdList = Array();
        
        $(this).find('input[name="city"]').prop('checked', false);
        $(this).find('input[name="city"]').prop('disabled', false);
        
        dayItemDetail.find('.panel-day-item').each(function() {
            cityIdList.push($(this).data('id'));
        });
        
        for (var i = 0; i < cityIdList.length; i++) {
            $(this).find('input[name="city"][value="' + cityIdList[i] + '"]').prop('disabled', true);
        }
    });
    
    $('#modal-place').on('show.bs.modal', function() {
        var dayNo = $(this).find('input[name="day"]').val(),
            cityNo = $(this).find('input[name="city"]').val(),
            placeContainer = $('#city-place-' + dayNo + '-' + cityNo),
            placeIdList = Array();
            
        $(this).find('input[name="place"]').prop('checked', false);
        $(this).find('input[name="place"]').prop('disabled', false);
        
        placeContainer.find('.city-place-item').each(function() {
            placeIdList.push($(this).data('id'));
        });
        
        for (var i = 0; i < placeIdList.length; i++) {
            $(this).find('input[name="place"][value="' + placeIdList[i] + '"]').prop('checked', true);
        }
    });
    
    $(document.body).on('click', '.btn-edit-place', function() {
        var dayNo = $(this).data('day'),
            cityNo = $(this).data('city'),
            city_id = $(this).data('id');
            
        if (city_id == $('#modal-place').find('input[name="id"]').val()) {
            $('#modal-place').modal('show');
            return;
        }
        
        $('#modal-place').find('input[name="day"]').val(dayNo);
        $('#modal-place').find('input[name="city"]').val(cityNo);
        $('#modal-place').find('input[name="id"]').val(city_id);
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/city/places/' + city_id,
            contentType: 'application/json; charset=UTF-8',
            type: 'GET',
            data: { },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    var places = _places = data.places,
                        placeContainer = $('#city-place-container'),
                        markup = '';
                    for (var i = 0; i < places.length; i++) {
                        var place = places[i];
                        markup +=
                            '<div class="col-xs-6 col-md-3">' +
                                '<div class="thumbnail place-option">' +
                                    '<img src="' + place.image_source + '" alt="' + place.title + '">' +
                                    '<div class="caption">' +
                                        '<input type="checkbox" name="place" value="' + place.place_id + '">' + place.title +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                    }                    
                    placeContainer.html(markup);
                    $('#modal-place').modal('show');
                }
            }
        });
    });
    
    $(document.body).on('click', '.place-option', function() {
        var cbPlace = $(this).find('input[name="place"]');
        if (cbPlace.prop('disabled') == false) {
            cbPlace.prop('checked', !cbPlace.prop('checked'));
        }
    });
    
    $('.btn-add-city').click(function() {
        var dayNo = $(this).data('day');
        $('#modal-city').find('input[name="day"]').val(dayNo);
        $('#modal-city').modal('show');
    });
    
    $('.city-option').click(function() {
        if ($(this).find('input[name="city"]').prop('disabled') == false) {
            $(this).find('input[name="city"]').prop('checked', true);
        }
    });
    
    $('#btn-city-select').click(function() {
        var dayNo = $('#modal-city').find('input[name="day"]').val(),
            city_id = $('#modal-city').find('input[name="city"]:checked').val();
            
        if (city_id != undefined) {
            addCity(dayNo, city_id);
        }
        $('#modal-city').modal('hide');
    });
    
    $('#btn-place-select').click(function() {
        var dayNo = $('#modal-place').find('input[name="day"]').val(),
            cityNo = $('#modal-place').find('input[name="city"]').val(),
            selectedPlace = $('#modal-place').find('input[name="place"]:checked'),
            places = Array();
        
        for (var i = 0; i < selectedPlace.length; i++) {
            var place_id = selectedPlace[i].value;
            places.push($.grep(_places, function(e){ return e.place_id == place_id; })[0]);
        }
        updatePlace(dayNo, cityNo, places);
        $('#modal-place').modal('hide');
    });
    
    $('#btn-next').click(function() {
        var form = $('#form-preferencesdata'),
            dayCount = $('#tab-day-item').children().length,
            itemDetails = Array();
        
        for (var i = 0; i < dayCount; i++) {
            var itemContainer = $('#day-item-detail-' + (i + 1)),
                cityNo = 1,
                itemDetail = {
                    'it_day_id' : itemContainer.data('it_day_id'),
                    'date' : itemContainer.data('date'),
                    'cities' : Array()
                };
            
            itemContainer.find('.panel-day-item').each(function() {
                var city = {
                        'city_id' : $(this).data('id'),
                        'places' : Array()
                    },
                    placeContainer = $('#city-detail-' + (i + 1) + '-' + cityNo);
                
                placeContainer.find('.city-place-item').each(function() {
                    city.places.push({
                        'place_id' : $(this).data('id')
                    });
                });
                
                itemDetail.cities.push(city);
                cityNo++;
            });
            
            itemDetails.push(itemDetail);
        }
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/preferencesdata',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            type: 'POST',
            data: { 
                'budget' : form.find('input[name="budget"]').val(),
                'pace' : form.find('input[name="pace"]').val(),
                'mustsee' : form.find('input[name="mustsee"]').val(),
                'cuisine' : form.find('input[name="cuisine"]').val(),
                'art' : form.find('input[name="art"]').val(),
                'entertainment' : form.find('input[name="entertainment"]').val(),
                'adventure' : form.find('input[name="adventure"]').val(),
                'culture' : form.find('input[name="culture"]').val(),
                'nature' : form.find('input[name="nature"]').val(),
                'shopping' : form.find('input[name="shopping"]').val(),
                'sports' : form.find('input[name="sports"]').val(),
                'item_details': JSON.stringify(itemDetails)
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    window.location.replace('/pickitinerators/' + _itineraryId);
                } else {
                    
                }
            }
        });
    });
});
