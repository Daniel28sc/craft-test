<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use Illuminate\Http\Request;

use App\Http\Controllers\PreferencesData_Controller;
use App\MCity;
use App\MPlace;

Route::group(['middleware' => ['web']], function () {
    // HOMEPAGE
    Route::get('/', 'ArrivalReturn_Controller@index');
    // Route::get('/', function () {
    //     return view('homepage');
    // });

    // ITINERATOR
    Route::get('/itinerator', function () {
        return view('itinerator');
    });

    // SAMPLE
    Route::get('/sample', function () {
        return view('sample');
    });

    // HELP
    Route::get('/help', function () {
        return view('help');
    });

    // ABOUT US
    Route::get('/aboutus', function () {
        return view('aboutus');
    });

    // TOU (TERM OF USE)
    Route::get('/tou', function () {
        return view('tou');
    });

    // INPUT ITINERARY PAGES:
    // 1. ARRIVAL RETURN PAGE (HOME)
    // Route::get('arrivalreturn_controller', function () {

    // });

    // 2. INPUT STYLE PAGE
    Route::get('/inputstyle','InputStyle_Controller@indexInputStyle');
    // Route::get('/inputstyle', function () {
    //     return view('inputstyle');
    // });

    // 3. PREFERENCES DATA PAGE
    Route::get('/preferencesdata/{itid}', function ($itid) {
        if ($itid > 0) {
            return Redirect::route('prefdata_route');
        }
        else {
            return redirect('/');
        }
    });
    Route::get('/preferencesdata/{itid}', array(
        'as' => 'prefdata_route',
        'uses' => 'PreferencesData_Controller@show'
    ));
    Route::get('/preferencesdata', function () {
        return redirect('/');
    });

    // 4. PICK ITINERATORS PAGE
    Route::get('/pickitinerators/{itid}', function ($itid) {
        if ($itid > 0) {
            return Redirect::route('pickitinerators_route');
        }
        else {
            return redirect('/');
        }
    });
    Route::get('/pickitinerators/{itid}', array(
        'as' => 'pickitinerators_route',
        'uses' => 'PickItinerators_Controller@showItineratorsList'
    ));
    Route::get('/pickitinerators', function () {
        return redirect('/');
    });

    // 5. SUMMARY PAGE
    Route::get('/summary/{itid}', function ($itid) {
        if ($itid > 0) {
            return Redirect::route('summary_route');
        }
        else {
            return redirect('/');
        }
    });
    Route::get('/summary/{itid}',array(
        'as' => 'summary_route',
        'uses' => 'Summary_Controller@show'
    ));
    Route::get('/summary', function () {
        return redirect('/');
    });

    // REGISTRATION PAGE
    Route::get('/registration','Registration_Controller@indexRegistration');

    // LOGIN PAGE
    Route::get('/login','Login_Controller@indexLoginPage');
    // Route::get('/login', function () {
    //     return view('login');
    // });
    Route::post('/login', 'Login_Controller@signin');
    Route::get('/signout', 'Login_Controller@signout');
    Route::post('/register', 'Registration_Controller@register');

    // VALIDATION PAGE
    Route::get('/validation/{verificationtoken}', function ($verificationtoken) {
        if ($verificationtoken != "") {
            return Redirect::route('validation_route');
        }
        else{
            return redirect('/');
        }
    });
    Route::get('/validation/{verificationtoken}', array(
        'as' => 'validation_route',
        'uses' => 'Registration_Controller@indexValidation'
    ));
    Route::get('/validation', function () {
        return redirect('/');
    });
    Route::get('/validation/{verificationtoken}/{it_id}', 'Registration_Controller@validates');

    // USER PROFILE PAGES:
    // 1. PROFILE PAGE
    Route::get('/profile','Profile_Controller@showProfile');

    // 2. MY ITINERARY PAGE
    Route::get('/myitinerary','Profile_Controller@showMyItinerary');

    // 2. PAYMENT HISTORY PAGE
    Route::get('/paymenthistory','Profile_Controller@showPaymentHistory');

    // City
    Route::get('/city/places/{city_id}', 'CityController@places');

    // CRUD Page for city & location
    Route::get('/uploadcitylocation', 'ImageUploader_Controller@showCityLoc');
    Route::delete('/uploadcitylocation/cityid/{city_id}', 'ImageUploader_Controller@deleteCity');
    Route::delete('/uploadcitylocation/placeid/{place_id}', 'ImageUploader_Controller@deletePlace');

    // ALL ADMIN PAGES
    // ADMIN HOME
    Route::get('/adminhome', 'AdminHome_Controller@indexAdminHome');

    // ITINERATORS PAGES
    Route::get('/itineratorssignout', 'ItineratorsPage_Controller@signout');

    // ITINERATORS HOME
    Route::get('/itineratorshome', 'ItineratorsPage_Controller@indexItineratorsPage');

    // ITINERATORS LOGIN INDEX
    Route::get('/itineratorslogin', 'ItineratorsPage_Controller@loginIndexItineratorsPage');

    // ITINERATORS PAGE PROCESS
    Route::post('/itineratorspage_controller', 'ItineratorsPage_Controller@itineratorsPageFormHandler');

    // WORK DAY FORM FOR ITINERATORS
    Route::get('/requestwork', function () {
        return redirect('/');
    });
    Route::get('/requestwork/{itinerators_id}/{it_id}', 'ItineratorsPage_Controller@itineraryRequestForm');
    Route::post('/itineratorspage_controller', 'ItineratorsPage_Controller@itineratorsPageFormHandler');

    // ITINERATORS - MY JOB
    Route::get('/myjob', 'ItineratorsPage_Controller@indexMyJob');

    // ITINERATORS - HISTORY
    Route::get('/itinerators_history', 'ItineratorsPage_Controller@indexItineratorsHistory');

    // ALL POST PROCESSES
    // Route::post('arrivalreturn_controller','ArrivalReturn_Controller@getDestinationOptions');
    Route::post('arrivalreturn_controller','ArrivalReturn_Controller@getArrivalReturnData');
    Route::post('inputstyle_controller','InputStyle_Controller@getCalendarPicked');
    Route::post('preferencesdata','PreferencesData_Controller@insert');
    Route::post('preferencesdata/{itid}','PreferencesData_Controller@tes');
    Route::post('pickitinerators_controller','PickItinerators_Controller@insertItineratorsData');
    Route::post('login_controller','Login_Controller@loginUser');
    Route::post('summary/login_controller','Login_Controller@loginUser');
    Route::post('loginregistration_ajax_controller','LoginRegistration_Ajax_Controller@registrationFromPage');
    Route::post('summary/loginregistration_ajax_controller','LoginRegistration_Ajax_Controller@registrationFromPage');
    Route::post('summary/summary_controller','Summary_Controller@saveItinerary');
    Route::post('profile','Profile_Controller@changeUserData');
    Route::post('profile_controller','Profile_Controller@profileFormHandler');
    Route::post('imageuploader_controller','ImageUploader_Controller@addEditCityPlace');

    // ITINERATORS
    Route::get('/uploaditinerators', 'ItineratorsController@index');
    Route::post('/uploaditinerators/all', 'ItineratorsController@all');
    Route::put('/uploaditinerators', 'ItineratorsController@add');
    Route::post('/uploaditinerators', 'ItineratorsController@update');
    Route::delete('/uploaditinerators/{itinerators_id}', 'ItineratorsController@delete');



});
