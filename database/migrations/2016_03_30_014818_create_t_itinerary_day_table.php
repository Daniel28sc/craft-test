<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTItineraryDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_itinerary_day', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('itday_id');
            $table->integer('it_id')->unsigned();
            $table->string('date');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });

        Schema::table('t_itinerary_day', function($table) {
            $table->foreign('it_id')->references('it_id')
                ->on('t_itinerary')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_itinerary_day');
    }
}
