<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_payment', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('payment_id');
            $table->integer('it_id');
            $table->integer('user_id');
            $table->integer('billing');
            $table->integer('paid');
            $table->string('description');
            $table->enum('type', ['cash', 'bank'])->default('cash');
            $table->enum('status', ['0', '1'])->default('0');
            $table->dateTime('added_on');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_payment');
    }
}
