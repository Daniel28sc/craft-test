<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMItineratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_itinerators', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('itinerators_id');
            $table->string('name');
            $table->integer('rating');
            $table->string('location');
            $table->string('photo');
            $table->integer('skill_mustsee')->default('1');
            $table->integer('skill_cuisine')->default('1');
            $table->integer('skill_art')->default('1');
            $table->integer('skill_entertainment')->default('1');
            $table->integer('skill_adventure')->default('1');
            $table->integer('skill_culture')->default('1');
            $table->integer('skill_nature')->default('1');
            $table->integer('skill_shopping')->default('1');
            $table->integer('skill_sports')->default('1');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_itinerators');
    }
}
