<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTItineraryItineratorsRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('t_itinerary_itinerators_relation', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('it_id')->unsigned();
            $table->integer('itinerators_id')->unsigned();
            // $table->integer('picked_itinerators')->unsigned();
            $table->integer('work_day');
            $table->enum('selected_itinerator', ['true', 'false'])->default('false');
            $table->enum('pick_type', ['random', 'manual'])->default('random');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });

        Schema::table('t_itinerary_itinerators_relation', function($table) {
            $table->foreign('it_id')->references('it_id')
                ->on('t_itinerary')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('itinerators_id')->references('itinerators_id')
                ->on('m_itinerators')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_itinerary_itinerators_relation');
    }
}
