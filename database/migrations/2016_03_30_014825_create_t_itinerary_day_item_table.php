<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTItineraryDayItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('t_itinerary_day_item', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('itday_id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });

        Schema::table('t_itinerary_day_item', function($table) {
            $table->foreign('itday_id')->references('itday_id')
                ->on('t_itinerary_day')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('place_id')->references('place_id')
                ->on('m_place')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('city_id')->references('city_id')
                ->on('m_city')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_itinerary_day_item');
    }
}
