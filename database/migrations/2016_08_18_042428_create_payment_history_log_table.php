<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistoryLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_history_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('log_id');
            $table->integer('it_id');
            $table->integer('user_id');
            $table->string('description');
            $table->dateTime('added_on');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_history_log');
    }
}
