<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_place', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('place_id');
            $table->integer('city_id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->string('image_source');
            $table->enum('status', ['1', '2', '3', '4', '5'])->default('1');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });

        Schema::table('m_place', function($table) {
            $table->foreign('city_id')->references('city_id')
                ->on('m_city')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_place');
    }
}
