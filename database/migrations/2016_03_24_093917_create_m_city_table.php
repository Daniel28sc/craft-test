<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_city', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('city_id');
            $table->string('title');
            $table->string('initial');
            $table->string('description');
            $table->string('image_source');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_city');
    }
}
