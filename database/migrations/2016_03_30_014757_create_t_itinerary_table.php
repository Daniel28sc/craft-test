<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTItineraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_itinerary', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('it_id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('day');
            $table->integer('budget')->default('1');
            $table->integer('pace')->default('1');
            $table->integer('mustsee')->default('1');
            $table->integer('cuisine')->default('1');
            $table->integer('art')->default('1');
            $table->integer('entertainment')->default('1');
            $table->integer('adventure')->default('1');
            $table->integer('culture')->default('1');
            $table->integer('nature')->default('1');
            $table->integer('shopping')->default('1');
            $table->integer('sports')->default('1');
            $table->enum('del', ['0', '1'])->default('0');
            $table->timestamps();
        });

        Schema::table('t_itinerary', function($table) {
            $table->foreign('user_id')->references('user_id')
                ->on('m_user')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_itinerary');
    }
}
