<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMAirportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_airport', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->string('icao_code', 4);
            $table->string('iata_code', 3);
            $table->string('airport_name');
            $table->string('city');
            $table->string('country');
            $table->string('latitude_degrees', 2);
            $table->string('latitude_minutes', 2);
            $table->string('latitude_seconds', 2);
            $table->string('latitude_direction', 1);
            $table->string('longitude_degrees', 2);
            $table->string('longitude_minutes', 2);
            $table->string('longitude_seconds', 2);
            $table->string('longitude_direction', 1);
            $table->string('altitude', 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_airport');
    }
}
