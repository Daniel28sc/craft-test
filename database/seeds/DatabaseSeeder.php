<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(MCityTableSeeder::class);
        $this->call(MPlaceTableSeeder::class);

        Model::reguard();
    }
}
