<?php

use Illuminate\Database\Seeder;

class MPlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_place')->insert([
            'city_id'  => 1,
            'title'  => 'Tokyo Tower',
            'description'  => 'Tokyo Tower is a communications and observation tower located in the Shiba-koen district of Minato, Tokyo, Japan. At 332.9 metres, it is the second-tallest structure in Japan.',
            'image_source'    => '../images/1.jpg',
            'status'    => '5',
        ]);

        DB::table('m_place')->insert([
            'city_id'  => 2,
            'title'  => 'Osaka Castle',
            'description'  => "Osaka Castle is a Japanese castle in Chūō-ku, Osaka, Japan. The castle is one of Japan's most famous landmarks and it played a major role in the unification of Japan during the sixteenth century of the Azuchi-Momoyama period",
            'image_source'    => '../images/2.jpg',
            'status'    => '3',
        ]);

        DB::table('m_place')->insert([
            'city_id'  => 3,
            'title'  => 'Shinjuku Gyoen',
            'description'  => "Shinjuku Gyoen National Garden is a large park with an eminent garden in Shinjuku and Shibuya, Tokyo, Japan. It was originally a residence of the Naitō family in the Edo period.",
            'image_source'    => '../images/3.jpg',
            'status'    => '2',
        ]);
    }
}
