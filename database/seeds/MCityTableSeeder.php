<?php

use Illuminate\Database\Seeder;

class MCityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_city')->insert([
            'title'  => 'Tokyo',
            'initial'  => 'TOKYO',
            'description'  => 'Tokyo, Japan’s bustling capital, mixes the ultramodern and the traditional, from neon-lit skyscrapers and anime shops to cherry trees and temples. The opulent Meiji Shinto Shrine is known for its towering gate and surrounding forests. The Imperial Palace sits amid sprawling public gardens. The city is famed for its vibrant food scene, and its Shibuya and Harajuku districts are the heart of its trendy teen fashion scene.',
            'image_source'    => '../images/1.jpg',
        ]);

        DB::table('m_city')->insert([
            'title'  => 'Osaka',
            'initial'  => 'OSAKA',
            'description'  => 'Osaka, a large port city and commercial center on the Japanese island of Honshu, is known for its modern architecture, boisterous nightlife and hearty street food. The 16th-century shogunate Osaka Castle is its main historical landmark, surrounded by a moat and park with plum, peach and cherry trees. The Shinto Sumiyoshi-taisha Shrine is among Japan’s oldest.',
            'image_source'    => '../images/2.jpg',
        ]);

        DB::table('m_city')->insert([
            'title'  => 'Shinjuku',
            'initial'  => 'SHINJUKU',
            'description'  => 'Shinjuku is a special ward in Tokyo, Japan. It is a major commercial and administrative centre, housing the busiest railway station in the world and the Tokyo Metropolitan Government Building, the administration centre for the government of Tokyo.',
            'image_source'    => '../images/3.jpg',
        ]);
    }
}
