<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\MPlace;
use DB;

class MCity extends Model
{
    protected $table = 'm_city';

    public static function getCityList()
    {
        return MCity::orderBy('city_id', 'asc')->get();
    	// return MCity::where('del','0')->orderBy('city_id', 'asc')->get();
    }

    public static function getCityPictureList($cityid)
    {
    	return MCity::where('city_id',$cityid)->get()->pluck('image_source')->first();
    }
    
    public static function find($city_id) {
        return MCity::where('city_id', $city_id)->get();
    }

    public function newCity($title, $kanji, $initial, $description, $imageFileType)
    {
        $this->insert([
            'title' => $title,
            'kanji' => $kanji,
            'initial' => $initial,
            'description' => $description       
        ]);

        $cityid = $this->orderBy('city_id', 'desc')->pluck('city_id')->first();
        $imgsource = '../images/city/'.$cityid.'.'.$imageFileType;
        $imgthumb = '../images/city/thumb_'.$cityid.'.'.$imageFileType;
        $this->where('city_id',$cityid)->update([
            'image_source' => $imgsource,
            'image_thumbnail' => $imgthumb       
        ]);

        return $cityid;
    }

    public function editCity($cityid, $title, $kanji, $initial, $description, $editimg, $imageFileType)
    {
        $filetype = $this->where('city_id', $cityid)->pluck('image_source')->first();
        if ($filetype == '') {
            $imageFileType = 'jpg';
            $getfiletype = 'jpg';
        }
        else{
            $filetype = explode('/', $filetype);
            $filetype1 = $filetype[3];
            $filetype1 = explode('.', $filetype1);
            $getfiletype = $filetype1[1];
            $imageFileType = $getfiletype;
        }

        if ($editimg == 0) {
            $this->where('city_id',$cityid)->update([
                'title' => $title,
                'kanji' => $kanji,
                'initial' => $initial,
                'description' => $description       
            ]);
        }
        else{
            $imgsource = '../images/city/'.$cityid.'.'.$imageFileType;
            $imgthumb = '../images/city/thumb_'.$cityid.'.'.$imageFileType;
            $this->where('city_id',$cityid)->update([
                'title' => $title,
                'kanji' => $kanji,
                'initial' => $initial,
                'description' => $description,
                'image_source' => $imgsource,    
                'image_thumbnail' => $imgthumb      
            ]);
        }
        
        return $getfiletype;
    }

    public static function removeCity($city_id) {
        MCity::where('city_id', $city_id)->delete();
        $last_cityid = MCity::max('city_id');
        DB::statement("ALTER TABLE m_city AUTO_INCREMENT = $last_cityid");

        MPlace::where('city_id', $city_id)->delete();
        $mplace = new MPlace();
        $last_placeid = $mplace->max('place_id');
        DB::statement("ALTER TABLE m_place AUTO_INCREMENT = $last_placeid");
        // MCity::where('city_id', $city_id)->update(['del' => 1]);
        // MPlace::where('city_id', $city_id)->update(['del' => 1]);
        return true;
    }
}
