<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MCountry extends Model
{
    protected $table = 'm_country';

    public function getCountry()
    {
    	return $this->orderBy('cid', 'asc')->get();
    }

    public function getCountryByName($name)
    {
    	$name = strtoupper($name);
    	return $this->where('name',$name)->get()->first();
    }
}
