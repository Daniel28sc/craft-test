<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistoryLog extends Model
{
    protected $table = 'payment_history_log';

    public function newPaymentLog($itid, $userid, $msg, $addedon)
    {
    	$this->insert([
            'it_id' => $itid,
            'user_id' => $userid,
            'description' => $msg,
            'added_on' => $addedon
        ]);
    }

    public function getPaymentLogByUserID($userid)
    {
        return PaymentHistoryLog::where('user_id',$userid)->get();
    }
}
