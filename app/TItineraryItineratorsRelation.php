<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TItineraryItineratorsRelation extends Model
{
    protected $table = 't_itinerary_itinerators_relation';

    public static function insertItineraryItineratorsRelation($it_id, $itinerators_id, $pick_type)
    {
        TItineraryItineratorsRelation::insert([
            'it_id' => $it_id,
            'itinerators_id' => $itinerators_id,
            'pick_type' => $pick_type                
        ]);
    }

    public function updateWorkDay($it_id, $itinerators_id, $work_day)
    {
    	$updateworkday = $this->where('it_id',$it_id)->where('itinerators_id',$itinerators_id)->update(['work_day' => $work_day]);
    }

    public static function getItineratorsByItID($itid)
    {
        return TItineraryItineratorsRelation::where('it_id',$itid)->where('del',0)->get();
    }

    public static function deleteItineraryItineratorsRelationByItID($itid)
    {
        TItineraryItineratorsRelation::where('it_id',$itid)->delete();
    }

    public function updateChosenItinerator($itid,$itineratorsid,$workday)
    {
        $relation = $this->where('it_id',$itid)->where('itinerators_id',$itineratorsid)->update(['work_day'=>$workday, 'selected_itinerator'=>'true']);
        // $this->where('it_id',$itid)->where('selected_itinerator','false')->delete();
        $this->where('it_id',$itid)->where('selected_itinerator','false')->update(['del'=>1]);
    }

    public function getRelationByItineratorsId($itinerators_id)
    {
        return TItineraryItineratorsRelation::where('itinerators_id',$itinerators_id)->orderBy('it_id','asc')->get();
    }

    public function getRelationWhichHaveUserId($itinerators_id)
    {
        $list = TItineraryItineratorsRelation::where('itinerators_id',$itinerators_id)->orderBy('it_id','asc')->get();
        $relation = array();
        for ($i=0; $i < count($list); $i++) { 
            $itid = $list[$i]['it_id'];
            $itinerary = TItinerary::getTItineraryByID($itid);
            if ($itinerary['user_id'] != 0 || $itinerary['user_id'] != '') {
                array_push($relation, $list[$i]);
            }
        }

        return $relation;
    }

    public function updateRelationStatus($itid, $itineratorsid, $status)
    {
        $relation = $this->where('it_id',$itid)->where('itinerators_id',$itineratorsid)->update(['status'=>$status]);
    }

    public function getSpecificRelation($itid, $itineratorsid)
    {
        return TItineraryItineratorsRelation::where('it_id',$itid)->where('itinerators_id',$itineratorsid)->get();
    }

    public function updateRevisionNote($itid, $itineratorsid, $revcounter, $note)
    {
        $relation = $this->where('it_id',$itid)->where('itinerators_id',$itineratorsid)->update(['revision_counter'=>$revcounter, 'revision_note'=>$note]);
    }
}
