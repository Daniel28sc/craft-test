<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;

use App\MItinerators as MItinerators;
use App\MUser;

class ItineratorsController extends Controller
{
    //
    public function index() {
        $userid = session('userid');
        $muser = new MUser();

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }
        
        return view('uploaditinerators')->with('user', $user);
    }
    
    public function all() {
        return Response::json(MItinerators::where('del', 0)->get());
    }
    
    public function add(Request $request) {
        // define('UPLOAD_DIR', base_path('/public/images/itinerators/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/images/itinerators/');
        $itinerator = $request->all()["itinerator"];

        $filetype = $itinerator['filetype'];
        $dataimg = $itinerator['imgdata'];
        $dataimg1 = str_replace('data:image/png;base64,', '', $dataimg);
        $dataimg1 = str_replace(' ', '+', $dataimg1);
        $imgdata = base64_decode($dataimg1);

        $itinerators_id = MItinerators::add($itinerator, $filetype);
        $newfilename = $itinerators_id.'.'.$filetype;
        $newtarget_dir = UPLOAD_DIR; 
        if (!file_exists($newtarget_dir)) {
            mkdir($newtarget_dir, 0777, true);
        }
        $newtarget_file = $newtarget_dir . $newfilename;
        $file = $newtarget_file;
        $success = file_put_contents($file, $imgdata);

        if ($itinerators_id > 0) {
            return Response::json(array('success' => true));
        } else {
            return Response::json(array('success' => false));
        }
    }
    
    public function update(Request $request) {
        // define('UPLOAD_DIR', base_path('/public/images/itinerators/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/images/itinerators/');
        $itinerator = $request->all()['itinerator'];
        $itinerators_id = $itinerator['itinerators_id'];

        $mitinerator = new MItinerators();
        $itineratordata = $mitinerator->getItineratorByID($itinerators_id);
        $iphoto = $itineratordata[0]['photo'];
        $iphoto = explode('/', $iphoto);
        $iphoto1 = $iphoto[3];
        $iphotoname = explode('.', $iphoto1);
        $iphototype = $iphotoname[1];

        $filetype = $itinerator['filetype'];
        $dataimg = $itinerator['imgdata'];
        if ($filetype != '' || $dataimg != '') {
            $filetype = $iphototype;
            $dataimg1 = str_replace('data:image/png;base64,', '', $dataimg);
            $dataimg1 = str_replace(' ', '+', $dataimg1);
            $imgdata = base64_decode($dataimg1);

            $newfilename = $itinerators_id.'.'.$filetype;
            $newtarget_dir = UPLOAD_DIR; 
            if (!file_exists($newtarget_dir)) {
                mkdir($newtarget_dir, 0777, true);
            }
            $newtarget_file = $newtarget_dir . $newfilename;
            $file = $newtarget_file;
            $success = file_put_contents($file, $imgdata);
        }
        

        if (MItinerators::modify($itinerator, $filetype) > 0) {
            return Response::json(array('success' => true));
        } else {
            return Response::json(array('success' => false));
        }
    }
    
    public function delete($itinerators_id) {
        // define('UPLOAD_DIR', base_path('/public/images/itinerators/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/images/itinerators/');
        
        $mitinerator = new MItinerators();

        $itineratorsdata = $mitinerator->getItineratorByID($itinerators_id);
        $imgfile = $itineratorsdata[0]['photo'];
        if ($imgfile != '') {
            $imgfile = explode('/', $imgfile);
            $imgfilename = $imgfile[3];
            $imgfilename = explode('.', $imgfilename);
            $imagefiletype = $imgfilename[1];
            $imagetobedeleted = UPLOAD_DIR.$itinerators_id.'.'.$imagefiletype;
            if (file_exists($imagetobedeleted)) {
                unlink($imagetobedeleted);
            }  
        }

        if (MItinerators::remove($itinerators_id) > 0) {
            return Response::json(array('success' => true));
        } else {
            return Response::json(array('success' => false));
        }
    }
}
