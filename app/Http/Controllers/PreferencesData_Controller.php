<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Response;

use App\MUser;
use App\MCity as MCity;
use App\MPlace as MPlace;
use App\TItinerary as TItinerary;
use App\TItineraryDay as TItineraryDay;
use App\TItineraryDayItem as TItineraryDayItem;
use App\HistoryLog;
use Carbon\Carbon;

class PreferencesData_Controller extends Controller
{
    public function show($it_id)
    {
        session(['temp_itid' => $it_id]);
        $userid = session('userid');
        $muser = new MUser();

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }

    	return view('preferencesdata')->with('user', $user)->with('data', array(
            'itineraryId' => $it_id,
            'cityList' => MCity::getCityList(),
            'placeList' => MPlace::getAllPlace(),
            'itinerary' => TItinerary::getTItineraryByID($it_id),
            'itineraryDay' => TItineraryDay::find($it_id),
            'itineraryDayItem' => TItineraryDayItem::findByItId($it_id)
        ));
    }

    public function insert(Request $request)
    {
        $titinerary = new TItinerary();
        $titineraryday = new TItineraryDay();
        $historylog = new HistoryLog();
        $carbon = new Carbon();
        $data = $request->all();
        
        $it_id = session('temp_itid');
        $item_details = json_decode($data['item_details']);
        $is_same = json_decode($data['is_same']);
        $is_same_hotel = json_decode($data['is_same_hotel']);
        
        $type = $data['type'];
        $budget = $data['budget'];
        // $pace = $data['pace'];
        $pace = "1";
        $mustsee = $data['mustsee'];
        $cuisine = $data['cuisine'];
        $history = $data['history'];
        $entertainment = $data['entertainment'];
        // $adventure = $data['adventure'];
        $adventure = "1";
        $culture = $data['culture'];
        $nature = $data['nature'];
        $shopping = $data['shopping'];
        $sports = $data['sports'];
        $family = $data['family'];
        $backpack = $data['backpack'];
        $vegetarian = $data['vegetarian'];
        $romantic = $data['romantic'];
        $culinaire = $data['culinaire'];
        $cheapeat = $data['cheapeat'];
        $barclub = $data['barclub'];
        $nightseeing = $data['nightseeing'];

        $itinerary = $titinerary->getTItineraryByID($it_id);

        TItinerary::updatePreferencesData($it_id, $budget, $pace, $mustsee,
                $cuisine, $history, $entertainment, $adventure, $culture, $nature,
                $shopping, $sports, $family, $backpack, $vegetarian, $romantic, $culinaire, $cheapeat, $barclub, $nightseeing);

        //DIUBAH vvv. Bila type=manual, maka insert seperti biasa. Bila type=all, maka tiap it_day_id memiliki item_details yang sama.
        if ($type=='manual') {
            // Bila tidak ada place yg dipilih maka place_id = null.
            for ($i = 0; $i < count($item_details); $i++) {
                TItineraryDayItem::deleteByItDayId($item_details[$i]->it_day_id);
                if ($is_same_hotel[$i] == '') {
                    $is_same_hotel[$i] = 'no';
                }
                if ($is_same_hotel[$i] == 'yes') {
                    $titineraryday->updateSameHotel($item_details[$i]->it_day_id, $item_details[$i-1]->hotel, $is_same_hotel[$i]);
                }
                else{
                    $titineraryday->updateSameHotel($item_details[$i]->it_day_id, $item_details[$i]->hotel, $is_same_hotel[$i]);
                }
                
                if ($is_same[$i] == 'yes') {
                    $titineraryday->updateSameAsPrevious($item_details[$i]->it_day_id, $is_same[$i]);
                    foreach($item_details[$i-1]->cities as $city) {
                        if (count($city->places) == 0) {
                            TItineraryDayItem::insertItem(
                                $item_details[$i]->it_day_id,
                                $city->city_id,
                                null,
                                $city->note,
                                $type
                            );
                        }
                        else{
                            foreach($city->places as $place) {
                                TItineraryDayItem::insertItem(
                                    $item_details[$i]->it_day_id,
                                    $city->city_id,
                                    $place->place_id,
                                    $city->note,
                                    $type
                                );
                            }
                        }
                    }
                    continue;
                }
                else{
                    if ($is_same[$i] == '') {
                        $is_same[$i] = 'no';
                    }
                    $titineraryday->updateSameAsPrevious($item_details[$i]->it_day_id, $is_same[$i]);
                }
                foreach($item_details[$i]->cities as $city) {
                    if (count($city->places) == 0) {
                        TItineraryDayItem::insertItem(
                            $item_details[$i]->it_day_id,
                            $city->city_id,
                            null,
                            $city->note,
                            $type
                        );
                    }
                    else{
                        foreach($city->places as $place) {
                            TItineraryDayItem::insertItem(
                                $item_details[$i]->it_day_id,
                                $city->city_id,
                                $place->place_id,
                                $city->note,
                                $type
                            );
                        }
                    }
                }
            }



            // foreach ($item_details as $item_detail) {
            //     TItineraryDayItem::deleteByItDayId($item_detail->it_day_id);
            //     foreach($item_detail->cities as $city) {
            //         if (count($city->places) == 0) {
            //             TItineraryDayItem::insertItem(
            //                 $item_detail->it_day_id,
            //                 $city->city_id,
            //                 null,
            //                 $city->note,
            //                 $type
            //             );
            //         }
            //         else{
            //             foreach($city->places as $place) {
            //                 TItineraryDayItem::insertItem(
            //                     $item_detail->it_day_id,
            //                     $city->city_id,
            //                     $place->place_id,
            //                     $city->note,
            //                     $type
            //                 );
            //             }
            //         }
            //     }
            // }
        }
        else{
            $itdaylist = TItineraryDay::find($it_id);
            for ($i=0; $i < count($itdaylist); $i++) { 
                $itdayid = $itdaylist[$i]['itday_id'];
                TItineraryDayItem::deleteByItDayId($itdayid);
                $titineraryday->updateSameHotel($itdayid, $item_details[0]->hotel, 'yes');
                $titineraryday->updateSameAsPrevious($itdayid, 'yes');
                for ($j=0; $j < count($item_details[0]->cities); $j++) { 
                    if (count($item_details[0]->cities[$j]->places) == 0) {
                        TItineraryDayItem::insertItem(
                            $itdayid,
                            $item_details[0]->cities[$j]->city_id,
                            null,
                            $item_details[0]->cities[$j]->note,
                            $type
                        );
                    }
                    else{
                        for ($k=0; $k < count($item_details[0]->cities[$j]->places); $k++) { 
                            TItineraryDayItem::insertItem(
                                $itdayid,
                                $item_details[0]->cities[$j]->city_id,
                                $item_details[0]->cities[$j]->places[$k]->place_id,
                                $item_details[0]->cities[$j]->note,
                                $type
                            );
                        }
                    }
                }
            }
        }
        $msg = 'Edited preferences for itinerary '.$it_id.' ('.$itinerary['name'].').';
        $datetime = Carbon::now();
        $historylog->newLog($it_id, $msg, $datetime);
        
        // ^^^
        
        // foreach ($item_details as $item_detail) {
        //     TItineraryDayItem::deleteByItDayId($item_detail->it_day_id);
        //     foreach($item_detail->cities as $city) {
        //         foreach($city->places as $place) {
        //             TItineraryDayItem::insertItem(
        //                 $item_detail->it_day_id,
        //                 $city->city_id,
        //                 $place->place_id,
        //                 $type
        //             );
        //         }
        //     }
        // }
        echo json_encode(array('success' => true));
    }

    public function tes(Request $request)
    {
        $cityid = $request->cityid;
        $dummy_date = $request->dummydate;
        $loclist = array();
        $locationlist = MPlace::getPlaceByCity($cityid);
        foreach ($locationlist as $loc) {
            array_push($loclist, ['loc'=>$loc, 'dummy_date'=>$dummy_date]);
        }
        return Response::json($loclist);
    }
}
