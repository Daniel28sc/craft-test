<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Response;

use App\MCity;
use App\MPlace;
use App\MUser;

class ImageUploader_Controller extends Controller
{
    public function showCityLoc()
    {
        $mcity = new MCity();
        $cityList = $mcity->getCityList();

        $mplace = new MPlace();
        $placeList = $mplace->getAllPlace();

        $userid = session('userid');
        $muser = new MUser();

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }

        return view('uploadcitylocation')->with('cityList',$cityList)->with('placeList',$placeList)->with('user', $user);
    }

    public function previewCityLocImg(Request $request)
    {
    	$data = $request->all();
    	$imgname = $data['img'];
    	$imgpath = '../public/images/'.$imgname;

    	return Response::json($imgpath);
    }

    public function checkBrowser()
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $agent = $_SERVER['HTTP_USER_AGENT'];
        }
        if (strlen(strstr($agent, 'Firefox')) > 0) {
            $browser = 'firefox';
        }
        else{
            $browser = '';
        }

        return $browser;
    }

    public function addEditPlacePrefData($data)
    {
        $cityorloc = $data['cityorloc'];
        if ($cityorloc=="location") {
            $pref = 'addplace';
        }
        else{
            $pref = 'editplace';
        }

        if (isset($data[$pref.'-family'])) {
            $family = 'yes';
        }
        else{
            $family = 'no';
        }

        if (isset($data[$pref.'-backpack'])) {
            $backpack = 'yes';
        }
        else{
            $backpack = 'no';
        }

        if (isset($data[$pref.'-vegetarian'])) {
            $vegetarian = 'yes';
        }
        else{
            $vegetarian = 'no';
        }

        if (isset($data[$pref.'-romantic'])) {
            $romantic = 'yes';
        }
        else{
            $romantic = 'no';
        }

        if (isset($data[$pref.'-culinaire'])) {
            $culinaire = 'yes';
        }
        else{
            $culinaire = 'no';
        }

        if (isset($data[$pref.'-cheapeat'])) {
            $cheapeat = 'yes';
        }
        else{
            $cheapeat = 'no';
        }

        if (isset($data[$pref.'-barclub'])) {
            $barclub = 'yes';
        }
        else{
            $barclub = 'no';
        }

        if (isset($data[$pref.'-nightseeing'])) {
            $nightseeing = 'yes';
        }
        else{
            $nightseeing = 'no';
        }

        if (isset($data[$pref.'-popularplace'])) {
            $popularplace = 'yes';
        }
        else{
            $popularplace = 'no';
        }

        if (isset($data[$pref.'-culture'])) {
            $culture = 'yes';
        }
        else{
            $culture = 'no';
        }

        if (isset($data[$pref.'-cuisine'])) {
            $cuisine = 'yes';
        }
        else{
            $cuisine = 'no';
        }

        if (isset($data[$pref.'-nature'])) {
            $nature = 'yes';
        }
        else{
            $nature = 'no';
        }

        if (isset($data[$pref.'-entertainment'])) {
            $entertainment = 'yes';
        }
        else{
            $entertainment = 'no';
        }

        if (isset($data[$pref.'-history'])) {
            $history = 'yes';
        }
        else{
            $history = 'no';
        }

        if (isset($data[$pref.'-shopping'])) {
            $shopping = 'yes';
        }
        else{
            $shopping = 'no';
        }

        if (isset($data[$pref.'-sports'])) {
            $sports = 'yes';
        }
        else{
            $sports = 'no';
        }

        $placedata = [
            'family' => $family,
            'backpack' => $backpack,
            'vegetarian' => $vegetarian,
            'romantic' => $romantic,
            'culinaire' => $culinaire,
            'cheapeat' => $cheapeat,
            'barclub' => $barclub,
            'nightseeing' => $nightseeing,
            'popularplace' => $popularplace,
            'culture' => $culture,
            'cuisine' => $cuisine,
            'nature' => $nature,
            'entertainment' => $entertainment,
            'history' => $history,
            'shopping' => $shopping,
            'sports' => $sports
        ];

        return $placedata;
    }

    public function addEditCityPlace(Request $request)
    {
        $browser = $this->checkBrowser();   
        // define('UPLOAD_DIR', base_path('/public/images/city/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/images/city/');
        $movefile = 1;

        $mcity = new MCity();
    	$mplace = new MPlace();

    	$data = $request->all();
        $cityorloc = $data['cityorloc'];
    	$title = $data['title'];
    	$description = $data['description'];

        if (isset($data['kanji'])) {
            $kanji = $data['kanji'];
        }
        else{
            $kanji = '';
        }

        if ($cityorloc == 'editlocation') {
            if (isset($data['placeimgindex'])) {
                $placeimgindex = $data['placeimgindex'];
            }
            else{
                $placeimgindex[0] = -1;
                $arr_placeimgpath[1] = '';
                $arr_placeimgpath[2] = '';
                $arr_placeimgpath[3] = '';
                $arr_placeimgpath[4] = '';
                $arr_placeimgpath[5] = '';
                $hiddenimgpath_filename = '';
            }
            
        }

        if (isset($data['hiddenimgpath'])) {
            // $hiddenimgpath = $data['hiddenimgpath'];
            // $hiddenimgpath_filepath = explode('\\', $hiddenimgpath);
            // if ($browser == 'firefox') {
            //     $hiddenimgpath_filepath = $hiddenimgpath_filepath[0];
            // }
            // else{
            //     $hiddenimgpath_filepath = $hiddenimgpath_filepath[2];
            // }
            // $hiddenimgpath_filetype = explode('.', $hiddenimgpath_filepath);
            if ($cityorloc == 'editlocation') {
                $arr_placeimgpath[1]='';
                $arr_placeimgpath[2]='';
                $arr_placeimgpath[3]='';
                $arr_placeimgpath[4]='';
                $arr_placeimgpath[5]='';
            }
            else{
                $arr_placeimgpath = array();
            }

            $arr_filename = array();
            if ($cityorloc=='city' || $cityorloc=='editcity') {
                $hiddenimgpath = $data['hiddenimgpath'];
                $hiddenimgpath_filepath = explode('\\', $hiddenimgpath);
                if ($browser == 'firefox') {
                    $hiddenimgpath_filepath = $hiddenimgpath_filepath[0];
                }
                else{
                    $hiddenimgpath_filepath = $hiddenimgpath_filepath[2];
                }
                $hiddenimgpath_filetype = explode('.', $hiddenimgpath_filepath);
                $hiddenimgpath_filename = $hiddenimgpath_filetype[0];
            }
            else{
                $placeimgpath = $data['hiddenimgpath'];
                $x = 0;
                $originalfilename_index = 0;
                foreach ($placeimgpath as $placeimgpath) {
                    $hiddenimgpath_filepath = explode('\\', $placeimgpath);
                    if ($browser == 'firefox') {
                        $hiddenimgpath_filepath = $hiddenimgpath_filepath[0];
                    }
                    else{
                        $hiddenimgpath_filepath = $hiddenimgpath_filepath[2];
                    }

                    if ($cityorloc == 'location') {
                        array_push($arr_placeimgpath, $hiddenimgpath_filepath);
                        $hiddenimgpath_filetype = explode('.', $hiddenimgpath_filepath);
                        $hiddenimgpath_filename = $hiddenimgpath_filetype[0];
                        array_push($arr_filename, $hiddenimgpath_filename);
                    }
                    else{
                        $idx = $placeimgindex[$x];
                        $arr_placeimgpath[$idx] = $hiddenimgpath_filepath;
                        $hiddenimgpath_filetype = explode('.', $hiddenimgpath_filepath);
                        $hiddenimgpath_filename = $hiddenimgpath_filetype[0];
                        $arr_filename[$idx] = $hiddenimgpath_filename;
                        if ($arr_filename[$idx] != '') {
                            $originalfilename_index = $idx;
                        }
                        $x++;
                    }
                }
            }
            $hiddenimgpath_filetype1 = 'png';
            
            // $hiddenimgpath_filetype1 = $hiddenimgpath_filetype[1];
            // $hiddenimgpath_filename = $hiddenimgpath_filetype[0];
        }
        else{
            $hiddenimgpath_filetype1 = '';
            $hiddenimgpath_filename = '';
        }

        if (isset($data['hiddenimgval'])) {
            if ($cityorloc=='city' || $cityorloc=='editcity') {
                $dataimg = $data['hiddenimgval'];
                $dataimg1 = str_replace('data:image/png;base64,', '', $dataimg);
                $dataimg1 = str_replace(' ', '+', $dataimg1);
                $imgdata = base64_decode($dataimg1);
            }
            else{
                $arr_dataimg = $data['hiddenimgval'];
                $arr_placeimgval = array();
                foreach ($arr_dataimg as $arr_dataimg) {
                    $dataimg1 = str_replace('data:image/png;base64,', '', $arr_dataimg);
                    $dataimg1 = str_replace(' ', '+', $dataimg1);
                    $imgdata = base64_decode($dataimg1);
                    array_push($arr_placeimgval, $imgdata);
                }
            }
            // $originalfilename = $hiddenimgpath_filename;
            // $imageFileType = $hiddenimgpath_filetype1;
            
            if ($cityorloc=='city' || $cityorloc=='editcity') {
                $originalfilename = $hiddenimgpath_filename;
            }
            else{
                if ($hiddenimgpath_filename == '') {
                    $originalfilename = '';
                }
                else{
                    $originalfilename = $arr_filename[$originalfilename_index];
                }
            }

            $imageFileType = 'png';
        }
        else{
            $originalfilename = '';
            $imageFileType = '';
        }

        if ($cityorloc == "city") {
            $initial = $data['initial'];
            $newcityid = $mcity->newCity($title, $kanji, $initial, $description, $imageFileType);
            $newfilename = $newcityid.'.'.$imageFileType;
            $newtarget_dir = UPLOAD_DIR;
        }
        elseif ($cityorloc == "location") {
            $cityid = $data['city-id'];
            $status = $data['status'];
            $hashtag = $data['hashtag'];
            $placedata = $this->addEditPlacePrefData($data);

            $numplaceimg = count($arr_placeimgpath);
            $newplaceid = $mplace->newPlace($cityid, $title, $kanji, $description, $status, $hashtag, $imageFileType, $numplaceimg, $placedata['family'], $placedata['backpack'], $placedata['vegetarian'], $placedata['romantic'], $placedata['culinaire'], $placedata['cheapeat'], $placedata['barclub'], $placedata['nightseeing'], $placedata['popularplace'], $placedata['culture'], $placedata['cuisine'], $placedata['nature'], $placedata['entertainment'], $placedata['history'], $placedata['shopping'], $placedata['sports']);
            // $newfilename = $newplaceid.'.'.$imageFileType;
            $i=1;
            foreach ($arr_placeimgpath as $arr_placeimgpath) {
                $newfilename[$i] = $newplaceid.'_'.$i;
                // $newfilename[$i] = $newplaceid.'_'.$i.'.'.$imageFileType;
                $i++;
            }
            $newtarget_dir = UPLOAD_DIR.$cityid.'/';
        }
        elseif ($cityorloc == 'editcity') {
            $cityid = $data['editcityid'];
            $initial = $data['initial'];
            if ($originalfilename == '') {
                $editimg=0;
                $movefile=0;
            }
            else{
                $editimg=1;
            }
            $getfiletype = $mcity->editCity($cityid, $title, $kanji, $initial, $description, $editimg, $imageFileType);
            
            $imageFileType = $getfiletype;
            $newfilename = $cityid.'.'.$imageFileType;
            $newtarget_dir = UPLOAD_DIR;            
        }
        else{
            $placeid = $data['editplaceid'];
            $cityid = $data['city-id'];
            $status = $data['status'];
            $hashtag = $data['hashtag'];
            if ($originalfilename == '') {
                $editimg=0;
                $movefile=0;
            }
            else{
                $editimg=1;
            }

            $placedata = $this->addEditPlacePrefData($data);

            $numplaceimg = count($arr_placeimgpath);
            $getfiletype = $mplace->editPlace($placeid, $cityid, $title, $kanji, $description, $status, $hashtag, $editimg, $arr_placeimgpath, $placedata['family'], $placedata['backpack'], $placedata['vegetarian'], $placedata['romantic'], $placedata['culinaire'], $placedata['cheapeat'], $placedata['barclub'], $placedata['nightseeing'], $placedata['popularplace'], $placedata['culture'], $placedata['cuisine'], $placedata['nature'], $placedata['entertainment'], $placedata['history'], $placedata['shopping'], $placedata['sports']);

            $imageFileType = $getfiletype;
            
            $i=1;
            if ($movefile == 1) {
                foreach ($arr_placeimgpath as $arr_placeimgpath) {
                    if ($arr_placeimgpath == '') {
                        $newfilename[$i] = '';
                    }
                    else{
                        $newfilename[$i] = $placeid.'_'.$i;
                    }
                    // $newfilename[$i] = $placeid.'_'.$i.'.'.$imageFileType;
                    $i++;
                }
            }

            $newtarget_dir = UPLOAD_DIR.$cityid."/";
        }

        // Preparing image
        if (!file_exists($newtarget_dir)) {
            mkdir($newtarget_dir, 0777, true);
        }

        if ($cityorloc=='editcity' || $cityorloc=="city") {
            if ($movefile == 1) {
                $newtarget_file = $newtarget_dir . $newfilename;
                $file = $newtarget_file;
                $success = file_put_contents($file, $imgdata);
                $this->makeThumbnails($cityorloc, UPLOAD_DIR, $newfilename, '');
            }
        }
        else{
            if ($movefile == 1) {
                $i=1;
                $j = 0;
                for ($i=1; $i <= count($newfilename); $i++) { 
                    if ($newfilename[$i] == '') {
                        continue;
                    }
                    $newtarget_file = $newtarget_dir . ($newfilename[$i]) . '.' . $imageFileType;
                    $file = $newtarget_file;
                    $success = file_put_contents($file, $arr_placeimgval[$j]);
                    $this->makeThumbnails($cityorloc, $newtarget_dir, $newfilename[$i], '');
                    $j++;
                }
                // foreach ($arr_placeimgval as $arr_placeimgval) {
                //     $newtarget_file = $newtarget_dir . ($newfilename[$i]) . '.' . $imageFileType;
                //     $file = $newtarget_file;
                //     $success = file_put_contents($file, $arr_placeimgval);
                //     $this->makeThumbnails($cityorloc, $newtarget_dir, $newfilename[$i], '');
                //     $i++;
                // }
            }
        }
    	
    	return redirect('/uploadcitylocation');
    }

    public function deleteCity($city_id) {
        //Delete city image file
        // define('UPLOAD_DIR', base_path('/public/images/city/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/images/city/');

        $mcity = new MCity();
        $citydata = $mcity->find($city_id);
        $imgfile = $citydata[0]['image_source'];
        // $imgfile = explode('/', $imgfile);
        // $imgfilename = $imgfile[3];
        // $imgfilename = explode('.', $imgfilename);
        // $imagefiletype = $imgfilename[1];
        $imagefiletype = 'png';
        $cityimagetobedeleted = UPLOAD_DIR.$city_id.'.'.$imagefiletype;     
        $citythumbimagetobedeleted = UPLOAD_DIR.'thumb_'.$city_id.'.'.$imagefiletype;     
        if (file_exists($cityimagetobedeleted)) {
            unlink($cityimagetobedeleted);
        }
        if (file_exists($citythumbimagetobedeleted)) {
            unlink($citythumbimagetobedeleted);
        }

        //Delete place image files
        $mplace = new MPlace();
        $placedata = $mplace->getPlaceByCity($city_id);
        for ($i=0; $i < count($placedata); $i++) { 
            $place_id = $placedata[$i]['place_id'];
            $imgfile1 = $placedata[$i]['image_source'];
            // $imgfile1 = explode('/', $imgfile1);
            // $imgfilename1 = $imgfile1[4];
            // $imgfilename1 = explode('.', $imgfilename1);
            // $imagefiletype1 = $imgfilename1[1];
            $imagefiletype1 = 'png';
            for ($ii=1; $ii < 6; $ii++) { 
                $filetobedeleted = UPLOAD_DIR.$city_id.'/'.$place_id.'_'.$ii.'.'.$imagefiletype1;
                if (file_exists($filetobedeleted)) {
                    unlink($filetobedeleted);
                }
                $filethumbtobedeleted = UPLOAD_DIR.$city_id.'/'.$place_id.'_'.$ii.'_thumb.'.$imagefiletype1;
                if (file_exists($filethumbtobedeleted)) {
                    unlink($filethumbtobedeleted);
                }
            }
        }
        //Delete folder place image files
        $foldertobedeleted = UPLOAD_DIR.$city_id.'/';
        if (is_dir($foldertobedeleted)) {
            chmod($foldertobedeleted,0777);
            rmdir($foldertobedeleted);
        }

        if ($mcity->removeCity($city_id) > 0) {
            return Response::json(array('success' => true));
        } else {
            return Response::json(array('success' => false));
        }
    }

    public function deletePlace($place_id) {
        // define('UPLOAD_DIR', base_path('/public/images/city/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/images/city/');

        $mplace = new MPlace();

        $placedata = $mplace->getPlaceByPlaceID($place_id);
        $cityid = $placedata[0]['city_id'];
        $imgfile = $placedata[0]['image_source'];
        if ($imgfile == '') {
            $imagefiletype = '';
        }
        else{
            // $imgfile = explode('/', $imgfile);
            // $imgfilename = $imgfile[4];
            // $imgfilename = explode('.', $imgfilename);
            // $imagefiletype = $imgfilename[1];
            $imagefiletype = 'png';
        }
        
        for ($i=1; $i < 6; $i++) { 
            $filetobedeleted = UPLOAD_DIR.$cityid.'/'.$place_id.'_'.$i.'.'.$imagefiletype;
            if (file_exists($filetobedeleted)) {
                unlink($filetobedeleted);
            }
            $filethumbtobedeleted = UPLOAD_DIR.$cityid.'/'.$place_id.'_'.$i.'_thumb.'.$imagefiletype;
            if (file_exists($filethumbtobedeleted)) {
                unlink($filethumbtobedeleted);
            }
        }
        

        if ($mplace->removePlace($place_id) > 0) {
            return Response::json(array('success' => true));
        } else {
            return Response::json(array('success' => false));
        }
    }

    function makeThumbnails($cityorloc, $updir, $img, $id)
    {
        $thumbnail_width = 200;
        $thumbnail_height = 200;
        $thumb_beforeword = "thumb";

        if (($cityorloc == 'city') || ($cityorloc == 'editcity')) {
            $arr_image_details = getimagesize("$updir" . "$img"); // pass id to thumb name
        }
        else{
            $imgnameonly = $img;
            $img = $img.".png";
            $arr_image_details = getimagesize("$updir" . "$img"); // pass id to thumb name
        }
        
        $original_width = $arr_image_details[0];
        $original_height = $arr_image_details[1];

        if ($thumbnail_width > $original_width) {
            $thumbnail_width = $original_width;
        }
        if ($thumbnail_height > $original_height) {
            $thumbnail_height = $original_height;
        }

        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_height);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_width);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);
        if ($arr_image_details[2] == 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        }
        if ($arr_image_details[2] == 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        }
        if ($arr_image_details[2] == 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }
        if ($imgt) {
            $old_image = $imgcreatefrom("$updir" . "$img");
            $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
            // Change black space to white
            // $backgroundColor = imagecolorallocate($new_image, 255, 255, 255);
            // imagefill($new_image, 0, 0, $backgroundColor);
            
            // Change black space to transparent
            // $transparentbg = imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
            // imagefill($new_image, 0, 0, $transparentbg);

            // imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            // imagecopyresampled ($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            // imagecopy($new_image, $old_image, $dest_x, $dest_y, 0, 0, $original_width, $original_height);
            imagecopymerge ($new_image, $old_image, $dest_x, $dest_y, 0, 0, $original_width, $original_height, 100);

            if (($cityorloc == 'city') || ($cityorloc == 'editcity')) {
                $imgt($new_image, "$updir" . "$thumb_beforeword" . '_' . $id . "$img"); 
            }
            else{
                $imgt($new_image, "$updir" . "$imgnameonly". '_' . "$thumb_beforeword" . '.png'); 
            }
        }
    }
}
