<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;
use Mail;
use Carbon\Carbon;

use App\MUser;

class LoginRegistration_Ajax_Controller extends Controller
{
	public function registrationFromPage(Request $request)
	{
		$muser = new MUser();

		$val = $request->all();

		$email = $val['email'];
		$password = $val['password'];
		$passwordconfirm = $val['passwordconfirm'];
		$name = $val['name'];
		$dob = $val['dob'];
		$country = $val['country'];
		$address = $val['address'];
		$registertype = $val['registertype'];
		
		if ($email == '' || $password == '' || $passwordconfirm == '' || $name == '' || $dob == '' || $country == '' || $address == '') {
			$msg = "All fields must be filled.";
		}
		else if ($password!=$passwordconfirm) {
			$msg = "Password confirm error.";
		}
		else{
			$msg = "";

			$userid = $muser->newUser($email, $password, $name, $dob, $country, $address);

			$validation_token = (($userid * 2) + 1515)*7;
			
			$muser->updateValidateToken($userid, $validation_token);
			$this->sendVerificationEmail($userid);
		}
		
		$array_regdata = array();
		array_push($array_regdata, ['email'=>$email, 'password'=>$password, 'passwordconfirm'=>$passwordconfirm, 'name'=>$name, 'dob'=>$dob, 'country'=>$country, 'address'=>$address, 'msg'=>$msg, 'registertype'=>$registertype]);
		return Response::json($array_regdata);
	}

	public function sendVerificationEmail($userid)
	{
		$user = MUser::where('user_id', '=', $userid)->firstOrFail();
		$carbon = new Carbon();
		
		$now = Carbon::now();
		$now1 = $now->addDays(1)->getTimestamp();
		$token = $user->validation_token;
		$link = 'http://craftrip.com/validation/'.$now1.$token;

        Mail::send('emails.tesemail', ['user' => $user, 'link' => $link], function ($m) use ($user) {
            // $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->password)->subject('Verify Your Account!');
        });
	}
}
