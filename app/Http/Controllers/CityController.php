<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MPlace as MPlace;

class CityController extends Controller
{
    //
    public function places($city_id) {
        echo json_encode( array('success' => true, 'places' => MPlace::getPlaceByCity($city_id)) );
    }
}
