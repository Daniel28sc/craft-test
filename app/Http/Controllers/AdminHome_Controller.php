<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MUser;

class AdminHome_Controller extends Controller
{
    public function indexAdminHome()
    {
        $loggedinuser = session('userid');
        $muser = new MUser();

        if($loggedinuser > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }

        return view('adminhome')->with('user', $user);
    }
}
