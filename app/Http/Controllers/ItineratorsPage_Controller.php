<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MCity;
use App\MPlace;
use App\MItinerators;
use App\TItinerary;
use App\TItineraryDay;
use App\TItineraryDayItem;
use App\TItineraryItineratorsRelation;
use App\HistoryLog;
use App\ItineratorHistoryLog;
use Carbon\Carbon;

class ItineratorsPage_Controller extends Controller
{
	public function indexItineratorsPage()
	{
		$itineratorsid = session('itinerators_loggedin');
		if ($itineratorsid == '') {
			return redirect('itineratorslogin');
		}
		$mcity = new MCity();
    	$mplace = new MPlace();
    	$mitinerators = new MItinerators();
    	$titinerary = new TItinerary();
    	$titineraryday = new TItineraryDay();
    	$titinerarydayitem = new TItineraryDayItem();
    	$iirelation = new TItineraryItineratorsRelation();

    	$itinerators = $mitinerators->getItineratorByID($itineratorsid);
        $iirelation_list = $iirelation->getRelationWhichHaveUserId($itineratorsid);
    	// $iirelation_list = $iirelation->getRelationByItineratorsId($itineratorsid);

    	$itinerary = array();
    	$itinerary_day = array();
    	$itinerary_day_item = array();
    	for ($i=0; $i < count($iirelation_list); $i++) { 
    		$itid = $iirelation_list[$i]['it_id'];
    		$itinerary_data = $titinerary->getTItineraryByID($itid);
    		$itineraryday_data = $titineraryday->getTItineraryDayByItID($itid);
    		$itinerarydayitem_data = $titinerarydayitem->getTItineraryDayItemByItDayID($itid);
    		array_push($itinerary, $itinerary_data);
    		array_push($itinerary_day, $itineraryday_data);
    		array_push($itinerary_day_item, $itinerarydayitem_data);
    	}

		return view('itineratorshome')->with('data', array(
			'itineratorsid' => $itineratorsid,
			'itinerators' => $itinerators,
			'iirelation_list' => $iirelation_list,
			'itinerary' => $itinerary,
			'itinerary_day' => $itinerary_day,
			'itinerary_day_item' => $itinerary_day_item,
			'cityList' => MCity::getCityList(),
            'placeList' => MPlace::getAllPlace()
			));
	}

	public function loginIndexItineratorsPage()
	{
		$itineratorsid = session('itinerators_loggedin');
    	if ($itineratorsid != '') {
			return redirect('itineratorshome');
		}
		return view('itineratorslogin');
	}

    public function itineraryRequestForm($itinerators_id, $it_id)
    {
    	$itineratorsid = session('itinerators_loggedin');
    	if ($itineratorsid == '') {
			return redirect('itineratorslogin');
		}
		$relation = new TItineraryItineratorsRelation();
		$mitinerators = new MItinerators();
        $currentrelation = $relation->getSpecificRelation($it_id, $itinerators_id);
        if (count($currentrelation) > 0) {
            return view('requestwork')->with('data', array(
                'itinerators_id' => $itinerators_id,
                'it_id' => $it_id,
                'cityList' => MCity::getCityList(),
                'placeList' => MPlace::getAllPlace(),
                'itinerary' => TItinerary::getTItineraryByID($it_id),
                'itineraryDay' => TItineraryDay::find($it_id),
                'itineraryDayItem' => TItineraryDayItem::findByItId($it_id),
                'itinerators' => MItinerators::getItineratorsList(),
                'iirelation' => TItineraryItineratorsRelation::getItineratorsByItID($it_id),
                'currentrelation' => $currentrelation,
                'currentitinerator' => $mitinerators->getItineratorByID($itinerators_id)
            ));
        }
    	else{
            return redirect('/itineratorslogin');
        }
    }

    public function indexMyJob()
    {
    	$itineratorsid = session('itinerators_loggedin');
    	if ($itineratorsid == '') {
			return redirect('itineratorslogin');
		}
		$mcity = new MCity();
    	$mplace = new MPlace();
    	$mitinerators = new MItinerators();
    	$titinerary = new TItinerary();
    	$titineraryday = new TItineraryDay();
    	$titinerarydayitem = new TItineraryDayItem();
    	$iirelation = new TItineraryItineratorsRelation();
        $historylog = new HistoryLog();

    	$itinerators = $mitinerators->getItineratorByID($itineratorsid);
    	$iirelation_list = $iirelation->getRelationByItineratorsId($itineratorsid);

    	$itinerary = array();
    	$itinerary_day = array();
    	$itinerary_day_item = array();
        $log = array();
    	for ($i=0; $i < count($iirelation_list); $i++) { 
    		$itid = $iirelation_list[$i]['it_id'];
    		$itinerary_data = $titinerary->getTItineraryByID($itid);
            if (count($itinerary_data) == 0) {
                continue;
            }
    		$itineraryday_data = $titineraryday->getTItineraryDayByItID($itid);
    		$itinerarydayitem_data = $titinerarydayitem->getTItineraryDayItemByItDayID($itid);
            $loglist = $historylog->getLogByItId($itid);
    		array_push($itinerary, $itinerary_data);
    		array_push($itinerary_day, $itineraryday_data);
    		array_push($itinerary_day_item, $itinerarydayitem_data);
            array_push($log, $loglist);
    	}

		return view('myjob')->with('data', array(
			'itineratorsid' => $itineratorsid,
			'itinerators' => $itinerators,
			'iirelation_list' => $iirelation_list,
			'itinerary' => $itinerary,
			'itinerary_day' => $itinerary_day,
			'itinerary_day_item' => $itinerary_day_item,
			'cityList' => MCity::getCityList(),
            'placeList' => MPlace::getAllPlace(),
            'log' => $log
			));
    }

    public function indexItineratorsHistory()
    {
        $mitinerators = new MItinerators();
        $titinerary = new TItinerary();
        $titineraryday = new TItineraryDay();
    	$titinerarydayitem = new TItineraryDayItem();
    	$iirelation = new TItineraryItineratorsRelation();
        $historylog = new HistoryLog();

    	$itineratorsid = session('itinerators_loggedin');
    	if ($itineratorsid == '') {
			return redirect('itineratorslogin');
		}
		$itinerators = $mitinerators->getItineratorByID($itineratorsid);

        $log = array();
        // $relation_list = $iirelation->getRelationByItineratorsId($itineratorsid);
        // for ($i=0; $i < count($relation_list); $i++) { 
        //     $itid = $relation_list[$i]['it_id'];
        //     $loglist = $historylog->getLogByItId($itid);
        //     $itinerarylist = $titinerary->getTItineraryByID($itid);
        //     array_push($log, $loglist);
        // }

        $arr_itid = array();
        $relation_list = $iirelation->getRelationByItineratorsId($itineratorsid);
        for ($i=0; $i < count($relation_list); $i++) { 
            $itid = $relation_list[$i]['it_id'];
            array_push($arr_itid, $itid);
        }

        $logs = $historylog->getAllLog();
        foreach ($logs as $logs) {
            $log_itid = $logs['it_id'];
            $check = array_search($log_itid, $arr_itid); //returns index of the matching it_id in arr_itid
            if ($check<0) {
                continue;
            }
            else{
                array_push($log, $logs);
            }
        }

    	return view('itinerators_history')->with('data', array(
			'itineratorsid' => $itineratorsid,
			'itinerators' => $itinerators,
            'log' => $log
			));
    }

    public function itineratorsPageFormHandler(Request $request)
    {
    	$data = $request->all();
    	$formtype = $data['itineratorspage_formtype'];
       
    	switch ($formtype) {
    		case 'submitrequestwork':
		    	$itineratorsid = $data['itineratorsid'];
		    	$itid = $data['itid'];
    			$check_itineratorsid = $this->submitRequestWorkday($request);
    			if ($check_itineratorsid == true) {
    				return redirect('/itineratorshome');
    			}
    			else{
    				return redirect('requestwork/'.$itineratorsid.'/'.$itid);
    			}
    			break;
    		case 'loginitinerators':
    			$login = $this->itineratorsLoginProcess($request);
    			if ($login == true) {
    				return redirect('/itineratorshome');
    			}
    			else{
    				return redirect('/itineratorslogin');
    			}
    			break;
            case 'uploaditinerarypdf':
                $up = $this->uploadItineraryFile($request);

                if ($up == true) {
                    return redirect('/myjob');
                }
                else{
                    return redirect('/myjob');
                }
                break;
            case 'itineratorconfirm':
                $this->afterItineratorConfirm($request);
                return redirect('/myjob');
                break;
    		default:
    			return redirect('/itineratorslogin');
    			break;
    	}
    }

    public function submitRequestWorkday(Request $request)
    {
    	$data = $request->all();
        $currentitineratorid = session('itinerators_loggedin');
    	$itineratorsid = $data['itineratorsid'];
    	$itid = $data['itid'];
    	$workday = $data['submitworkday'];

        $titinerary = new TItinerary();
    	$titineraryitineratorsrelation = new TItineraryItineratorsRelation();
        $itineratorhistory = new ItineratorHistoryLog();
        $carbon = new Carbon();

    	$relation_list = $titineraryitineratorsrelation->getItineratorsByItID($itid);
    	$check_itineratorsid = false;

    	foreach ($relation_list as $relation_list) {
    		if ($relation_list['itinerators_id'] == $itineratorsid) {
    			$check_itineratorsid = true;
    			break;
    		}
    	}

    	if ($check_itineratorsid == true) {
            if ($currentitineratorid != $itineratorsid) {
                $check_itineratorsid = false;
            }
            else{
                $titineraryitineratorsrelation->updateWorkDay($itid,$itineratorsid,$workday);
                $itinerary = $titinerary->getTItineraryByID($itid);
                $msg = 'Itinerator ID '.$itineratorsid.' submitted workday estimation by '.$workday.' day(s) to work on itinerary '.$itid.' ('.$itinerary['name'].').';
                $datetime = Carbon::now();
                $itineratorhistory->newItineratorLog($itid, $itineratorsid, $msg, $datetime);
            }
    		// return redirect('requestwork/'.$itineratorsid.'/'.$itid);
    	}

    	return $check_itineratorsid;
    }

    public function itineratorsLoginProcess(Request $request)
    {
    	$data = $request->all();
    	$username = $data['username'];
    	$password = $data['password'];

    	$mitinerators = new MItinerators();
    	$itinerator = $mitinerators->getItineratorsFromLogin($username, $password);

    	if (count($itinerator) > 0) {
    		$login = true;
    		session(['itinerators_loggedin' => $itinerator[0]['itinerators_id']]);
    	}
    	else{
    		$login = false;
    	}

    	return $login;
    }

    public function signout()
    {
    	session(['itinerators_loggedin' => '']);
    	return redirect('itineratorslogin');
    }

    public function uploadItineraryFile(Request $request)
    {
        // define('UPLOAD_DIR', base_path('/public/uploads/'));
        define('UPLOAD_DIR', '/home/dev003/public_html/uploads/');
        $titinerary = new TItinerary();
        $historylog = new HistoryLog();
        $iirelation = new TItineraryItineratorsRelation();
        $carbon = new Carbon();
        $data = $request->all();
        $itid = $data['dummy_itid'];
        $itineratorsid = $data['dummy_itineratorid'];
        $file = $data['itinerarypdffile'];
        $filesize = $_FILES["itinerarypdffile"]["size"];
        $filename = basename($_FILES["itinerarypdffile"]["name"]);
        $newfilename = 'itinerary'.$itid.'_by'.$itineratorsid.'.pdf';
        $path = UPLOAD_DIR.$filename;
        $newpath = UPLOAD_DIR.$newfilename;
        $filetype = pathinfo($path,PATHINFO_EXTENSION);
        $filepathfordb = '../uploads/'.$newfilename;
        // $filepathfordb = '../uploads/'.$newfilename;
        if ($filetype == 'pdf' || $filetype == 'PDF') {
            if (move_uploaded_file($_FILES["itinerarypdffile"]["tmp_name"], $newpath)) {
                $titinerary->updateItineraryFile($itid,$filepathfordb);
                $itinerary = $titinerary->getTItineraryByID($itid);
                $msg = 'Itinerator '.$itineratorsid.' uploaded an itinerary file for itinerary '.$itid.' ('.$itinerary['name'].').';
                $datetime = Carbon::now();
                $historylog->newLog($itid, $msg, $datetime);
                $iirelation->updateRelationStatus($itid, $itineratorsid, 'confirmresult');
                $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "confirm result".';
                $datetime = Carbon::now();
                $historylog->newLog($itid, $msg, $datetime);
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public function afterItineratorConfirm(Request $request)
    {
        $data = $request->all();
        $itid = $data['dummy_itid'];
        $itineratorsid = $data['dummy_itineratorid'];
        $iirelation = new TItineraryItineratorsRelation();
        $titinerary = new TItinerary();
        $carbon = new Carbon();
        $historylog = new HistoryLog();

        $itinerary = $titinerary->getTItineraryByID($itid);
        $iirelation->updateRelationStatus($itid, $itineratorsid, 'currently');
        $datetime = Carbon::now();
        $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "currently".';
        $historylog->newLog($itid, $msg, $datetime);
    }
}
