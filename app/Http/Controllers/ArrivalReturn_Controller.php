<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\TItinerary;
use App\TItineraryDay;
use App\TItineraryDayItem;
use App\MAirport;
use App\MCountry;
use App\MItinerators;
use App\MCity;
use App\MPlace;
use App\MUser;

class ArrivalReturn_Controller extends Controller
{
	public function index()
	{
		$userid = session('userid');
		$itid = session('temp_itid');

		$titinerary = new TItinerary();
		$titineraryday = new TItineraryDay();
		$titinerarydayitem = new TItineraryDayItem();
		$mitinerators = new MItinerators();
		$mplace = new MPlace();
		$mcity = new MCity();
		$muser = new MUser();
		$mcountry = new MCountry();

		if($userid > 0){
			$user = $muser->getCurrentLoggedInData();
		}
		else{
			$user = '';
		}

		$featured_destination = $mcity->getCityList();
		if (count($featured_destination)<1) {
			$featured_destination = '';
		}

		$featured_itinerators = $mitinerators->getItineratorsList();
		if (count($featured_itinerators)<1) {
			$featured_itinerators = '';
		}

		$ti_list = $titinerary->getTItineraryByID($itid);
		$tid_list = $titineraryday->getTItineraryDayByItID($itid);
		$tidi_list = $titinerarydayitem->getTItineraryDayItemByItDayID($itid);
		$country = $mcountry->getCountry();

		return view('homepage')->with('user', $user)->with('featured_destination', $featured_destination)->with('featured_itinerators', $featured_itinerators)->with('ti_list', $ti_list)->with('tid_list', $tid_list)->with('tidi_list', $tidi_list)->with('country', $country);
	}

	public function getArrivalReturnData(Request $request)
	{
		$type = $request->arrivalreturntype;
		
		if ($type == 'arrivalreturn') {
			$button = $request->button;
			if($button == "continue"){
				session(['temp_itid' => 0]);
				session(['destination_country' => $request->destination_country]);
				$arrivalairport = $this->getArrivalAirport($request);
				$arrivaldate = $this->getArrivalDate($request);
				$arrivaltime = $this->getArrivalTime($request);

				$returnairport = $this->getReturnAirport($request);
				$returndate = $this->getReturnDate($request);
				$returntime = $this->getReturnTime($request);

				$itinerarynod = $this->getItineraryNoD($request);
				return redirect('/inputstyle');
			}
			else{
				$itid = session('temp_itid');
				return redirect('/preferencesdata/'.$itid);
			}
		}
		else if ($type == 'startover') {
			session(['temp_itid' => 0]);
			return redirect('/');
		}
		else{
			$mcountry = new MCountry();
			$result = array();
			$country = $request->country;
			$selectedcountry = $mcountry->getCountryByName($country);
			$bgcountry = $selectedcountry['image_source'];

			$md = new MAirport();
			$list = $md->getInternationalDestinationByCountry($country);

			$destination = array();
			foreach ($list as $list) {
				array_push($destination, $list);
			}

			array_push($result, $destination);
			array_push($result, $bgcountry);

			// return $destination;
			return json_encode($result);
		}
	}

	public function getDestinationOptions(Request $request)
	{
		$country = $request->country;
		return json_encode($country);
	}

	public function getArrivalAirport(Request $request)
	{
		$arrivalairport = $request->arrivalairport;
		session(['arrivalairport' => $arrivalairport]);
    	// $request->session()->push('arrivalreturn.arrivalairport', $arrivalairport);
		return $arrivalairport;
	}

	public function getArrivalDate(Request $request)
	{
		$arrivaldate = $request->arrivaldate;
		session(['arrivaldate' => $arrivaldate]);
		return $arrivaldate;
	}

	public function getArrivalTime(Request $request)
	{
		$arrivaltime = $request->arrivaltime;
		session(['arrivaltime' => $arrivaltime]);
		return $arrivaltime;
	}

	public function getReturnAirport(Request $request)
	{
		$returnairport = $request->returnairport;
		session(['returnairport' => $returnairport]);
		return $returnairport;
	}

	public function getReturnDate(Request $request)
	{
		$returndate = $request->returndate;
		session(['returndate' => $returndate]);
		return $returndate;
	}

	public function getReturnTime(Request $request)
	{
		$returntime = $request->returntime;
		session(['returntime' => $returntime]);
		return $returntime;
	}

	public function getItineraryNoD(Request $request)
	{
		$itinerarynod = $request->itinerarynod;
		session(['itinerarynod' => $itinerarynod]);
		return $itinerarynod;
	}

	public function printArrivalAirport()
	{
		$arrivalairport='';
		if ($request->session()->has('arrivalairport')) {
    		$arrivalairport = session("arrivalairport");
		}
		return $arrivalairport;
	}

	public function printArrivalDate()
	{
		$arrivaldate='';
		if ($request->session()->has('arrivaldate')) {
    		$arrivaldate = session("arrivaldate");
		}
		return $arrivaldate;
	}

	public function printArrivalTime()
	{
		$arrivaltime='';
		if ($request->session()->has('arrivaltime')) {
    		$arrivaltime = session("arrivaltime");
		}
		return $arrivaltime;
	}

	public function printReturnAirport()
	{
		$returnairport='';
		if ($request->session()->has('returnairport')) {
    		$returnairport = session("returnairport");
		}
		return $returnairport;
	}

	public function printReturnDate()
	{
		$returndate='';
		if ($request->session()->has('returndate')) {
    		$returndate = session("returndate");
		}
		return $returndate;
	}

	public function printReturnTime()
	{
		$returntime='';
		if ($request->session()->has('returntime')) {
    		$returntime = session("returntime");
		}
		return $returntime;
	}

	public function printItineraryNoD()
	{
		$itinerarynod='';
		if ($request->session()->has('itinerarynod')) {
    		$itinerarynod = session("itinerarynod");
		}
		return $itinerarynod;
	}


}
