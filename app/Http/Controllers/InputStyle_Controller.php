<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\MUser;
use App\TItinerary;
use App\TItineraryDay;
use App\TItineraryDayItem;
use App\HistoryLog;
use Carbon\Carbon;

class InputStyle_Controller extends Controller
{
	public function indexInputStyle()
	{
		$userid = session('userid');
        $muser = new MUser();

        $temp_itid = session('temp_itid');
        $tiday = new TItineraryDay();
        $tidaylist = $tiday->getTItineraryDayByItID($temp_itid);

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }

        return view('inputstyle')->with('user',$user)->with('tidaylist',$tidaylist);
	}
	
	public function getCalendarPicked(Request $request)
	{
		$val = $request->all();
		$button = $val['button'];
		if ($button == "NEXT") {
			$inputstylecalendar = $val['inputstylecalendar'];
			// $inputstyletype = $val['inputstyletype'];
			$itinerarynod = $val['itinerarynod'];
			$itineraryname = $val['itineraryname'];

			// echo $itinerarynod;
			session(['inputstylecalendar' => $inputstylecalendar]);
			// session(['inputstyletype' => $inputstyletype]);
			session(['temp_itid' => 0]);
			$itid = $this->insertTItinerary($inputstylecalendar, $itinerarynod, $itineraryname);
			
			session(['temp_itid' => $itid]);
			
			// $itid = session('temp_itid');
			// echo session('inputstyletype');
			// echo session('arrivalairport');
			// echo session('arrivaldate');
			// echo session('arrivaltime');
			// echo session('returnairport');
			// echo session('returndate');
			// echo session('returntime');
			// echo session('itinerarynod');
			// return session('inputstylecalendar');
			return redirect('/preferencesdata/'.$itid);
		}
		elseif ($button == "BACK") {
			return redirect('/index.php');
		}
		else{

		}
		
	}

	public function insertTItinerary($inputstylecalendar, $itinerarynod, $itineraryname)
	{
		$x = new TItinerary();
		$y = new TItineraryDay();
		$z = new TItineraryDayItem();
		$historylog = new HistoryLog();
		$carbon = new Carbon();
		
		$day = $itinerarynod;
		$itineraryid = $x->newTItinerary($day, $inputstylecalendar, $itineraryname);
		$msg = 'Itinerary '.$itineraryid.' ('.$itineraryname.') has been created.';
		$datetime = Carbon::now();
		$historylog->newLog($itineraryid, $msg, $datetime);
		// for ($i=0; $i < count($inputstylecalendar); $i++) { 
		// 	$itinerarydayid = $y->newTItineraryDay($itineraryid,$inputstylecalendar[$i]);
		// 	array_push($temp_itdayid, $itinerarydayid);
		// }
		// session(['temp_itdayid' => $temp_itdayid]);
		return $itineraryid;
	}

}
