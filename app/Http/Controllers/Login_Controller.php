<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MUser as MUser;
use App\TItinerary as TItinerary;
use App\HistoryLog;
use Carbon\Carbon;

class Login_Controller extends Controller
{
    public function indexLoginPage()
    {
        $loggedinuser = session('userid');
        $muser = new MUser();

        if($loggedinuser > 0){
            $user = $muser->getCurrentLoggedInData();
            return redirect('profile');
        }
        else{
            $user = '';
            return view('login')->with('user', $user);
        }        
    }
    
    public function loginUser(Request $request) {
    	$logindata = $request->all();

    	$username = $logindata['username'];
    	$password = $logindata['password'];
        $logintype = $logindata['logintype'];

        $user = MUser::getUserFromLogin($username, $password);
        if (count($user) > 0) {
            session(['userid' => $user[0]->user_id]);
            if ($logintype=='frompage') {
                return redirect('/profile');
            } else {
                $it_id = session('temp_itid');
                return redirect('/summary/'.$it_id);
            }
        } else {
            if ($logintype=='frompage') {
                return redirect('/login');
            } else {
                $it_id = session('temp_itid');
                return redirect('/summary/'.$it_id);
            }
        }
    }
    
    public function signin(Request $request) {
        $data = $request->all();
        
        $username = $data['username'];
        $password = $data['password'];
        $it_id = (array_key_exists('it_id', $data)) ? $data['it_id'] : null;
        
        $user = MUser::getUserFromLogin($username, $password);
        
        if (count($user) > 0) {
            session(['userid' => $user[0]->user_id]);
            if ($it_id != null) TItinerary::updateItineraryUserId($it_id, $user[0]->user_id);
            $msg = 'User ID '.$user.' has saved itinerary '.$it_id;
            $datetime = Carbon::now();
            $historylog->newLog($it_id, $msg, $datetime);
            return json_encode(array('success' => true));
        } else {
            return json_encode(array('success' => false));
        }
    }
    
    public function signout(Request $request) {
        session(['userid' => NULL]);
        return redirect('/');
    }
}
