<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MCity;
use App\MPlace;
use App\MUser;
use App\MItinerators;
use App\MPayment;
use App\TItinerary;
use App\TItineraryDay;
use App\TItineraryDayItem;
use App\TItineraryItineratorsRelation;
use App\HistoryLog;
use App\PaymentHistoryLog;
use App\ItineratorHistoryLog;
use Carbon\Carbon;
use Mail;


class Profile_Controller extends Controller
{
    public function showProfile()
    {
        $userid = session('userid');
        $muser = new MUser();
        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
            return view('profile')->with('user',$user)->with('changepwnotice','');
        }
        else{
            $user = '';
            return redirect('login');
        }
    }

    public function showMyItinerary()
    {
        $userid = session('userid');
    	$itid = session('temp_itid');
    	$itinerary = new TItinerary();
    	$itineraryday = new TItineraryDay();
    	$itinerarydayitem = new TItineraryDayItem();
    	$city = new MCity();
    	$place = new MPlace();
    	$itinerators = new MItinerators();
    	$itineratorsitineraryrelation = new TItineraryItineratorsRelation();
        $historylog = new HistoryLog();
        $itineratorhistory = new ItineratorHistoryLog();

        $muser = new MUser();
        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
            return redirect('login');
        }

        $itinerarylistbyuserid = $itinerary->getTItineraryByUserID($userid);

        $arrayitinerarylist[0] = '';
        for ($i=0; $i < count($itinerarylistbyuserid); $i++) { 
            $list = $itinerary->getTItineraryByID($itinerarylistbyuserid[$i]['it_id']);
            array_push($arrayitinerarylist, $list);
        }
        
        $arrayitinerarydaylist[0] = '';
        for ($i=0; $i < count($itinerarylistbyuserid); $i++) { 
            $listday = $itineraryday->getTItineraryDayByItID($itinerarylistbyuserid[$i]['it_id']);
            array_push($arrayitinerarydaylist, $listday);
        }

        $arrayitineratorslist[0] = '';
        for ($i=0; $i < count($itinerarylistbyuserid); $i++) { 
            $listiirelation = $itineratorsitineraryrelation->getItineratorsByItID($itinerarylistbyuserid[$i]['it_id']);
            array_push($arrayitineratorslist, $listiirelation);
        }

        $arrayitinerarydayitemlist[0] = '';
        for ($i=0; $i < count($itinerarylistbyuserid); $i++) {
            // $listdayitem = $itinerarydayitem->findByItId($itinerarylistbyuserid[$i]['it_id']);
            $listdayitem = $itinerarydayitem->getTItineraryDayItemByItDayID($itinerarylistbyuserid[$i]['it_id']);
            array_push($arrayitinerarydayitemlist, $listdayitem);
        }

    	$itinerarylist = $itinerary->getTItineraryByID($itid);
    	$itinerarydaylist = $itineraryday->getTItineraryDayByItID($itid);
    	$itinerarydayitemlist = $itinerarydayitem->findByItId($itid);
    	$citylist = $city->getCityList();
    	$placelist = $place->getAllPlace();
    	$itineratorslist = $itinerators->getItineratorsList();
    	$iirelationlist = $itineratorsitineraryrelation->getItineratorsByItID($itid);

    	$array_itinerarydayitemlist = array();
    	$array_city[0]="";
    	$array_place[0]="";
    	$array_itinerators[0]="";

    	// foreach ($itinerarydayitemlist as $itinerarydayitemlist) {
    	// 	array_push($array_itinerarydayitemlist, $itinerarydayitemlist);
    	// }

    	foreach ($citylist as $citylist) {
            // if ($citylist['del'] == 1) {
            //     $citylist = '';
            // }
    		array_push($array_city, $citylist);
    	}

    	foreach ($placelist as $placelist) {
    		array_push($array_place, $placelist);
    	}

    	foreach ($itineratorslist as $itineratorslist) {
    		array_push($array_itinerators, $itineratorslist);
    	}

        $array_log[0] = '';
        for ($i=1; $i < count($arrayitinerarylist); $i++) { 
            $itid = $arrayitinerarylist[$i]['it_id'];
            $log_list = $historylog->getLogByItId($itid);
            array_push($array_log, $log_list);
        }

        $itinerator_log[0] = '';
        for ($i=1; $i < count($arrayitineratorslist); $i++) { 
            $itid = $arrayitineratorslist[$i][0]['it_id'];
            $i_log = $itineratorhistory->getItineratorLogByItID($itid);
            $itinerator_log[$i] = $i_log;
        }

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
            return view('myitinerary')->with('user',$user)->with('itinerarylistbyuserid',$itinerarylistbyuserid)->with('arrayitinerarylist',$arrayitinerarylist)->with('arrayitinerarydaylist',$arrayitinerarydaylist)->with('arrayitineratorslist',$arrayitineratorslist)->with('arrayitinerarydayitemlist',$arrayitinerarydayitemlist)->with('citylist',$array_city)->with('placelist',$array_place)->with('itineratorslist',$array_itinerators)->with('iirelationlist',$iirelationlist)->with('history_log',$array_log)->with('itinerator_log',$itinerator_log);
        }

        // return view('myitinerary')->with('itinerarylistbyuserid',$itinerarylistbyuserid)->with('arrayitinerarylist',$arrayitinerarylist)->with('arrayitinerarydaylist',$arrayitinerarydaylist)->with('arrayitineratorslist',$arrayitineratorslist)->with('arrayitinerarydayitemlist',$arrayitinerarydayitemlist)->with('itinerarylist', $itinerarylist)->with('itinerarydaylist', $itinerarydaylist)->with('itinerarydayitemlist', $array_itinerarydayitemlist)->with('citylist',$array_city)->with('placelist',$array_place)->with('itineratorslist',$array_itinerators)->with('iirelationlist',$iirelationlist);
    }

    public function showPaymentHistory()
    {
        $userid = session('userid');
        $muser = new MUser();
        $paymentlog = new PaymentHistoryLog();
        $log = $paymentlog->getPaymentLogByUserID($userid);
        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
            return view('paymenthistory')->with('user',$user)->with('changepwnotice','')->with('log', $log);
        }
        else{
            $user = '';
            return redirect('login');
        }
    }

    public function changeUserData(Request $request)
    {
        $userid = session('userid');
        $muser = new MUser();
        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }
        $changedata = $request->all();
        $changedatatype = $changedata['changedatatype'];
       
        if ($changedatatype == "Change Password") {
            $name = '';
            $country = '';
            $address = '';
            $oldpw = $changedata['oldpw'];
            $newpw = $changedata['newpw'];
            $newpwconfirm = $changedata['newpwconfirm'];
        }
        else{
            $name = $changedata['nama'];
            $country = $changedata['country'];
            $address = $changedata['address'];
            $oldpw = '';
            $newpw = '';
            $newpwconfirm = '';
        }

        if ($changedatatype == 'Change Data') {
            $changepwnotice = "";
        }
        else{
            if ($oldpw == '' || $newpw == '' || $newpwconfirm == '') {
                $changepwnotice = "All fields must be filled.";
            }
            else if ($oldpw != $user[0]['password']) {
                $changepwnotice = 'Wrong old password.';
            }
            else if($newpw != $newpwconfirm){
                $changepwnotice = 'New password confirm not match.';
            }
            else{
                $changepwnotice = 'Password has been changed.';
            }
        }

        if ($changepwnotice == '' || $changepwnotice == 'Password has been changed.') {
            $muser->updateUserData($changedatatype,$userid, $name, $country, $address, $newpw);
        }

        return view('profile')->with('changepwnotice',$changepwnotice)->with('user',$user);
    }

    public function profileFormHandler(Request $request)
    {
        $data = $request->all();
        $profileformtype = $data['profileformtype'];
        
        switch ($profileformtype) {
            case 'chosenitinerator':
                $this->updateWorkAndChosenItinerator($request);
                return redirect('/myitinerary');
                break;
            case 'delitinerary':
                $itid = $data['dummyitid'];
                $this->deleteItineraryList($request);
                $historylog = new HistoryLog();
                $carbon = new Carbon();
                $titinerary = new TItinerary();
                $itinerary = $titinerary->getTItineraryByID($itid);
                $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') has been deleted.';
                $datetime = Carbon::now();
                $historylog->newLog($itid, $msg, $datetime);
                return redirect('/myitinerary');
                break;
            case 'sendrequest':
                $this->sendEmailRequest($request);
                return redirect('/myitinerary');
                break;
            case 'itineraryrevision':
                $this->updateRevisionRequest($request);
                return redirect('/myitinerary');
                break;
            case 'paymentconfirmation':
                $this->updateItineraryPayment($request);
                return redirect('/myitinerary');
                break;
            default:
                return redirect('/myitinerary');
                break;
        }
    }

    public function updateWorkAndChosenItinerator(Request $request)
    {
        $data = $request->all();
        $itid = $data['dummyitid'];
        $val = $data['workestimation'];
        $val = explode('|', $val);
        $workestimation = $val[0];
        $chooseitinerator = $val[1];
        $userid = session('userid');

        $iirelation = new TItineraryItineratorsRelation();
        $iirelation->updateChosenItinerator($itid, $chooseitinerator, $workestimation);
        $iirelation->updateRelationStatus($itid, $chooseitinerator, 'needpayment');
        $historylog = new HistoryLog();
        $carbon = new Carbon();
        $titinerary = new TItinerary();
        $itinerary = $titinerary->getTItineraryByID($itid);
        $msg = 'User '.$userid.' chose an itinerator to work on itinerary '.$itid.'.';
        $datetime = Carbon::now();
        $historylog->newLog($itid, $msg, $datetime);
        $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "need payment".';
        $datetime = Carbon::now();
        $historylog->newLog($itid, $msg, $datetime);
    }

    public function deleteItineraryList(Request $request)
    {
        $data = $request->all();
        $itid = $data['dummyitid'];
        $titinerary = new TItinerary();
        $titinerary->deleteItinerary($itid);
    }

    public function sendEmailRequest(Request $request)
    {
        $data = $request->all();
        $itid = $data['dummyitid'];
        $itineratorsitineraryrelation = new TItineraryItineratorsRelation();
        $mitinerators = new MItinerators();
        $titinerary = new TItinerary();
        $historylog = new HistoryLog();
        $carbon = new Carbon();
        $itinerary = $titinerary->getTItineraryByID($itid);
        $relationlist = $itineratorsitineraryrelation->getItineratorsByItID($itid);
        foreach ($relationlist as $relationlist) {
            $itinerators_id = $relationlist['itinerators_id'];
            $itinerators_data = $mitinerators->getItineratorByID($itinerators_id);
            $itineratorsitineraryrelation->updateRelationStatus($itid, $itinerators_id, 'pickitinerator');
            $email = $itinerators_data[0]['email'];
            if ($email == '') {
                continue;
            }
            // $link = 'http://localhost/development/laravel/itineration/public/requestwork/'.$itinerators_id.'/'.$itid;
            $link = 'http://craftrip.com/requestwork/'.$itinerators_id.'/'.$itid;
            Mail::send('emails.requestworkmail', ['itinerators_data' => $itinerators_data, 'link' => $link], function ($m) use ($itinerators_data) {
                $m->to($itinerators_data[0]->email)->subject('You have a new itinerary request!');
            });
        }
        $msg = 'User '.$itinerary['user_id'].' sent emails for each listed itinerators in itinerary '.$itid.' ('.$itinerary['name'].').';
        $datetime = Carbon::now();
        $historylog->newLog($itid, $msg, $datetime);
        $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "pick itinerator".';
        $datetime = Carbon::now();
        $historylog->newLog($itid, $msg, $datetime);
    }

    public function updateRevisionRequest(Request $request)
    {
        $historylog = new HistoryLog();
        $carbon = new Carbon();
        $iirelation = new TItineraryItineratorsRelation();
        $titinerary = new TItinerary();
        
        $data = $request->all();
        $itid = $data['dummyitid'];
        $itineratorid = $data['dummyitineratorid'];
        $action = $data['revisionoption'];
        $rel_data = $iirelation->getSpecificRelation($itid, $itineratorid);
        $itinerary = $titinerary->getTItineraryByID($itid);
        $rev_counter = $rel_data[0]['revision_counter'];
        if ($action == 'OK') {
            $iirelation->updateRelationStatus($itid, $itineratorid, 'finished');
            $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "finished".';
            $datetime = Carbon::now();
            $historylog->newLog($itid, $msg, $datetime);
        }
        else{
            $note = $data['revisionnote'];
            $new_rev_counter = $rev_counter+1;
            $iirelation->updateRevisionNote($itid, $itineratorid, $new_rev_counter, $note);
            $msg = 'User '.$itinerary['user_id'].' asked revision for itinerary '.$itid.' ('.$itinerary['name'].').';
            $datetime = Carbon::now();
            $historylog->newLog($itid, $msg, $datetime);
            $iirelation->updateRelationStatus($itid, $itineratorid, 'revision');
            $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "revision".';
            $datetime = Carbon::now();
            $historylog->newLog($itid, $msg, $datetime);
        }
    }

    public function updateItineraryPayment(Request $request)
    {
        $mpayment = new MPayment();
        $titinerary = new TItinerary();
        $iirelation = new TItineraryItineratorsRelation();
        $historylog = new HistoryLog();
        $paymenthistorylog = new PaymentHistoryLog();
        $carbon = new Carbon();

        $userid = session('userid');
        $billing = 100000;
        $description = '';
        $status = 1;
        $data = $request->all();
        $itid = $data['dummyitid'];
        $itineratorid = $data['dummyitineratorid'];
        $action = $data['paymentconfirmaction'];
        $rel_data = $iirelation->getSpecificRelation($itid, $itineratorid);
        $itinerary = $titinerary->getTItineraryByID($itid);
        $datetime = Carbon::now();
        if ($action == 'Pay') {
            $type = 'cash';
            $nama = $data['pay-name1'];
            $nominal = $data['pay-nominal1'];
            $msg = 'User '.$itinerary['user_id'].' paid the bill for itinerary '.$itid.' ('.$itinerary['name'].').';
            // $historylog->newLog($itid, $msg, $datetime);
            $paymenthistorylog->newPaymentLog($itid, $userid, $msg, $datetime);
        }
        else{
            $type = 'bank';
            $nama = $data['pay-name2'];
            $nominal = $data['pay-nominal2'];
            $bank = $data['pay-bank'];
            $msg = 'User '.$itinerary['user_id'].' confirmed payment for itinerary '.$itid.' ('.$itinerary['name'].').';
            // $historylog->newLog($itid, $msg, $datetime);
            $paymenthistorylog->newPaymentLog($itid, $userid, $msg, $datetime);
        }
        $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "itineratorconfirmation".';
        // $msg = 'Itinerary '.$itid.' ('.$itinerary['name'].') status has been changed to "currently".';
        $historylog->newLog($itid, $msg, $datetime);
        $iirelation->updateRelationStatus($itid, $itineratorid, 'itineratorconfirmation');
        // $iirelation->updateRelationStatus($itid, $itineratorid, 'currently');
        $mpayment->newPayment($itid, $userid, $billing, $nominal, $description, $type, $status, $datetime);
    }
}