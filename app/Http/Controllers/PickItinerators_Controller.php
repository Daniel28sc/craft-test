<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TItinerary;
use App\MItinerators as MItinerators;
use App\TItineraryItineratorsRelation as TItineraryItineratorsRelation;
use App\MUser;
use App\HistoryLog;
use Carbon\Carbon;

class PickItinerators_Controller extends Controller
{
    public function showItineratorsList($itid)
    {
        session(['temp_itid' => $itid]);
        $userid = session('userid');
        $muser = new MUser();

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }
        
    	$itineratorslist = MItinerators::getItineratorsList();
        $relationlist = TItineraryItineratorsRelation::getItineratorsByItID($itid);
    	return view('pickitinerators')->with('user', $user)->with('itid',$itid)->with('itineratorslist', $itineratorslist)->with('relationlist', $relationlist);
    }

    public function insertItineratorsData(Request $request)
    {
    	$tempitid = session('temp_itid');
    	$val = $request->all();
        $button = $val['button'];

        $titinerary = new TItinerary();
        $historylog = new HistoryLog();
        $carbon = new Carbon();

        if($button == "NEXT"){
            $pick_type = $val['pick_type'];
            $it_id = $tempitid;
            $itinerators_id = $val['itinerators'];
            $itinerary = $titinerary->getTItineraryByID($it_id);

            TItineraryItineratorsRelation::deleteItineraryItineratorsRelationByItID($it_id);
            foreach ($itinerators_id as $itinerators_id) {
                TItineraryItineratorsRelation::insertItineraryItineratorsRelation($it_id, $itinerators_id, $pick_type);
            }
            $msg = 'Edited itinerators for itinerary '.$it_id.' ('.$itinerary['name'].').';
            $datetime = Carbon::now();
            $historylog->newLog($it_id, $msg, $datetime);

            // if ($pick_type == "random") {
            //     // pick_type = random
            //     $itinerators_list = $mitinerators->getItineratorsList();
            //     foreach ($itinerators_list as $itinerators_list) {
            //         $titineraryitineratorsrelation->insertItineraryItineratorsRelation($it_id, $itinerators_id, $pick_type);
            //     }
            // }
            // else{
            //     // pick_type = manual
            //     foreach ($itinerators_id as $itinerators_id) {
            //         $titineraryitineratorsrelation->insertItineraryItineratorsRelation($it_id, $itinerators_id, $pick_type);
            //     }
            // }
            return redirect('/summary/'.$tempitid);
        }
        elseif ($button == "BACK") {
            return redirect('/preferencesdata/'.$tempitid);
        }
        else{
            
        }
    }
}
