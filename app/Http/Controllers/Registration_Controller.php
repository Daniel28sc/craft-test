<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;
use Mail;
use Carbon\Carbon;

use App\Http\Controllers\LoginRegistration_Ajax_Controller;

use App\MUser as MUser;
use App\TItinerary as TItinerary;
use App\HistoryLog;
use Carbon\Carbon;

class Registration_Controller extends Controller
{
    public function indexRegistration() {
        $userid = session('userid');
        $muser = new MUser();

        if($userid > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }
    	return view('registration')->with('user',$user);
    }

    public function indexValidation($verificationtoken) {
        $lrac = new LoginRegistration_Ajax_Controller();
    	$carbon = new Carbon();
		$now = Carbon::now()->getTimestamp();
        $timetoken = substr($verificationtoken, 0, 10);
        $validationtoken = substr($verificationtoken, 10);
        $userid = (($validationtoken/7)-1515)/2;

        $loggedinuser = session('userid');
        $muser = new MUser();

        if($loggedinuser > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }

        if ($this->isUserValidate($userid)) {
            $headmsg = "Your account has been validated.";
            $msg = '<a href="'.url('/').'/login"><button class="tologin">Login Now</button></a>';
        } 
        else{
            if ($now<=$timetoken) {
                MUser::setValid($userid, true);
                $headmsg = "Verification Success.";
                $msg = '<a href="'.url('/').'/login"><button class="tologin">Please Login</button></a>';
            }
            else{
                $headmsg = "Verification Expired";
                $msg = 'Resending verification email....Check your email.';
                $lrac->sendVerificationEmail($userid);
            }
        }
        
    	
    	return view('validation')->with('user',$user)->with('verificationtoken',$verificationtoken)->with('headmsg',$headmsg)->with('msg',$msg)->with('userid',$userid)->with('now',$now)->with('timetoken',$timetoken);
    }
    
    public function register(Request $request) {
        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];
        $it_id = array_key_exists('it_id', $data) ? $data['it_id'] : null;
        
        if ($email == '' || $password == '') {
            return Response::json(array('success' => false));
        } else {
            if (count(MUser::find($email)) > 0) {
                return Response::json(array(
                    'success' => false,
                    'message' => 'Email already registered'
                ));
            } else {
                // $check_user = MUser::getUserFromLogin($email, $password);
                // if (count($check_user) > 0) {
                //     return Response::json(array('success' => false));
                // }
                $user_id = MUser::newUser($email, $password);
                $validation_token = (($user_id * 2) + 1515)*7;
                MUser::updateValidateToken($user_id, $validation_token);
                $this->sendVerificationEmail($user_id, $it_id);
                return Response::json(array('success' => true));
            }
        }
    }
    
    public function validates($token, $it_id) {
    	$carbon = new Carbon();
        $now = Carbon::now()->getTimestamp();
        $timetoken = substr($token, 0, 10);
        $validation_token = substr($token, 10);
        $user_id = (($validation_token / 7) - 1515) / 2;

        $loggedinuser = session('userid');
        $muser = new MUser();

        if($loggedinuser > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }
        
        if ($this->isItineraryHasUserId($it_id, $user_id) == 'hasotherid') {
            $headmsg = "This itinerary already belongs to another user.";
            $msg = '';
        }
        else if($this->isItineraryHasUserId($it_id, $user_id) == 'true'){
            $headmsg = "You have already saved this itinerary.";
            $msg = '<a href="' . url('/') . '/login"><button class="tologin">Please Login</button></a>';
        }
        else{
            if ($this->isUserValidate($user_id)) {
                $headmsg = "Your account has been validated and your itinerary has been saved.";
                TItinerary::updateItineraryUserId($it_id, $user_id);
                $msglog = 'User ID: '.$user.' has saved itinerary '.$it_id;
                $datetime = Carbon::now();
                $historylog->newLog($it_id, $msglog, $datetime);
                $msg = '<a href="'.url('/').'/login"><button class="tologin">Login Now</button></a>';
            }   
            else{
                if ($now <= $timetoken) {
                    MUser::setValid($user_id, true);
                    TItinerary::updateItineraryUserId($it_id, $user_id);
                    $msglog = 'User ID: '.$user.' has saved itinerary '.$it_id;
                    $datetime = Carbon::now();
                    $historylog->newLog($it_id, $msglog, $datetime);
                    $headmsg = "Verification success and your itinerary has been saved.";
                    $msg = '<a href="' . url('/') . '/login"><button class="tologin">Please Login</button></a>';
                } else {
                    $headmsg = "Verification Expired";
                    $msg = 'Resending verification email....Check your email.';
                    $this->sendVerificationEmail($user_id, $it_id);
                }
            }
        }
        
        return view('validation')->with('verificationtoken', $token)
            ->with('headmsg', $headmsg)->with('msg', $msg)
            ->with('userid', $user_id)->with('now', $now)
            ->with('timetoken', $timetoken)->with('user',$user);
    }
    
    public function sendVerificationEmail($user_id, $it_id = null) {
        $user = MUser::where('user_id', '=', $user_id)->firstOrFail();
        $carbon = new Carbon();
        $now = Carbon::now();
        $now1 = $now->addDays(1)->getTimestamp();
        $token = $user->validation_token;
        $link = url('/') . '/validation/' . $now1 . $token;
        if ($it_id != null) $link .= "/".$it_id;

        Mail::send('emails.tesemail', ['user' => $user, 'link' => $link], function ($m) use ($user) {
            // $m->from('hello@app.com', 'Your Application');
            $m->to($user->email, $user->password)->subject('Verify Your Account!');
        });
    }

    public function isUserValidate($userid)
    {   
        $muser = new MUser();
        $usernow = $muser->getUserByID($userid);
        if ($usernow[0]['valid'] == 'yes') {
            return true;
        }
        else{
            return false;
        }
    }

    public function isItineraryHasUserId($it_id, $user_id)
    {
        $titinerary = new TItinerary();
        $itinerary = $titinerary->getTItineraryByID($it_id);

        if ($itinerary['user_id'] != null || $itinerary['user_id'] != '') {
            if ($itinerary['user_id'] != $user_id) {
                return 'hasotherid';
            }
            else{
                return 'true';
            }
        }
        else{
            return 'false';
        }
    }
}
