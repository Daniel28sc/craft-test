<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MUser;
use App\MCity as MCity;
use App\MPlace as MPlace;
use App\TItinerary as TItinerary;
use App\TItineraryDay as TItineraryDay;
use App\TItineraryDayItem as TItineraryDayItem;
use App\MItinerators as MItinerators;
use App\TItineraryItineratorsRelation as TItineraryItineratorsRelation;
use App\HistoryLog;
use Carbon\Carbon;

class Summary_Controller extends Controller
{
    public function show($it_id)
    {
        session(['temp_itid' => $it_id]);
        $loggedinuser = session('userid');
        $muser = new MUser();

        if($loggedinuser > 0){
            $user = $muser->getCurrentLoggedInData();
        }
        else{
            $user = '';
        }

        return view('summary')->with('user',$user)->with('data', array(
            'userId' => session('userid'),
            'cityList' => MCity::getCityList(),
            'placeList' => MPlace::getAllPlace(),
            'itinerary' => TItinerary::getTItineraryByID($it_id),
            'itineraryDay' => TItineraryDay::find($it_id),
            'itineraryDayItem' => TItineraryDayItem::findByItId($it_id),
            'itinerators' => MItinerators::getItineratorsList(),
            'iirelation' => TItineraryItineratorsRelation::getItineratorsByItID($it_id)
        ));
    }

    public function saveItinerary(Request $request)
    {
        $data = $request->all();
        // $user = $data['userid'];
        // $itid = $data['currentitid'];
        $user = session('userid');
        $itid = session('temp_itid');

        $ti = new TItinerary();
        $muser = new MUser();
        $historylog = new HistoryLog();
        $carbon = new Carbon();

        $itinerary = $ti->getTItineraryByID($itid);
        if($itinerary['user_id'] != null || $itinerary['user_id'] != ''){
            if($itinerary['user_id'] != $user){
                return json_encode('false');
            }
            else{
                $msg = 'User ID: '.$user.' has saved itinerary '.$itid.' ('.$itinerary['name'].').';
                $datetime = Carbon::now();
                $historylog->newLog($itid, $msg, $datetime);
                return json_encode('true');
            }
            // return redirect('/summary/'.$itid);
            
        }
        else{
            $msg = 'User ID: '.$user.' has saved itinerary '.$itid.' ('.$itinerary['name'].').';;
            $datetime = Carbon::now();
            $historylog->newLog($itid, $msg, $datetime);
            $ti->updateItineraryUserID($itid,$user);
            // return redirect('/myitinerary');
            return json_encode('true');
        }
    }
}