<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MDestination extends Model
{
    protected $table = 'm_destination';

    public function getDestinationByCountry($country)
    {
    	return $this->where('country',$country)->get();
    }
}
