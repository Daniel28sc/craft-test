<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItineratorHistoryLog extends Model
{
    protected $table = 'itinerator_history_log';

    public function newItineratorLog($itid, $itineratorid, $msg, $addedon)
    {
    	$this->insert([
            'it_id' => $itid,
            'itinerator_id' => $itineratorid,
            'description' => $msg,
            'added_on' => $addedon
        ]);
    }

    public function getItineratorLogByItineratorID($itineratorid)
    {
        return ItineratorHistoryLog::where('itinerator_id',$itineratorid)->get();
    }

    public function getItineratorLogByItID($itid)
    {
        return ItineratorHistoryLog::where('it_id',$itid)->get();
    }
}
