<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\TItineraryDay as TItineraryDay;
use App\MAirport;

class TItinerary extends Model
{
    protected $table = 't_itinerary';

    public function newTItinerary($day, $inputstylecalendar, $itineraryname)
    {
        $temp_itid = session('temp_itid');
        $mairport = new MAirport();

        $arrivalairport = session('arrivalairport');
        $a_airport = explode('(', $arrivalairport);
        $a_airport_iatacode = $a_airport[1];
        $a_airport_iatacode1 = explode(')', $a_airport_iatacode);
        $a_airport_iatacode = $a_airport_iatacode1[0];
        $a_airport_name = $a_airport[0];
        $airportdata1 = $mairport->getAirportByIata($a_airport_iatacode);

        $icao_code1 = $airportdata1['icao_code'];
        $arrivaldate = session('arrivaldate');
        $arrivaltime = session('arrivaltime');

        $returnairport = session('returnairport');
        $r_airport = explode('(', $returnairport);
        $r_airport_iatacode = $r_airport[1];
        $r_airport_iatacode1 = explode(')', $r_airport_iatacode);
        $r_airport_iatacode = $r_airport_iatacode1[0];
        $r_airport_name = $r_airport[0];
        $airportdata2 = $mairport->getAirportByIata($r_airport_iatacode);

        $icao_code2 = $airportdata2['icao_code'];
        $returndate = session('returndate');
        $returntime = session('returntime');
        // INSERT TO t_itinerary TABLE
        $it = $this->getTItineraryByID($temp_itid);
        if (count($it)<1) {
            $newitinerary = $this->insert([
                'day' => $day,
                'name' => $itineraryname,
                'arrival_airport' => $a_airport_name,
                'arrival_date' => $arrivaldate,
                'arrival_time' => $arrivaltime,
                'return_airport' => $r_airport_name,
                'return_date' => $returndate,
                'return_time' => $returntime
            ]);
            $newitineraryid = $this->orderBy('it_id', 'desc')->pluck('it_id')->first();
        }
        else{
            // $this->updatePreferencesData($temp_itid,$it['budget'],$it['pace'],$it['mustsee'],$it['cuisine'],$it['history'],$it['entertainment'],$it['adventure'],$it['culture'],$it['nature'],$it['shopping'],$it['sports']);
            $newitineraryid = $this->where('it_id',$temp_itid)->pluck('it_id')->first();
        }

        session(['temp_itid' => $newitineraryid]);

        // INSERT TO t_itinerary_day TABLE
        $temp_itdayid = array();
        $list_itdayitem = TItineraryDay::find($newitineraryid);

        if (count($list_itdayitem)<1) {
            for ($i=0; $i < count($inputstylecalendar); $i++) { 
                $itinerarydayid = TItineraryDay::add($newitineraryid,$inputstylecalendar[$i]);
                array_push($temp_itdayid, $itinerarydayid);
            }
        }
        else{
            for ($i=0; $i < count($inputstylecalendar); $i++) { 
                $itinerarydayid = TItineraryDay::updateDate($newitineraryid,$list_itdayitem[$i]['itday_id'],$inputstylecalendar[$i]);
                array_push($temp_itdayid, $itinerarydayid);
            }
        }

        // for ($i=0; $i < count($inputstylecalendar); $i++) { 
        //     $itinerarydayid = $y->newTItineraryDay($newitineraryid,$inputstylecalendar[$i]);
        //     array_push($temp_itdayid, $itinerarydayid);
        // }
        session(['temp_itdayid' => $temp_itdayid]);

    	return $newitineraryid;
    }

    public static function updatePreferencesData($itid, $budget, $pace, $mustsee, 
            $cuisine, $history, $entertainment, $adventure, $culture, $nature,
            $shopping, $sports, $family, $backpack, $vegetarian, $romantic, $culinaire, $cheapeat, $barclub, $nightseeing)
    {
    	$updatePrefData = TItinerary::where('it_id',$itid)->update([
            'budget' => $budget,
            'pace' => $pace,
            'mustsee' => $mustsee, 
            'cuisine' => $cuisine,
            'history' => $history,
            'entertainment' => $entertainment,
            'adventure' => $adventure,
            'culture' => $culture,
            'nature' => $nature,
            'shopping' => $shopping,
            'sports' => $sports,
            'family' => $family,
            'backpack' => $backpack,
            'vegetarian' => $vegetarian,
            'romantic' => $romantic,
            'culinaire' => $culinaire,
            'cheapeat' => $cheapeat,
            'barclub' => $barclub,
            'nightseeing' => $nightseeing]
        );
    }

    public static function getTItineraryByID($itid)
    {
        return TItinerary::where('it_id',$itid)->where('del',0)->get()->first();
    }

    public function getTItineraryByUserID($userid)
    {
        return $this->where('user_id',$userid)->where('del',0)->get();
    }

    public static function updateItineraryUserId($it_id, $user_id)
    {
        TItinerary::where('it_id', $it_id)->update(['user_id' => $user_id]);
    }

    public function deleteItinerary($itid)
    {
        // TItinerary::where('it_id', $itid)->delete();
        TItinerary::where('it_id', $itid)->update(['del' => 1]);
    }

    public function updateItineraryFile($itid, $filepath)
    {
        $this->where('it_id',$itid)->update(['itinerary_file'=>$filepath]);
    }
}
