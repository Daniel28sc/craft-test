<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class MPlace extends Model
{
    protected $table = 'm_place';

    public static function getPlaceByCity($cityid)
    {
    	return MPlace::where('city_id',$cityid)->orderBy('place_id', 'asc')->get();
    }

    public function getPlaceByPlaceID($place_id)
    {
        return MPlace::where('place_id',$place_id)->get();
    }

    public static function getAllPlace()
    {
    	return MPlace::orderBy('place_id', 'asc')->get();
    }

    public function newPlace($cityid, $title, $kanji, $description, $status, $hashtag, $imageFileType, $numplaceimg, $family, $backpack, $vegetarian, $romantic, $culinaire, $cheapeat, $barclub, $nightseeing, $popularplace, $culture, $cuisine, $nature, $entertainment, $history, $shopping, $sports)
    {
        $this->insert([
        	'city_id' => $cityid,
            'title' => $title,
            'kanji' => $kanji,
            'description' => $description,
            'status' => $status,
            'hashtag' => $hashtag,  
            'family' => $family,  
            'backpack' => $backpack,  
            'vegetarian' => $vegetarian,  
            'romantic' => $romantic,  
            'culinaire' => $culinaire,  
            'cheapeat' => $cheapeat,  
            'barclub' => $barclub,  
            'nightseeing' => $nightseeing,  
            'popularplace' => $popularplace,  
            'culture' => $culture,  
            'cuisine' => $cuisine,  
            'nature' => $nature,  
            'entertainment' => $entertainment,  
            'history' => $history,  
            'shopping' => $shopping,  
            'sports' => $sports  
        ]);

        $placeid = $this->orderBy('place_id', 'desc')->pluck('place_id')->first();

        // $imgsource = '../images/city/'.$cityid.'/'.$placeid.'.'.$imageFileType;
        // $this->where('place_id',$placeid)->update([
        //     'image_source' => $imgsource        
        // ]);
        
        $arr_imgsource = array();
        for ($i=1; $i < 6; $i++) { 
            if ($i<=$numplaceimg) {
                $imgsource = '../images/city/'.$cityid.'/'.$placeid.'_'.$i.'.png';
            }
            else{
                $imgsource = '';
            }
            $arr_imgsource[$i]=$imgsource;
        }

        $this->where('place_id',$placeid)->update([
            'image_source' => $arr_imgsource[1],
            'image_source2' => $arr_imgsource[2],
            'image_source3' => $arr_imgsource[3],
            'image_source4' => $arr_imgsource[4],
            'image_source5' => $arr_imgsource[5]
        ]);

        return $placeid;
    }

    public function editPlace($placeid, $cityid, $title, $kanji, $description, $status, $hashtag, $editimg, $arr_placeimgpath, $family, $backpack, $vegetarian, $romantic, $culinaire, $cheapeat, $barclub, $nightseeing, $popularplace, $culture, $cuisine, $nature, $entertainment, $history, $shopping, $sports)
    {
    	$filetype = $this->where('place_id', $placeid)->pluck('image_source')->first();
        if ($filetype == '') {
            $imageFileType = 'png';
            $getfiletype = 'png';
        }
        else{
            // $filetype = explode('/', $filetype);
            // $filetype1 = $filetype[4];
            // $filetype1 = explode('.', $filetype1);
            // $getfiletype = $filetype1[1];
            // $imageFileType = $getfiletype;
            $imageFileType = 'png';
        }

        $this->where('place_id',$placeid)->update([
                'city_id' => $cityid,
                'title' => $title,
                'kanji' => $kanji,
                'description' => $description,
                'status' => $status,
                'hashtag' => $hashtag,
                'family' => $family,  
                'backpack' => $backpack,  
                'vegetarian' => $vegetarian,  
                'romantic' => $romantic,  
                'culinaire' => $culinaire,  
                'cheapeat' => $cheapeat,  
                'barclub' => $barclub,  
                'nightseeing' => $nightseeing,  
                'popularplace' => $popularplace,  
                'culture' => $culture,  
                'cuisine' => $cuisine,  
                'nature' => $nature,  
                'entertainment' => $entertainment,  
                'history' => $history,  
                'shopping' => $shopping,  
                'sports' => $sports   
            ]);
        
        if ($editimg == 0) {
            
        }
        else{
            $imgsource = '../images/city/'.$cityid.'/'.$placeid.'.png';
            // $imgsource = '../images/city/'.$cityid.'/'.$placeid.'.'.$imageFileType;

            $placeimgs =  $this->where('place_id', $placeid)->first();
            $arr_imgsource = array();
            for ($i=1; $i <= count($arr_placeimgpath); $i++) { 
                if ($arr_placeimgpath[$i] != '') {
                    $imgsource = '../images/city/'.$cityid.'/'.$placeid.'_'.$i.'.png';
                }
                else{
                    if ($i==1) {
                        if ($placeimgs['image_source'] != '') {
                            $imgsource = $placeimgs['image_source'];
                        }
                        else{
                            $imgsource = '';
                        }
                    }
                    else{
                        if ($placeimgs['image_source'.$i] != '') {
                            $imgsource = $placeimgs['image_source'.$i];
                        }
                        else{
                            $imgsource = '';
                        }
                    }
                    
                }
                $arr_imgsource[$i]=$imgsource;
            }

            $this->where('place_id',$placeid)->update([
                'image_source' => $arr_imgsource[1],
                'image_source2' => $arr_imgsource[2],
                'image_source3' => $arr_imgsource[3],
                'image_source4' => $arr_imgsource[4],
                'image_source5' => $arr_imgsource[5]
            ]);
        }
        $getfiletype = 'png';
        return $getfiletype;
    }

    public static function removePlace($place_id) {
        MPlace::where('place_id', $place_id)->delete();
        $last_placeid = MPlace::max('place_id');
        DB::statement("ALTER TABLE m_place AUTO_INCREMENT = $last_placeid");
        // MPlace::where('place_id', $place_id)->update(['del' => 1]);
        return true;
    }
}
