<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MUser extends Model
{
    protected $table = 'm_user';
    
    public static function find($email) {
        return MUser::where('email', $email)->get();
    }

    public function getUserByID($user_id)
    {
         return MUser::where('user_id', $user_id)->get();
    }
    
    public static function setValid($user_id, $valid) {
        return MUser::where('user_id', $user_id)->update(['valid' => $valid]);
    }


    public static function newUser($email, $password, $name, $dob, $country, $address) {
    	MUser::insert([
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'dob' => $dob,
            'country' => $country,
            'address' => $address          
        ]);
    	return MUser::orderBy('user_id', 'desc')->pluck('user_id')->first();
    }

    public static function updateValidateToken($user_id, $validation_token) {
    	MUser::where('user_id', $user_id)->update(['validation_token' => $validation_token]);
    }

    public static function getUserFromLogin($email, $password) {
        return MUser::where('email', $email)->where('password', $password)->get();
    }

    public function getCurrentLoggedInData()
    {
        $userid = session('userid');
        return $this->where('user_id',$userid)->get();
    }

    public function updateUserData($changedatatype, $userid, $name, $country, $address, $newpw)
    {
        if ($changedatatype == 'Change Data') {
            $this->where('user_id',$userid)->update(['name' => $name, 'country' => $country, 'address' => $address]);
        }
        else{
            $this->where('user_id',$userid)->update(['password' => $newpw]);
        }
    }
}
