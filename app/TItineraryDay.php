<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TItineraryDay extends Model
{
    protected $table = 't_itinerary_day';
    
    public static function find($it_id)
    {
        return TItineraryDay::where('it_id',$it_id)->get();
    }
    
    public static function add($it_id, $date)
    {
        TItineraryDay::insert(['it_id' => $it_id, 'date' => $date]);        
    	return TItineraryDay::where('it_id', $it_id)->where('date', $date)
                ->orderBy('date', 'asc')->pluck('itday_id')->first();
    }

    public static function updateDate($it_id, $it_day_id, $date)
    {
        TItineraryDay::where('it_id', $it_id)->where('itday_id', $it_day_id)
                ->update(['date' => $date]);
        return TItineraryDay::where('it_id', $it_id)->where('date', $date)
                ->orderBy('date', 'asc')->pluck('itday_id')->first();
    }

    public static function getTItineraryDayByItID($itid)
    {
        return TItineraryDay::where('it_id',$itid)->get();
    }

    public function updateSameHotel($itdayid, $hotel, $samehotel)
    {
        TItineraryDay::where('itday_id', $itdayid)->update(['hotel' => $hotel, 'same_hotel' => $samehotel]);
    }

    public function updateSameAsPrevious($itdayid, $is_same)
    {
        TItineraryDay::where('itday_id', $itdayid)->update(['same_as_previous' => $is_same]);
    }
}
