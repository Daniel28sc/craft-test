<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class MItinerators extends Model
{
    protected $table = 'm_itinerators';
    
    public static function add($itinerator, $filetype) {
        MItinerators::insert([
            'name' => $itinerator['name'],
            'kanji' => $itinerator['kanji'],
            'rating' => $itinerator['rating'],
            'skill_mustsee' => $itinerator['skill_mustsee'],
            'skill_cuisine' => $itinerator['skill_cuisine'],
            'skill_history' => $itinerator['skill_history'],
            'skill_entertainment' => $itinerator['skill_entertainment'],
            'skill_adventure' => $itinerator['skill_adventure'],
            'skill_culture' => $itinerator['skill_culture'],
            'skill_nature' => $itinerator['skill_nature'],
            'skill_shopping' => $itinerator['skill_shopping'],
            'skill_sports' => $itinerator['skill_sports']
        ]);
        $itineratorid = MItinerators::orderBy('itinerators_id', 'desc')->pluck('itinerators_id')->first();
        $photo = '../images/itinerators/'.$itineratorid.'.'.$filetype;
        MItinerators::where('itinerators_id',$itineratorid)->update([
            'photo' => $photo        
        ]);
        return $itineratorid;  
    }
    
    public static function modify($itinerator, $filetype) {
        $itinerators_id = $itinerator['itinerators_id'];
        MItinerators::where('itinerators_id', $itinerator['itinerators_id'])->update([
            'name' => $itinerator['name'],
            'kanji' => $itinerator['kanji'],
            'rating' => $itinerator['rating'],
            'skill_mustsee' => $itinerator['skill_mustsee'],
            'skill_cuisine' => $itinerator['skill_cuisine'],
            'skill_history' => $itinerator['skill_history'],
            'skill_entertainment' => $itinerator['skill_entertainment'],
            'skill_adventure' => $itinerator['skill_adventure'],
            'skill_culture' => $itinerator['skill_culture'],
            'skill_nature' => $itinerator['skill_nature'],
            'skill_shopping' => $itinerator['skill_shopping'],
            'skill_sports' => $itinerator['skill_sports']
        ]);

        if ($filetype != '') {
            $photo = '../images/itinerators/'.$itinerators_id.'.'.$filetype;
            MItinerators::where('itinerators_id',$itinerators_id)->update([
                'photo' => $photo        
            ]);
        }
        return true;     
    }
    
    public static function remove($itinerators_id) {
        MItinerators::where('itinerators_id', $itinerators_id)->delete();
        $last_itineratorsid = MItinerators::max('itinerators_id');
        DB::statement("ALTER TABLE m_itinerators AUTO_INCREMENT = $last_itineratorsid");
        // MItinerators::where('itinerators_id', $itinerators_id)->update(['del' => 1]);
        return true;
    }

    public function insertItinerators($name, $kanji, $rating, $location, $skill_mustsee, $skill_cuisine, $skill_history, $skill_entertainment, $skill_adventure, $skill_culture, $skill_nature, $skill_shopping, $skill_sports)
    {
    	$newitinerators = $this->insert([
                'name' => $name,
                'kanji' => $kanji,
                'rating' => $rating,
                'location' => $location,
                'skill_mustsee' => $skill_mustsee,
                'skill_cuisine' => $skill_cuisine,
                'skill_history' => $skill_history,
                'skill_entertainment' => $skill_entertainment,
                'skill_adventure' => $skill_adventure,
                'skill_culture' => $skill_culture,
                'skill_nature' => $skill_nature,
                'skill_shopping' => $skill_shopping,
                'skill_sports' => $skill_sports,                
            ]);
    	$newitineratorsid = $this->orderBy('itinerators_id', 'desc')->pluck('itinerators_id')->first();
    	return $newitineratorsid;
    }

    public static function getItineratorsList()
    {
    	return MItinerators::orderBy('itinerators_id', 'asc')->get();
    }

    public function getItineratorByID($itinerators_id)
    {
        return MItinerators::where('itinerators_id',$itinerators_id)->get();
    }

    public function getItineratorsFromLogin($email, $password)
    {
        return MItinerators::where('email', $email)->where('password', $password)->get();
    }
}
