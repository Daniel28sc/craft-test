<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryLog extends Model
{
    protected $table = 'history_log';

    public function newLog($itid, $msg, $datetime)
    {
    	$this->insert([
            'it_id' => $itid,
            'description' => $msg,
            'added_on' => $datetime      
        ]);
    }

    public function getAllLog()
    {
    	return HistoryLog::orderBy('log_id', 'asc')->get();
    }

    public function getLogByItId($itid)
    {
    	return HistoryLog::where('it_id',$itid)->get();
    }
}
