<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TItineraryDayItem extends Model
{
    protected $table = 't_itinerary_day_item';
    
    public static function find($itday_id, $place_id, $city_id)
    {
        return TItineraryDayItem::where('itday_id', $itday_id)->get();
    }
    
    public static function findByItId($it_id)
    {
        $return = array();
        $it_days = TItineraryDay::find($it_id);
        foreach ($it_days as $it_day) {
            $result = TItineraryDayItem::where('itday_id', $it_day->itday_id)->get();
            foreach($result as $it_day_item) {
                $return[] = $it_day_item->original;
            }
        }
        return $return;
    }
    
    public function get()
    {
    	return $this->get();
    }

    public static function insertItem($itday_id, $city_id, $place_id, $note, $type)
    {
        TItineraryDayItem::insert([
            'itday_id' => $itday_id,
            'city_id' => $city_id,
            'place_id' => $place_id,
            'note' => $note,
            'type' => $type
        ]);
    }

    public function updateItem($itday_id, $city_id, $place_id, $note, $type)
    {
    	$this->where('itday_id', $itday_id)->update([
            'city_id' => $city_id,
            'place_id' => $place_id,
            'note' => $note,
            'type' => $type
        ]);
    }

    public function deleteItem($itday_id, $place_id, $city_id)
    {
    	$this->where('itday_id', $itday_id)->where('place_id', $place_id)
                ->where('city_id', $city_id)->delete();
    }

    public static function deleteByItDayId($itday_id)
    {
        TItineraryDayItem::where('itday_id', $itday_id)->delete();
    }

    public static function getTItineraryDayItemByItDayID($itid)
    {
        $itday = TItineraryDay::getTItineraryDayByItID($itid);
        $itdayid = array();
        foreach ($itday as $itday) {
            array_push($itdayid, $itday->itday_id);
        }

        $itdayitem = array();
        foreach ($itdayid as $itdayid) {
            $tempitem = TItineraryDayItem::where('itday_id',$itdayid)->get();
            array_push($itdayitem, $tempitem);
        }

        return $itdayitem;
    }
}
