<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MAirport extends Model
{
    protected $table = 'm_airport';

    public function getInternationalDestinationByCountry($country)
    {
    	return $this->where('country',$country)->where('is_international','1')->get();
    }

    public function getAirportByIata($iata_code)
    {
    	return $this->where('iata_code',$iata_code)->get()->first();
    }
}
