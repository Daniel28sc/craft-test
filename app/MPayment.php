<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MPayment extends Model
{
    protected $table = 'm_payment';

    public function newPayment($itid, $userid, $billing, $paid, $description, $type, $status, $addedon)
    {
    	$this->insert([
            'it_id' => $itid,
            'user_id' => $userid,
            'billing' => $billing,
            'paid' => $paid,
            'description' => $description,
            'type' => $type,
            'status' => $status,
            'added_on' => $addedon
        ]);
    }
}
