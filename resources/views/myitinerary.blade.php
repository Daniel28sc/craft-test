<!-- resources/views/myitinerary.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<?php
// $count_itinerarydayitemlist = count($itinerarydayitemlist);
// $count_itinerarydayitemlist = 0;

// $counter_itinerarydayitemlist = 0;
?>
<div class="container">
	<div class="row profile-heading">
		<div class="col-md-12">
			<p>Welcome back, {{ $user[0]['name'] }}!</p>
			<ul class="topprofilemenu">
				<li>
					Setting
				</li>
				<li>
					|
				</li>
				<li>
                    <a href="{{ url('/signout') }}">Logout</a>
				</li>
			</ul>
		</div>
	</div>
	
	<div class="row inputstyle-sectionline">
		<div class="col-md-12">
		</div>
	</div>

	<div class="row userprofile">
		<div class="col-md-3 profile-menu">
			<ul>
				<li class="menu-myprofile">
					<p><a href="{{ url('/profile') }}">My Profile</a></p>
				</li>
				<li class="menu-myitinerary">
					<p><a href="{{ url('/myitinerary') }}">My Itinerary</a></p>
				</li>
				<li class="menu-paymenthistory">
					<p><a href="{{ url('/paymenthistory') }}">Payment History</a></p>
				</li>
				<li class="menu-itineratorhistory">
					<p>Itinerator History</p>
				</li>
			</ul>
		</div>

		<div class="col-md-9 profile-content">
			<div class="userprofile-itinerarylist">
				<p>
					<ul>
						@for($x=1; $x<count($arrayitinerarylist); $x++)
						<?php
						if ($arrayitinerarylist[$x]['arrival_date'] != '') {
							$date1 = $arrayitinerarylist[$x]['arrival_date'];
							$datedata1 = date_parse($date1);
							$year1 = $datedata1['year'];
							$month1 = $datedata1['month'];
							$day1 = $datedata1['day'];
							$strdate1 = $year1.'/'.$month1.'/'.$day1;
							$cal1 = strtoupper(date('Y M d', strtotime($strdate1)));

							$date2 = $arrayitinerarylist[$x]['return_date'];
							$datedata2 = date_parse($date2);
							$year2 = $datedata2['year'];
							$month2 = $datedata2['month'];
							$day2 = $datedata2['day'];
							$strdate2 = $year2.'/'.$month2.'/'.$day2;
							if ($year1 == $year2) {
								$cal2 = strtoupper(date('M d', strtotime($strdate2)));
							}
							else{
								$cal2 = strtoupper(date('Y M d', strtotime($strdate2)));
							}

							$itineraryschedule = $cal1.' - '.$cal2;
						}
						else{
							$itineraryschedule = '';
						}
						?>
						<li>
							<div class="itinerarylist-box">
								<table class="myprofile-listbox">
									<tr>
										<td colspan="3">
											<ul class="cities">
												<li class="totalday">
													<?php
													if ($arrayitinerarylist[$x]['name'] != '') {
														echo $arrayitinerarylist[$x]['name'].": ";
													}
													?>
													{{ $arrayitinerarylist[$x]['day'] }} Days
												</li>

												<?php
												$maxcity = 0;
												$temp_citytitle = '';
												$arr_citytitle = array();
												
												for ($x1=0; $x1 < count($arrayitinerarydayitemlist[$x]); $x1++) { 
													for ($x2=0; $x2 < count($arrayitinerarydayitemlist[$x][$x1]); $x2++) {
														$cityid = $arrayitinerarydayitemlist[$x][$x1][$x2]['city_id'];

														$citylistid = '';
														for ($xx=1; $xx < count($citylist); $xx++) { 
															if ($citylist[$xx]['city_id'] == $cityid) {
																$citylistid = $xx;
																break;
															}
														}

														if ($citylistid != '') {
															if ($citylist[$citylistid]['del'] == '1') {
																continue;
															}
															$citytitle = $citylist[$citylistid]['title'];
															if (in_array($citytitle, $arr_citytitle)) {
																continue;
															}
															if ($citytitle == $temp_citytitle) {
																continue;
															}
															else{
																$temp_citytitle = $citytitle;
															}
															array_push($arr_citytitle, $citytitle);
															$maxcity++;
														}
													}
												}

												$countermaxcity=1;
												$temp_citytitle = '';
												$arr_citytitle = array();
												for ($x1=0; $x1 < count($arrayitinerarydayitemlist[$x]); $x1++) { 
													for ($x2=0; $x2 < count($arrayitinerarydayitemlist[$x][$x1]); $x2++) {
														$cityid = $arrayitinerarydayitemlist[$x][$x1][$x2]['city_id'];

														$citylistid = '';
														for ($xx=1; $xx < count($citylist); $xx++) { 
															if ($citylist[$xx]['city_id'] == $cityid) {
																$citylistid = $xx;
																break;
															}
														}

														if ($citylistid != '') {
															if ($citylist[$citylistid]['del'] == '1'){
																continue;
															}
															$citytitle = $citylist[$citylistid]['title'];

															if (in_array($citytitle, $arr_citytitle)) {
																continue;
															}

															if ($citytitle == $temp_citytitle) {
																continue;
															}
															else{
																$temp_citytitle = $citytitle;
															}
															array_push($arr_citytitle, $citytitle);
															echo '<li>';
															echo $citytitle;
															if ($countermaxcity != $maxcity) {
																echo " - ";
															}
															
															echo '</li>';
															$countermaxcity++;
														}
													}
												}
												?>
											</ul>
										</td>
										<td rowspan="2" colspan="2" class="buttonreq-part">
											<div class="requestitinerary">
												<?php
												if (count($arrayitineratorslist[$x]) == 1) {
													if ($arrayitineratorslist[$x][0]['status'] == 'currently') {
														?>
														<div class="request status-currently">
															<button type="button" class="myitinerarymodaltrigger onprogresstrigger">ON PROGRESS</button>
														<!-- <button type="button" class="myitinerarymodaltrigger onprogresstrigger" data-toggle="modal" data-target="#onprogressmodal-<?php echo $x; ?>">ON PROGRESS</button> -->
														<?php
													}
													else if($arrayitineratorslist[$x][0]['status'] == 'revision'){
														?>
														<div class="request status-revision">
														<!-- <button type="button" class="myitinerarymodaltrigger revisiontrigger" data-toggle="modal" data-target="#revisionmodal-<?php echo $x; ?>">REVISION</button> -->
															<button type="button" class="myitinerarymodaltrigger">REVISION</button>
														<?php
													}
													else if($arrayitineratorslist[$x][0]['status'] == 'confirmresult'){
														?>
														<div class="request status-confirmresult">
															<button type="button" class="myitinerarymodaltrigger revisiontrigger" data-toggle="modal" data-target="#revisionmodal-<?php echo $x; ?>">CONFIRM RESULT</button>
														<?php
													}
													else if($arrayitineratorslist[$x][0]['status'] == 'finished'){
														?>
														<div class="request status-done">
															<button type="button" class="myitinerarymodaltrigger donetrigger" data-toggle="modal" data-target="#donemodal-<?php echo $x; ?>">DONE</button>
														<?php
													}
													else if($arrayitineratorslist[$x][0]['status'] == 'needpayment'){
														?>
														<div class="request status-needpayment">
															<button type="button" class="myitinerarymodaltrigger needpaymenttrigger" data-toggle="modal" data-target="#needpaymentmodal-<?php echo $x; ?>">WAITING PAYMENT</button>
														<?php
													}
													else if($arrayitineratorslist[$x][0]['status'] == 'itineratorconfirmation'){
														?>
														<div class="request status-waitingitineratorconfirm">
															<button type="button" class="myitinerarymodaltrigger">Waiting Itinerator Confirmation</button>
														<?php
													}
													else{
														?>
														<div class="request">
															<button type="button" class="myitinerarymodaltrigger requestmodaltrigger" data-toggle="modal" data-target="#requestmodal-<?php echo $x; ?>">REQUEST</button>
														<?php
													}
												}
												else{
													if ($arrayitineratorslist[$x][0]['status'] == 'pickitinerator') {
														?>
														<div class="request status-pickitinerator">
														<button type="button" class="myitinerarymodaltrigger requestmodaltrigger" data-toggle="modal" data-target="#pickitineratormodal-<?php echo $x; ?>">PICK ITINERATOR</button>
														<?php
													}
													else{
														?>
														<div class="request">
														<button type="button" class="myitinerarymodaltrigger requestmodaltrigger" data-toggle="modal" data-target="#requestmodal-<?php echo $x; ?>">REQUEST</button>
														<?php
													}
												}
												?>
												</div> 
												<?php
												if ($arrayitineratorslist[$x][0]['status'] == 'itineratorconfirmation') {
													echo '<div class="request-x" style="height:52px;">';
												}
												else{
													echo '<div class="request-x">';
												}
												?>
												
													<!-- <form action="profile_controller" method="POST" name="delitineraryform" class="delitineraryform">
														{!! csrf_field() !!}
														<input type="hidden" name="profileformtype" class="profileformtype" value="delitinerary">
														<input type="hidden" name="dummyitid" class="dummyitid" value="<?php echo $arrayitinerarylist[$x]['it_id']; ?>">
														<input type="submit" value="X">
													</form> -->
													
													<button type="button" class="delconfirmtrigger delconfirmtrigger-<?php echo $x; ?>" data-toggle="modal" data-target="#delitineraryconfirmmodal-<?php echo $x; ?>">X</button>
													
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="3">
											<ul class="dates">
												<?php
												for ($j=0; $j < count($arrayitinerarydaylist[$x]); $j++) {
													echo "<li>";
													echo " ".$arrayitinerarydaylist[$x][$j]['date'];
													echo "</li>";
												}
												?>
											</ul>
										</td>
									</tr>
									<tr>
										<td>
											<?php
											$est_msg = '-';
											if (count($arrayitineratorslist[$x]) == 1) {
												$est = $arrayitineratorslist[$x][0]['work_day'];
												for ($i=0; $i < count($history_log[$x]); $i++) { 
													if (strpos($history_log[$x][$i]['description'], 'status has been changed to "currently"') !== false) {
														$addedon1 = $history_log[$x][$i]['added_on'];
														$addedon1_1 = strtotime('+'.$est.' day', strtotime($addedon1));
														$addedon1_1 = date('Y/m/d',$addedon1_1);
														$est1 = strtotime('+1 day', strtotime($addedon1_1));
														$est1 = date('Y/m/d',$est1);
														$est2 = strtotime('+3 day', strtotime($addedon1_1));
														$est2 = date('Y/m/d',$est2);
														$est_msg = $est1.' - '.$est2;
													}
												}
											}
											?>
											Delivery Estimation: <?php echo $est_msg; ?>
										</td>
										<td>
											<button type="button" class="itineratorsitinerary foritineratorslist-<?php echo $x; ?>" data-toggle="modal" data-target="#itineratorsmodal-<?php echo $x; ?>"><i class="fa fa-users" aria-hidden="true"></i> Itinerator</button>
										</td>
										<td>
											<?php
											if(($arrayitineratorslist[$x][0]['status'] == 'none') || 
												($arrayitineratorslist[$x][0]['status'] == 'pickitinerator') || 
												($arrayitineratorslist[$x][0]['status'] == 'needpayment')){
												$disable = '';
											}
											else{
												$disable = 'disabled';
											}
											?>

											<button class="edititinerary id<?php echo $arrayitinerarylist[$x]['it_id']; ?>" <?php echo $disable; ?>><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
										</td>
										<td>
											<button class="chatitinerary"><i class="fa fa-comments" aria-hidden="true"></i> Chat</button>
										</td>
										<td>
											@if($arrayitinerarylist[$x]['itinerary_file'] != '')
												@if($arrayitineratorslist[$x][0]['status'] != 'revision')
												<a href="{{ $arrayitinerarylist[$x]['itinerary_file'] }}">
													<button class="resultitinerary">
														<i class="fa fa-file-text-o" aria-hidden="true"></i> Result
													</button>
												</a>
												@else
												<button class="resultitinerary"><i class="fa fa-file-text-o" aria-hidden="true"></i> Result</button>
												@endif
											@else
											<button class="resultitinerary"><i class="fa fa-file-text-o" aria-hidden="true"></i> Result</button>
											@endif
										</td>
									</tr>
								</table>

								<!-- Itinerators Modal -->
								<div id="itineratorsmodal-<?php echo $x; ?>" class="modal fade" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Itinerator(s): </h4>
											</div>
											<div class="modal-body">
												<div class="row request-ibox">
													<?php
													if ($arrayitineratorslist[$x][0]['pick_type'] == 'random') {
														echo "We will choose for you.";
													}
													else{
														for ($i=0; $i < count($arrayitineratorslist[$x]); $i++) { 
															$itineratorid = $arrayitineratorslist[$x][$i]['itinerators_id'];
															for ($ii=1; $ii < count($itineratorslist); $ii++) { 
																if ($itineratorslist[$ii]['itinerators_id'] == $itineratorid) {
																	$itinerators_id = $ii;
																	break;
																}
															}
															if ($itinerators_id != '') {
																if ($itineratorslist[$itinerators_id]['del'] != 1) {
																	echo "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12 col-centered ibox'>";
																	echo '<div class="ibox-photo">';
																	echo "<img src='".$itineratorslist[$itinerators_id]['photo']."'>";
																	echo '</div>';
																	echo '<div class="ibox-desc">';
																	echo $itineratorslist[$itinerators_id]['name']."<br>";
																	echo $itineratorslist[$itinerators_id]['kanji'];
																	echo '</div>';
																	echo "</div>";
																}
															}
														}
													}
													?>	
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- Request Modal -->
								<div id="requestmodal-<?php echo $x; ?>" class="modal fade myitineraryactionmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Request: </h4>
											</div>
											<div class="modal-body">
												<form action="profile_controller" method="POST" name="sendrequestform" class="sendrequestform">
												{!! csrf_field() !!}
													<input type="hidden" name="profileformtype" class="profileformtype" value="sendrequest">
													<input type="hidden" name="dummyitid" class="dummyitid" value="<?php echo $arrayitinerarylist[$x]['it_id']; ?>">
													<table>
														<?php
														if ($arrayitineratorslist[$x][0]['pick_type'] == 'random') {
															echo "We will choose for you";
														}
														else{
														?>
															<tr>
																<th></th>
																<th>
																	Itinerator(s)
																</th>
															</tr>
															<?php
															$itinerators_id = '';
															for ($i=0; $i < count($arrayitineratorslist[$x]); $i++) { 
																$itineratorid = $arrayitineratorslist[$x][$i]['itinerators_id'];
																for ($ii=1; $ii < count($itineratorslist); $ii++) { 
																	if ($itineratorslist[$ii]['itinerators_id'] == $itineratorid) {
																		$itinerators_id = $ii;
																		break;
																	}
																}
																if ($itinerators_id != '') {
																	if ($itineratorslist[$itinerators_id]['del'] != 1) {
																		$workday = $arrayitineratorslist[$x][$i]['work_day'];
																		if ($workday == 0) {
																			$disabled = 'disabled';
																			$estimation = 'Not Ready';
																		}
																		else{
																			$disabled = '';
																			$estimation = $workday;
																		}
																		echo "<tr>";
																		echo "<td>".$itineratorslist[$itinerators_id]['name']."</td>";
																		echo "</tr>";
																	}
																}
															}
														}
														?>
														<tr>
															<td></td>
															<td></td>
															<td>
																<input type="submit" value="Send Email" name="sendrequestitinerator" class='sendrequestitinerator'>
															</td>
														</tr>
													</table>
													<div class="row request-ibox">	
													</div>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- Pick Itinerator Modal -->
								<div id="pickitineratormodal-<?php echo $x; ?>" class="modal fade myitineraryactionmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Request: </h4>
											</div>
											<div class="modal-body">
												<?php
												$not_ready = 0;
												$added_on = '';
												$now = date('Y-m-d H:i:s');
												for ($i=0; $i < count($arrayitineratorslist[$x]); $i++) { 
													if ($arrayitineratorslist[$x][$i]['work_day'] == 0) {
														$not_ready++;
													}
												}
												for ($i=0; $i < count($history_log[$x]); $i++) { 
													if (strpos($history_log[$x][$i]['description'], 'status has been changed to "pick itinerator"') !== false) {
														$added_on = $history_log[$x][$i]['added_on'];
														$added_on1 = strtotime($added_on);
														$add1day = strtotime('+1 day', $added_on1);
														$add1day = date("Y-m-d H:i:s", $add1day);
													}
												}
												if ($not_ready==count($arrayitineratorslist[$x])) {
													if ($added_on != '') {
														if ($now >= $add1day) {
        													// echo "Let us pick for you.";
        													?>
        													<button type="button" class="btn btn-default" data-dismiss="modal">Let us pick for you</button>
        													<a href="{{ url('/pickitinerators',$arrayitinerarylist[$x]['it_id']) }}"><button type="button" class="btn btn-default repickitinerator">Pick another</button></a>
        													<button type="button" class="btn btn-default" data-dismiss="modal">Wait Others</button>
        													<?php
														}
														else{
															echo "We are still searching the best within 1x24 hour for you";
														}
													}
												}
												else{
												?>
												<form action="profile_controller" method="POST" name="chooseitineratorform" class="chooseitineratorform">
												{!! csrf_field() !!}
													<input type="hidden" name="profileformtype" class="profileformtype" value="chosenitinerator">
													<input type="hidden" name="dummyitid" class="dummyitid" value="<?php echo $arrayitinerarylist[$x]['it_id']; ?>">
													<table>
														<tr>
															<th></th>
															<th>
																Itinerator(s)
															</th>
															<th>
																Work Estimation (day)
															</th>
														</tr>
														
														<?php
														$itinerators_id = '';
														
														for ($i=0; $i < count($arrayitineratorslist[$x]); $i++) { 
															$itineratorid = $arrayitineratorslist[$x][$i]['itinerators_id'];
															for ($ii=1; $ii < count($itineratorslist); $ii++) { 
																if ($itineratorslist[$ii]['itinerators_id'] == $itineratorid) {
																	$itinerators_id = $ii;
																	break;
																}
															}
															if ($itinerators_id != '') {
																if ($itineratorslist[$itinerators_id]['del'] != 1) {
																	$workday = $arrayitineratorslist[$x][$i]['work_day'];
																	if ($workday == 0) {
																		$disabled = 'disabled';
																		$estimation = 'Not Ready';
																	}
																	else{
																		$disabled = '';
																		$estimation = $workday;
																	}
																	echo "<tr>";
																	echo "<td><input type='radio' name='workestimation' class='workestimation' value='".$workday."|".$itineratorslist[$itinerators_id]['itinerators_id']."' ".$disabled."></td>";
																	echo "<td>".$itineratorslist[$itinerators_id]['name']."</td>";
																	echo "<td>".$estimation."</td>";
																	// echo "<input type='hidden' name='chooseitinerator' value='".$itineratorslist[$itinerators_id]['itinerators_id']."' class='chooseitinerator'>";
																	echo "</tr>";
																}
															}
														}
														?>
														<tr>
															<td></td>
															<td></td>
															<td>
																<input type="submit" value="Save" name="savechosenitinerator" class='btn btn-default savechosenitinerator'>
																<?php
																$temp_editedworkday = '';
																
																for ($i=0; $i < count($itinerator_log[$x]); $i++) { 
																	if (strpos($itinerator_log[$x][$i]['description'], 'submitted workday estimation') === false) {
																		continue;
																	}
																	else{
																		$temp_editedworkday = $itinerator_log[$x][$i]['added_on'];
																		break;
																	}
																}
																
																if ($not_ready==0) {
																?>
																	<!-- <input type="submit" value="Save" name="savechosenitinerator" class='savechosenitinerator'> -->
																<?php
																}
																else{
																	$last_editedworkday = $temp_editedworkday;
																	$add6hour = strtotime('+6 hour', strtotime($last_editedworkday));
																	$add6hour = date('Y-m-d H:i:s',$add6hour);
																	if ($now >= $add6hour) {
																		?>
																		<a href="{{ url('/pickitinerators',$arrayitinerarylist[$x]['it_id']) }}"><button type="button" class="btn btn-default repickitinerator">Pick another</button></a>
																		<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Wait Others</button> -->
																		<?php
																	}
																	else{
																	?>
																		<!-- <a href="{{ url('/pickitinerators',$arrayitinerarylist[$x]['it_id']) }}"><button type="button" class="btn btn-default repickitinerator">Pick another</button></a> -->
																		<button type="button" class="btn btn-default" data-dismiss="modal">Wait Others</button>
																	<?php 
																	}
																}
																?>
															</td>
														</tr>
													</table>
													<?php
												}
												?>
													<div class="row request-ibox">
													<?php
													// for ($i=0; $i < count($arrayitineratorslist[$x]); $i++) { 
													// 	$itineratorid = $arrayitineratorslist[$x][$i]['itinerators_id'];
													// 	for ($ii=1; $ii < count($itineratorslist); $ii++) { 
													// 		if ($itineratorslist[$ii]['itinerators_id'] == $itineratorid) {
													// 			$itinerators_id = $ii;
													// 			break;
													// 		}
													// 	}
													// 	if ($itinerators_id != '') {
													// 		if ($itineratorslist[$itinerators_id]['del'] != 1) {
													// 			echo "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12 col-centered ibox'>";
													// 			echo "<input type='radio' name='workestimation' class='workestimation' value='".($i+1)."|".$itineratorslist[$itinerators_id]['itinerators_id']."'>";
													// 			echo '<div class="ibox-photo">';
													// 			echo "<img src='".$itineratorslist[$itinerators_id]['photo']."'>";
													// 			echo '</div>';
													// 			echo '<div class="ibox-desc">';
													// 			echo $itineratorslist[$itinerators_id]['name']."<br>";
													// 			echo $itineratorslist[$itinerators_id]['kanji'];
													// 			echo '</div>';
													// 			echo "</div>";
													// 		}
													// 	}
													// }
													?>	
													</div>
													<!-- <div class="row">
														<div class="col-md-12">
															<input type="submit" value="Save" name="savechosenitinerator" class='savechosenitinerator'>
														</div>
													</div> -->
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- On Progress Modal -->
								<div id="onprogressmodal-<?php echo $x; ?>" class="modal fade myitineraryactionmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">On Progress: </h4>
											</div>
											<div class="modal-body">

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- Revision Modal -->
								<div id="revisionmodal-<?php echo $x; ?>" class="modal fade myitineraryactionmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Revision: </h4>
											</div>
											<div class="modal-body">
												<form action="profile_controller" method="POST" name="revisionform" class="revisionform">
												{!! csrf_field() !!}
													<input type="hidden" name="profileformtype" class="profileformtype" value="itineraryrevision">
													<input type="hidden" name="dummyitid" class="dummyitid" value="<?php echo $arrayitinerarylist[$x]['it_id']; ?>">
													<input type="hidden" name="dummyitineratorid" class="dummyitineratorid" value="<?php echo $arrayitineratorslist[$x][0]['itinerators_id']; ?>">
													<div class="row">
														<div class="col-md-6">
															<input type="submit" name="revisionoption" value="OK">
														</div>
														<div class="col-md-6">
															<?php 
															$rev_counter = $arrayitineratorslist[$x][0]['revision_counter'];
															if ($rev_counter >= 5) {
																$disabled = 'disabled';
																echo "<p>You can not ask for more revisions again.</p>";
															}
															else{
																$disabled = '';
															}
															?>
															<textarea name="revisionnote" id="" cols="30" rows="10" <?php echo $disabled; ?>></textarea>
															<br>
															<input type="submit" name="revisionoption" value="Revision" <?php echo $disabled; ?>>
														</div>
													</div>
												</form>						
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- Done Modal -->
								<div id="donemodal-<?php echo $x; ?>" class="modal fade myitineraryactionmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Finished: </h4>
											</div>
											<div class="modal-body">
																			
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- Need Payment Modal -->
								<div id="needpaymentmodal-<?php echo $x; ?>" class="modal fade myitineraryactionmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Payment Confirmation: </h4>
											</div>
											<div class="modal-body">
												<button class="paymentmethod showcashmethod" value="pay">Pay</button>
												<button class="paymentmethod showbankmethod" value="bank">Confirm</button>
												<form action="profile_controller" method="POST" name="paymentconfirmationform" class="paymentconfirmationform">
												{!! csrf_field() !!}
													<input type="hidden" name="profileformtype" class="profileformtype" value="paymentconfirmation">
													<input type="hidden" name="dummyitid" class="dummyitid" value="<?php echo $arrayitinerarylist[$x]['it_id']; ?>">
													<input type="hidden" name="dummyitineratorid" class="dummyitineratorid" value="<?php echo $arrayitineratorslist[$x][0]['itinerators_id']; ?>">
													<!-- <input type="submit" value="Pay" name="paymentconfirmaction">
													<input type="submit" value="Confirm" name="paymentconfirmaction"> -->
													<div class="cashmethod">
														<div class="form-group">
															<label for="pay-name">Name:</label>
															<input type="text" class="form-control" id="pay-name1" name="pay-name1">
														</div>
														<div class="form-group">
															<label for="pay-nominal">Nominal:</label>
															<input type="text" class="form-control" id="pay-nominal1" name="pay-nominal1">
														</div>
														<input type="submit" value="Pay" name="paymentconfirmaction">
													</div>
													<div class="bankmethod">
														<div class="form-group">
															<label for="pay-name">Name:</label>
															<input type="text" class="form-control" id="pay-name2" name="pay-name2">
														</div>
														<div class="form-group">
															<label for="pay-nominal">Nominal:</label>
															<input type="text" class="form-control" id="pay-nominal2" name="pay-nominal2">
														</div>
														<div class="form-group">
															<label for="pay-bank">Bank:</label>
															<input type="text" class="form-control" id="pay-bank" name="pay-bank">
														</div>
														<input type="submit" value="Confirm" name="paymentconfirmaction">
													</div>
												</form>												
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>

								<!-- Konfirmasi Delete Itinerary Modal -->
								<div id="delitineraryconfirmmodal-<?php echo $x; ?>" class="modal fade delitineraryconfirmmodal" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Do you want to delete this itinerary? </h4>
												<form action="profile_controller" method="POST" name="delitineraryform" class="delitineraryform">
													{!! csrf_field() !!}
													<input type="hidden" name="profileformtype" class="profileformtype" value="delitinerary">
													<input type="hidden" name="dummyitid" class="dummyitid" value="<?php echo $arrayitinerarylist[$x]['it_id']; ?>">
													<input type="submit" value="Yes" class="delconfirmbutton">
													<input type="submit" value="No" class="delconfirmbutton">
												</form>
											</div>
											<div class="modal-body summary-profile">
												<table>
													<tr>
														<td colspan="2">
															<p>
																{{ $arrayitinerarylist[$x]['name'] }}
															</p>
														</td>
													</tr>
													<tr class="summary-day">
								                        <td>
								                            <p>
								                                {{ $arrayitinerarylist[$x]['day'] }} Days
								                            </p>
								                        </td>
								                        <td>
								                            <p>
								                                {{ $itineraryschedule }}
								                            </p>
								                        </td>
								                    </tr>
								                    <tr>
								                        <td>
								                            <i class="fa fa-plane fa-2x" aria-hidden="true"></i>
								                        </td>
								                        <td>
								                            <p>
								                                {{ $arrayitinerarylist[$x]['arrival_airport'] }} ({{ $arrayitinerarylist[$x]['arrival_date'] }} {{ $arrayitinerarylist[$x]['arrival_time'] }})
								                            </p>
								                        </td>
								                    </tr>
								                    <tr>
								                        <td>
								                            <i class="fa fa-plane fa-2x" aria-hidden="true"></i>
								                        </td>
								                        <td>
								                            <p>
								                                {{ $arrayitinerarylist[$x]['return_airport'] }} ({{ $arrayitinerarylist[$x]['return_date'] }} {{ $arrayitinerarylist[$x]['return_time'] }})
								                            </p>
								                        </td>
								                    </tr>
													<tr>
														<td>
															<i class="fa fa-jpy fa-2x" aria-hidden="true"></i>
														</td>
														<td>
															<p>
																{{ $arrayitinerarylist[$x]['budget'] }}
															</p>
														</td>
													</tr>
													<tr class="summary-pref-1">
														<td>
															<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
														</td>
														<td>
															<?php
															$i1 = 1;
															$i2 = 1;
															$arr_pref_var1 = ['','Family','Backpack','Vegetarian','Romantic','Culinaire','Cheap Eat','Bar & Club','Night Seeing'];
															$arr_pref_var1_1 = ['','family','backpack','vegetarian','romantic','culinaire','cheapeat','barclub','nightseeing'];
															while ($i1 < 9) {
																if ($arrayitinerarylist[$x][$arr_pref_var1_1[$i1]] == '1') {
																	if ($i2%3 == 1) {
																		echo '<ul class="ul-cpvar-1">';
																	}

																	echo '<li>'.$arr_pref_var1[$i1].'</li>';

																	if ($i2%3 == 0) {
																		echo '</ul>';
																	}
																	$i2++;
																}
																$i1++;
															}
															?>
														</td>
													</tr>
													<tr class="summary-pref-2">
														<td>
															<i class="fa fa-map-o fa-2x" aria-hidden="true"></i>
														</td>
														<td>
															<?php
															$arr_pref_var2 = ['','Popular Places','Culture & Art','Cuisine','Nature','Entertainment','History','Shopping','Sports'];
															$arr_pref_var2_1 = ['','mustsee','culture','cuisine','nature','entertainment','history','shopping','sports'];
															for ($i=1; $i < 9; $i++) { 
																$pref_rate = $arrayitinerarylist[$x][$arr_pref_var2_1[$i]];
																if ($i%3 == 1) {
																	echo '<ul class="ul-cpvar-2">';
																}
																echo '<li>';
																echo $arr_pref_var2[$i];
																echo '<span>';
																for ($ii=0; $ii < $pref_rate; $ii++) { 
																	echo '<i class="fa fa-circle prefratecircle" aria-hidden="true"></i>';  
																}
																echo '</span>';
																echo '</li>';
																if ($i%3 == 0) {
																	echo '</ul>';
																}
															}
															?>
														</td>
													</tr>
												</table>

												<div class="summary-preferences">
													<div class="summary-cityplace">
														<div class="row">
															<?php
															$tempdate = '';
															$temp_cpday = $arrayitinerarydayitemlist[$x][0][0]['itday_id'];
															$counterday = 0;
															$temp_city = '';
															for ($i=0; $i < count($arrayitinerarydaylist[$x]); $i++) { 
																for ($j=0; $j < count($arrayitinerarydayitemlist[$x][$i]); $j++) {
																	$index_city = -1;
																	$index_place = -1;

																	$cid = $arrayitinerarydayitemlist[$x][$i][$j]['city_id'];
																	for ($k=1; $k < count($citylist); $k++) { 
																		if ($citylist[$k]['city_id'] == $cid) {
																			$index_city = $k;
																			break;
																		}
																	}
																	$cp_bg = $citylist[$index_city]['image_source'];

																	$pid = $arrayitinerarydayitemlist[$x][$i][$j]['place_id'];
																	for ($k=1; $k < count($placelist); $k++) { 
																		if ($placelist[$k]['place_id'] == $pid) {
																			$index_place = $k;
																			break;
																		}
																	}

																	if ($arrayitinerarydayitemlist[$x][$i][$j]['itday_id'] == $temp_cpday) {

																	}
																	else{
																		$counterday++;
																		$temp_cpday = $arrayitinerarydayitemlist[$x][$i][$j]['itday_id'];
																	}

																	if ($arrayitinerarydaylist[$x][$counterday]['date'] != $tempdate) {
																		if ($tempdate!='') {
																			echo "</div>";
																		}
																		$tempdate = $arrayitinerarydaylist[$x][$counterday]['date'];
																		echo "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12 summary-cpbox'>";
																		echo "<span class='summary-cpdate'>".$arrayitinerarydaylist[$x][$counterday]['date'].'</span>';
																	}
																	else{

																	}

																	if ($temp_city != $citylist[$index_city]['title']) {
																		if ($index_city>-1) {
																			echo '<p>'.$citylist[$index_city]['title'].'</p>';
																			$temp_city = $citylist[$index_city]['title'];
																		}
																	}

																	$write_note = 0;
																	if ($arrayitinerarydaylist[$x][$counterday]['same_as_previous'] == 'no') {
																		if ($index_place>-1) {
																			echo '<p>- '.$placelist[$index_place]['title'].'</p>';
																			$write_note = 1;
																		}
																	}

																	$hotelname = '-';
																	$hoteladdress = '-';
																	$hotelphone = '-';
																	$write_hotel = 0;
																	$hotel = $arrayitinerarydaylist[$x][$counterday]['hotel'];
																	$hotel = explode('#|#', $hotel);

																	if ($arrayitinerarydayitemlist[$x][$i][$j]['note'] != '') {
																		if (($j == count($arrayitinerarydayitemlist[$x][$i])-1)) {
																			if ($write_note == 1) {
																				echo "<p>Note: ".$arrayitinerarydayitemlist[$x][$i][$j]['note']."</p><br>";
																			}
																			$write_hotel = 1;
																		}
																		else{
																			if ($arrayitinerarydayitemlist[$x][$i][$j]['city_id'] != $arrayitinerarydayitemlist[$x][$i][$j+1]['city_id']) {
																				if ($write_note == 1) {
																					echo "<p>Note: ".$arrayitinerarydayitemlist[$x][$i][$j]['note']."</p><br>";
																				}
																				
																				$write_hotel = 1;
																			}
																		}
																	}
																}

																if ($write_hotel==1) {
																	if (!empty($hotel[0])) {
																		$hotelname = $hotel[0];
																	}
																	if (!empty($hotel[1])) {
																		$hoteladdress = $hotel[1];
																	}
																	if (!empty($hotel[2])) {
																		$hotelphone = $hotel[2];
																	}
																}

																echo "<h2>Hotel</h2>";
																echo "Name: ".$hotelname;
																echo "<br>Address: ".$hoteladdress;
																echo "<br>Phone: ".$hotelphone;
																
																$temp_city = '';

																if ($i == count($arrayitinerarydaylist[$x])-1) {
																	echo '</div>';
																}
															}
															?>
														<!-- </div> -->
														</div>
													</div>

													<!--<div class="summary-itinerarydesc">
														<div class="row">
															<div class="col-md-12">
																<h1>
																	Itinerary Details
																</h1>
																<?php
																$temp_itdayid = 0;
																$counterday = 0;
																for ($i=0; $i < count($arrayitinerarydaylist[$x]); $i++) { 
																	for ($j=0; $j < count($arrayitinerarydayitemlist[$x][$i]); $j++) {
																		if ($arrayitinerarydayitemlist[$x][$i][$j]['itday_id'] != $temp_itdayid) {
																			if ($arrayitinerarydayitemlist[$x][$i][$j]['note'] != '') {
																				echo "<h4>".$arrayitinerarydaylist[$x][$i]['date']."</h4>";
																				echo "<p>".$arrayitinerarydayitemlist[$x][$i][$j]['note']."</p>";
																				echo "<br>";
																			}
																			$counterday++;
																			$temp_itdayid = $arrayitinerarydayitemlist[$x][$i][$j]['itday_id'];
																		}
																	}
																}
																?>
															</div>
														</div>
													</div>-->

													<div class="summary-itinerators">
														<h1>Itinerators</h1>
															@if($arrayitineratorslist[$x][0]['pick_type'] == 'random')
																We will choose for you.
																<div class="row">
															@else
																<h2>We will inform these...</h2>
																<div class="row">
																@foreach($arrayitineratorslist[$x] as $iirelation)
																<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 ibox">
																	<?php
																	$iid = $iirelation['itinerators_id'];
																	for ($j=1; $j < count($itineratorslist); $j++) { 
																		if ($itineratorslist[$j]['itinerators_id'] == $iid) {
																			$iindex = $j;
																			break;
																		}
																	}
																	?>
																	<div class="ibox-photo">
																		<img src="{{ $itineratorslist[$iindex]['photo'] }}" alt="">
																	</div>
																	<div class="ibox-desc">
																		{{ $itineratorslist[$iindex]['name'] }}
																		<br>
																		{{ $itineratorslist[$iindex]['kanji'] }}
																	</div>
																</div>
																@endforeach
															@endif
														</div>
													</div>
												</div>

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>
								
								<!-- <div class="myprofile-showitinerators list-<?php echo $x; ?>">
									<div class="popup-content">
										<button class="close-showitinerators">X</button>
										<br>Itinerator(s):
										<br>-->
										<?php
										// for ($i=0; $i < count($arrayitineratorslist[$x]); $i++) { 
										// 	$itineratorid = $arrayitineratorslist[$x][$i]['itinerators_id'];
										// 	echo $itineratorslist[$itineratorid]['name']."<br>";
										// }
										?>
									<!-- </div>
								</div> -->
							
							</div>
						</li>
						@endfor
					</ul>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection