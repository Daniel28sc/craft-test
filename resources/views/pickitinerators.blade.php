<!-- resources/views/pickitinerators.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script type="text/javascript">
$(function() {
	var numchecked = $('.itinerators-checkbox:checked').length;
	if (numchecked > 3) {
		$('.pickrandom').prop("checked",true);
	}
	// else if(numchecked == $('.itinerators-checkbox').length){
	// 	$('.pickrandom').prop("checked",true);
	// }
	else{
		$('.pickmanual').prop("checked",true);
	}
});
</script>
<div class="container">
	<?php
	$checked = '';
	session(['temp_itid' => $itid]);
	$temp_itid = session('temp_itid');
	?>
	<div id="stepsbar-wrap">
        <div id="stepsbar" class="stepsbar">
            <ol>
                <a href="{{ URL::to('/') }}"><li><span>Arrival & Return</span></li></a>

                <a href="{{ URL::to('/preferencesdata/'.$temp_itid) }}"><li><span>Travel Preferences</span></li></a>
                <a href=""><li class="active"><span>Meet the Itinerators</span></li></a>
                <li><span>Finish</span></li>
            </ol>
        </div>        
    </div>

    <div class="row startover">
        <div class="col-md-2"></div>
        <div class="col-md-8 startoverbox">
            <div class="row startoverbox-content">
                <div class="col-md-9 startoverbox-content-p1">
                    <input type="hidden" name="itinerarynod" class="itinerarynod" value="{{ Session::get('itinerarynod') }}">
                    <h1>
                        My {{ Session::get('itinerarynod') }} days trip in {{ Session::get('destination_country') }}
                    </h1>
                    <h2>
                        {{ Session::get('itineraryschedule') }}
                    </h2>
                </div>
                <div class="col-md-3 startoverbox-content-p2">
                    <a href="{{ URL::to('/') }}">
                        <form action="../arrivalreturn_controller" method="post">
							{!! csrf_field() !!}
							<input type="hidden" name="arrivalreturntype" value="startover">
							<button name="startoverbutton" value="startoverbutton" class="startoverbutton pull-right">
								START OVER
							</button>
						</form>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>

	<div class="row picktype">
		<div class="col-md-12">
		<form action="../pickitinerators_controller" method="post" id="pickitinerators_form" name="pickitinerators_form">
		{!! csrf_field() !!}
			<!-- {{ Session::get('temp_itid') }} -->
			<input type="radio" name="pick_type" value="random" class="picktypeoption pickrandom"> We will choose for you
			<input type="radio" name="pick_type" value="manual" class="picktypeoption pickmanual"> Manual
		</div>
	</div>
	
	<div class="row notification-limitpick reg-warning">
		<div class="col-md-12">
			<p>
				
			</p>
		</div>
	</div>

	<div class="row pickitinerators-content">
		<p>
			<?php $k=1; ?>
			@foreach($itineratorslist as $itineratorslist)
			<?php
			// if ($itineratorslist['del'] == 1) {
			// 	continue;
			// 	// $k++;
			// }
			for ($j=0; $j < count($relationlist); $j++) { 
				if ($itineratorslist['itinerators_id']==$relationlist[$j]['itinerators_id']) {
					$checked = 'checked';
					break;
				}
				else{
					$checked = '';
				}
			}
			?>
				<div class="col-md-4 itinerators-box">
					<table>
						<tr class="itinerator-name">
							<td colspan="2">
								<input type="checkbox" id="itinerators-checkbox<?php echo $k; ?>" name="itinerators[]" class="itinerators-checkbox" value="{{ $itineratorslist->itinerators_id }}" <?php echo $checked; ?>> 
								<label for="itinerators-checkbox<?php echo $k; ?>">{{ $itineratorslist->name }}</label>
							</input>
						</td>
						</tr>
						<tr class="itinerator-photo">
							<td colspan="2"><img src="{{ $itineratorslist->photo }}"></td>
						</tr>
						<tr class="itinerator-skill">
							<td>Must See: </td>
							<!-- <td>{{ $itineratorslist->skill_mustsee }}</td> -->
							<td>
								<?php
								$mustsee = $itineratorslist->skill_mustsee;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$mustsee; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Cuisine</td>
							<!-- <td>{{ $itineratorslist->skill_cuisine }}</td> -->
							<td>
								<?php
								$cuisine = $itineratorslist->skill_cuisine;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$cuisine; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>History</td>
							<!-- <td>{{ $itineratorslist->skill_history }}</td> -->
							<td>
								<?php
								$history = $itineratorslist->skill_history;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$history; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Entertainment</td>
							<!-- <td>{{ $itineratorslist->skill_entertainment }}</td> -->
							<td>
								<?php
								$entertainment = $itineratorslist->skill_entertainment;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$entertainment; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Adventure</td>
							<!-- <td>{{ $itineratorslist->skill_adventure }}</td> -->
							<td>
								<?php
								$adventure = $itineratorslist->skill_adventure;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$adventure; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Culture & Art</td>
							<!-- <td>{{ $itineratorslist->skill_culture }}</td> -->
							<td>
								<?php
								$culture = $itineratorslist->skill_culture;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$culture; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Nature</td>
							<!-- <td>{{ $itineratorslist->skill_nature }}</td> -->
							<td>
								<?php
								$nature = $itineratorslist->skill_nature;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$nature; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Shopping</td>
							<!-- <td>{{ $itineratorslist->skill_shopping }}</td> -->
							<td>
								<?php
								$shopping = $itineratorslist->skill_shopping;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$shopping; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
						<tr class="itinerator-skill">
							<td>Sports</td>
							<!-- <td>{{ $itineratorslist->skill_sports }}</td> -->
							<td>
								<?php
								$sports = $itineratorslist->skill_sports;
								?>
								<ul class="itineratorsrating">
								@for($i=0; $i<$sports; $i++)
								<li>
									<img src="{{ URL::asset('images/star.png') }}">
								</li>
								@endfor
								</ul>
							</td>
						</tr>
					</table>
				</div>
				<?php $k++; ?>
			@endforeach
		</p>
	</div>

	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 input-style-backnextbutton">
			<ul>
				<li>
					<input class="input-style-backbutton" type="submit" name="button" id="inputstyleback" value="BACK"></input>
					
				</li>
				<li>
					<input class="input-style-nextbutton pickitineratorspagenext" type="submit" name="button" id="inputstylecontinue" value="NEXT"></input>
					
				</li>
			</ul>
		</div>
	</div>
	</form>

	<div class="row inputstyle-sectionline">
		<div class="col-md-12"></div>
	</div>
</div>
@endsection