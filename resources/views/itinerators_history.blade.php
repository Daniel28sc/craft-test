<!-- resources/views/itinerators_history.blade.php -->

@extends('layouts.itineratorspagetemplate')

@section('content')
<?php
$itinerators_id = $data['itineratorsid'];
$itinerators = $data['itinerators'];
$log = $data['log'];
// $iirelation_list = $data['iirelation_list'];
// $itinerary = $data['itinerary'];
// $itinerary_day = $data['itinerary_day'];
// $itinerary_day_item = $data['itinerary_day_item'];
// $cityList = $data['cityList'];
// $placeList = $data['placeList'];
?>
<script>
	$(function() {
        var columns = [{
        	field: 'no',
        	title: 'No.',
        	sortable: true,
        	halign: 'center',
        	align: 'center'
        },{
        	field: 'description',
        	title: 'Description',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'added_on',
        	title: 'Date & Time',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        }];
        $('#itineratorshistorytable').bootstrapTable({
        	pagination: true,
        	smartDisplay: true,
        	striped: true,
        	sortable: true,
        	columns: columns,
        	pageSize: 15,
        	search: 'true'
        });
    });
</script>
<div class="container-fluid itineratorshome">
	<div class="row navbar">
		<div class="col-md-6 navbar1">
			<ul>
				<li>
					<a href="{{ url('/itineratorshome') }}">LIST</a>
				</li>
				<li>
					<a href="{{ url('/myjob') }}">MY JOB</a>
				</li>
				<li>
					<a href="{{ url('/itinerators_history') }}">HISTORY</a>
				</li>
			</ul>
		</div>
		<div class="col-md-6 navbar2">
			<ul class="pull-right">
				<li>
					<a href="{{ url('/itineratorshome') }}">{{ $itinerators[0]['name'] }}</a>
				</li>
				<li>
					<a href="{{ url('/itineratorssignout') }}">SIGN OUT</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="container itineratorshistory">
	<div class="row">
		<div class="col-md-12">
			@if(count($log)>0)
			<table id="itineratorshistorytable" class="table table-hover">
				<!-- <tr>
					<th>No.</th>
					<th>Description</th>
					<th>Date & Time</th>
				</tr> -->
				<?php $x = 1; ?>
				@for($i=0; $i<count($log); $i++)
					<tr>
						<td>{{ $x }}</td>
						<td>{{ $log[$i]['description'] }}</td>
						<td>{{ $log[$i]['added_on'] }}</td>
					</tr>
					<?php $x++; ?>
				@endfor
			</table>
			@endif
		</div>
	</div>
</div>
@endsection