<!-- resources/views/layouts/itineratorspagetemplate.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Craftrip - Itinerary by Locals</title>

    <!-- CSS And JavaScript -->
    @include('includes.javascript')
    @include('includes.css')

</head>

<body>
    <div class="container-fluid">
    @include('includes.itineratorspageheader')

    @yield('content')

    @include('includes.footer')

    </div>
</body>
</html>
