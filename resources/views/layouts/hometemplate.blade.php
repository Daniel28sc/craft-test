<!-- resources/views/layouts/hometemplate.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Craftrip - Itinerary by Locals</title>

    <!-- CSS And JavaScript -->
    @include('includes.javascript')
    @include('includes.css')

</head>

<body>
    <div class="container-fluid">
    @include('includes.homeheader')

    @yield('content')

    @include('includes.footer')

    </div>
</body>
</html>
