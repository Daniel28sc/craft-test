<!-- resources/views/validation.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<div class="container">
	<div class="row validation-content">
		<h1>
			{{ $headmsg }}
		</h1>
		<p>
			{{ $msg }}
			<br>
		</p>
	</div>
</div>

@endsection