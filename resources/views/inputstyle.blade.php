<!-- resources/views/inputstyle.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<!-- {{ Session::get('temp_itid') }} -->
<?php
if (session('arrivaldate') != '') {
	$date1 = session('arrivaldate');
	$datedata1 = date_parse($date1);
	$year1 = $datedata1['year'];
	$month1 = $datedata1['month'];
	$day1 = $datedata1['day'];
	$strdate1 = $year1.'/'.$month1.'/'.$day1;
	$cal1 = strtoupper(date('Y M d', strtotime($strdate1)));

	$date2 = session('returndate');
	$datedata2 = date_parse($date2);
	$year2 = $datedata2['year'];
	$month2 = $datedata2['month'];
	$day2 = $datedata2['day'];
	$strdate2 = $year2.'/'.$month2.'/'.$day2;
	if ($year1 == $year2) {
		$cal2 = strtoupper(date('M d', strtotime($strdate2)));
	}
	else{
		$cal2 = strtoupper(date('Y M d', strtotime($strdate2)));
	}

	$itineraryschedule = $cal1.' - '.$cal2;
}
else{
	$itineraryschedule = '';
}

session(['itineraryschedule' => $itineraryschedule]);
?>
<script>
	$(function() {
		$('.inputstylecalendar').trigger('change');
	});
</script>
<div class="container">
	<!-- <div class="row input-navigation">
		<div class="col-md-12">
			<ul>
				<li>
					<p class="inputcircle"></p>
				</li>
				<li class="inputnav-line"></li>
				<li>
					<p class="inputcircle-active"></p>
				</li>
				<li class="inputnav-line"></li>
				<li>
					<p class="inputcircle"></p>
				</li>
				<li class="inputnav-line"></li>
				<li>
					<p class="inputcircle"></p>
				</li>
				<li class="inputnav-line"></li>
				<li>
					<p class="inputcircle"></p>
				</li>
			</ul>
			<ul>
				<li>
					<p class="inputcircle-text">Arrival & Return</p>
				</li>
				<li class="inputnav-line-white"></li>
				<li>
					<p class="inputcircle-text active">Input Style</p>
				</li>
				<li class="inputnav-line-white"></li>
				<li>
					<p class="inputcircle-text">Preferences & Data</p>
				</li>
				<li class="inputnav-line-white"></li>
				<li>
					<p class="inputcircle-text">Pick Itinerator</p>
				</li>
				<li class="inputnav-line-white"></li>
				<li>
					<p class="inputcircle-text">Submit Request</p>
				</li>
			</ul>
		</div>
	</div> -->

	<!-- <div class="row input-style-heading">
		<div class="col-md-12">
			<div id="itinerarynod_val" class="{{ Session::get('itinerarynod') }}"></div>
			<h1>My {{ Session::get('itinerarynod') }} days trip</h1>
			<table class="input-style-heading-table">
				<tr>
					<td class="input-style-heading-table-left">
						<p>
							{{ Session::get('arrivaldate') }}							
							<br>
							{{ Session::get('arrivalairport') }}
						</p>
					</td>
					<td class="input-style-heading-table-middle">
						<img src="{{ URL::asset('images/arrow.png') }}">
					</td>
					<td class="input-style-heading-table-right">
						<p>
							{{ Session::get('returndate') }}
							<br>
							{{ Session::get('returnairport') }}
						</p>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row inputstyle-sectionline">
		<div class="col-md-12"></div>
	</div> -->

	<div id="stepsbar-wrap">
		<div id="stepsbar" class="stepsbar">
			<ol>
				<a href=""><li class="active"><span>Arrival & Return</span></li></a>

				<li ><span>Travel Preferences</span></li>
				<li><span>Meet the Itinerators</span></li>
				<li><span>Finish</span></li>
			</ol>
		</div>        
	</div>

	<div class="row startover">
		<div class="col-md-2"></div>
		<div class="col-md-8 startoverbox">
			<div class="row startoverbox-content">
				<div class="col-md-9 startoverbox-content-p1">
					<input type="hidden" name="itinerarynod" class="itinerarynod" value="{{ Session::get('itinerarynod') }}">
					<h1>
						My {{ Session::get('itinerarynod') }} days trip in {{ Session::get('destination_country') }}
					</h1>
					<h2>
						{{ Session::get('itineraryschedule') }}
					</h2>
				</div>
				<div class="col-md-3 startoverbox-content-p2">
					<a href="{{ URL::to('/') }}">
						<form action="arrivalreturn_controller" method="post">
							{!! csrf_field() !!}
							<input type="hidden" name="arrivalreturntype" value="startover">
							<button name="startoverbutton" value="startoverbutton" class="startoverbutton pull-right">
								START OVER
							</button>
						</form>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	
	<div class="row notification-limitpick reg-warning">
		<div class="col-md-12">
			<p>
				<!-- You have already selected {{ Session::get('itinerarynod') }} days -->
			</p>
		</div>
	</div>

	<div class="row input-style-content">
		<p class="inputitineraryinstruction1">Please choose which day will be included in your itinerary</p>
		<!-- <div class="col-md-2"></div> -->
		<div class="col-md-12 input-style-calendar">
			<div id="calendar">
				<?php
				$arrivaldate = Session::get('arrivaldate');
				$returndate = Session::get('returndate');

				$arrivaldate_day = date("d",strtotime($arrivaldate));
				$arrivaldate_month = date("m",strtotime($arrivaldate));
				$arrivaldate_year = date("Y",strtotime($arrivaldate));

				$returndate_day = date("d",strtotime($returndate));
				$returndate_month = date("m",strtotime($returndate));
				$returndate_year = date("Y",strtotime($returndate));

				$lastdayinarrivaldate = date("Y/m/t",strtotime($arrivaldate));
				$lastdayinarrivaldate_day = date("t",strtotime($arrivaldate));
				$lastdayinarrivaldate_month = date("m",strtotime($arrivaldate));
				$lastdayinarrivaldate_year = date("Y",strtotime($arrivaldate));

				$lastdayinreturndate = date("Y/m/t",strtotime($returndate));
				$lastdayinreturndate_day = date("t",strtotime($returndate));
				$lastdayinreturndate_month = date("m",strtotime($returndate));
				$lastdayinreturndate_year = date("Y",strtotime($returndate));
				
				$counterdate = $arrivaldate;
				$counterdate_day = date("d",strtotime($counterdate));
				$counterdate_month = date("m", strtotime($counterdate));
				$counterdate_year = date("Y", strtotime($counterdate));
				
				//This gets today's date 
				$date =time () ;

				//This puts the day, month, and year in seperate variables 
				// $day = date('d', $date) ; 
				// $month = date('m', $date) ; 
				// $year = date('Y', $date) ;
				?>
				<div class="row">
				<form action="inputstyle_controller" method="post" id="inputstyle_form" name="inputstyle_form">
				{!! csrf_field() !!}
				<input type="hidden" name="itinerarynod" value="{{ Session::get('itinerarynod') }}">
				<div class="form-group">
					<label for="itineraryname">Itinerary Name:</label>
					<input type="text" class="form-control itineraryname" id="itineraryname" name="itineraryname">
				</div>
				<?php
				while ($counterdate_month <= $returndate_month) {
					$day = $counterdate_day;
					$month = $counterdate_month;
					$year = $counterdate_year;

					//Here we generate the first day of the month 
					$first_day = mktime(0,0,0,$month, 1, $year) ; 

					//This gets us the month name 
					$title = date('F', $first_day) ;

					//Here we find out what day of the week the first day of the month falls on 
					$day_of_week = date('D', $first_day) ; 

					//Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero
					switch($day_of_week){ 
						case "Sun": $blank = 0; break; 
						case "Mon": $blank = 1; break; 
						case "Tue": $blank = 2; break; 
						case "Wed": $blank = 3; break; 
						case "Thu": $blank = 4; break; 
						case "Fri": $blank = 5; break; 
						case "Sat": $blank = 6; break; 
					}

					//We then determine how many days are in the current month
					$days_in_month = cal_days_in_month(0, $month, $year) ; 

					//Here we start building the table heads 
					echo "<div class='col-md-6 inputstyle-calendarbox'>";
					echo "<table border=1 width=350 class='calendar-table'>";
					echo "<tr><th colspan=7> $title $year </th></tr>";
					echo "<tr height=40 >
					<td width=40 class='calendar-table-dayname'>S</td>
					<td width=40 class='calendar-table-dayname'>M</td>
					<td	width=40 class='calendar-table-dayname'>T</td>
					<td width=40 class='calendar-table-dayname'>W</td>
					<td width=40 class='calendar-table-dayname'>T</td>
					<td width=40 class='calendar-table-dayname'>F</td>
					<td width=40 class='calendar-table-dayname'>S</td>
					</tr>";

					//This counts the days in the week, up to 7
					$day_count = 1;

					echo "<tr height=40 >";

					//first we take care of those blank days
					while ( $blank > 0 ) 
					{ 
						echo "<td></td>"; 
						$blank = $blank-1; 
						$day_count++;
					} 

					//sets the first day of the month to 1 
					$day_num = 1;

					//count up the days, untill we've done all of them in the month
					while ( $day_num <= $days_in_month ) 
					{
						$dayval_day = $day_num;
						$dayval_month = $counterdate_month;
						$dayval_year = $counterdate_year;
						$dayval = $dayval_year.'/'.$dayval_month.'/'.$dayval_day;
						$dayval = date('Y/m/d',strtotime($dayval));

						if (($day_num<$arrivaldate_day) && ($counterdate_month == $arrivaldate_month)) {
							echo "<td class='daynum-outdated'>$day_num</td>";
						}
						elseif (($day_num>$returndate_day) && ($counterdate_month == $returndate_month)) {
							echo "<td class='daynum-outdated'>$day_num</td>";
						}
						else{
							$checked = '';
							if (count($tidaylist) > 0) {
								for ($i=0; $i < count($tidaylist); $i++) { 
									if ($tidaylist[$i]['date'] == $dayval) {
										$checked = 'checked';
										break;
									}
								}
							}
							echo "<td class='daynum'>
							<label class='spandaynum'>
							<input type='checkbox' name='inputstylecalendar[]' value='$dayval' class='inputstylecalendar' $checked/>
							$day_num </label>

							</td>"; 
						}

						// if ($counterdate_month == $arrivaldate_month) {
						// 	if ($day_num<$arrivaldate_day) {
						// 		echo "<td class='daynum-outdated'>$day_num</td>"; 

						// 	}
						// 	else{
						// 		echo "<td class='daynum'>
						// 		<label class='spandaynum'>
						// 		<input type='checkbox' name='inputstylecalendar[]' value=$dayval class='inputstylecalendar'/>
						// 		$day_num </label>
								
						// 		</td>"; 
						// 	}
						// }
						// elseif ($counterdate_month == $returndate_month) {

						// 	if ($day_num>$returndate_day) {

						// 		echo "<td class='daynum-outdated'> $day_num </td>"; 
						// 	}
						// 	else{
						// 		echo "<td class='daynum'>
						// 		<label class='spandaynum'>
						// 		<input type='checkbox' name='inputstylecalendar[]' value=$dayval class='inputstylecalendar'/>
						// 		$day_num </label>
								
						// 		</td>"; 
						// 	}
						// }
						// else{
						// 	echo "<td class='daynum'>
						// 	<label class='spandaynum'>
						// 	<input type='checkbox' name='inputstylecalendar[]' value=$dayval class='inputstylecalendar'/>
						// 	$day_num</label>
							
						// 	</td>"; 
						// }

						$day_num++; 
						$day_count++;

						//Make sure we start a new row every week
						if ($day_count > 7)
						{
							echo "</tr><tr height=40>";
							$day_count = 1;
						}
					} 

					//Finaly we finish out the table with some blank details if needed
					while ( $day_count >1 && $day_count <=7 ) 
					{ 
						echo "<td> </td>"; 
						$day_count++; 
					}
					echo "</tr></table>";
					echo "</div>";
					$counterdate_day = '1';
					$counterdate_month = $counterdate_month+1;
					$counterdate_year = $counterdate_year;
					$counterdate = $counterdate_month.'/'.$counterdate_day.'/'.$counterdate_year;
					$counterdate = date('d/m/Y',strtotime($counterdate));
				}

				?>
			</div>
			</div>
		</div>
		<!-- <div class="col-md-2 input-style-button">
			<div class="row">
				<div class="col-md-12 input-style-button-simple">
					<label>
					<input type="radio" name="inputstyletype" value="simple" class="inputstyletype"></input>
					<img src="{{ URL::asset('images/checkmark.png') }}" class="input-style-checkmark checkmark1">
					<h1>Simple</h1>
					<p>
						Just tell us your preferences once, and we will create your unforgetable journey plan
					</p>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 input-style-button-detail">
					<label>
					<input type="radio" name="inputstyletype" value="detail" class="inputstyletype"></input>
					<img src="{{ URL::asset('images/checkmark.png') }}" class="input-style-checkmark checkmark2">
					<h1>Detail</h1>
					<p>
						Tell us when, where, and how you would like to travel on every single day
					</p>
					</label>
				</div>
			</div>
		</div> -->
	</div>
	
	<!-- <input type="hidden" name="arrivalairport" id="arrivalairport" value="{{ Session::get('arrivalairport') }}" />
	<input type="hidden" name="arrivaldate" id="arrivaldate" value="{{ Session::get('arrivaldate') }}" />
	<input type="hidden" name="arrivaltime" id="arrivaltime" value="{{ Session::get('arrivaltime') }}" />
	<input type="hidden" name="returnairport" id="returnairport" value="{{ Session::get('returnairport') }}" />
	<input type="hidden" name="returndate" id="returndate" value="{{ Session::get('returndate') }}" />
	<input type="hidden" name="returntime" id="returntime" value="{{ Session::get('returntime') }}" />
	<input type="hidden" name="itinerarynod" id="itinerarynod" value="{{ Session::get('itinerarynod') }}" /> -->

	<!-- <div class="row">
		<div class="col-md-12 input-style-backnextbutton inputstylebottomnav">
			<ul>
				<li>
					<input class="input-style-backbutton" type="submit" name="button" id="inputstyleback" value="BACK"></input>
					
				</li>
				<li>
					<input class="input-style-nextbutton" type="submit" name="button" id="inputstylecontinue" value="NEXT"></input>
					
				</li>
			</ul>
		</form>
		</div>
	</div> -->

	<div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6 input-style-backnextbutton">
            <ul>
                <li>
                    <a href="{{ URL::to('/') }}"><input class="input-style-backbutton" type="button" name="button" id="btn-back" value="BACK"></input></a>
                </li>
                <li>
                    <input class="input-style-nextbutton inputstylepagenext" type="submit" name="button" id="inputstylecontinue" value="NEXT"></input>
                </li>
            </ul>
        </div>
    </div>

	<div class="row inputstyle-sectionline inputstylepage">
		<div class="col-md-12"></div>
	</div>

</div>

@endsection