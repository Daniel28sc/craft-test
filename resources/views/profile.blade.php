<!-- resources/views/profile.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script>
$(function() {
	$("#country").countrySelect();
});
</script>
<div class="container">
	<div class="row profile-heading">
		<div class="col-md-12">
			<p>Welcome back, {{ $user[0]['name'] }}!</p>
			<ul class="topprofilemenu">
				<li>
					Setting
				</li>
				<li>
					|
				</li>
				<li>
                    <a href="{{ url('/signout') }}">Logout</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="row inputstyle-sectionline">
		<div class="col-md-12"></div>
	</div>

	<div class="row userprofile">
		<div class="col-md-3 profile-menu">
			<ul>
				<li class="menu-myprofile">
					<p><a href="{{ url('/profile') }}">My Profile</a></p>
				</li>
				<li class="menu-myitinerary">
					<p><a href="{{ url('/myitinerary') }}">My Itinerary</a></p>
				</li>
				<li class="menu-paymenthistory">
					<p><a href="{{ url('/paymenthistory') }}">Payment History</a></p>
				</li>
				<li class="menu-itineratorhistory">
					<p>Itinerator History</p>
				</li>
			</ul>
		</div>

		<div class="col-md-9 profile-content">
			<div class="myprofile active-profilecontent">
				<form action="" method="POST" name="changedataform" class="changedataform">
				{!! csrf_field() !!}
					<table>
						<tr>
							<td>
								Name
							</td>
							<td>
								:
							</td>
							<td>
								<input type="text" class="name" name="nama" value="{{ $user[0]['name'] }}">
							</td>
						</tr>
						<tr>
							<td>
								Email
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user[0]['email'] }}
							</td>
						</tr>
					</table>
					<p>
						- Information -
					</p>
					<table>
						<tr>
							<td>
								Date of Birth
							</td>
							<td>
								:
							</td>
							<td>
								{{ $user[0]['dob'] }}
							</td>
						</tr>
						<tr>
							<td>
								Country
							</td>
							<td>
								:
							</td>
							<td>
								<input type="text" id="country" name="country">
								<!-- <input type="text" class="country" name="country" value="{{ $user[0]['country'] }}"> -->
							</td>
						</tr>
						<tr>
							<td>
								Address
							</td>
							<td>
								:
							</td>
							<td>
								<input type="text" class="address" name="address" value="{{ $user[0]['address'] }}">
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<input type="submit" name="changedatatype" value="Change Data" class="changedata">
							</td>
						</tr>
					</table>
				</form>
				<p>
					- Change Password -
				</p>
				<p>
					{{ $changepwnotice }}
				</p>
				<form action="" method="POST" name="changepwform" class="changepwform">
				{!! csrf_field() !!}
					<table>
						<tr>
							<td>
								Old Password
							</td>
							<td>
								:
							</td>
							<td>
								<input type="password" name="oldpw" class="oldpw">
							</td>
						</tr>
						<tr>
							<td>
								New Password
							</td>
							<td>
								:
							</td>
							<td>
								<input type="password" name="newpw" class="newpw">
							</td>
						</tr>
						<tr>
							<td>
								New Password (confirm)
							</td>
							<td>
								:
							</td>
							<td>
								<input type="password" name="newpwconfirm" class="newpwconfirm">
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<input type="submit" name="changedatatype" value="Change Password" class="changepw">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	var country = '<?php echo $user[0]["country"]; ?>';
	if (country!='') {
		$("#country").countrySelect("setCountry", country);
	};
});
</script>
@endsection