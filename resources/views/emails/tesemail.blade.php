<h1>
	Thank you for registering.
</h1>
<p>
	Please verify your account by clicking the link below.
	<br>
	{{ $link }}
</p>