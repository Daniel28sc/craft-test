<h1>
	You have a new work request.
</h1>
<p>
	Please check the request by clicking the link below.
	<br>
	{{ $link }}
</p>