<!-- resources/views/uploaditinerators.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script type="text/javascript">
    var _siteurl = "{{ url('/') }}";
    // For Image Cropper
    $(function() {
        $('.cropme').simpleCropper();
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/itinerators.js') }}"></script>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-success pull-right" id="btn-add"><i class="glyphicon glyphicon-plus"></i> Add Itinerators</button>
            <table id="itinerators-table" class="table-condensed"></table>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="modal-itinerators" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span id="modal-itinerators-title-action"></span> Itinerators</h4>
                </div>
                
                <form class="form-horizontal" id="form-itinerators">
                    
                <div class="modal-body">
                    
                        <input type="hidden" name="action" id="modal-itinerators-action" value=""/>
                        <input type="hidden" name="itinerators_id" id="field-itinerators_id" value=""/>
                        <div class="row imgpreview" id="imgpreview">
                            <div class="col-md-12">
                                Select image to upload:
                                <div class="cropme" style="width: 200px; height: 200px;"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-name" class="control-label col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="field-name" name="name" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="field-kanji" class="control-label col-sm-4">Kanji</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="field-kanji" name="kanji" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="field-rating" class="control-label col-sm-4">Rating</label>
                            <div class="col-sm-1">
                                <output id="output_rating">3</output>
                            </div>
                            <div class="col-sm-7">
                                <input type="range" class="form-control bar field-range-type" id="field-rating" min="1" max="5"  name="rating">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="skill" class="control-label col-sm-4">Skill</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <span>Must See : <output id="output_mustsee">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-mustsee" min="1" max="5"  name="mustsee">
                                    </div>
                                    <div class="col-sm-4">
                                        <span>Cuisine : <output id="output_cuisine">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-cuisine" min="1" max="5"  name="cuisine">
                                    </div>
                                    <div class="col-sm-4">
                                        <span>History : <output id="output_history">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-history" min="1" max="5"  name="history">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <span>Entertainment : <output id="output_entertainment">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-entertainment" min="1" max="5"  name="entertainment">
                                    </div>
                                    <div class="col-sm-4">
                                        <span>Adventure : <output id="output_adventure">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-adventure" min="1" max="5"  name="adventure">
                                    </div>
                                    <div class="col-sm-4">
                                        <span>Culture & Art : <output id="output_culture">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-culture" min="1" max="5"  name="culture">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <span>Nature : <output id="output_nature">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-nature" min="1" max="5"  name="nature">
                                    </div>
                                    <div class="col-sm-4">
                                        <span>Shopping : <output id="output_shopping">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-shopping" min="1" max="5"  name="shopping">
                                    </div>
                                    <div class="col-sm-4">
                                        <span>Sports : <output id="output_sports">3</output></span>
                                        <input type="range" class="form-control bar field-range-type" id="field-sports" min="1" max="5"  name="sports">
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btn-save">Save</button>
                </div>
                
                </form>

            </div>
        </div>
    </div>
    <!-- Modal -->
</div>
@endsection