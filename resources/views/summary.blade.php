<!-- resources/views/summary.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script>
$(function() {
	$("#country").countrySelect();
});
</script>

<?php
    $userId = $data['userId'];
    $cityList = $data['cityList'];
    $placeList = $data['placeList'];
    $itinerary = $data['itinerary'];
    $itineraryDay = $data['itineraryDay'];
    $itineraryDayItem = $data['itineraryDayItem'];
    $itinerators = $data['itinerators'];
    $iirelation = $data['iirelation'];
    $temp_itid = session('temp_itid');
    for ($i=0; $i < count($cityList); $i++) { 
        $cityList[$i]['description'] = str_replace('"', "'", $cityList[$i]['description']);
    }
    for ($i=0; $i < count($placeList); $i++) { 
        $placeList[$i]['description'] = str_replace('"', "'", $placeList[$i]['description']);
    }

    if ($itinerary['arrival_date'] != '') {
        $date1 = $itinerary['arrival_date'];
        $datedata1 = date_parse($date1);
        $year1 = $datedata1['year'];
        $month1 = $datedata1['month'];
        $day1 = $datedata1['day'];
        $strdate1 = $year1.'/'.$month1.'/'.$day1;
        $cal1 = strtoupper(date('Y M d', strtotime($strdate1)));

        $date2 = $itinerary['return_date'];
        $datedata2 = date_parse($date2);
        $year2 = $datedata2['year'];
        $month2 = $datedata2['month'];
        $day2 = $datedata2['day'];
        $strdate2 = $year2.'/'.$month2.'/'.$day2;
        if ($year1 == $year2) {
            $cal2 = strtoupper(date('M d', strtotime($strdate2)));
        }
        else{
            $cal2 = strtoupper(date('Y M d', strtotime($strdate2)));
        }

        $itineraryschedule = $cal1.' - '.$cal2;
    }
    else{
        $itineraryschedule = '';
    }
?>

<script type="text/javascript">
    var _userId = '{{ $userId }}',
        _cityList = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($cityList)); ?>'),
        _placeList = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($placeList)); ?>'),
        _itineraryId = '{{ $itinerary['it_id'] }}',
        _itineraryDayItem = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($itineraryDayItem)); ?>');
</script>
<script type="text/javascript" src="{{ URL::asset('js/common.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/summary.js') }}"></script>

<div class="container">
    {{-- {{ $itid }} --}}
    {{-- {{ $itinerary['day'] }} --}}
    {{-- {{ $itineraryday[0]['date'] }} --}}
    <div id="stepsbar-wrap">
        <div id="stepsbar" class="stepsbar">
            <ol>
                <a href="{{ URL::to('/') }}"><li><span>Arrival & Return</span></li></a>

                <a href="{{ URL::to('/preferencesdata/'.$temp_itid) }}"><li><span>Travel Preferences</span></li></a>
                <a href="{{ URL::to('/pickitinerators/'.$temp_itid) }}"><li><span>Meet the Itinerators</span></li></a>
                <a href=""><li class="active laststep"><span>Finish</span></li></a>
            </ol>
        </div>        
    </div>

    <!-- <div class="row startover">
        <div class="col-md-2"></div>
        <div class="col-md-8 startoverbox">
            <div class="row startoverbox-content">
                <div class="col-md-9 startoverbox-content-p1">
                    <input type="hidden" name="itinerarynod" class="itinerarynod" value="{{ Session::get('itinerarynod') }}">
                    <h1>
                        My {{ Session::get('itinerarynod') }} days trip in {{ Session::get('destination_country') }}
                    </h1>
                    <h2>
                        {{ Session::get('itineraryschedule') }}
                    </h2>
                </div>
                <div class="col-md-3 startoverbox-content-p2">
                    <a href="{{ URL::to('/') }}">
                        <form action="../arrivalreturn_controller" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="arrivalreturntype" value="startover">
                            <button name="startoverbutton" value="startoverbutton" class="startoverbutton pull-right">
                                START OVER
                            </button>
                        </form>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div> -->

    <div class="row summaryitinerary-content">
        <div class="col-md-12">
            <div class="reg-warning warn-summarypage">
                This itinerary is already saved by other user.   
            </div>

            <div class="summary-profile">
                <table>
                    <tr>
                        <td colspan="2">
                            <h1>ITINERARY SUMMARY</h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p>
                                {{ $itinerary['name'] }}
                            </p>
                        </td>
                    </tr>
                    <tr class="summary-day">
                        <td>
                            <p>
                                {{ $itinerary['day'] }} Days
                            </p>
                        </td>
                        <td>
                            <p class="pull-right">
                                <?php echo $itineraryschedule; ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-plane fa-2x" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>
                                {{ $itinerary['arrival_airport'] }} ({{ $itinerary['arrival_date'] }} {{ $itinerary['arrival_time'] }})
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-plane fa-2x" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>
                                {{ $itinerary['return_airport'] }} ({{ $itinerary['return_date'] }} {{ $itinerary['return_time'] }})
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-jpy fa-2x" aria-hidden="true"></i>
                        </td>
                        <td>
                            <p>
                                {{ $itinerary['budget'] }}
                            </p>
                        </td>
                    </tr>
                    <tr class="summary-pref-1">
                        <td>
                            <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                        </td>
                        <td>
                            <?php
                            $x = 1;
                            $x1 = 1;
                            $arr_pref_var1 = ['','Family','Backpack','Vegetarian','Romantic','Culinaire','Cheap Eat','Bar & Club','Night Seeing'];
                            $arr_pref_var1_1 = ['','family','backpack','vegetarian','romantic','culinaire','cheapeat','barclub','nightseeing'];
                            while ($x < 9) {

                                if ($itinerary[$arr_pref_var1_1[$x]] == '1') {
                                    if ($x1%3 == 1) {
                                        echo '<ul class="ul-cpvar-1">';
                                    }

                                    echo '<li>'.$arr_pref_var1[$x].'</li>';

                                    if ($x1%3 == 0) {
                                        echo '</ul>';
                                    }
                                    $x1++;
                                }

                                $x++;
                            }
                            ?>
                        </td>
                    </tr>
                    <tr class="summary-pref-2">
                        <td>
                            <i class="fa fa-map-o fa-2x" aria-hidden="true"></i>
                        </td>
                        <td>
                            <?php
                            $arr_pref_var2 = ['','Popular Places','Culture & Art','Cuisine','Nature','Entertainment','History','Shopping','Sports'];
                            $arr_pref_var2_1 = ['','mustsee','culture','cuisine','nature','entertainment','history','shopping','sports'];
                            for ($i=1; $i < 9; $i++) { 
                                $pref_rate = $itinerary[$arr_pref_var2_1[$i]];
                                if ($i%3 == 1) {
                                    echo '<ul class="ul-cpvar-2">';
                                }
                                echo '<li>';
                                echo $arr_pref_var2[$i];
                                echo '<span>';
                                for ($ii=0; $ii < $pref_rate; $ii++) { 
                                    echo '<i class="fa fa-circle prefratecircle" aria-hidden="true"></i>';
                                }
                                echo '</span>';
                                echo '</li>';
                                if ($i%3 == 0) {
                                    echo '</ul>';
                                }
                            }
                            ?>
                        </td>
                    </tr>
                </table>


                <div class="summary-preferences">
                    <div class="summary-cityplace">
                        <div class="row">
                            <?php
                            $tempdate = '';
                            $temp_cpday = $itineraryDayItem[0]['itday_id'];
                            $counterday = 0;
                            $temp_city = '';
                            $counter_itday = -1;
                            for ($i=0; $i < count($itineraryDayItem); $i++) { 
                                $index_city = -1;
                                $index_place = -1;

                                $cid = $itineraryDayItem[$i]['city_id'];
                                for ($j=0; $j < count($cityList); $j++) { 
                                    if ($cityList[$j]['city_id'] == $cid) {
                                        $index_city = $j;
                                        break;
                                    }
                                }

                                $pid = $itineraryDayItem[$i]['place_id'];
                                for ($j=0; $j < count($placeList); $j++) { 
                                    if ($placeList[$j]['place_id'] == $pid) {
                                        $index_place = $j;
                                        break;
                                    }
                                }

                                $cp_bg = $cityList[$index_city]['image_source'];
                                
                                if ($itineraryDayItem[$i]['itday_id'] == $temp_cpday) {

                                }
                                else{
                                    $counterday++;
                                    $temp_cpday = $itineraryDayItem[$i]['itday_id'];
                                }

                                if ($itineraryDay[$counterday]['date'] != $tempdate) {
                                    if ($tempdate!='') {
                                        echo "</div>";
                                    }
                                    $tempdate = $itineraryDay[$counterday]['date'];
                                    $temp_city = '';
                                    $counter_itday++;
                                    echo "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12 summary-cpbox'>";
                                    echo "<span class='summary-cpdate'>".$itineraryDay[$counterday]['date'].'</span>';
                                }
                                else{

                                }
                                
                                if ($temp_city != $cityList[$index_city]['title']) {
                                    if ($index_city>-1) {
                                        echo '<p>'.$cityList[$index_city]['title'].'</p>';
                                        $temp_city = $cityList[$index_city]['title'];
                                    }
                                }

                                $write_note = 0;
                                if ($itineraryDay[$counterday]['same_as_previous'] == 'no') {
                                    if ($index_place>-1) {
                                        echo '<p>- '.$placeList[$index_place]['title'].'</p>';
                                        $write_note=1;
                                    }
                                }

                                $hotelname = '-';
                                $hoteladdress = '-';
                                $hotelphone = '-';
                                $write_hotel = 0;
                                $hotel = $itineraryDay[$counter_itday]['hotel'];
                                $hotel = explode('#|#', $hotel);

                                if ($itineraryDayItem[$i]['note'] != '') {
                                    if ($i == count($itineraryDayItem)-1) {
                                        if ($write_note == 1) {
                                            echo "<p>Note: ".$itineraryDayItem[$i]['note']."</p><br>";
                                        }
                                        $write_hotel=1;
                                    }
                                    else{
                                        if (($itineraryDayItem[$i]['itday_id'] != $itineraryDayItem[$i+1]['itday_id']) ||
                                            ($itineraryDayItem[$i]['city_id'] != $itineraryDayItem[$i+1]['city_id'])) {
                                            if ($write_note == 1) {
                                                echo "<p>Note: ".$itineraryDayItem[$i]['note']."</p><br>";
                                            }
                                        }
                                        if ($itineraryDayItem[$i]['itday_id'] != $itineraryDayItem[$i+1]['itday_id']) {
                                            $write_hotel=1;
                                        }
                                    }
                                } 

                                if ($write_hotel==1) {
                                    if (!empty($hotel[0])) {
                                        $hotelname = $hotel[0];
                                    }
                                    if (!empty($hotel[1])) {
                                        $hoteladdress = $hotel[1];
                                    }
                                    if (!empty($hotel[2])) {
                                        $hotelphone = $hotel[2];
                                    }
                                }
                                
                                if ($hotelname!='-' || $hoteladdress!='-' || $hotelphone!='-') {
                                    echo "<h2>Hotel</h2>";
                                    echo "Name: ".$hotelname;
                                    echo "<br>Address: ".$hoteladdress;
                                    echo "<br>Phone: ".$hotelphone;
                                }

                                if ($itineraryDay[$counterday]['date'] != $tempdate) {
                                    // echo "</div>";
                                }

                                if ($i == count($itineraryDayItem)-1) {
                                    echo "</div>";
                                }
                            }
                            ?>
                       <!--  </div> --> <!-- For last ibox -->
                        </div>
                    </div>

                   <!--  <div class="summary-itinerarydesc">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>
                                    Itinerary Details
                                </h1>
                                <?php
                                $temp_itdayid = 0;
                                $counterday = 0;
                                for ($i=0; $i < count($itineraryDayItem); $i++) {
                                    if ($temp_itdayid != $itineraryDayItem[$i]['itday_id']) {
                                        if ($itineraryDayItem[$i]['note'] != '') {    
                                            echo "<h4>".$itineraryDay[$counterday]['date']."</h4>";
                                            echo "<p>".$itineraryDayItem[$i]['note']."</p>";
                                            echo "<br>";
                                        }
                                        $counterday++;
                                        $temp_itdayid = $itineraryDayItem[$i]['itday_id'];
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div> -->

                    <div class="summary-itinerators">
                        <h1>Itinerator(s)</h1>
                        <?php
                        if ($iirelation[0]['pick_type'] == 'random') {
                            echo "<h2>We will choose for you.</h2>";
                        }
                        else{
                        ?>
                            <h2>We will inform these...</h2>
                            <div class="row">
                                @foreach($iirelation as $iirelation)
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ibox">
                                    <?php
                                    $iid = $iirelation['itinerators_id'];
                                    for ($j=0; $j < count($itinerators); $j++) { 
                                        if ($itinerators[$j]['itinerators_id'] == $iid) {
                                            $iindex = $j;
                                            break;
                                        }
                                    }
                                    ?>
                                    <div class="ibox-photo">
                                        <img src="{{ $itinerators[$iindex]['photo'] }}" alt="">
                                    </div>
                                    <div class="ibox-desc">
                                        {{ $itinerators[$iindex]['name'] }}
                                        <br>
                                        {{ $itinerators[$iindex]['kanji'] }}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <!-- DIUBAH vvv -->
            <!-- <div class="col-md-4">
                <div class="summary-preferences">
                    <h1>Summary</h1>
                    <table>
                        <tr>
                            <td>
                                Budget:
                            </td>
                            <td>
                                {{ $itinerary['budget'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Pace:
                            </td>
                            <td>
                                {{ $itinerary['pace'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Must See:
                            </td>
                            <td>
                                {{ $itinerary['mustsee'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cuisine:
                            </td>
                            <td>
                                {{ $itinerary['cuisine'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                History:
                            </td>
                            <td>
                                {{ $itinerary['history'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Entertainment:
                            </td>
                            <td>
                                {{ $itinerary['entertainment'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Adventure:
                            </td>
                            <td>
                                {{ $itinerary['adventure'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Culture & Art:
                            </td>
                            <td>
                                {{ $itinerary['culture'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nature:
                            </td>
                            <td>
                                {{ $itinerary['nature'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Shopping:
                            </td>
                            <td>
                                {{ $itinerary['shopping'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Sports:
                            </td>
                            <td>
                                {{ $itinerary['sports'] }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="summary-itinerators">
                    <h1>Itinerators</h1>
                    <ul> -->
                        @foreach($iirelation as $iirelation)
                        <?php
                            // $itineratorid = $iirelation['itinerators_id'];
                            // $itinerators_id = -1;
                            // for ($i=0; $i < count($itinerators); $i++) { 
                            //     if ($itinerators[$i]['itinerators_id'] == $itineratorid) {
                            //         $itinerators_id = $i;
                            //         break;
                            //     }
                            // }
                            // if ($itinerators_id > -1) {
                            //     if ($itinerators[$itinerators_id]['del'] != 1) {
                            //         echo "<li>".$itinerators[$itinerators_id]['name']."</li>";
                            //     }
                            // }
                        ?>
                        @endforeach
                    <!-- </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="summary-days">
                    <ul> -->
                        <?php $dayNo = 1; ?>
                        @foreach ($itineraryDay as $itDay)
                            <!-- <li>
                                <h2>Day {{ $dayNo }} - {{ $itDay->date }}</h2> -->

                                <!-- Not needed (?) vvv -->
                                <!--
                                <ul>
                                    @foreach ($itineraryDayItem as $itDayItem)
                                        @if ($itDayItem['itday_id'] == $itDay->itday_id)
                                            <li>{{ $itDayItem['city_id'] . " -> " . $itDayItem['place_id'] }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                                -->
                                <!-- ^^^ -->
                                
                                <!-- <div class="panel-group" id="day-item-detail-{{ $dayNo }}" 
                                     data-day="{{ $dayNo }}"
                                     data-date="{{ $itDay->date }}"
                                     data-it_day_id="{{ $itDay->itday_id }}"
                                     role="tablist" aria-multiselectable="true">
                                </div>
                            </li> -->
                            <?php $dayNo++; ?>
                        @endforeach
                    <!-- </ul>
                </div>
            </div> -->
            <!-- ^^^ -->
        </div>
    </div>

	<div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6 input-style-backnextbutton">
            <ul>
                <li>
                    <a href="{{ URL::to('/') }}">
                        <form action="../arrivalreturn_controller" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="arrivalreturntype" value="startover">
                            <button name="startoverbutton" value="startoverbutton" class="input-style-backbutton startoverbutton">
                                START OVER
                            </button>
                        </form>
                    </a>
                </li>
                <li>
                    <?php
                    $userid = session('userid');
                    $itid = session('temp_itid');
                    ?>
                    <input type="hidden" class="dummyitid" value="<?php echo $itid; ?>">
                    <input class="input-style-backbutton summaryback" type="submit" name="button" id="inputstyleback" value="BACK"></input>

                </li>
                <li>
                    <?php
                    // session(['userid' => '']);
                    if (session('userid')=='') {
                    ?>
                    <button type="button" class="input-style-nextbutton" id="savesummary" data-toggle="modal" data-target="#modal-signin"> LOGIN</button>
                    <!-- <input class="input-style-nextbutton" type="submit" name="button" id="inputstylecontinue" value="LOGIN"></input> -->
                    <?php
                    }
                    else{
                    ?>
                    <form action="summary_controller" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" name="userid" class="userid" value="<?php echo $userid; ?>">
                        <input type="hidden" name="currentitid" class="currentitid" value="<?php echo $itid; ?>">
                        <input class="input-style-nextbutton" type="submit" name="button" id="inputstylecontinue" value="SAVE"></input>
                    </form>
                    <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="row inputstyle-sectionline">
        <div class="col-md-12"></div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="modal-signin" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Sign In</h4>
                </div>

                <!-- Modal content-->
                <div class="modal-body">
                    <div class="row modal-regverificationdone">
                        <div class="col=col-md-12">
                            <p>
                                We have sent you a verification email.
                                <br>Please check your email.
                                <br><span class="showregemail"></span>
                            </p>
                        </div>
                    </div>
                    <div class="row modal-loginreg active-regpage">
                        <div class="col-md-6">
                            <h4 class="modal-title">Login </h4>
                            <br>
                            <form action="login_controller" method="post" name="loginfromsummary">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label for="loginfromsummary-username">Email:</label>
                                    <input type="text" class="form-control" id="loginfromsummary-username" name="username">
                                </div>
                                <div class="form-group">
                                    <label for="loginfromsummary-password">Password:</label>
                                    <input type="password" class="form-control" id="loginfromsummary-password" name="password">
                                </div>
                                <input type="hidden" name="logintype" value="fromsummary">
    								<!-- <div class="checkbox">
    									<label><input type="checkbox"> Remember me</label>
    								</div> -->
    							<button type="submit" class="btn btn-default">Sign In</button>
    						</form>
					    </div>
    					<div class="col-md-6">
    						<h4 class="modal-title">Register </h4>
    						<br>
    						<p class="reg-warning"></p>

                            <form action="" method="post" id="registration_form" name="registration_form">
                                {!! csrf_field() !!}
								<div class="form-group">
									<label for="regfromsummary-email">Email:</label>
									<input type="email" class="form-control reg-email" id="regfromsummary-email" name="reg-email">
								</div>
								<div class="form-group">
									<label for="regfromsummary-password">Password:</label>
									<input type="password" class="form-control reg-password" id="regfromsummary-password" name="reg-password">
								</div>
								<div class="form-group">
									<label for="regfromsummary-passwordconfirm">Password (confirm):</label>
									<input type="password" class="form-control reg-passwordconfirm" id="regfromsummary-passwordconfirm" name="reg-passwordconfirm">
								</div>
								<div class="form-group">
									<label for="regfromsummary-name">Name:</label>
									<input type="text" class="form-control reg-name" id="regfromsummary-name" name="reg-name">
								</div>
								<div class="form-group">
									<label for="regfromsummary-dob">Date of Birth:</label>
									<input type="text" class="form-control reg-dob" id="regfromsummary-dob" name="reg-dob">
								</div>
								<div class="form-group">
									<label for="regfromsummary-country">Country:</label>
									<br>
									<input type="text" id="country" class="form-control reg-country" name="country">
									<!-- <input type="text" class="form-control reg-country" id="regfromsummary-country" name="reg-country"> -->
								</div>
								<div class="form-group">
									<label for="regfromsummary-address">Address:</label>
									<input type="text" class="form-control reg-address" id="regfromsummary-address" name="reg-address">
								</div>
								<input type="hidden" name="reg-type" value="fromsummary" class="reg-type">
								<button type="submit" class="btn btn-default reg-signup" value="Sign Up">Sign Up</button><span class="load"></span>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

        </div>
    </div>
    <!-- Modal -->
    
</div>

@endsection