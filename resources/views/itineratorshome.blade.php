<!-- resources/views/itineratorshome.blade.php -->

@extends('layouts.itineratorspagetemplate')

@section('content')
<?php
$itinerators_id = $data['itineratorsid'];
$itinerators = $data['itinerators'];
$iirelation_list = $data['iirelation_list'];
$itinerary = $data['itinerary'];
$itinerary_day = $data['itinerary_day'];
$itinerary_day_item = $data['itinerary_day_item'];
$cityList = $data['cityList'];
$placeList = $data['placeList'];
?>
<div class="container-fluid itineratorshome">
	<div class="row navbar">
		<div class="col-md-6 navbar1">
			<ul>
				<li>
					<a href="{{ url('/itineratorshome') }}">LIST</a>
				</li>
				<li>
					<a href="{{ url('/myjob') }}">MY JOB</a>
				</li>
				<li>
					<a href="{{ url('/itinerators_history') }}">HISTORY</a>
				</li>
			</ul>
		</div>
		<div class="col-md-6 navbar2">
			<ul class="pull-right">
				<li>
					<a href="{{ url('/itineratorshome') }}">{{ $itinerators[0]['name'] }}</a>
				</li>
				<li>
					<a href="{{ url('/itineratorssignout') }}">SIGN OUT</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="container itineratorshome">
	<div class="row itinerators-list">
		<div class="col-md-12">
			<table class='itineratorsjoblist table table-hover table-bordered'>
			<?php
			for ($i=0; $i < count($itinerary); $i++) { 
				$status = $iirelation_list[$i]['status'];
				if ($status == 'none') {
					continue;
				}
			?>
				<tr>
					<td><p><?php echo $itinerary[$i]['name']; ?>: <?php echo $itinerary[$i]['day']; ?> Days</p></td>
					<td>
						<button type="button" class="modaldetailitinerary" data-toggle="modal" data-target="#modaldetailitinerary-<?php echo ($i+1); ?>">DETAIL</button>
					</td>
					<td>
						<a href="{{ url('/requestwork/'.$itinerators_id.'/'.$itinerary[$i]['it_id']) }}">
							<button data-itid='<?php echo $itinerary[$i]['it_id']; ?>' value='<?php echo $itinerary[$i]['it_id']; ?>'>TAKE</button>
						</a>
					</td>
				</tr>
			<?php
			}
			?>
			</table>
		</div>
	</div>


	<!-- Itinerary Detail Modal -->
	@for($x=0; $x < count($itinerary); $x++)
	<?php
		$countermodal = $x+1;
		if ($itinerary[$x]['arrival_date'] != '') {
			$date1 = $itinerary[$x]['arrival_date'];
			$datedata1 = date_parse($date1);
			$year1 = $datedata1['year'];
			$month1 = $datedata1['month'];
			$day1 = $datedata1['day'];
			$strdate1 = $year1.'/'.$month1.'/'.$day1;
			$cal1 = strtoupper(date('Y M d', strtotime($strdate1)));

			$date2 = $itinerary[$x]['return_date'];
			$datedata2 = date_parse($date2);
			$year2 = $datedata2['year'];
			$month2 = $datedata2['month'];
			$day2 = $datedata2['day'];
			$strdate2 = $year2.'/'.$month2.'/'.$day2;
			if ($year1 == $year2) {
				$cal2 = strtoupper(date('M d', strtotime($strdate2)));
			}
			else{
				$cal2 = strtoupper(date('Y M d', strtotime($strdate2)));
			}

			$itineraryschedule = $cal1.' - '.$cal2;
		}
		else{
			$itineraryschedule = '';
		}
	?>
		<div id="modaldetailitinerary-<?php echo $countermodal; ?>" class="modal fade modaldetailitinerary" role="dialog">
			<div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h1>{{ $itinerary[$x]['name'] }}</h1>
					</div>
					<div class="modal-body summary-profile">
						<table>
							<tr class="summary-day">
								<td>
									<p>
										{{ $itinerary[$x]['day'] }} Days
									</p>
								</td>
								<td>
									<p>
										{{ $itineraryschedule }}
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-plane fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<p>
										{{ $itinerary[$x]['arrival_airport'] }} ({{ $itinerary[$x]['arrival_date'] }} {{ $itinerary[$x]['arrival_time'] }})
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-plane fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<p>
										{{ $itinerary[$x]['return_airport'] }} ({{ $itinerary[$x]['return_date'] }} {{ $itinerary[$x]['return_time'] }})
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-jpy fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<p>
										{{ $itinerary[$x]['budget'] }}
									</p>
								</td>
							</tr>
							<tr class="summary-pref-1">
								<td>
									<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<?php
									$i1 = 1;
									$i2 = 1;
									$arr_pref_var1 = ['','Family','Backpack','Vegetarian','Romantic','Culinaire','Cheap Eat','Bar & Club','Night Seeing'];
									$arr_pref_var1_1 = ['','family','backpack','vegetarian','romantic','culinaire','cheapeat','barclub','nightseeing'];
									while ($i1 < 9) {
										if ($itinerary[$x][$arr_pref_var1_1[$i1]] == '1') {
											if ($i2%3 == 1) {
												echo '<ul class="ul-cpvar-1">';
											}

											echo '<li>'.$arr_pref_var1[$i1].'</li>';

											if ($i2%3 == 0) {
												echo '</ul>';
											}
											$i2++;
										}
										$i1++;
									}
									?>
								</td>
							</tr>
							<tr class="summary-pref-2">
								<td>
									<i class="fa fa-map-o fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<?php
									$arr_pref_var2 = ['','Popular Places','Culture & Art','Cuisine','Nature','Entertainment','History','Shopping','Sports'];
									$arr_pref_var2_1 = ['','mustsee','culture','cuisine','nature','entertainment','history','shopping','sports'];
									for ($i=1; $i < 9; $i++) { 
										$pref_rate = $itinerary[$x][$arr_pref_var2_1[$i]];
										if ($i%3 == 1) {
											echo '<ul class="ul-cpvar-2">';
										}
										echo '<li>';
										echo $arr_pref_var2[$i];
										echo '<span>';
										for ($ii=0; $ii < $pref_rate; $ii++) { 
											echo ' <i class="fa fa-circle prefratecircle" aria-hidden="true"></i>';  
										}
										echo '</span>';
										echo '</li>';
										if ($i%3 == 0) {
											echo '</ul>';
										}
									}
									?>
								</td>
							</tr>
						</table>

						<div class="summary-preferences">
							<div class="summary-cityplace">
								<div class="row">
									<?php
									$tempdate = '';
									$temp_cpday = $itinerary_day_item[$x][0][0]['itday_id'];
									$counterday = 0;
									$temp_city = '';
									for ($i=0; $i < count($itinerary_day[$x]); $i++) { 
										for ($j=0; $j < count($itinerary_day_item[$x][$i]); $j++) {
											$index_city = 0;
											$index_place = 0;

											$cid = $itinerary_day_item[$x][$i][$j]['city_id'];
											for ($k=1; $k < count($cityList); $k++) { 
												if ($cityList[$k]['city_id'] == $cid) {
													$index_city = $k;
													break;
												}
											}
											$cp_bg = $cityList[$index_city]['image_source'];

											$pid = $itinerary_day_item[$x][$i][$j]['place_id'];
											for ($k=1; $k < count($placeList); $k++) { 
												if ($placeList[$k]['place_id'] == $pid) {
													$index_place = $k;
													break;
												}
											}

											if ($itinerary_day_item[$x][$i][$j]['itday_id'] == $temp_cpday) {

											}
											else{
												$counterday++;
												$temp_cpday = $itinerary_day_item[$x][$i][$j]['itday_id'];
											}

											if ($itinerary_day[$x][$counterday]['date'] != $tempdate) {
												if ($tempdate!='') {
													echo "</div>";
												}
												$tempdate = $itinerary_day[$x][$counterday]['date'];
												echo "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12 summary-cpbox'>";
												echo "<span class='summary-cpdate'>".$itinerary_day[$x][$counterday]['date'].'</span>';
											}
											else{

											}

											if ($temp_city != $cityList[$index_city]['title']) {
												if ($index_city>-1) {
													echo '<p>'.$cityList[$index_city]['title'].'</p>';
													$temp_city = $cityList[$index_city]['title'];
												}
											}

											$write_note = 0;
											if ($itinerary_day[$x][$counterday]['same_as_previous'] == 'no') {
												if ($index_place>-1) {
													echo '<p>- '.$placeList[$index_place]['title'].'</p>';
													$write_note = 1;
												}
											}

											$hotelname = '-';
											$hoteladdress = '-';
											$hotelphone = '-';
											$write_hotel = 0;
											$hotel = $itinerary_day[$x][$counterday]['hotel'];
											$hotel = explode('#|#', $hotel);
											if ($itinerary_day_item[$x][$i][$j]['note'] != '') {
												if ($j == count($itinerary_day_item[$x][$i])-1) {
													if ($write_note == 1) {
														echo "<p>Note: ".$itinerary_day_item[$x][$i][$j]['note']."</p><br>";
													}
													$write_hotel = 1;
												}
												else{
													if (($itinerary_day_item[$x][$i][$j]['itday_id'] != $itinerary_day_item[$x][$i][$j+1]['itday_id']) || ($itinerary_day_item[$x][$i][$j]['city_id'] != $itinerary_day_item[$x][$i][$j+1]['city_id'])) {
														if ($write_note == 1) {
															echo "<p>Note: ".$itinerary_day_item[$x][$i][$j]['note']."</p><br>";
														}
													}
													if ($itinerary_day_item[$x][$i][$j]['itday_id'] != $itinerary_day_item[$x][$i][$j+1]['itday_id']) {
														$write_hotel = 1;
													}
												}
											}

											if ($write_hotel==1) {
												if (!empty($hotel[0])) {
													$hotelname = $hotel[0];
												}
												if (!empty($hotel[1])) {
													$hoteladdress = $hotel[1];
												}
												if (!empty($hotel[2])) {
													$hotelphone = $hotel[2];
												}

												echo "<h2>Hotel</h2>";
												echo "Name: ".$hotelname;
												echo "<br>Address: ".$hoteladdress;
												echo "<br>Phone: ".$hotelphone;
											}											
										}
										$temp_city = '';

										if ($i == count($itinerary_day[$x])-1) {
											echo '</div>';
										}
									}
									?>
								</div>
							</div>

							<!--<div class="summary-itinerarydesc">
								<div class="row">
									<div class="col-md-12">
										<h1>
											Itinerary Details
										</h1>
										<?php
										$temp_itdayid = 0;
										$counterday = 0;
										for ($i=0; $i < count($itinerary_day[$x]); $i++) { 
											for ($j=0; $j < count($itinerary_day_item[$x][$i]); $j++) {
												if ($itinerary_day_item[$x][$i][$j]['itday_id'] != $temp_itdayid) {
													if ($itinerary_day_item[$x][$i][$j]['note'] != '') {
														echo "<h4>".$itinerary_day[$x][$i]['date']."</h4>";
														echo "<p>".$itinerary_day_item[$x][$i][$j]['note']."</p>";
														echo "<br>";
													}
													$counterday++;
													$temp_itdayid = $itinerary_day_item[$x][$i][$j]['itday_id'];
												}
											}
										}
										?>
									</div>
								</div>
							</div>-->
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	@endfor
</div>
@endsection