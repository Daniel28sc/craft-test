<!-- resources/views/adminhome.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<div class="container">
	<div class="row adminhome">
		<div class="col-md-12 uploaddatanav">
			<ul>
				<li>
					<a href="{{ URL::to('/uploadcitylocation') }}" target="_blank"><button>UPLOAD CITY & PLACE</button></a>
				</li>
				<li>
					<a href="{{ URL::to('/uploaditinerators') }}" target="_blank"><button>UPLOAD ITINERATORS</button></a>
				</li>
			</ul>
		</div>
	</div>
</div>
@endsection