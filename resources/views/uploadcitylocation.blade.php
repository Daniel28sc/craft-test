<!-- resources/views/upload-citylocation.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script type="text/javascript">
    var _siteurl = "{{ url('/') }}";
    var _city = "<?php json_encode($cityList); ?>";
    // For Image Cropper
    $(function() {
		$('.cropme').simpleCropper();

		var columnscity = [{
        	field: 'no',
        	title: 'No.',
        	sortable: true,
        	halign: 'center',
        	align: 'center'
        },{
        	field: 'city_id',
        	title: 'City ID',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'image_source',
        	title: 'Photo',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'title',
        	title: 'Title',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'kanji',
        	title: 'Kanji',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'initial',
        	title: 'Initial',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'description',
        	title: 'Description',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'actioneditcity',
        	title: '',
        	sortable: false,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'actiondeletecity',
        	title: '',
        	sortable: false,
        	halign: 'center',
        	align: 'left'
        }];
        $('#citylisttable').bootstrapTable({
        	pagination: true,
        	smartDisplay: true,
        	striped: true,
        	sortable: true,
        	columns: columnscity,
        	pageSize: 15,
        	search: 'true'
        });

        var columnsplace = [{
        	field: 'no',
        	title: 'No.',
        	sortable: true,
        	halign: 'center',
        	align: 'center'
        },{
        	field: 'place_id',
        	title: 'Place ID',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'city_id',
        	title: 'City ID',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'image_source',
        	title: 'Photo',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'title',
        	title: 'Title',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'kanji',
        	title: 'Kanji',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'description',
        	title: 'Description',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'status',
        	title: 'Status',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'hashtag',
        	title: 'Hashtag',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'actioneditplace',
        	title: '',
        	sortable: false,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'actiondeleteplace',
        	title: '',
        	sortable: false,
        	halign: 'center',
        	align: 'left'
        }];
        $('#placelisttable').bootstrapTable({
        	pagination: true,
        	smartDisplay: true,
        	striped: true,
        	sortable: true,
        	columns: columnsplace,
        	pageSize: 15,
        	search: 'true'
        });
    });
</script>
<script src="{{ URL::asset('js/uploadcitylocation.js') }}" type="text/javascript"></script>
<div class="container addcityloc">
	<div class="row cityorlocshowpicker">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-6">
					<input type="radio" id="picker-city" class="showcityorloc" name="showcityorloc" value="city" checked><label for="picker-city">City</label>
				</div>
				<div class="col-md-6">
					<input type="radio" id="picker-loc" class="showcityorloc" name="showcityorloc" value="location"><label for="picker-loc">Place</label>
				</div>
			</div>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row citylist">
		<div class="col-md-12">
			<button type="button" class="btn btn-info addcitymodaltrigger" data-toggle="modal" data-target="#addcitymodal">Add City</button>
			<?php $countercitylist = 1; ?>
			<table id="citylisttable" class="table table-hover">
				<!-- <thead>
					<tr>
						<th>#</th>
						<th>City ID</th>
						<th>Photo</th>
						<th>Title</th>
						<th>Kanji</th>
						<th>Initial</th>
						<th>Description</th>
					</tr>
				</thead> -->
				<tbody>
					@for($i=0; $i<count($cityList); $i++)
						<?php
						if ($cityList[$i]['del'] == 1) {
							continue;
						}
						?>
						<tr>
							<td><?php echo $countercitylist; ?></td>
							<td>{{ $cityList[$i]['city_id'] }}</td>
							<td><img class="photo-thumbnail" src="{{ $cityList[$i]['image_source'] }}"></td>
							<td>{{ $cityList[$i]['title'] }}</td>
							<td>{{ $cityList[$i]['kanji'] }}</td>
							<td>{{ $cityList[$i]['initial'] }}</td>
							<td>{{ $cityList[$i]['description'] }}</td>
							<td>
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editcitymodal-{{ $cityList[$i]['city_id'] }}">Edit City</button>
							</td>
							<td>
								<button type="button" class="btn btn-info deletecity" data-id="{{ $cityList[$i]['city_id'] }}">Delete</button>
							</td>
						</tr>
						<?php $countercitylist++; ?>
					@endfor
				</tbody>
			</table>
		</div>
	</div>

	<div class="row locationlist" style="display:none;">
		<div class="col-md-12">
			<button type="button" class="btn btn-info addplacemodaltrigger" data-toggle="modal" data-target="#addplacemodal">Add Place</button>
			<?php $counterplacelist = 1; ?>
			<table id="placelisttable" class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Place ID</th>
						<th>City ID</th>
						<th>Photo</th>
						<th>Title</th>
						<th>Kanji</th>
						<th>Description</th>
						<th>Status</th>
						<th>Hashtag</th>
					</tr>
				</thead>
				<tbody>
					@for($i=0; $i<count($placeList); $i++)
						<tr>
							<td><?php echo $counterplacelist; ?></td>
							<td>{{ $placeList[$i]['place_id'] }}</td>
							<td>{{ $placeList[$i]['city_id'] }}</td>
							<td><img class="photo-thumbnail" src="{{ $placeList[$i]['image_source'] }}"></td>
							<td>{{ $placeList[$i]['title'] }}</td>
							<td>{{ $placeList[$i]['kanji'] }}</td>
							<td>{{ $placeList[$i]['description'] }}</td>
							<td>{{ $placeList[$i]['status'] }}</td>
							<td>{{ $placeList[$i]['hashtag'] }}</td>
							<td>
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editplacemodal-{{ $placeList[$i]['place_id'] }}">Edit Place</button>
							</td>
							<td>
								<button type="button" class="btn btn-info deleteloc" data-id="{{ $placeList[$i]['place_id'] }}">Delete</button>
							</td>
						</tr>
						<?php $counterplacelist++; ?>
					@endfor
				</tbody>
			</table>
		</div>
	</div>

	
	<!-- MODAL FOR ADDING CITY OR PLACE-->
	<!-- Add City Modal -->
	<div id="addcitymodal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add City: </h4>
				</div>
				<div class="modal-body">
					<div class="row city-uploader">
						<div class="col-md-12">
							<p class="errorinput" style="display:none;">
									
							</p>
							<form action="imageuploader_controller" method="POST" class="city-imageuploader" name="city-imageuploader" enctype="multipart/form-data">
								{!! csrf_field() !!}
								<input type="hidden" name="cityorloc" value="city">
								<div class="form-group">
									<label for="title">Title:</label>
									<input type="text" class="form-control title" id="title" name="title">
								</div>
								<div class="form-group">
									<label for="kanji">Kanji:</label>
									<input type="text" class="form-control kanji" id="kanji" name="kanji">
								</div>
								<div class="form-group">
									<label for="initial">Initial:</label>
									<input type="text" class="form-control initial" id="initial" name="initial">
								</div>
								<div class="form-group">
									<label for="description">Description:</label>
									<input type="text" class="form-control description" id="description" name="description">
								</div>
								<div class="row imgpreview" id="imgpreview">
									<div class="col-md-12">
										Select image to upload:
										<div class="cropme" style="width: 1000px; height: 200px;"></div>
									</div>
									<!-- <div class="col-md-12">
										Select image for thumbnail:
										<div class="cropme cropcitythumbnail" style="width: 200px; height: 200px;"></div>
									</div> -->
								</div>
								<input type="submit" value="Submit" name="submitcity" class="submitcityloc submitcity newcity">
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Location Modal -->
	<div id="addplacemodal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Place: </h4>
				</div>
				<div class="modal-body">
					<div class="row location-uploader">
						<div class="col-md-12">
							<p class="errorinput" style="display:none;">
									
							</p>
							<form action="imageuploader_controller" method="POST" class="location-imageuploader" name="location-imageuploader" enctype="multipart/form-data">
								{!! csrf_field() !!}
								<input type="hidden" name="cityorloc" value="location">
								<div id="groupcityid" class="form-group">
									<label for="city-id">Choose city:</label>
									<select name="city-id" id="city-id" class="form-control city-id">
										@for($i=0; $i<count($cityList); $i++)
											<option value="{{ $cityList[$i]['city_id'] }}">{{ $cityList[$i]['title'] }}</option>
										@endfor
									</select>
								</div>
								<div class="form-group">
									<label for="title">Title:</label>
									<input type="text" class="form-control title" id="title" name="title">
								</div>
								<div class="form-group">
									<label for="kanji">Kanji:</label>
									<input type="text" class="form-control kanji" id="kanji" name="kanji">
								</div>
								<div class="form-group">
									<label for="description">Description:</label>
									<input type="text" class="form-control description" id="description" name="description">
								</div>
								<div class="form-group">
									<label for="description">Status:</label>
									<select name="status" id="status" class="form-control status">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</div>
								<div class="form-group">
									<label for="description">Hashtag:</label>
									<input type="text" class="form-control description" id="hashtag" name="hashtag">
								</div>
								<div class="place-rates">
									<ul>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-family" name="addplace-family" value="no">
												<label for="addplace-family">
													Family
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-backpack" name="addplace-backpack" value="no">
												<label for="addplace-backpack">
													Backpack
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-vegetarian" name="addplace-vegetarian" value="no">
												<label for="addplace-vegetarian">
													Vegetarian
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-romantic" name="addplace-romantic" value="no">
												<label for="addplace-romantic">
													Romantic
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-culinaire" name="addplace-culinaire" value="no">
												<label for="addplace-culinaire">
													Culinaire
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-cheapeat" name="addplace-cheapeat" value="no">
												<label for="addplace-cheapeat">
													Cheap Eat
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-barclub" name="addplace-barclub" value="no">
												<label for="addplace-barclub">
													Bar & Club
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-nightseeing" name="addplace-nightseeing" value="no">
												<label for="addplace-nightseeing">
													Night Seeing
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-popularplace" name="addplace-popularplace" value="no">
												<label for="addplace-popularplace">
													Popular Place
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-culture" name="addplace-culture" value="no">
												<label for="addplace-culture">
													Culture & Art
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-cuisine" name="addplace-cuisine" value="no">
												<label for="addplace-cuisine">
													Cuisine
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-nature" name="addplace-nature" value="no">
												<label for="addplace-nature">
													Nature
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-entertainment" name="addplace-entertainment" value="no">
												<label for="addplace-entertainment">
													Entertainment
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-history" name="addplace-history" value="no">
												<label for="addplace-history">
													History
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-shopping" name="addplace-shopping" value="no">
												<label for="addplace-shopping">
													Shopping
												</label>
											</div>
										</li>
										<li>
											<div class="checkbox checkbox-primary">
												<input type="checkbox" id="addplace-sports" name="addplace-sports" value="no">
												<label for="addplace-sports">
													Sports
												</label>
											</div>
										</li>
									</ul>
								</div>
								<div class="row imgpreview" id="imgpreview">
									<div class="col-md-12">
										<!-- Select image to upload: -->
										<div class="cropme placecrop1" style="width: 600px; height: 600px;"></div>
										<div class="cropme placecrop2" style="width: 600px; height: 600px;"></div>
										<div class="cropme placecrop3" style="width: 600px; height: 600px;"></div>
										<div class="cropme placecrop4" style="width: 600px; height: 600px;"></div>
										<div class="cropme placecrop5" style="width: 600px; height: 600px;"></div>
									</div>
								</div>
								<input type="submit" value="Submit" name="submitloc" class="submitcityloc newloc">
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<!-- MODAL FOR EDITING CITY DATA -->
	@for($i=0; $i<count($cityList); $i++)
		<div id="editcitymodal-{{ $cityList[$i]['city_id'] }}" class="modal fade editcitymodal " role="dialog">
			<div class="modal-dialog modal-lg">
				
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit City: </h4>
					</div>
					<div class="modal-body">
						<div class="row editcity">
							<div class="col-md-12">
								<p class="errorinput" style="display:none;">
									
								</p>
								<form action="imageuploader_controller" method="POST" class="editcityform editcity-{{ $cityList[$i]['city_id'] }}" name="editcityform" enctype="multipart/form-data">
									{!! csrf_field() !!}
									<input type="hidden" name="cityorloc" value="editcity">
									<input type="hidden" name="editcityid" value="{{ $cityList[$i]['city_id'] }}">
									<div class="form-group">
										<label for="title">Title:</label>
										<input type="text" class="form-control title" id="title" name="title" value="{{ $cityList[$i]['title'] }}">
									</div>
									<div class="form-group">
										<label for="kanji">Kanji:</label>
										<input type="text" class="form-control title" id="kanji" name="kanji" value="{{ $cityList[$i]['kanji'] }}">
									</div>
									<div class="form-group">
										<label for="initial">Initial:</label>
										<input type="text" class="form-control initial" id="initial" name="initial" value="{{ $cityList[$i]['initial'] }}">
									</div>
									<div class="form-group">
										<label for="description">Description:</label>
										<input type="text" class="form-control description" id="description" name="description" value="{{ $cityList[$i]['description'] }}">
									</div>
									<div class="row imgpreview" id="imgpreview">
										<div class="col-md-12">
											Select image to upload:
											<div class="cropme" style="width: 1000px; height: 200px; background-image: url('{{ $cityList[$i]['image_source'] }}');"></div>
										</div>
										<!-- <div class="col-md-12">
											Select image for thumbnail:
											<div class="cropme cropcitythumbnail" style="width: 200px; height: 200px;"></div>
										</div> -->
									</div>
									<input type="submit" value="Submit" name="submitcity" class="submitcityloc submitcity">
								</form>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	@endfor


	<!-- MODAL FOR EDITING PLACE DATA -->
	@for($i=0; $i<count($placeList); $i++)
		<div id="editplacemodal-{{ $placeList[$i]['place_id'] }}" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit Place: </h4>
					</div>
					<div class="modal-body">
						<div class="row editplace">
							<div class="col-md-12">
								<p class="errorinput" style="display:none;">
									
								</p>
								<form action="imageuploader_controller" method="POST" class="editplaceform editplace-{{ $placeList[$i]['place_id'] }}" name="editplaceform" enctype="multipart/form-data">
									{!! csrf_field() !!}
									<input type="hidden" name="editplaceid" value="{{ $placeList[$i]['place_id'] }}">
									<input type="hidden" name="cityorloc" value="editlocation">
									<div id="groupcityid" class="form-group">
										<label for="city-id">City:</label>
										<?php
										for($j=0; $j<count($cityList); $j++){
											if ($cityList[$j]['city_id']==$placeList[$i]['city_id']) {
												?>
												<input type="hidden" name="city-id" id="city-id" class="form-control city-id" value="{{ $cityList[$j]['city_id'] }}">
												<p>
													{{ $cityList[$j]['title'] }}
												</p>
												<?php
												break;
											}
										}
										?>
									</div>
									<div class="form-group">
										<label for="title">Title:</label>
										<input type="text" class="form-control title" id="title" name="title" value="{{ $placeList[$i]['title'] }}">
									</div>
									<div class="form-group">
										<label for="kanji">Kanji:</label>
										<input type="text" class="form-control kanji" id="kanji" name="kanji" value="{{ $placeList[$i]['kanji'] }}">
									</div>
									<div class="form-group">
										<label for="description">Description:</label>
										<input type="text" class="form-control description" id="description" name="description" value="{{ $placeList[$i]['description'] }}">
									</div>
									<div class="form-group">
										<label for="description">Status:</label>
										<select name="status" id="status" class="form-control status">
											<option value="1" <?php if ($placeList[$i]['status']==1) { echo "selected";} ?>>1</option>
											<option value="2" <?php if ($placeList[$i]['status']==2) { echo "selected";} ?>>2</option>
											<option value="3" <?php if ($placeList[$i]['status']==3) { echo "selected";} ?>>3</option>
											<option value="4" <?php if ($placeList[$i]['status']==4) { echo "selected";} ?>>4</option>
											<option value="5" <?php if ($placeList[$i]['status']==5) { echo "selected";} ?>>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="description">Hashtag:</label>
										<input type="text" class="form-control description" id="hashtag" name="hashtag" value="{{ $placeList[$i]['hashtag'] }}">
									</div>
									<div class="place-rates">
										<ul>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-family<?php echo $i; ?>" name="editplace-family" value="{{ $placeList[$i]['family'] }}" <?php if ($placeList[$i]['family']=='yes') { echo "checked";} ?>>
													<label for="editplace-family<?php echo $i; ?>">
														Family
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-backpack<?php echo $i; ?>" name="editplace-backpack" value="{{ $placeList[$i]['backpack'] }}" <?php if ($placeList[$i]['backpack']=='yes') { echo "checked";} ?>>
													<label for="editplace-backpack<?php echo $i; ?>">
														Backpack
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-vegetarian<?php echo $i; ?>" name="editplace-vegetarian" value="{{ $placeList[$i]['vegetarian'] }}" <?php if ($placeList[$i]['vegetarian']=='yes') { echo "checked";} ?>>
													<label for="editplace-vegetarian<?php echo $i; ?>">
														Vegetarian
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-romantic<?php echo $i; ?>" name="editplace-romantic" value="{{ $placeList[$i]['romantic'] }}" <?php if ($placeList[$i]['romantic']=='yes') { echo "checked";} ?>>
													<label for="editplace-romantic<?php echo $i; ?>">
														Romantic
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-culinaire<?php echo $i; ?>" name="editplace-culinaire" value="{{ $placeList[$i]['culinaire'] }}" <?php if ($placeList[$i]['culinaire']=='yes') { echo "checked";} ?>>
													<label for="editplace-culinaire<?php echo $i; ?>">
														Culinaire
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-cheapeat<?php echo $i; ?>" name="editplace-cheapeat" value="{{ $placeList[$i]['cheapeat'] }}" <?php if ($placeList[$i]['cheapeat']=='yes') { echo "checked";} ?>>
													<label for="editplace-cheapeat<?php echo $i; ?>">
														Cheap Eat
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-barclub<?php echo $i; ?>" name="editplace-barclub" value="{{ $placeList[$i]['barclub'] }}" <?php if ($placeList[$i]['barclub']=='yes') { echo "checked";} ?>>
													<label for="editplace-barclub<?php echo $i; ?>">
														Bar & Club
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-nightseeing<?php echo $i; ?>" name="editplace-nightseeing" value="{{ $placeList[$i]['nightseeing'] }}" <?php if ($placeList[$i]['nightseeing']=='yes') { echo "checked";} ?>>
													<label for="editplace-nightseeing<?php echo $i; ?>">
														Night Seeing
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-popularplace<?php echo $i; ?>" name="editplace-popularplace" value="{{ $placeList[$i]['popularplace'] }}" <?php if ($placeList[$i]['popularplace']=='yes') { echo "checked";} ?>>
													<label for="editplace-popularplace<?php echo $i; ?>">
														Popular Place
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-culture<?php echo $i; ?>" name="editplace-culture" value="{{ $placeList[$i]['culture'] }}" <?php if ($placeList[$i]['culture']=='yes') { echo "checked";} ?>>
													<label for="editplace-culture<?php echo $i; ?>">
														Culture & Art
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-cuisine<?php echo $i; ?>" name="editplace-cuisine" value="{{ $placeList[$i]['cuisine'] }}" <?php if ($placeList[$i]['cuisine']=='yes') { echo "checked";} ?>>
													<label for="editplace-cuisine<?php echo $i; ?>">
														Cuisine
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-nature<?php echo $i; ?>" name="editplace-nature" value="{{ $placeList[$i]['nature'] }}" <?php if ($placeList[$i]['nature']=='yes') { echo "checked";} ?>>
													<label for="editplace-nature<?php echo $i; ?>">
														Nature
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-entertainment<?php echo $i; ?>" name="editplace-entertainment" value="{{ $placeList[$i]['entertainment'] }}" <?php if ($placeList[$i]['entertainment']=='yes') { echo "checked";} ?>>
													<label for="editplace-entertainment<?php echo $i; ?>">
														Entertainment
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-history<?php echo $i; ?>" name="editplace-history" value="{{ $placeList[$i]['history'] }}" <?php if ($placeList[$i]['history']=='yes') { echo "checked";} ?>>
													<label for="editplace-history<?php echo $i; ?>">
														History
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-shopping<?php echo $i; ?>" name="editplace-shopping" value="{{ $placeList[$i]['shopping'] }}" <?php if ($placeList[$i]['shopping']=='yes') { echo "checked";} ?>>
													<label for="editplace-shopping<?php echo $i; ?>">
														Shopping
													</label>
												</div>
											</li>
											<li>
												<div class="checkbox checkbox-primary">
													<input type="checkbox" id="editplace-sports<?php echo $i; ?>" name="editplace-sports" value="{{ $placeList[$i]['sports'] }}" <?php if ($placeList[$i]['sports']=='yes') { echo "checked";} ?>>
													<label for="editplace-sports<?php echo $i; ?>">
														Sports
													</label>
												</div>
											</li>
										</ul>
									</div>
									<div class="row imgpreview" id="imgpreview">
										<div class="col-md-12">
											<!-- Select image to upload: -->
											<?php
											$p1 = $placeList[$i]["image_source"];
											if ($p1 == '') {
												$style1 = "width: 600px; height: 600px;";
											}
											else{
												$style1 = "width: 600px; height: 600px; background-image:url('".$p1."');";
											}
											echo '<div class="cropme placecrop1" style="'.$style1.'"></div>';

											for ($pp=2; $pp < 6; $pp++) { 
												$p = $placeList[$i]["image_source".$pp];
												if ($p == '') {
													$style = "width: 600px; height: 600px;";
												}
												else{
													$style = "width: 600px; height: 600px; background-image:url('".$p."');";
												}
												echo '<div class="cropme placecrop'.$pp.'" style="'.$style.'"></div>';
											}
											?>
										</div>
									</div>
									<input type="submit" value="Submit" name="submitplace" class="submitcityloc">
								</form>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	@endfor

</div>

@endsection