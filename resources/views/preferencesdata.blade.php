<!-- resources/views/preferencesdata.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<?php
$itinerary = $data['itinerary'];
$itineraryDay = $data['itineraryDay'];
$itineraryDayItem = $data['itineraryDayItem'];
$cityList = $data['cityList'];
$placeList = $data['placeList'];
session(['temp_itid' => $data['itineraryId']]);
for ($i=0; $i < count($cityList); $i++) { 
    $cityList[$i]['description'] = str_replace('"', "'", $cityList[$i]['description']);
}
for ($i=0; $i < count($placeList); $i++) { 
    $placeList[$i]['description'] = str_replace('"', "'", $placeList[$i]['description']);
}
?>

<script type="text/javascript">
var _cityList = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($cityList)); ?>'),
_placeList = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($placeList)); ?>'),
_itineraryId = '{{ $itinerary['it_id'] }}',
_itineraryDay = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($itineraryDay)); ?>');
_itineraryDayItem = JSON.parse('<?php echo str_replace("'", "\\'", json_encode($itineraryDayItem)); ?>');
</script>
<script type="text/javascript" src="{{ URL::asset('js/common.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/trippref.js') }}"></script>

<div class="container">
    {{-- {{ $itid }} --}}
    {{-- {{ $itinerary['day'] }} --}}
    {{-- {{ $itineraryDay[0]['date'] }} --}}
    
    <div id="stepsbar-wrap">
        <div id="stepsbar" class="stepsbar">
            <ol>
                <a href="{{ URL::to('/') }}"><li><span>Arrival & Return</span></li></a>

                <a href=""><li class="active"><span>Travel Preferences</span></li></a>
                <li><span>Meet the Itinerators</span></li>
                <li><span>Finish</span></li>
            </ol>
        </div>        
    </div>

    <div class="row startover">
        <div class="col-md-2"></div>
        <div class="col-md-8 startoverbox">
            <div class="row startoverbox-content">
                <div class="col-md-9 startoverbox-content-p1">
                    <input type="hidden" name="itinerarynod" class="itinerarynod" value="{{ Session::get('itinerarynod') }}">
                    <h1>
                        My {{ Session::get('itinerarynod') }} days trip in {{ Session::get('destination_country') }}
                    </h1>
                    <h2>
                        {{ Session::get('itineraryschedule') }}
                    </h2>
                </div>
                <div class="col-md-3 startoverbox-content-p2">
                    <a href="{{ URL::to('/') }}">
                        <form action="../arrivalreturn_controller" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="arrivalreturntype" class="arrivalreturntype" value="startover">
                            <button name="startoverbutton" value="startoverbutton" class="startoverbutton pull-right">
                                START OVER
                            </button>
                        </form>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row preferencesdata-content">

        <!-- NEW -->
<form action="../preferencesdata" method="post" id="form-preferencesdata" name="preferencesdata_form">
    {!! csrf_field() !!}
        <div class="row prefbox tripbudget">
            <div class="col-md-12">
                <h1>
                    Money that I will bring
                </h1>
                <table>
                    <tr>
                        <td>
                            <div class="form-group">
                                <select class="form-control" id="currency" name="currency">
                                    <option value="USD">USD ($)</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" class="form-control budget" id="budget" name="budget" value="{{ $itinerary['budget'] }}">
                            </div>
                        </td>
                        <td>
                            =
                        </td>
                        <td>
                            <div class="form-group">
                                <select class="form-control" id="currencyconversion" name="currencyconversion">
                                    <option value="USD">USD ($)</option>
                                </select>
                            </div>
                        </td>
                        <td class="currencyconversiontd">
                            <span class="currencyconversionresult"></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row prefbox pref-interest">
            <div class="col-md-12">
                <div class="prefprofile1">
                    <h1>
                        My trip profile
                    </h1>
                    <table>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-family" name="family" <?php if($itinerary['family'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-family">
                                        FAMILY
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-backpack" name="backpack" <?php if($itinerary['backpack'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-backpack">
                                        BACKPACK
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-vegetarian" name="vegetarian" <?php if($itinerary['vegetarian'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-vegetarian">
                                        VEGETARIAN
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-romantic" name="romantic" <?php if($itinerary['romantic'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-romantic">
                                        ROMANTIC
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-culinaire" name="culinaire" <?php if($itinerary['culinaire'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-culinaire">
                                        CULINAIRE
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-cheapeat" name="cheapeat" <?php if($itinerary['cheapeat'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-cheapeat">
                                        CHEAP EAT
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </table> 
                </div>
                
                <div class="prefprofile2">
                    <p class="pp2head">
                        Point Available: <span class="pointallocation"></span> / 10
                    </p>
                    <table>
                        <tr>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Popular Places
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="mustsee" min="1" value="{{ $itinerary['mustsee'] }}" max="3" name="mustsee"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Culture & Art
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="culture" min="1" value="{{ $itinerary['culture'] }}" max="3" name="culture"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Cuisine
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="cuisine" min="1" value="{{ $itinerary['cuisine'] }}" max="3" name="cuisine"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Nature
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="nature" min="1" value="{{ $itinerary['nature'] }}" max="3" name="nature"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Entertainment
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="entertainment" min="1" value="{{ $itinerary['entertainment'] }}" max="3" name="entertainment"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            History
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="history" min="1" value="{{ $itinerary['history'] }}" max="3" name="history"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Shopping
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="shopping" min="1" value="{{ $itinerary['shopping'] }}" max="3" name="shopping"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        <p class="prefval">
                                            Sports
                                        </p>
                                    </li>
                                    <li>
                                        <div class="range-slider prefdataslider">
                                            <div id="slider">
                                                <input class="bar" type="range" id="sports" min="1" value="{{ $itinerary['sports'] }}" max="3" name="sports"/>
                                                <span class="highlight"></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <span class="interestrate"></span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="prefprofile3">
                    <table>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-barclub" name="barclub" <?php if($itinerary['barclub'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-barclub">
                                        Bar & Club
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="pp-nightseeing" name="nightseeing" <?php if($itinerary['nightseeing'] == "1"){ echo "checked"; } ?>>
                                    <label for="pp-nightseeing">
                                        Night Seeing
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </table> 
                </div>
                
            </div>
        </div>

        <div class="row prefbox pref-cityplace">
            <div class="col-md-12">
                <h1>
                    Specific Preferences <div class="checkbox-prefdata-applyall checkbox checkbox-primary"><input type="checkbox" id="prefdata-applyall" name="prefdata-applyall" value="applyall"><label for="prefdata-applyall">Apply All</label></div>
                </h1>
                <div class="reg-warning warn-prefdatapage">
                    
                </div>
                <p class="inputitineraryinstruction1">
                    The following days will refer to the previous data place, if you want to set each day to different places please uncheck the "Continue from the previous" in each day and choose your preference places.
                </p>
                <div class="col-md-2 preferences-datelist">
                    <ul class="datelist-manual">
                        <?php
                        $counterdatenav = 1;
                        $z = count($itineraryDay);
                        ?>
                        @foreach ($itineraryDay as $itineraryday)
                        <?php
                        $date = $itineraryday['date'];
                        $datedata = date_parse($date);
                        $year = $datedata['year'];
                        $month = $datedata['month'];
                        $day = $datedata['day'];

                        $strdate = $year.'/'.$month.'/'.$day;
                        $cal = date('Y m D', strtotime($strdate));
                        $cal = explode(' ', $cal);

                        $strdate1 = $year.'/'.$month.'/'.$day;
                        $cal1 = date('Y m d', strtotime($strdate1));
                        $cal1 = explode(' ', $cal1);
                        ?>
                        <li class="tab-link{{ ($counterdatenav == 1) ? ' active' : '' }}">
                            <!-- <a href="#tab-<?php echo $counterdatenav; ?>" data-toggle="tab">{{ $itineraryday['date'] }}</a> -->
                            <a href="#tab-<?php echo $counterdatenav; ?>" data-toggle="tab">
                                <?php
                                echo $cal[2];
                                echo "<br>".$cal1[2].' / '.$cal[1];
                                ?>
                            </a>

                            <?php
                            if ($itineraryday['date'] != $itineraryDay[count($itineraryDay)-1]['date']) {
                                echo '<div class="prefdate-sectionline"></div>';
                            }
                            ?>
                        </li>
                        <?php $counterdatenav++; ?>
                        @endforeach
                    </ul>
                    <ul class="datelist-all">
                        <li class="tab-link active">
                            <a href="#tab-0" data-toggle="tab">
                                All days
                            </a>
                            <div class="prefdate-sectionline"></div>
                        </li>
                    </ul>
                </div>

                <div class="tab-content tab-content-parent pref-manual" id="tab-day-item">

                    @for ($dayNo = 1; $dayNo < count($itineraryDay) + 1; $dayNo++)
                    <div class="tab-pane{{ ($dayNo == 1) ? ' active' : '' }}" id="tab-{{ $dayNo }}">
                        <div class="col-md-10 preferences-option">
                            
                            <?php
                            if ($dayNo > 1) {
                            ?>
                            <div class="sameasprev" data-day="{{ $dayNo }}">
                                <div class="checkbox-prefdata-sameasprev checkbox checkbox-primary">
                                    <input type="checkbox" id="prefdata-sameasprev{{ $dayNo }}" class="prefdata-sameasprev" name="prefdata-sameasprev" value="sameasprev" checked>
                                    <label for="prefdata-sameasprev{{ $dayNo }}">Continue from the previous</label>
                                    <input type="hidden" id="issameasprevious{{ $dayNo }}" class="issameasprevious" value="yes">
                                </div>
                            </div>
                            <?php
                            }
                            ?>

                            <div class="row row-day-item">
                                <div class="col-md-12">
                                    <div class="panel-group" id="day-item-detail-{{ $dayNo }}" 
                                    data-day="{{ $dayNo }}"
                                    data-date="{{ $itineraryDay[$dayNo - 1]['date'] }}"
                                    data-it_day_id="{{ $itineraryDay[$dayNo - 1]['itday_id'] }}"
                                    role="tablist" aria-multiselectable="true">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="addnewprefcity">
                                <button type="button" class="btn btn-success pull-right btn-add-city" data-day="{{ $dayNo }}">
                                    <!-- <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> --> Add Region
                                </button>
                            </div>

                            <?php
                            if ($dayNo > 1) {
                            ?>
                            <div class="samehotel" data-day="{{ $dayNo }}">
                                <div class="checkbox-prefdata-samehotel checkbox checkbox-primary">
                                    <input type="checkbox" id="prefdata-samehotel{{ $dayNo }}" class="prefdata-samehotel" name="prefdata-samehotel" value="samehotel" checked>
                                    <label for="prefdata-samehotel{{ $dayNo }}">Same hotel as previous</label>
                                    <input type="hidden" id="issamehotel{{ $dayNo }}" class="issamehotel" value="yes">
                                </div>
                            </div>
                            <?php
                            }
                            ?>

                            <div id="prefhotel{{ $dayNo }}" class="prefhotel">
                                <h1>
                                    Hotel
                                </h1>
                                <!-- <textarea class="form-control pickhotel" rows="5" value="" name="pickhotel" data-day="{{ $dayNo }}">{{ $itineraryDay[$dayNo - 1]['hotel'] }}</textarea> -->
                                <?php
                                $hotel = $itineraryDay[$dayNo - 1]['hotel'];
                                if ($hotel != '') {
                                    $hotel = explode('#|#', $hotel);
                                    $hotelname = $hotel[0];
                                    if (!empty($hotel[1])) {
                                        $hoteladdress = $hotel[1];
                                    }
                                    else{
                                        $hoteladdress = '';
                                    }
                                    if (!empty($hotel[2])) {
                                        $hotelphone = $hotel[2];
                                    }
                                    else{
                                        $hotelphone = '';
                                    }
                                }
                                else{
                                    $hotelname = '';
                                    $hoteladdress = '';
                                    $hotelphone = '';
                                }
                                ?>
                                <div class="form-group hotelinput1">
                                    <label for="hotelname">Name: </label>
                                    <input type="text" id="hotelname" class="form-control hotel-name" value="{{ $hotelname }}" name="pickhotel-name" data-day="{{ $dayNo }}">
                                </div>
                                <div class="form-group hotelinput2">
                                    <label for="hoteladdress">Address:</label>
                                    <textarea id="hoteladdress" class="form-control pickhotel hotel-address" rows="5" value="" name="pickhotel-address" data-day="{{ $dayNo }}">{{ $hoteladdress }}</textarea>
                                </div>
                                <div class="form-group hotelinput3">
                                    <label for="hotelphone">Phone number: </label>
                                    <input type="text" id="hotelphone" class="form-control hotel-phone" value="{{ $hotelphone }}" name="pickhotel-phone" data-day="{{ $dayNo }}">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    @endfor

                </div>

                <div class="tab-content tab-content-parent pref-all" id="tab-day-item">

                    <div class="tab-pane active" id="tab-0">
                        <div class="col-md-10 preferences-option">

                            <div class="row row-day-item">
                                <div class="col-md-12">
                                    <div class="panel-group" id="day-item-detail-0" 
                                    data-day="0"
                                    data-date="{{ $itineraryDay[0]['date'] }}"
                                    data-it_day_id="{{ $itineraryDay[0]['itday_id'] }}"
                                    role="tablist" aria-multiselectable="true">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="addnewprefcity">
                                <button type="button" class="btn btn-success pull-right btn-add-city" data-day="0">
                                    Add Region
                                </button>
                            </div>
                            
                            <div id="prefhotel0" class="prefhotel">
                                <h1>
                                    Hotel
                                </h1>
                                <!-- <textarea class="form-control pickhotel" rows="5" value="" name="pickhotel" data-day="0">{{ $itineraryDay[0]['hotel'] }}</textarea> -->
                                <?php
                                $hotel = $itineraryDay[0]['hotel'];
                                if ($hotel != '') {
                                    $hotel = explode('#|#', $hotel);
                                    $hotelname = $hotel[0];
                                    if (count($hotel)>1) {
                                        $hoteladdress = $hotel[1];
                                    }
                                    else{
                                        $hoteladdress = '';
                                    }
                                }
                                else{
                                    $hotelname = '';
                                    $hoteladdress = '';
                                }
                                ?>
                                <div class="form-group hotelinput1">
                                    <label for="hotelname">Name: </label>
                                    <input type="text" id="hotelname" class="form-control hotel-name" value="{{ $hotelname }}" name="pickhotel-name" data-day="0">
                                </div>
                                <div class="form-group hotelinput2">
                                    <label for="hoteladdress">Address:</label>
                                    <textarea id="hoteladdress" class="form-control pickhotel hotel-address" rows="5" value="" name="pickhotel-address" data-day="0">{{ $hoteladdress }}</textarea>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </div>

            </div>
        </div>


            
        <!-- OLD -->
        <!-- <p>Total: <span class="pointallocation"></span> / 20</p>
        <form action="../preferencesdata" method="post" id="form-preferencesdata" name="preferencesdata_form">
            {!! csrf_field() !!}
            <div class="row preferences-interest">
                <div class="col-md-6">
                    Budget:
                    <input type="text" name="budget" value="{{ $itinerary['budget'] }}">
                </div>

                <div class="col-md-6">
                    <div class="range-slider prefdataslider">
                        <div id="slider">
                            <input class="bar" type="range" id="pace" min="1" value="{{ $itinerary['pace'] }}" max="3" onchange="pacevalue.value=value" name="pace"/>
                            <span class="highlight"></span>
                            <p class="prefval">
                                Pace: <output id="pacevalue">{{ $itinerary['pace'] }}</output>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="mustsee" min="1" value="{{ $itinerary['mustsee'] }}" max="3" onchange="mustseevalue.value=value" name="mustsee"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Must See: <output id="mustseevalue">{{ $itinerary['mustsee'] }}</output>
                                </p>
                            </div>
                        </div>
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="entertainment" min="1" value="{{ $itinerary['entertainment'] }}" max="3" onchange="entertainmentvalue.value=value" name="entertainment"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Entertainment: <output id="entertainmentvalue">{{ $itinerary['entertainment'] }}</output>
                                </p>
                            </div>
                        </div>
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="nature" min="1" value="{{ $itinerary['nature'] }}" max="3" onchange="naturevalue.value=value" name="nature"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Nature: <output id="naturevalue">{{ $itinerary['nature'] }}</output>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="cuisine" min="1" value="{{ $itinerary['cuisine'] }}" max="3" onchange="cuisinevalue.value=value" name="cuisine"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Cuisine: <output id="cuisinevalue">{{ $itinerary['cuisine'] }}</output>
                                </p>
                            </div>
                        </div>
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="adventure" min="1" value="{{ $itinerary['adventure'] }}" max="3" onchange="adventurevalue.value=value" name="adventure"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Adventure: <output id="adventurevalue">{{ $itinerary['adventure'] }}</output>
                                </p>
                            </div>
                        </div>
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="shopping" min="1" value="{{ $itinerary['shopping'] }}" max="3" onchange="shoppingvalue.value=value" name="shopping"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Shopping: <output id="shoppingvalue">{{ $itinerary['shopping'] }}</output>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="art" min="1" value="{{ $itinerary['art'] }}" max="3" onchange="artvalue.value=value" name="art"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Art: <output id="artvalue">{{ $itinerary['art'] }}</output>
                                </p>
                            </div>
                        </div>
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="culture" min="1" value="{{ $itinerary['culture'] }}" max="3" onchange="culturevalue.value=value" name="culture"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Culture: <output id="culturevalue">{{ $itinerary['culture'] }}</output>
                                </p>
                            </div>
                        </div>
                        <div class="range-slider prefdataslider">
                            <div id="slider">
                                <input class="bar" type="range" id="sports" min="1" value="{{ $itinerary['sports'] }}" max="3" onchange="sportsvalue.value=value" name="sports"/>
                                <span class="highlight"></span>
                                <p class="prefval">
                                    Sports: <output id="sportsvalue">{{ $itinerary['sports'] }}</output>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 preferences-datelist">
                <ul>
                    <?php
                    $counterdatenav = 1;
                    $z = count($itineraryDay);
                    ?>
                    @foreach ($itineraryDay as $itineraryday)
                    <li class="tab-link{{ ($counterdatenav == 1) ? ' active' : '' }}">
                        <a href="#tab-<?php echo $counterdatenav; ?>" data-toggle="tab">{{ $itineraryday['date'] }}</a>
                    </li>
                    <?php $counterdatenav++; ?>
                    @endforeach
                </ul>
            </div>
            
            <div class="tab-content tab-content-parent" id="tab-day-item">

                @for ($dayNo = 1; $dayNo < count($itineraryDay) + 1; $dayNo++)
                <div class="tab-pane{{ ($dayNo == 1) ? ' active' : '' }}" id="tab-{{ $dayNo }}">
                    <div class="col-md-8 preferences-option">

                        <div class="row row-day-item">
                            <div class="col-md-12">
                                <div class="panel-group" id="day-item-detail-{{ $dayNo }}" 
                                data-day="{{ $dayNo }}"
                                data-date="{{ $itineraryDay[$dayNo - 1]['date'] }}"
                                data-it_day_id="{{ $itineraryDay[$dayNo - 1]['itday_id'] }}"
                                role="tablist" aria-multiselectable="true">
                            </div>
                        </div>
                    </div>

                    <button type="button" class="btn btn-success pull-right btn-add-city" data-day="{{ $dayNo }}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New</button>
                </div>
            </div>
            @endfor

        </div> -->



        <!-- Modal -->
        <div class="modal fade" id="modal-city" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Select City</h4>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" name="day" value=""/>
                        <div class="row">
                            @foreach ($cityList as $city)
                            <?php
                            if ($city['del'] == 1) {
                                continue;
                            }
                            ?>
                            <div class="col-xs-6 col-md-3">
                                <div class="thumbnail city-option">
                                    <img src="{{ $city['image_source'] }}" alt="{{ $city['title'] }}">
                                    <div class="caption">
                                        <input type="radio" name="city" value="{{ $city['city_id'] }}">{{ $city['title'] }}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="btn-city-select">Select</button>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-place" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Select Place</h4>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" name="day" value=""/>
                        <input type="hidden" name="city" value=""/>
                        <input type="hidden" name="id" value=""/>
                        <div class="row" id="city-place-container">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="btn-place-select">Select</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- Modal -->
    </div>
</form>
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6 input-style-backnextbutton">
            <ul>
                <li>
                    <input class="input-style-backbutton noback" type="button" name="button" id="btn-back" value="BACK"></input>
                </li>
                <li>
                    <form action="../arrivalreturn_controller" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="arrivalreturntype" class="arrivalreturntype" value="startover">
                        <button name="startoverbutton" value="startoverbutton" class="startoverbutton prefdatapage">
                            START OVER
                        </button>
                    </form>
                </li>
                <li>
                    <input class="input-style-nextbutton prefdatapagenext" type="button" name="button" id="btn-next" value="NEXT"></input>
                </li>
            </ul>
        </div>
    </div>


<div class="row inputstyle-sectionline">
    <div class="col-md-12"></div>
</div>
</div>

@endsection