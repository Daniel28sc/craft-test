<!-- resources/views/itineratorslogin.blade.php -->

@extends('layouts.itineratorspagetemplate')

@section('content')
<div class="container">
	<div class="row userlogin">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<h1>ITINERATORS - SIGN IN</h1>
			<div class="loginformbox">
				<form action="itineratorspage_controller" method="post" id="userlogin_form" name="userlogin_form">
				{!! csrf_field() !!}
					<input type="hidden" name="itineratorspage_formtype" value="loginitinerators">
					<div class="form-group">
						<input type="text" name="username" class="user-login login-username" placeholder="Email">
					</div>
					<div class="form-group">
						<input type="password" name="password" class="user-login login-password" placeholder="Password">
					</div>
					<input class="userlogin-submit" type="submit" name="button" id="userlogin-submit" value="LOG ME IN"></input>
				</form>
				<div class="login-sectionline"></div>
				<!-- <button class="userlogin-other google">BY GOOGLE ACCOUNT</button>
				<br>
				<button class="userlogin-other facebook">BY FACEBOOK ACCOUNT</button> -->
			</div>

			<div class="row loginpage-options">
				<div class="col-md-12">
					<ul>
						<li>
							<!-- <a href="{{ URL::to('/registration') }}"><button class="userlogin-other signin">DO NOT HAVE ACCOUNT</button></a> -->
						</li>
						<li>
							<a href="">
								<p>
									Forget Password?
								</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>
@endsection