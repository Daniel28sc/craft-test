<!-- resources/views/registration.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script>
$(function() {
	$("#country").countrySelect();
});
</script>
<div class="container">
	<div class="row registration-content">
		<div class="col-md-12 registrationform active-regpage">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<h1>
						Registration
					</h1>
					<p class="reg-warning"></p>
					<!-- <form action="" method="post" id="registration_form" name="registration_form">
						{!! csrf_field() !!}
						<table>
							<tr>
								<td>Email:</td>
								<td><input type="email" name="reg-email" class="reg-email"></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="reg-password" class="reg-password"></td>
							</tr>
							<tr>
								<td>Password (confirm):</td>
								<td><input type="password" name="reg-passwordconfirm" class="reg-passwordconfirm"></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="reg-signup" class="reg-signup" value="Sign Up"><span class="load"></span></td>
							</tr>
							<input type="hidden" name="reg-type" value="frompage" class="reg-type">
						</table>
					</form> -->
					<form action="" method="post" id="registration_form" name="registration_form">
						{!! csrf_field() !!}
						<div class="form-group">
							<label for="reg-email">Email:</label>
							<input type="email" class="form-control reg-email" id="reg-email" name="reg-email">
						</div>
						<div class="form-group">
							<label for="reg-password">Password:</label>
							<input type="password" class="form-control reg-password" id="reg-password" name="reg-password">
						</div>
						<div class="form-group">
							<label for="reg-passwordconfirm">Password (confirm):</label>
							<input type="password" class="form-control reg-passwordconfirm" id="reg-passwordconfirm" name="reg-passwordconfirm">
						</div>
						<div class="form-group">
							<label for="reg-name">Name:</label>
							<input type="text" class="form-control reg-name" id="reg-name" name="reg-name">
						</div>
						<div class="form-group">
							<label for="reg-dob">Date of Birth:</label>
							<input type="text" class="form-control reg-dob" id="reg-dob" name="reg-dob">
						</div>
						<div class="form-group">
							<label for="reg-country">Country:</label>
							<br>
							<input type="text" id="country" class="form-control reg-country" name="reg-country">
							<!-- <input type="text" class="form-control reg-country" id="reg-country" name="reg-country"> -->
						</div>
						<div class="form-group">
							<label for="reg-address">Address:</label>
							<input type="text" class="form-control reg-address" id="reg-address" name="reg-address">
						</div>
						<input type="hidden" name="reg-type" value="frompage" class="reg-type">
						<button type="submit" class="btn btn-default reg-signup" value="Sign Up">Sign Up</button><span class="load"></span>
					</form>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
		
		<div class="col-md-12 registrationapproval">
			<p>
				We have sent you a verification email.
				<br>Please check your email.
				<br><span class="showregemail"></span>
			</p>
		</div>
	</div>
</div>

@endsection