<!-- resources/views/homepage.blade.php -->

@extends('layouts.hometemplate')

@section('content')
<!-- <div class="row featureicons">
	<div class="col-md-12">
		<ul>
			<li>
				<i class="fa fa-check" aria-hidden="true"></i> Dummy icon1
			</li>
			<li>
				<i class="fa fa-check" aria-hidden="true"></i> Dummy icon2
			</li>
			<li>
				<i class="fa fa-check" aria-hidden="true"></i> Dummy icon3
			</li>
		</ul>
	</div>
</div> -->

<div class="row featureddestinations">
	<div class="col-md-12">
		<h1 class="section-title">Featured Destination</h1>
		<ul>
			@for ($i=0; $i < count($featured_destination); $i++)
			<li>
				<a href="">
					<div class="insideborder"></div>
					<div class="featured_image">
						<img src="{{ $featured_destination[$i]['image_source'] }}">
						<div class="blackmask"></div>
					</div>
					<div class="featureddestinations_caption">
						<p>
							{{ $featured_destination[$i]['kanji'] }}
							<br>
							{{ $featured_destination[$i]['title'] }}
						</p>
					</div>
				</a>
			</li>
			@if ($i==4)
				@break
			@endif
			@endfor
		</ul>
	</div>
	<div class="row alldestinations">
		<div class="col-md-12">
			<a href="">
				<button class="btn-orange">
					All Destinations
				</button>
			</a>
		</div>
	</div>
</div>

<div class="row featureditinerators">
	<div class="col-md-12">
		<h1 class="section-title">Our Itinerators</h1>
		<ul>
			@for ($i=0; $i < count($featured_itinerators); $i++)
			<li>
				<a href="">
					<div class="insideborder"></div>
					<div class="itineratorsrating">
						<p>{{ $featured_itinerators[$i]['rating'] }} <i class="fa fa-star" aria-hidden="true"></i></p>
					</div>
					<div class="featured_image">
						<img src="{{ $featured_itinerators[$i]['photo'] }}">
					</div>
					<div class="featureditinerators_caption">
						<p>
							{{ $featured_itinerators[$i]['name'] }} {{ $featured_itinerators[$i]['kanji'] }}
						</p>
					</div>
				</a>
			</li>
			@if ($i==4)
				@break
			@endif
			@endfor
		</ul>
	</div>
	<div class="row allitinerators">
		<div class="col-md-12">

			<a href="">
				<button class="btn-orange">
					All Itinerators
				</button>
			</a>
		</div>
	</div>
</div>


<div class="row why-itineration why-itineration-pc">
	<div class="col-md-12">
		<h1 class="section-title">Our Itinerators</h1>
		<ul>
			<li class="why-icon liwhy1">
				<img src="{{ URL::asset('images/map25.png') }}" alt="">
				<p class="why-subtext why1">Recommendation from locals</p>
				<div class=" triangle1 triangle active"></div>
			</li>
			<li class="why-icon liwhy2">
				<img src="{{ URL::asset('images/move14.png') }}" alt="">
				<p class="why-subtext why2">Flexible Routes and Places</p>
				<div class=" triangle2 triangle"></div>
			</li>
			<li class="why-icon liwhy3">
				<img src="{{ URL::asset('images/screenshot.png') }}" alt="">
				<p class="why-subtext why3">Detail and Precission Result</p>
				<div class=" triangle3 triangle"></div>
			</li>
			<li class="why-icon liwhy4">
				<img src="{{ URL::asset('images/time7.png') }}" alt="">
				<p class="why-subtext why4">No More Getting Lost</p>
				<div class=" triangle4 triangle"></div>
			</li>
		</ul>
		<div class="row why-itineration-desc">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<p class="why-desc1 why-desc active">
							Get Closer with the locals and get valuable recommendations about special landmarks, places and tips for your trip
						</p>
						<p class="why-desc2 why-desc">
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
						</p>
						<p class="why-desc3 why-desc">
							Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,
						</p>
						<p class="why-desc4 why-desc">
							Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
						</p>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row why-itineration why-itineration-sp">
	<div class="col-md-12">
		<h1 class="section-title">Why Cartrip ?</h1>
		<div class="row why-itineration-sp-content">
			<div class="col-md-12">
				<img src="{{ URL::asset('images/map25.png') }}" alt="">
				<p class="why-subtext">Recommendation from locals</p>
				<div class="triangle-sp"></div>
				<div class="row why-itineration-desc-sp">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-offset-2 col-md-8">
								<p class="why-desc-sp">
									Get Closer with the locals and get valuable recommendations about special landmarks, places and tips for your trip
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row why-itineration-sp-content">
			<div class="col-md-12">
				<img src="{{ URL::asset('images/move14.png') }}" alt="">
				<p class="why-subtext">Flexible Routes and Places</p>
				<div class="triangle-sp"></div>
				<div class="row why-itineration-desc-sp">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<p class="why-desc-sp">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
								</p>
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row why-itineration-sp-content">
			<div class="col-md-12">
				<img src="{{ URL::asset('images/screenshot.png') }}" alt="">
				<p class="why-subtext">Detail and Precission Result</p>
				<div class="triangle-sp"></div>
				<div class="row why-itineration-desc-sp">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<p class="why-desc-sp">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
								</p>
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row why-itineration-sp-content">
			<div class="col-md-12">
				<img src="{{ URL::asset('images/time7.png') }}" alt="">
				<p class="why-subtext">No More Getting Lost</p>
				<div class="triangle-sp"></div>
				<div class="row why-itineration-desc-sp">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<p class="why-desc-sp">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
								</p>
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="container">
	<div class="row easy-steps">
		<div class="col-md-12">
			<h1>3 EASY STEPS</h1>
			<div class="row step1">
				<div class="col-md-6">
					<img src="{{ URL::asset('images/step1.png') }}" alt="">
				</div>
				<div class="col-md-6 step1-text">
					<h2>Step 1</h2>
					<h3>Complete Your Profile</h3>
					<p>Tell us when, where and how you like to travel </p>
				</div>
			</div>
			<div class="row step2">
				<div class="col-md-6 step2-text">
					<h2>Step 2</h2>
					<h3>Decide Your Pathfinder</h3>
					<p>Choose your favorite local or we decide for you </p>
				</div>
				<div class="col-md-6">
					<img src="{{ URL::asset('images/step2.png') }}" alt="">
				</div>
			</div>
			<div class="row step3">
				<div class="col-md-6">
					<img src="{{ URL::asset('images/step3.png') }}" alt="">
				</div>
				<div class="col-md-6 step3-text">
					<h2>Step 3</h2>
					<h3>Be Ready For The Trip</h3>
					<p>Read your curated itinerary and be ready for your trip </p>
				</div>
			</div>
		</div>
	</div>
</div> -->


<div class="row home-bottom-start">
	<div class="col-md-12">
		<div class="home-bottom-start-content">
			<h1>WHAT ARE YOU WAITING FOR?</h1>
			<a href="{{ URL::to('#top') }}"><button>START PLANNING MY TRIP</button></a>
		</div>
	</div>
</div>
@endsection