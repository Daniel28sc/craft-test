<!-- resources/views/paymenthistory.blade.php -->

@extends('layouts.basictemplate')

@section('content')
<script>
	$(function() {
        var columns = [{
        	field: 'no',
        	title: 'No.',
        	sortable: true,
        	halign: 'center',
        	align: 'center'
        },{
        	field: 'description',
        	title: 'Description',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'added_on',
        	title: 'Date & Time',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        }];
        $('#paymenthistorytable').bootstrapTable({
        	pagination: true,
        	smartDisplay: true,
        	striped: true,
        	sortable: true,
        	columns: columns,
        	pageSize: 15,
        	search: 'true'
        });
    });
</script>
<div class="container">
	<div class="row profile-heading">
		<div class="col-md-12">
			<p>Welcome back, {{ $user[0]['name'] }}!</p>
			<ul class="topprofilemenu">
				<li>
					Setting
				</li>
				<li>
					|
				</li>
				<li>
                	<a href="{{ url('/signout') }}">Logout</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="row inputstyle-sectionline">
		<div class="col-md-12"></div>
	</div>

	<div class="row userprofile">
		<div class="col-md-3 profile-menu">
			<ul>
				<li class="menu-myprofile">
					<p><a href="{{ url('/profile') }}">My Profile</a></p>
				</li>
				<li class="menu-myitinerary">
					<p><a href="{{ url('/myitinerary') }}">My Itinerary</a></p>
				</li>
				<li class="menu-paymenthistory">
					<p><a href="{{ url('/paymenthistory') }}">Payment History</a></p>
				</li>
				<li class="menu-itineratorhistory">
					<p>Itinerator History</p>
				</li>
			</ul>
		</div>

		<div class="col-md-9 profile-content">
			<div class="userprofile-paymenthistory">
				<table id="paymenthistorytable" class="table table-hover">
					<?php $x=1; ?>
					@for($i=0; $i<count($log); $i++)
						<tr>
							<td>{{ $x }}</td>
							<td>{{ $log[$i]['description'] }}</td>
							<td>{{ $log[$i]['added_on'] }}</td>
						</tr>
						<?php $x++; ?>
					@endfor
				</table>
			</div>
		</div>
	</div>
</div>
@endsection