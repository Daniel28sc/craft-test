<!-- resources/views/myjob.blade.php -->

@extends('layouts.itineratorspagetemplate')

@section('content')
<?php
$itinerators_id = $data['itineratorsid'];
$itinerators = $data['itinerators'];
$iirelation_list = $data['iirelation_list'];
$itinerary = $data['itinerary'];
$itinerary_day = $data['itinerary_day'];
$itinerary_day_item = $data['itinerary_day_item'];
$cityList = $data['cityList'];
$placeList = $data['placeList'];
$log = $data['log'];
?>
<script>
	$(function() {
        var columns = [{
        	field: 'no',
        	title: 'No.',
        	sortable: true,
        	halign: 'center',
        	align: 'center'
        },{
        	field: 'description',
        	title: 'Description',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        },{
        	field: 'added_on',
        	title: 'Date & Time',
        	sortable: true,
        	halign: 'center',
        	align: 'left'
        }];
        $('.tablemodalhistory').bootstrapTable({
        	pagination: true,
        	smartDisplay: true,
        	striped: true,
        	sortable: true,
        	columns: columns,
        	pageSize: 15,
        	search: 'true'
        });
    });
</script>
<div class="container-fluid itineratorshome">
	<div class="row navbar">
		<div class="col-md-6 navbar1">
			<ul>
				<li>
					<a href="{{ url('/itineratorshome') }}">LIST</a>
				</li>
				<li>
					<a href="{{ url('/myjob') }}">MY JOB</a>
				</li>
				<li>
					<a href="{{ url('/itinerators_history') }}">HISTORY</a>
				</li>
			</ul>
		</div>
		<div class="col-md-6 navbar2">
			<ul class="pull-right">
				<li>
					<a href="{{ url('/itineratorshome') }}">{{ $itinerators[0]['name'] }}</a>
				</li>
				<li>
					<a href="{{ url('/itineratorssignout') }}">SIGN OUT</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="container myjob">
	<div class="row myjob-tab needconfirmation">
		<div class="col-md-12">
			<h1>
				Need Confirmation
			</h1>
			<table class='itineratorsjoblist table table-hover table-bordered'>
			<?php
			for ($i=0; $i < count($iirelation_list); $i++) { 
				$status = $iirelation_list[$i]['status'];
				if ($status == 'itineratorconfirmation') {
				?>
				<tr>
					<td><p><?php echo $itinerary[$i]['name']; ?>: <?php echo $itinerary[$i]['day']; ?> Days</p></td>
					<td>
						<button class='myjob-actionbuttons myjob-history' data-toggle='modal' data-target='#modal-historylog-<?php echo $itinerary[$i]['it_id']; ?>'>History</button>
					</td>
					<td>
						<!-- <button class='myjob-actionbuttons myjob-upload' value='<?php echo $itinerary[$i]['it_id']; ?>'>Upload</button> -->
						<form action="itineratorspage_controller" method="post" enctype="multipart/form-data">
							{!! csrf_field() !!}
							<input type="hidden" name="itineratorspage_formtype" value="itineratorconfirm">
							<input type="hidden" name="dummy_itid" value="<?php echo $itinerary[$i]['it_id']; ?>">
							<input type="hidden" name="dummy_itineratorid" value="<?php echo $itinerators[0]['itinerators_id']; ?>">
							<button class="itineratorconfirm">Confirm</button>
						</form>
					</td>
				</tr>
				<?php
				}
			}
			?>
			</table>
		</div>
	</div>
	<div class="row myjob-tab currentjob">
		<div class="col-md-12">
			<h1>
				Current Job
			</h1>
			<table class='itineratorsjoblist table table-hover table-bordered'>
			<?php
			for ($i=0; $i < count($itinerary); $i++) { 
				$status = $iirelation_list[$i]['status'];
				if ($status == 'currently' || $status == 'confirmresult') {
				?>
				<tr>
					<td><p><?php echo $itinerary[$i]['name']; ?>: <?php echo $itinerary[$i]['day']; ?> Days</p></td>
					<td>
						<button class='myjob-actionbuttons myjob-history' data-toggle='modal' data-target='#modal-historylog-<?php echo $itinerary[$i]['it_id']; ?>'>History</button>
					</td>
					<td>
						<!-- <button class='myjob-actionbuttons myjob-upload' value='<?php echo $itinerary[$i]['it_id']; ?>'>Upload</button> -->
						<form action="itineratorspage_controller" method="post" enctype="multipart/form-data">
							{!! csrf_field() !!}
							<input type="hidden" name="itineratorspage_formtype" value="uploaditinerarypdf">
							<input type="hidden" name="dummy_itid" value="<?php echo $itinerary[$i]['it_id']; ?>">
							<input type="hidden" name="dummy_itineratorid" value="<?php echo $itinerators[0]['itinerators_id']; ?>">
							<?php
							if ($itinerary[$i]['itinerary_file']!='') {
								echo "Current file: ".$itinerary[$i]['itinerary_file'];
							}
							?>
							<div class="form-group">
								<input type="file" class="uploaditinerarypdf" name="itinerarypdffile">
								<button type="submit" class="btn btn-default submititineraryfile">Submit</button>
							</div>
						</form>
					</td>
				</tr>
				<?php
				}
			}
			?>
			</table>
		</div>
	</div>
	<div class="row myjob-tab revisionjob">
		<div class="col-md-12">
			<h1>
				Revision
			</h1>
			<table class='itineratorsjoblist table table-hover table-bordered'>
			<?php
			for ($i=0; $i < count($itinerary); $i++) { 
				$status = $iirelation_list[$i]['status'];
				if ($status == 'revision') {
				?>
				<tr>
					<td><p><?php echo $itinerary[$i]['name']; ?>: <?php echo $itinerary[$i]['day']; ?> Days</p></td>
					<td>
						<button class='myjob-actionbuttons myjob-history' data-toggle='modal' data-target='#modal-historylog-<?php echo $itinerary[$i]['it_id']; ?>'>History</button>
					</td>
					<td>
						<!-- <button class='myjob-actionbuttons myjob-upload' value='<?php echo $itinerary[$i]['it_id']; ?>'>Upload</button> -->
						<form action="itineratorspage_controller" method="post" enctype="multipart/form-data">
							{!! csrf_field() !!}
							<input type="hidden" name="itineratorspage_formtype" value="uploaditinerarypdf">
							<input type="hidden" name="dummy_itid" value="<?php echo $itinerary[$i]['it_id']; ?>">
							<input type="hidden" name="dummy_itineratorid" value="<?php echo $itinerators[0]['itinerators_id']; ?>">
							<?php
							if ($itinerary[$i]['itinerary_file']!='') {
								echo "Current file: ".$itinerary[$i]['itinerary_file'];
							}
							?>
							<div class="form-group">
								<input type="file" class="uploaditinerarypdf" name="itinerarypdffile">
								<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</form>
					</td>
					@if($iirelation_list[$i]['revision_note'] != '')
					<td>
						{{ $iirelation_list[$i]['revision_note'] }}
					</td>
					@endif
				</tr>
				<?php
				}
			}
			?>
			</table>
		</div>
	</div>
	<div class="row myjob-tab finished">
		<div class="col-md-12">
			<h1>
				Finished
			</h1>
			<table class='itineratorsjoblist table table-hover table-bordered'>
			<?php
			for ($i=0; $i < count($iirelation_list); $i++) { 
				$status = $iirelation_list[$i]['status'];
				if ($status == 'finished') {
				?>
				<tr>
					<td><p><?php echo $itinerary[$i]['name']; ?>: <?php echo $itinerary[$i]['day']; ?> Days</p></td>
					<td>
						<button class='myjob-actionbuttons myjob-history' data-toggle='modal' data-target='#modal-historylog-<?php echo $itinerary[$i]['it_id']; ?>'>History</button>
					</td>
					<td>
						
					</td>
				</tr>
				<?php
				}
			}
			?>
			</table>
		</div>
	</div>
	
	<?php
	for ($i=0; $i < count($itinerary); $i++) { 
	?>
	<!-- Modal History Log -->
	<div class="modal fade" id="modal-historylog-<?php echo $itinerary[$i]['it_id']; ?>" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4>History Log for <?php echo $itinerary[$i]['name']; ?></h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@if(count($log[$i])>0)
							<table class="table table-hover tablemodalhistory">
								<!-- <tr>
									<th>No.</th>
									<th>Description</th>
									<th>Date & Time</th>
								</tr> -->
							<?php
							for ($j=0; $j < count($log[$i]); $j++) { 
							?>
								<tr>
									<td>
										<?php echo ($j+1); ?>
									</td>
									<td>
										<?php echo $log[$i][$j]['description']; ?>
									</td>
									<td>
										<?php echo $log[$i][$j]['added_on']; ?>
									</td>
								</tr>
							<?php
							}
							?>
							</table>
							@endif
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>
	<!-- Modal -->
	<?php
	}
	?>
	
</div>
@endsection