<!-- resources/includes/javascript.blade.php -->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


<!-- For Design -->
<script src="{{ URL::asset('js/validation.js') }}" type="text/javascript"></script>	
<script src="{{ URL::asset('js/custom.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/datepicker.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/timepicker.js') }}" type="text/javascript"></script>	
<script src="{{ URL::asset('js/countrypicker.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/bootstrap-table.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/bootstrap-validator.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/cropper.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/autocomplete.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/money.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/accounting.js') }}" type="text/javascript"></script>

<!-- For Process -->
<script src="{{ URL::asset('js/process.js') }}" type="text/javascript"></script>	
