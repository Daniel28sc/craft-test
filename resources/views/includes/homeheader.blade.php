<!-- resources/includes/homeheader.blade.php -->
<script type="text/javascript">
    var _siteurl = "{{ url('/') }}";
</script>
<?php
$itid = session("temp_itid");
$userid = session('userid');

$num_tid_list = count($tid_list);
$num_tidi_list = count($tidi_list);

if (count($ti_list)>0) {
	$temp_tidilist = 0;
	$arr_tidilist_city = array();
	for ($i=0; $i < $num_tidi_list; $i++) {
		for ($j=0; $j < count($tidi_list[$i]); $j++) {
			if ($tidi_list[$i][$j]['city_id'] == $temp_tidilist) {
				continue;
			}
			array_push($arr_tidilist_city, $tidi_list[$i][$j]['city_id']);
			$temp_tidilist = $tidi_list[$i][$j]['city_id'];
		}
	}

	$i = 0;
	$arr_cityname = array();
	while ($i < count($arr_tidilist_city)) {
		if (count($arr_cityname) == 5) {
			break;
		}
		$cityid = $arr_tidilist_city[$i];
		for ($j=0; $j < count($featured_destination); $j++) {
			if ($cityid == $featured_destination[$j]['city_id']) {
				$cityname = $featured_destination[$j]['title'];
				break;
			}
		}
		if (in_array($cityname, $arr_cityname)) {
			$i++;
			continue;
		}
		else{
			if ($i!=0) {
				array_push($arr_cityname, ' - ');

			}
		}

		array_push($arr_cityname, $cityname);
		// if ($i != 2) {
		// 	$x = count($arr_tidilist_city);
		// 	if ($i != ($x-1)) {
		// 		array_push($arr_cityname, ' - ');
		// 	}
		// }

		$i++;
	}
	$date1 = $tid_list[0]['date'];
	$datedata1 = date_parse($date1);
	$year1 = $datedata1['year'];
	$month1 = $datedata1['month'];
	$day1 = $datedata1['day'];
	$strdate1 = $year1.'/'.$month1.'/'.$day1;
	$cal1 = strtoupper(date('Y M d', strtotime($strdate1)));

	$date2 = $tid_list[$num_tid_list-1]['date'];
	$datedata2 = date_parse($date2);
	$year2 = $datedata2['year'];
	$month2 = $datedata2['month'];
	$day2 = $datedata2['day'];
	$strdate2 = $year2.'/'.$month2.'/'.$day2;
	if ($year1 == $year2) {
		$cal2 = strtoupper(date('M d', strtotime($strdate2)));
	}
	else{
		$cal2 = strtoupper(date('Y M d', strtotime($strdate2)));
	}

	$lastitinerarycities = implode('', $arr_cityname);
	$lastitinerarydates = $cal1.' - '.$cal2;
}
else{
	$lastitinerarycities = '';
	$lastitinerarydates = '';
}

?>
<header class="homeheader">
	<div class="patternover"></div>
	<div class="header-content">
		<nav class="navbar" id="top">
			<div class="navbg"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-6 navmenu">
						<ul>
							<li class="navlogo">
								<a href="{{ URL::to('/') }}">
									<ul>
										<li>.CRAFTRIP.</li>
										<li><img src="{{ URL::asset('images/itinerarybylocals.png') }}"></li>
									</ul>
								</a>
							</li>
							<li><a href="{{ URL::to('/') }}">FEATURES</a></li>
							<li><a href="{{ URL::to('/') }}">SAMPLE</a></li>
							<li><a href="{{ URL::to('/') }}">PRICING</a></li>
						</ul>
					</div>
					<div class="col-md-5 navaccount">
						<?php
						if (session('userid')>0) {
						?>
						<ul>
							<li>Hello, {{ $user[0]['name'] }}</li>
							<li><a href="{{ URL::to('/profile') }}">MY PROFILE</a></li>
							<li><a href="{{ URL::to('/signout') }}">SIGN OUT</a></li>
						</ul>
						<?php
						}
						else{
						?>
						<ul>
							<li><a href="{{ URL::to('/login') }}">SIGN IN</a></li>
							<li><a href="{{ URL::to('/registration') }}">SIGN UP</a></li>
						</ul>
						<?php
						}
						?>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</nav>

		<!-- <div class="row site-logo">
			<div class="col-md-12">
				<img src="{{ URL::asset('images/TOPLOGO.png') }}" alt="">
			</div>
		</div> -->

		<?php
		if (count($ti_list)) {
		?>
		<div class="row lastitinerary">
			<div class="col-md-2"></div>
			<div class="col-md-7">
				<button class="lastitinerarybox" id="lastcreateditinerary" name="button" value="<?php echo $itid; ?>">
					<div class="row">
						<div class="col-md-3 lastitinerarybox-c1">
							<p>YOUR LATEST TRIP</p>
						</div>
						<div class="col-md-5 lastitinerarybox-c2">
							<p><?php echo strtoupper($lastitinerarycities); ?></p>
						</div>
						<div class="col-md-4 lastitinerarybox-c3 text-right">
							<p><?php echo strtoupper($lastitinerarydates); ?></p>
						</div>
					</div>
				</button>
				<!-- <button class="lastitinerarybox" id="lastcreateditinerary" name="button" value="<?php echo $itid; ?>">
					<p>
						YOUR LATEST TRIP  <span class="lastitinerary_citydate"><?php echo $lastitinerarycities; ?> <?php echo $lastitinerarydates; ?></span>
					</p>
				</button> -->
			</div>
			<div class="col-md-2"></div>
		</div>
		<?php
		}
		?>

		<div class="row schedule-home" id="schedule-home">
			<div class="col-md-2"></div>
			<div class="col-md-7 schedule-home-content">
				<div class="schedule-home-content-bg">
					<div class="row schedulehome-selectdestination">
						<div class="col-md-12">
							<div class="form-group">
								<input name="_token" class="token" type="hidden" value="{!! csrf_token() !!}" />
								<input type="hidden" name="arrivalreturntype" class="arrivalreturntype" value="select destination">
								<!-- <label for="selectdestination">My Destination</label>
								<select class="form-control" id="selectdestination">
									<option value="none">Select Destination</option>
									<option value="JAPAN">Japan</option>
								</select> -->
								<div class="selectdestination-dropdown">
									<h1 class="home-title">We Are Craftrip</h1>
									<p class="home-subtitle">The experts in creating tailor-made holidays and honeymoons around the world</p>
									<ul>
										<li>
											<div class="searchbox">
												<select class="form-control" id="selectdestination">
													<option value="none">Select Destination</option>
													{{-- <option value="JAPAN">Japan</option> --}}
													@foreach ($country as $country)
														<option value="{{ $country['name'] }}">{{ $country['name'] }}</option>
													@endforeach
												</select>
												{{-- <span class="selectdestination-arrow"><i class="fa fa-chevron-down" aria-hidden="true"></i></span> --}}
											</div>
										</li>
										<li>
											<div class="selectdestination-go">
												<button>
													DISCOVER <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row schedulehome-contentform" style="display:none;">
						<div class="col-md-6 schedule-home-left">
							<h1>My Schedule</h1>
							<p class="dummyline1"></p>
							<div class="row schedule-home-arrival">
								<div class="col-md-12">
									<p class="reg-warning"></p>
									<h2>Arrival</h2>
									<form action="arrivalreturn_controller" method="post" name="arrivalreturnform" class="arrivalreturnform">
									{!! csrf_field() !!}
									{{-- <input name="_token" type="hidden" value="{!! csrf_token() !!}" /> --}}
									<input type="hidden" name="arrivalreturntype" class="arrivalreturntype" value="arrivalreturn">
									<div class="input-group group-home-airport">
										<span class="input-group-addon" id="basic-addon1">Airport</span>
										<!-- <input type="text" class="form-control home-airport" placeholder="Airport Name" aria-describedby="basic-addon1" name="arrivalairport" id="arrivalairport"> -->
										<input type="text" name="arrivalairport" id="autocomplete-airport" class="form-control home-airport"/>
										<!-- <select name="arrivalairport" id="arrivalairport" class="form-control home-airport"></select> -->
									</div>
									<div class="input-group group-home-date">
										<span class="input-group-addon" id="basic-addon1">Date</span>
										<input type="text" class="form-control home-date" aria-describedby="basic-addon1" name="arrivaldate" id="arrivaldate">
									</div>
									<div class="input-group group-home-time bootstrap-timepicker timepicker">
										<span class="input-group-addon add-on" id="basic-addon1">Time</span>
										<input input id="timepicker" class="form-control home-time" data-provide="timepicker" data-minute-step="1" data-modal-backdrop="true" name="arrivaltime">
										<!-- <input input id="timepicker" class="form-control home-time" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" name="arrivaltime"> -->
									</div>
								</div>
							</div>

							<div class="row schedule-home-return">
								<div class="col-md-12">
									<h2>Return</h2>
									<div class="input-group group-home-airport">
										<span class="input-group-addon" id="basic-addon1">Airport</span>
										<!-- <input type="text" class="form-control home-airport return-airport" placeholder="Airport Name" aria-describedby="basic-addon1" name="returnairport"> -->
										<input type="text" name="returnairport" id="autocomplete-airportreturn" class="form-control home-airport return-airport"/>
										<!-- <select class="form-control home-airport return-airport" name="returnairport"></select> -->
									</div>
									<div class="input-group group-home-date">
										<span class="input-group-addon" id="basic-addon1">Date</span>
										<input type="text" class="form-control home-date1 return-date" aria-describedby="basic-addon1" name="returndate" id="returndate">
									</div>
									<div class="input-group group-home-time bootstrap-timepicker timepicker">
										<span class="input-group-addon add-on" id="basic-addon1">Time</span>
										<input input id="timepicker" class="form-control home-time return-time" data-provide="timepicker" data-minute-step="1" data-modal-backdrop="true" name="returntime">
										<!-- <input input id="timepicker" class="form-control home-time return-time" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" name="returntime"> -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 schedule-home-right">
							<h1>Packages</h1>
							<p class="dummyline1"></p>
							<div class="row home-day">
								<div class="col-md-12">
									<div class="range-slider">
										<div id="slider">
											<input class="bar" type="range" id="rangeinput" min="1" value="1" max="1" onchange="rangevalue.value=value" name="itinerarynod"/>
											<output id="dayrangetooltip" for="itinerarynod" name="dayrangetooltip">1</output>
											<span class="highlight"></span>
											<p>
												Please help me plan <input type='text' id="rangevalue" name='rangevaluedays' value='1' oninput="rangeinput.value=value"> days trip
												{{-- Please help me plan <output id="rangevalue">1</output> days trip --}}
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row home-packages-continue">
								<div class="col-md-12">
										<button id="homepackagescontinuebutton" name="button" value="continue">CONTINUE</button>
									</form>
								</div>
							</div>
							<p class="dummyline1"></p>
							<div class="row home-registration">
								<p>Already have an account?</p>
								<a href="{{ URL::to('/login') }}" class="signin-home"><button name="button" value="register">SIGN IN</button></a>
								<p>
									<em><a href="">Forget password</a> or <a href="{{ URL::to('/registration') }}">Sign up</a></em>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</header>
