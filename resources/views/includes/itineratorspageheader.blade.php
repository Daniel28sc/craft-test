<!-- resources/includes/itineratorspageheader.blade.php -->
<script type="text/javascript">
    var _siteurl = "{{ url('/') }}";
</script>
<header class="basicheader">
	<div class="patternover basicheaderpattern"></div>
	<nav class="navbar basicheader-navbar" id="top">
		<div class="navbg"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-6 navmenu">
					<ul>
						<li class="navlogo">
							<a href="{{ URL::to('/') }}">
								<ul>
									<li>.ITINERARIUM.</li>
									<li><img src="{{ URL::asset('images/itinerarybylocals.png') }}"></li>
								</ul>
							</a>
						</li>
						<li><a href="{{ URL::to('/') }}">FEATURES</a></li>
						<li><a href="{{ URL::to('/') }}">SAMPLE</a></li>
						<li><a href="{{ URL::to('/') }}">PRICING</a></li>
					</ul>
				</div>
				<div class="col-md-5 navaccount">
					<?php
					if (session('userid')>0) {
						?>
						<ul>
							{{-- <li>Hello, {{ $user[0]['name'] }}</li>
							<li><a href="{{ URL::to('/profile') }}">MY PROFILE</a></li>
							<li><a href="{{ URL::to('/signout') }}">SIGN OUT</a></li> --}}
						</ul>
						<?php
					}
					else{
						?>
						<ul>
							<li><a href="{{ URL::to('/login') }}">SIGN IN</a></li>
							<li><a href="{{ URL::to('/registration') }}">SIGN UP</a></li>
						</ul>
						<?php
					}
					?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</nav>
	<!-- <div class="row site-logo">
		<div class="col-md-12">
			<img src="{{ URL::asset('images/TOPLOGO.png') }}" alt="">
		</div>
	</div> -->
</header>
