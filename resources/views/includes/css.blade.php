<!-- resources/includes/css.blade.php -->


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">

<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/datepicker.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/countrypicker.css') }}" type="text/css">
<link rel="stylesheet/less" href="{{ URL::asset('css/timepicker.less') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-table.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/cropper.css') }}" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('css/autocomplete.css') }}" type="text/css">

