<!-- resources/views/requestwork.blade.php -->

@extends('layouts.itineratorspagetemplate')

@section('content')
<?php
$itinerators_id = $data['itinerators_id'];
$it_id = $data['it_id'];
$cityList = $data['cityList'];
$placeList = $data['placeList'];
$itinerary = $data['itinerary'];
$itineraryDay = $data['itineraryDay'];
$itineraryDayItem = $data['itineraryDayItem'];
$itinerators = $data['itinerators'];
$iirelation = $data['iirelation'];
$currentrelation = $data['currentrelation'];
$currentitinerator = $data['currentitinerator'];

$temp_itid = session('temp_itid');
for ($i=0; $i < count($cityList); $i++) { 
	$cityList[$i]['description'] = str_replace('"', "'", $cityList[$i]['description']);
}
for ($i=0; $i < count($placeList); $i++) { 
	$placeList[$i]['description'] = str_replace('"', "'", $placeList[$i]['description']);
}

if ($itinerary['arrival_date'] != '') {
	$date1 = $itinerary['arrival_date'];
	$datedata1 = date_parse($date1);
	$year1 = $datedata1['year'];
	$month1 = $datedata1['month'];
	$day1 = $datedata1['day'];
	$strdate1 = $year1.'/'.$month1.'/'.$day1;
	$cal1 = strtoupper(date('Y M d', strtotime($strdate1)));

	$date2 = $itinerary['return_date'];
	$datedata2 = date_parse($date2);
	$year2 = $datedata2['year'];
	$month2 = $datedata2['month'];
	$day2 = $datedata2['day'];
	$strdate2 = $year2.'/'.$month2.'/'.$day2;
	if ($year1 == $year2) {
		$cal2 = strtoupper(date('M d', strtotime($strdate2)));
	}
	else{
		$cal2 = strtoupper(date('Y M d', strtotime($strdate2)));
	}

	$itineraryschedule = $cal1.' - '.$cal2;
}
else{
	$itineraryschedule = '';
}
?>
<div class="container-fluid itineratorshome">
	<div class="row navbar">
		<div class="col-md-6 navbar1">
			<ul>
				<li>
					<a href="{{ url('/itineratorshome') }}">LIST</a>
				</li>
				<li>
					<a href="{{ url('/myjob') }}">MY JOB</a>
				</li>
				<li>
					<a href="{{ url('/itinerators_history') }}">HISTORY</a>
				</li>
			</ul>
		</div>
		<div class="col-md-6 navbar2">
			<ul class="pull-right">
				<li>
					<a href="{{ url('/itineratorshome') }}">{{ $currentitinerator[0]['name'] }}</a>
				</li>
				<li>
					<a href="{{ url('/itineratorssignout') }}">SIGN OUT</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="container">
	<div class="row requestwork request-summary">
		<div class="col-md-12">
			<div class="row summaryitinerary-content">
				<div class="col-md-12">
					<div class="reg-warning warn-summarypage">
						This itinerary is already saved by other user.   
					</div>

					<div class="summary-profile">
						<table>
							<tr>
								<td colspan="2">
									<h1>ITINERARY SUMMARY</h1>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<p>
										{{ $itinerary['name'] }}
									</p>
								</td>
							</tr>
							<tr class="summary-day">
								<td>
									<p>
										{{ $itinerary['day'] }} Days
									</p>
								</td>
								<td>
									<p class="pull-right">
										<?php echo $itineraryschedule; ?>
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-plane fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<p>
										{{ $itinerary['arrival_airport'] }} ({{ $itinerary['arrival_date'] }} {{ $itinerary['arrival_time'] }})
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-plane fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<p>
										{{ $itinerary['return_airport'] }} ({{ $itinerary['return_date'] }} {{ $itinerary['return_time'] }})
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-jpy fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<p>
										{{ $itinerary['budget'] }}
									</p>
								</td>
							</tr>
							<tr class="summary-pref-1">
								<td>
									<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<?php
									$x = 1;
									$x1 = 1;
									$arr_pref_var1 = ['','Family','Backpack','Vegetarian','Romantic','Culinaire','Cheap Eat','Bar & Club','Night Seeing'];
									$arr_pref_var1_1 = ['','family','backpack','vegetarian','romantic','culinaire','cheapeat','barclub','nightseeing'];
									while ($x < 9) {

										if ($itinerary[$arr_pref_var1_1[$x]] == '1') {
											if ($x1%3 == 1) {
												echo '<ul class="ul-cpvar-1">';
											}

											echo '<li>'.$arr_pref_var1[$x].'</li>';

											if ($x1%3 == 0) {
												echo '</ul>';
											}
											$x1++;
										}

										$x++;
									}
									?>
								</td>
							</tr>
							<tr class="summary-pref-2">
								<td>
									<i class="fa fa-map-o fa-2x" aria-hidden="true"></i>
								</td>
								<td>
									<?php
									$arr_pref_var2 = ['','Popular Places','Culture & Art','Cuisine','Nature','Entertainment','History','Shopping','Sports'];
									$arr_pref_var2_1 = ['','mustsee','culture','cuisine','nature','entertainment','history','shopping','sports'];
									for ($i=1; $i < 9; $i++) { 
										$pref_rate = $itinerary[$arr_pref_var2_1[$i]];
										if ($i%3 == 1) {
											echo '<ul class="ul-cpvar-2">';
										}
										echo '<li>';
										echo $arr_pref_var2[$i];
										echo '<span>';
										for ($ii=0; $ii < $pref_rate; $ii++) { 
											echo '<i class="fa fa-circle prefratecircle" aria-hidden="true"></i>';
										}
										echo '</span>';
										echo '</li>';
										if ($i%3 == 0) {
											echo '</ul>';
										}
									}
									?>
								</td>
							</tr>
						</table>


						<div class="summary-preferences">
							<div class="summary-cityplace">
								<div class="row">
									<?php
									$tempdate = '';
									$temp_cpday = $itineraryDayItem[0]['itday_id'];
									$counterday = 0;
									$temp_city = '';
									$counter_itday = -1;
									for ($i=0; $i < count($itineraryDayItem); $i++) { 
										$index_city = -1;
										$index_place = -1;

										$cid = $itineraryDayItem[$i]['city_id'];
										for ($j=0; $j < count($cityList); $j++) { 
											if ($cityList[$j]['city_id'] == $cid) {
												$index_city = $j;
												break;
											}
										}

										$pid = $itineraryDayItem[$i]['place_id'];
										for ($j=0; $j < count($placeList); $j++) { 
											if ($placeList[$j]['place_id'] == $pid) {
												$index_place = $j;
												break;
											}
										}

										$cp_bg = $cityList[$index_city]['image_source'];
										
										if ($itineraryDayItem[$i]['itday_id'] == $temp_cpday) {

										}
										else{
											$counterday++;
											$temp_cpday = $itineraryDayItem[$i]['itday_id'];
										}

										if ($itineraryDay[$counterday]['date'] != $tempdate) {
											if ($tempdate!='') {
												echo "</div>";
											}
											$tempdate = $itineraryDay[$counterday]['date'];
											$temp_city = '';
											$counter_itday++;
											echo "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12 summary-cpbox'>";
											echo "<span class='summary-cpdate'>".$itineraryDay[$counterday]['date'].'</span>';
										}
										else{

										}

										if ($temp_city != $cityList[$index_city]['title']) {
											if ($index_city>-1) {
												echo '<p>'.$cityList[$index_city]['title'].'</p>';
												$temp_city = $cityList[$index_city]['title'];
											}
										}

										$write_note = 0;
										if ($itineraryDay[$counterday]['same_as_previous'] == 'no') {
											if ($index_place>-1) {
												echo '<p>- '.$placeList[$index_place]['title'].'</p>';
												$write_note = 1;
											}
										}

										$hotelname = '-';
		                                $hoteladdress = '-';
		                                $hotelphone = '-';
		                                $write_hotel = 0;
		                                $hotel = $itineraryDay[$counter_itday]['hotel'];
		                                $hotel = explode('#|#', $hotel);
		                                
		                                if ($itineraryDayItem[$i]['note'] != '') {
		                                    if ($i == count($itineraryDayItem)-1) {
		                                    	if ($write_note == 1) {
		                                    		echo "<p>Note: ".$itineraryDayItem[$i]['note']."</p><br>";
		                                    	}
		                                        $write_hotel=1;
		                                    }
		                                    else{
		                                        if (($itineraryDayItem[$i]['itday_id'] != $itineraryDayItem[$i+1]['itday_id']) ||
		                                        	($itineraryDayItem[$i]['city_id'] != $itineraryDayItem[$i+1]['city_id'])) {
		                                        	if ($write_note == 1) {
		                                        		echo "<p>Note: ".$itineraryDayItem[$i]['note']."</p><br>";
		                                        	}
		                                        }
		                                        if ($itineraryDayItem[$i]['itday_id'] != $itineraryDayItem[$i+1]['itday_id']) {
		                                        	$write_hotel=1;
		                                        }
		                                    }
		                                } 

		                                if ($write_hotel==1) {
		                                    if (!empty($hotel[0])) {
		                                        $hotelname = $hotel[0];
		                                    }
		                                    if (!empty($hotel[1])) {
		                                        $hoteladdress = $hotel[1];
		                                    }
		                                    if (!empty($hotel[2])) {
		                                        $hotelphone = $hotel[2];
		                                    }
		                                }
		                                
		                                if ($hotelname!='-' || $hoteladdress!='-' || $hotelphone!='-') {
		                                    echo "<h2>Hotel</h2>";
		                                    echo "Name: ".$hotelname;
		                                    echo "<br>Address: ".$hoteladdress;
		                                    echo "<br>Phone: ".$hotelphone;
		                                }

										if ($itineraryDay[$counterday]['date'] != $tempdate) {
                                    		// echo "</div>";
										}

										if ($i == count($itineraryDayItem)-1) {
											echo "</div>";
										}
									}
									?>
									<!--  </div> --> <!-- For last ibox -->
								</div>
							</div>

							<!-- <div class="summary-itinerarydesc">
								<div class="row">
									<div class="col-md-12">
										<h1>
											Itinerary Details
										</h1>
										<?php
										$temp_itdayid = 0;
										$counterday = 0;
										for ($i=0; $i < count($itineraryDayItem); $i++) {
											if ($temp_itdayid != $itineraryDayItem[$i]['itday_id']) {
												if ($itineraryDayItem[$i]['note'] != '') {
													echo "<h4>".$itineraryDay[$counterday]['date']."</h4>";
													echo "<p>".$itineraryDayItem[$i]['note']."</p>";
													echo "<br>";
												}
												$counterday++;
												$temp_itdayid = $itineraryDayItem[$i]['itday_id'];
											}
										}
										?>
									</div>
								</div>
							</div> -->

							<div class="summary-itinerators">
								<h1>Itinerators</h1>
								@if($iirelation[0]['pick_type'] == 'random')
									We will choose for you.
									<div class="row">
								@else
									<h2>We will inform these...</h2>
									<div class="row">
									@foreach($iirelation as $iirelation)
									<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 ibox">
										<?php
										$iid = $iirelation['itinerators_id'];
										for ($j=0; $j < count($itinerators); $j++) { 
											if ($itinerators[$j]['itinerators_id'] == $iid) {
												$iindex = $j;
												break;
											}
										}
										?>
										<div class="ibox-photo">
											<img src="{{ $itinerators[$iindex]['photo'] }}" alt="">
										</div>
										<div class="ibox-desc">
											{{ $itinerators[$iindex]['name'] }}
											<br>
											{{ $itinerators[$iindex]['kanji'] }}
										</div>
									</div>
									@endforeach
								@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row requestwork request-input">
		<div class="col-md-12">
			<form action="../../itineratorspage_controller" method="POST" name="submitrequestform" class="submitrequestform">
				{!! csrf_field() !!}
				<input type="hidden" name="itineratorsid" value="<?php echo $itinerators_id; ?>">
				<input type="hidden" name="itid" value="<?php echo $it_id; ?>">
				<input type="hidden" name="itineratorspage_formtype" value="submitrequestwork">
				<p>
					<label for="submitworkday">
						Estimated work day: 
					</label>
					<input type="text" id="submitworkday" name="submitworkday" value="{{ $currentrelation[0]['work_day'] }}">
				</p>
				<input type="submit" value="Submit" name="submitrequestitinerators" class='submitrequestitinerators'>
			</form>
		</div>
	</div>
</div>
@endsection